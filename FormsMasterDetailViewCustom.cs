﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using Telerik.Sitefinity.Abstractions;
using Telerik.Sitefinity.Forms.Model;
using Telerik.Sitefinity.Modules.Forms.Web.UI;
using Telerik.Sitefinity.Modules.Forms.Web.UI.Fields;
using Telerik.Sitefinity.Web.UI;
using Telerik.Sitefinity.Web.UI.ContentUI.Contracts;
using Telerik.Sitefinity.Web.UI.ContentUI.Views.Backend.Master;
using Telerik.Sitefinity.Web.UI.ContentUI.Views.Backend.Master.Contracts;

namespace SitefinityWebApp
{

    //ORDEN DE LOS CAMPOS DE LOS FORMULARIOS
    public class FormsMasterDetailViewCustom : FormsMasterDetailView
    {
        protected override void InitializeControls(GenericContainer container, IContentViewDefinition definition)
        {
            base.InitializeControls(container, definition);

            var masterDetailDefinition = definition as IContentViewMasterDetailDefinition;

            if(masterDetailDefinition != null)
            {
                var masterDefinition = masterDetailDefinition.MasterDefinition as IMasterViewDefinition;
                if (masterDefinition != null)
                {
                    var gridViewMode = masterDefinition.ViewModes[MasterGridView.GridModeKey] as IGridViewModeDefinition;

                    if (gridViewMode != null)
                    {
                        var fields = this.FormDescription.Controls.Cast<FormControl>().ToList();

                        List<FormControl> sortedFields = new List<FormControl>();
                        sortedFields.Add(fields.Single(f => f.SiblingId == Guid.Empty));
                        fields.Remove(sortedFields[0]);

                        Guid currentSiblingId = sortedFields[0].Id;

                        foreach (FormControl field in fields)
                        {
                            FormControl nextField = fields.Single(f => f.SiblingId == currentSiblingId);
                            sortedFields.Add(nextField);
                            currentSiblingId = nextField.Id;
                        }

                        var behaviorResolver = ObjectFactory.Resolve<IControlBehaviorResolver>();

                        var sortedFieldNames = new List<string>();

                        foreach (var sortedField in sortedFields)
                        {
                            var control = this.GetFormsManager().LoadControl(sortedField);
                            if (control != null)
                            {
                                var behaviorControl = behaviorResolver.GetBehaviorObject(control);
                                var formFieldControl = behaviorControl as IFormFieldControl;
                                if (formFieldControl != null)
                                {
                                    var field = formFieldControl.MetaField;

                                    sortedFieldNames.Add(field.FieldName);
                                }
                            }
                        }

                        var sortedGridColumns = new List<IColumnDefinition>();

                        foreach (var sortedFieldName in sortedFieldNames)
                        {
                            var gridColumnToAdd = gridViewMode.Columns.Single(c => c.Name == sortedFieldName);

                            sortedGridColumns.Add(gridColumnToAdd);
                        }

                        var totalColumns = gridViewMode.Columns.Count;  
                        var fieldColumns = sortedGridColumns.Count;

                        gridViewMode.Columns.RemoveRange((totalColumns - fieldColumns), fieldColumns);
                        gridViewMode.Columns.AddRange(sortedGridColumns);
                    }
                }
            }
        }

        public override IEnumerable<ScriptDescriptor> GetScriptDescriptors()
        {
            var descriptors = base.GetScriptDescriptors();
            var descriptor = descriptors.Last() as ScriptControlDescriptor;
            descriptor.Type = "Telerik.Sitefinity.Modules.Forms.Web.UI.FormsMasterDetailView"; // default control type (the inherited control type full name)
            return descriptors;
        }
    }
}