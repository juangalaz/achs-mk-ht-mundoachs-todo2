using System;
using System.Linq;
using System.Web.UI;
using Telerik.Sitefinity.Web.UI;
using Telerik.Sitefinity.Web.UI.ControlDesign;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Web;
using Telerik.Sitefinity.Localization;
using Telerik.Sitefinity.Modules.Pages;

namespace SitefinityWebApp.WidgetDesigners
{
    /// <summary>
    /// Represents a designer for the <typeparamref name="SitefinityWebApp.Mvc.Controllers.DocumentosWidgetController"/> widget
    /// </summary>
    public class DocumentosWidgetDesigner1 : ControlDesignerBase
    {
        #region Properties
        /// <summary>
        /// Obsolete. Use LayoutTemplatePath instead.
        /// </summary>
        protected override string LayoutTemplateName
        {
            get
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the layout template's relative or virtual path.
        /// </summary>
        public override string LayoutTemplatePath
        {
            get
            {
                if (string.IsNullOrEmpty(base.LayoutTemplatePath))
                    return DocumentosWidgetDesigner1.layoutTemplatePath;
                return base.LayoutTemplatePath;
            }
            set
            {
                base.LayoutTemplatePath = value;
            }
        }

        protected override HtmlTextWriterTag TagKey
        {
            get
            {
                return HtmlTextWriterTag.Div;
            }
        }
        #endregion

        #region Control references
        /// <summary>
        /// Gets the control that is bound to the titulo property
        /// </summary>
        protected virtual Control titulo
        {
            get
            {
                return this.Container.GetControl<Control>("titulo", true);
            }
        }

        /// <summary>
        /// Gets the control that is bound to the fecha property
        /// </summary>
        protected virtual Control fecha
        {
            get
            {
                return this.Container.GetControl<Control>("fecha", true);
            }
        }

        /// <summary>
        /// The LinkButton for selecting documento.
        /// </summary>
        /// <value>The page selector control.</value>
        protected internal virtual LinkButton SelectButtondocumento
        {
            get
            {
                return this.Container.GetControl<LinkButton>("selectButtondocumento", false);
            }
        }

        /// <summary>
        /// The LinkButton for deselecting documento.
        /// </summary>
        /// <value>The page selector control.</value>
        protected internal virtual LinkButton DeselectButtondocumento
        {
            get
            {
                return this.Container.GetControl<LinkButton>("deselectButtondocumento", false);
            }
        }

        /// <summary>
        /// Gets the RadEditor Manager dialog for inserting image, document or video for the documento property.
        /// </summary>
        /// <value>The RadEditor Manager dialog for inserting image, document or video.</value>
        protected EditorContentManagerDialog Selectordocumento
        {
            get
            {
                return this.Container.GetControl<EditorContentManagerDialog>("selectordocumento", false);
            }
        }

        #endregion

        #region Methods
        protected override void InitializeControls(Telerik.Sitefinity.Web.UI.GenericContainer container)
        {
            // Place your initialization logic here
        }
        #endregion

        #region IScriptControl implementation
        /// <summary>
        /// Gets a collection of script descriptors that represent ECMAScript (JavaScript) client components.
        /// </summary>
        public override System.Collections.Generic.IEnumerable<System.Web.UI.ScriptDescriptor> GetScriptDescriptors()
        {
            var scriptDescriptors = new List<ScriptDescriptor>(base.GetScriptDescriptors());
            var descriptor = (ScriptControlDescriptor)scriptDescriptors.Last();

            descriptor.AddElementProperty("titulo", this.titulo.ClientID);
            descriptor.AddElementProperty("fecha", this.fecha.ClientID);
            descriptor.AddElementProperty("selectButtondocumento", this.SelectButtondocumento.ClientID);
            descriptor.AddElementProperty("deselectButtondocumento", this.DeselectButtondocumento.ClientID);
            descriptor.AddComponentProperty("selectordocumento", this.Selectordocumento.ClientID);
            descriptor.AddProperty("documentServiceUrl", this.documentServiceUrl);

            return scriptDescriptors;
        }

        /// <summary>
        /// Gets a collection of ScriptReference objects that define script resources that the control requires.
        /// </summary>
        public override System.Collections.Generic.IEnumerable<System.Web.UI.ScriptReference> GetScriptReferences()
        {
            var scripts = new List<ScriptReference>(base.GetScriptReferences());
            scripts.Add(new ScriptReference(DocumentosWidgetDesigner1.scriptReference));
            return scripts;
        }

        /// <summary>
        /// Gets the required by the control, core library scripts predefined in the <see cref="ScriptRef"/> enum.
        /// </summary>
        protected override ScriptRef GetRequiredCoreScripts()
        {
            return ScriptRef.JQuery | ScriptRef.JQueryUI;
        }
        #endregion

        #region Private members & constants
        public static readonly string layoutTemplatePath = "~/WidgetDesigners/DocumentosWidgetDesigner1.ascx";
        public const string scriptReference = "~/WidgetDesigners/DocumentosWidgetDesigner1.js";
        private string documentServiceUrl = VirtualPathUtility.ToAbsolute("~/Sitefinity/Services/Content/DocumentService.svc/");
        #endregion
    }
}
 
