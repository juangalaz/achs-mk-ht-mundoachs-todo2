using System;
using System.Linq;
using System.Web.UI;
using Telerik.Sitefinity.Web.UI;
using Telerik.Sitefinity.Web.UI.ControlDesign;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Web;
using Telerik.Sitefinity.Localization;
using Telerik.Sitefinity.Modules.Pages;

namespace SitefinityWebApp.WidgetDesigners
{
    /// <summary>
    /// Represents a designer for the <typeparamref name="SitefinityWebApp.Mvc.Controllers.AcordeonArchivoWidget1Controller"/> widget
    /// </summary>
    public class AcordeonConArchivoWidgetDesigner5 : ControlDesignerBase
    {
        #region Properties
        /// <summary>
        /// Obsolete. Use LayoutTemplatePath instead.
        /// </summary>
        protected override string LayoutTemplateName
        {
            get
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the layout template's relative or virtual path.
        /// </summary>
        public override string LayoutTemplatePath
        {
            get
            {
                if (string.IsNullOrEmpty(base.LayoutTemplatePath))
                    return AcordeonConArchivoWidgetDesigner5.layoutTemplatePath;
                return base.LayoutTemplatePath;
            }
            set
            {
                base.LayoutTemplatePath = value;
            }
        }

        protected override HtmlTextWriterTag TagKey
        {
            get
            {
                return HtmlTextWriterTag.Div;
            }
        }
        #endregion

        #region Control references
        /// <summary>
        /// Gets the control that is bound to the titulo property
        /// </summary>
        protected virtual Control titulo
        {
            get
            {
                return this.Container.GetControl<Control>("titulo", true);
            }
        }

        /// <summary>
        /// Gets the control that is bound to the texto property
        /// </summary>
        protected virtual Control texto
        {
            get
            {
                return this.Container.GetControl<Control>("texto", true);
            }
        }

        /// <summary>
        /// The LinkButton for selecting documento.
        /// </summary>
        /// <value>The page selector control.</value>
        protected internal virtual LinkButton SelectButtondocumento
        {
            get
            {
                return this.Container.GetControl<LinkButton>("selectButtondocumento", false);
            }
        }

        /// <summary>
        /// The LinkButton for deselecting documento.
        /// </summary>
        /// <value>The page selector control.</value>
        protected internal virtual LinkButton DeselectButtondocumento
        {
            get
            {
                return this.Container.GetControl<LinkButton>("deselectButtondocumento", false);
            }
        }

        /// <summary>
        /// Gets the RadEditor Manager dialog for inserting image, document or video for the documento property.
        /// </summary>
        /// <value>The RadEditor Manager dialog for inserting image, document or video.</value>
        protected EditorContentManagerDialog Selectordocumento
        {
            get
            {
                return this.Container.GetControl<EditorContentManagerDialog>("selectordocumento", false);
            }
        }

        /// <summary>
        /// Gets the control that is bound to the texto2 property
        /// </summary>
        protected virtual Control texto2
        {
            get
            {
                return this.Container.GetControl<Control>("texto2", true);
            }
        }

        /// <summary>
        /// The LinkButton for selecting documento2.
        /// </summary>
        /// <value>The page selector control.</value>
        protected internal virtual LinkButton SelectButtondocumento2
        {
            get
            {
                return this.Container.GetControl<LinkButton>("selectButtondocumento2", false);
            }
        }

        /// <summary>
        /// The LinkButton for deselecting documento2.
        /// </summary>
        /// <value>The page selector control.</value>
        protected internal virtual LinkButton DeselectButtondocumento2
        {
            get
            {
                return this.Container.GetControl<LinkButton>("deselectButtondocumento2", false);
            }
        }

        /// <summary>
        /// Gets the RadEditor Manager dialog for inserting image, document or video for the documento2 property.
        /// </summary>
        /// <value>The RadEditor Manager dialog for inserting image, document or video.</value>
        protected EditorContentManagerDialog Selectordocumento2
        {
            get
            {
                return this.Container.GetControl<EditorContentManagerDialog>("selectordocumento2", false);
            }
        }

        /// <summary>
        /// Gets the control that is bound to the texto3 property
        /// </summary>
        protected virtual Control texto3
        {
            get
            {
                return this.Container.GetControl<Control>("texto3", true);
            }
        }

        /// <summary>
        /// The LinkButton for selecting documento3.
        /// </summary>
        /// <value>The page selector control.</value>
        protected internal virtual LinkButton SelectButtondocumento3
        {
            get
            {
                return this.Container.GetControl<LinkButton>("selectButtondocumento3", false);
            }
        }

        /// <summary>
        /// The LinkButton for deselecting documento3.
        /// </summary>
        /// <value>The page selector control.</value>
        protected internal virtual LinkButton DeselectButtondocumento3
        {
            get
            {
                return this.Container.GetControl<LinkButton>("deselectButtondocumento3", false);
            }
        }

        /// <summary>
        /// Gets the RadEditor Manager dialog for inserting image, document or video for the documento3 property.
        /// </summary>
        /// <value>The RadEditor Manager dialog for inserting image, document or video.</value>
        protected EditorContentManagerDialog Selectordocumento3
        {
            get
            {
                return this.Container.GetControl<EditorContentManagerDialog>("selectordocumento3", false);
            }
        }

        /// <summary>
        /// Gets the control that is bound to the texto4 property
        /// </summary>
        protected virtual Control texto4
        {
            get
            {
                return this.Container.GetControl<Control>("texto4", true);
            }
        }

        /// <summary>
        /// The LinkButton for selecting documento4.
        /// </summary>
        /// <value>The page selector control.</value>
        protected internal virtual LinkButton SelectButtondocumento4
        {
            get
            {
                return this.Container.GetControl<LinkButton>("selectButtondocumento4", false);
            }
        }

        /// <summary>
        /// The LinkButton for deselecting documento4.
        /// </summary>
        /// <value>The page selector control.</value>
        protected internal virtual LinkButton DeselectButtondocumento4
        {
            get
            {
                return this.Container.GetControl<LinkButton>("deselectButtondocumento4", false);
            }
        }

        /// <summary>
        /// Gets the RadEditor Manager dialog for inserting image, document or video for the documento4 property.
        /// </summary>
        /// <value>The RadEditor Manager dialog for inserting image, document or video.</value>
        protected EditorContentManagerDialog Selectordocumento4
        {
            get
            {
                return this.Container.GetControl<EditorContentManagerDialog>("selectordocumento4", false);
            }
        }

        /// <summary>
        /// Gets the control that is bound to the texto5 property
        /// </summary>
        protected virtual Control texto5
        {
            get
            {
                return this.Container.GetControl<Control>("texto5", true);
            }
        }

        /// <summary>
        /// The LinkButton for selecting documento5.
        /// </summary>
        /// <value>The page selector control.</value>
        protected internal virtual LinkButton SelectButtondocumento5
        {
            get
            {
                return this.Container.GetControl<LinkButton>("selectButtondocumento5", false);
            }
        }

        /// <summary>
        /// The LinkButton for deselecting documento5.
        /// </summary>
        /// <value>The page selector control.</value>
        protected internal virtual LinkButton DeselectButtondocumento5
        {
            get
            {
                return this.Container.GetControl<LinkButton>("deselectButtondocumento5", false);
            }
        }

        /// <summary>
        /// Gets the RadEditor Manager dialog for inserting image, document or video for the documento5 property.
        /// </summary>
        /// <value>The RadEditor Manager dialog for inserting image, document or video.</value>
        protected EditorContentManagerDialog Selectordocumento5
        {
            get
            {
                return this.Container.GetControl<EditorContentManagerDialog>("selectordocumento5", false);
            }
        }

        #endregion

        #region Methods
        protected override void InitializeControls(Telerik.Sitefinity.Web.UI.GenericContainer container)
        {
            // Place your initialization logic here
        }
        #endregion

        #region IScriptControl implementation
        /// <summary>
        /// Gets a collection of script descriptors that represent ECMAScript (JavaScript) client components.
        /// </summary>
        public override System.Collections.Generic.IEnumerable<System.Web.UI.ScriptDescriptor> GetScriptDescriptors()
        {
            var scriptDescriptors = new List<ScriptDescriptor>(base.GetScriptDescriptors());
            var descriptor = (ScriptControlDescriptor)scriptDescriptors.Last();

            descriptor.AddElementProperty("titulo", this.titulo.ClientID);
            descriptor.AddElementProperty("texto", this.texto.ClientID);
            descriptor.AddElementProperty("selectButtondocumento", this.SelectButtondocumento.ClientID);
            descriptor.AddElementProperty("deselectButtondocumento", this.DeselectButtondocumento.ClientID);
            descriptor.AddComponentProperty("selectordocumento", this.Selectordocumento.ClientID);
            descriptor.AddElementProperty("texto2", this.texto2.ClientID);
            descriptor.AddElementProperty("selectButtondocumento2", this.SelectButtondocumento2.ClientID);
            descriptor.AddElementProperty("deselectButtondocumento2", this.DeselectButtondocumento2.ClientID);
            descriptor.AddComponentProperty("selectordocumento2", this.Selectordocumento2.ClientID);
            descriptor.AddElementProperty("texto3", this.texto3.ClientID);
            descriptor.AddElementProperty("selectButtondocumento3", this.SelectButtondocumento3.ClientID);
            descriptor.AddElementProperty("deselectButtondocumento3", this.DeselectButtondocumento3.ClientID);
            descriptor.AddComponentProperty("selectordocumento3", this.Selectordocumento3.ClientID);
            descriptor.AddElementProperty("texto4", this.texto4.ClientID);
            descriptor.AddElementProperty("selectButtondocumento4", this.SelectButtondocumento4.ClientID);
            descriptor.AddElementProperty("deselectButtondocumento4", this.DeselectButtondocumento4.ClientID);
            descriptor.AddComponentProperty("selectordocumento4", this.Selectordocumento4.ClientID);
            descriptor.AddElementProperty("texto5", this.texto5.ClientID);
            descriptor.AddElementProperty("selectButtondocumento5", this.SelectButtondocumento5.ClientID);
            descriptor.AddElementProperty("deselectButtondocumento5", this.DeselectButtondocumento5.ClientID);
            descriptor.AddComponentProperty("selectordocumento5", this.Selectordocumento5.ClientID);
            descriptor.AddProperty("documentServiceUrl", this.documentServiceUrl);

            return scriptDescriptors;
        }

        /// <summary>
        /// Gets a collection of ScriptReference objects that define script resources that the control requires.
        /// </summary>
        public override System.Collections.Generic.IEnumerable<System.Web.UI.ScriptReference> GetScriptReferences()
        {
            var scripts = new List<ScriptReference>(base.GetScriptReferences());
            scripts.Add(new ScriptReference(AcordeonConArchivoWidgetDesigner5.scriptReference));
            return scripts;
        }

        /// <summary>
        /// Gets the required by the control, core library scripts predefined in the <see cref="ScriptRef"/> enum.
        /// </summary>
        protected override ScriptRef GetRequiredCoreScripts()
        {
            return ScriptRef.JQuery | ScriptRef.JQueryUI;
        }
        #endregion

        #region Private members & constants
        public static readonly string layoutTemplatePath = "~/WidgetDesigners/AcordeonConArchivoWidgetDesigner5.ascx";
        public const string scriptReference = "~/WidgetDesigners/AcordeonConArchivoWidgetDesigner5.js";
        private string documentServiceUrl = VirtualPathUtility.ToAbsolute("~/Sitefinity/Services/Content/DocumentService.svc/");
        #endregion
    }
}
 
