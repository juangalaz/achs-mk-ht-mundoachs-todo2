<%@ Control %>
<%@ Register Assembly="Telerik.Sitefinity" TagPrefix="sf" Namespace="Telerik.Sitefinity.Web.UI" %>
<%@ Register Assembly="Telerik.Sitefinity" TagPrefix="sitefinity" Namespace="Telerik.Sitefinity.Web.UI" %>
<%@ Register Assembly="Telerik.Sitefinity" TagPrefix="sfFields" Namespace="Telerik.Sitefinity.Web.UI.Fields" %>

<sitefinity:ResourceLinks ID="resourcesLinks" runat="server">
    <sitefinity:ResourceFile Name="Styles/Ajax.css" />
</sitefinity:ResourceLinks>
<div id="designerLayoutRoot" class="sfContentViews sfSingleContentView" style="max-height: 400px; overflow: auto; ">
<ol>        
    <li class="sfFormCtrl">
    <asp:Label runat="server" AssociatedControlID="titulo" CssClass="sfTxtLbl">Titulo</asp:Label>
    <asp:TextBox ID="titulo" runat="server" CssClass="sfTxt" />
    <div class="sfExample"></div>
    </li>
    
    <li class="sfFormCtrl">
    <asp:Label runat="server" AssociatedControlID="texto"  CssClass="sfTxtLbl">Texto</asp:Label>
    <asp:TextBox ID="texto" runat="server" CssClass="sfTxt" TextMode="MultiLine" />
    <div class="sfExample"></div>
    </li>
    
    <li class="sfFormCtrl">
    <asp:Label runat="server" AssociatedControlID="link" CssClass="sfTxtLbl">Link</asp:Label>
    <asp:TextBox ID="link" runat="server" CssClass="sfTxt" />
    <div class="sfExample"></div>
    </li>
    
    <li class="sfFormCtrl">
    <asp:Label runat="server" AssociatedControlID="texto_link" CssClass="sfTxtLbl">Texto Link</asp:Label>
    <asp:TextBox ID="texto_link" runat="server" CssClass="sfTxt" />
    <div class="sfExample"></div>
    </li>
    
</ol>
</div>
