using System;
using System.Linq;
using System.Web.UI;
using Telerik.Sitefinity.Web.UI;
using Telerik.Sitefinity.Web.UI.ControlDesign;
using System.Collections.Generic;

namespace SitefinityWebApp.WidgetDesigners
{
    /// <summary>
    /// Represents a designer for the <typeparamref name="SitefinityWebApp.Mvc.Controllers.BannerTituloVerdeWidget1Controller"/> widget
    /// </summary>
    public class BannerTituloVerdeWidgetDesigner1 : ControlDesignerBase
    {
        #region Properties
        /// <summary>
        /// Obsolete. Use LayoutTemplatePath instead.
        /// </summary>
        protected override string LayoutTemplateName
        {
            get
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the layout template's relative or virtual path.
        /// </summary>
        public override string LayoutTemplatePath
        {
            get
            {
                if (string.IsNullOrEmpty(base.LayoutTemplatePath))
                    return BannerTituloVerdeWidgetDesigner1.layoutTemplatePath;
                return base.LayoutTemplatePath;
            }
            set
            {
                base.LayoutTemplatePath = value;
            }
        }

        protected override HtmlTextWriterTag TagKey
        {
            get
            {
                return HtmlTextWriterTag.Div;
            }
        }
        #endregion

        #region Control references
        /// <summary>
        /// Gets the control that is bound to the titulo property
        /// </summary>
        protected virtual Control titulo
        {
            get
            {
                return this.Container.GetControl<Control>("titulo", true);
            }
        }

        /// <summary>
        /// Gets the control that is bound to the texto property
        /// </summary>
        protected virtual Control texto
        {
            get
            {
                return this.Container.GetControl<Control>("texto", true);
            }
        }

        /// <summary>
        /// Gets the control that is bound to the link property
        /// </summary>
        protected virtual Control link
        {
            get
            {
                return this.Container.GetControl<Control>("link", true);
            }
        }

        /// <summary>
        /// Gets the control that is bound to the texto_link property
        /// </summary>
        protected virtual Control texto_link
        {
            get
            {
                return this.Container.GetControl<Control>("texto_link", true);
            }
        }

        #endregion

        #region Methods
        protected override void InitializeControls(Telerik.Sitefinity.Web.UI.GenericContainer container)
        {
            // Place your initialization logic here
        }
        #endregion

        #region IScriptControl implementation
        /// <summary>
        /// Gets a collection of script descriptors that represent ECMAScript (JavaScript) client components.
        /// </summary>
        public override System.Collections.Generic.IEnumerable<System.Web.UI.ScriptDescriptor> GetScriptDescriptors()
        {
            var scriptDescriptors = new List<ScriptDescriptor>(base.GetScriptDescriptors());
            var descriptor = (ScriptControlDescriptor)scriptDescriptors.Last();

            descriptor.AddElementProperty("titulo", this.titulo.ClientID);
            descriptor.AddElementProperty("texto", this.texto.ClientID);
            descriptor.AddElementProperty("link", this.link.ClientID);
            descriptor.AddElementProperty("texto_link", this.texto_link.ClientID);

            return scriptDescriptors;
        }

        /// <summary>
        /// Gets a collection of ScriptReference objects that define script resources that the control requires.
        /// </summary>
        public override System.Collections.Generic.IEnumerable<System.Web.UI.ScriptReference> GetScriptReferences()
        {
            var scripts = new List<ScriptReference>(base.GetScriptReferences());
            scripts.Add(new ScriptReference(BannerTituloVerdeWidgetDesigner1.scriptReference));
            return scripts;
        }
        #endregion

        #region Private members & constants
        public static readonly string layoutTemplatePath = "~/WidgetDesigners/BannerTituloVerdeWidgetDesigner1.ascx";
        public const string scriptReference = "~/WidgetDesigners/BannerTituloVerdeWidgetDesigner1.js";
        #endregion
    }
}
 
