Type.registerNamespace("SitefinityWebApp.WidgetDesigners");

SitefinityWebApp.WidgetDesigners.AcordeonWidgetDesigner1 = function (element) {
    /* Initialize titulo fields */
    this._titulo = null;
    
    /* Initialize contenido fields */
    this._contenido = null;
    
    /* Calls the base constructor */
    SitefinityWebApp.WidgetDesigners.AcordeonWidgetDesigner1.initializeBase(this, [element]);
}

SitefinityWebApp.WidgetDesigners.AcordeonWidgetDesigner1.prototype = {
    /* --------------------------------- set up and tear down --------------------------------- */
    initialize: function () {
        /* Here you can attach to events or do other initialization */
        SitefinityWebApp.WidgetDesigners.AcordeonWidgetDesigner1.callBaseMethod(this, 'initialize');
    },
    dispose: function () {
        /* this is the place to unbind/dispose the event handlers created in the initialize method */
        SitefinityWebApp.WidgetDesigners.AcordeonWidgetDesigner1.callBaseMethod(this, 'dispose');
    },

    /* --------------------------------- public methods ---------------------------------- */

    findElement: function (id) {
        var result = jQuery(this.get_element()).find("#" + id).get(0);
        return result;
    },

    /* Called when the designer window gets opened and here is place to "bind" your designer to the control properties */
    refreshUI: function () {
        var controlData = this._propertyEditor.get_control().Settings; /* JavaScript clone of your control - all the control properties will be properties of the controlData too */

        /* RefreshUI titulo */
        jQuery(this.get_titulo()).val(controlData.titulo);

        /* RefreshUI contenido */
        this.get_contenido().control.set_value(controlData.contenido);
    },

    /* Called when the "Save" button is clicked. Here you can transfer the settings from the designer to the control */
    applyChanges: function () {
        var controlData = this._propertyEditor.get_control().Settings;

        /* ApplyChanges titulo */
        controlData.titulo = jQuery(this.get_titulo()).val();

        /* ApplyChanges contenido */
        controlData.contenido = this.get_contenido().control.get_value();
    },

    /* --------------------------------- event handlers ---------------------------------- */

    /* --------------------------------- private methods --------------------------------- */

    /* --------------------------------- properties -------------------------------------- */

    /* titulo properties */
    get_titulo: function () { return this._titulo; }, 
    set_titulo: function (value) { this._titulo = value; },

    /* contenido properties */
    get_contenido: function () { return this._contenido; }, 
    set_contenido: function (value) { this._contenido = value; }
}

SitefinityWebApp.WidgetDesigners.AcordeonWidgetDesigner1.registerClass('SitefinityWebApp.WidgetDesigners.AcordeonWidgetDesigner1', Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesignerBase);
