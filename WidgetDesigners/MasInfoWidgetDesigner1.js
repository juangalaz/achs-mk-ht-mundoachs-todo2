Type.registerNamespace("SitefinityWebApp.WidgetDesigners");

SitefinityWebApp.WidgetDesigners.MasInfoWidgetDesigner1 = function (element) {
    /* Initialize titulo fields */
    this._titulo = null;
    
    /* Initialize texto fields */
    this._texto = null;
    
    /* Initialize link fields */
    this._link = null;
    
    /* Initialize texto_link fields */
    this._texto_link = null;
    
    /* Calls the base constructor */
    SitefinityWebApp.WidgetDesigners.MasInfoWidgetDesigner1.initializeBase(this, [element]);
}

SitefinityWebApp.WidgetDesigners.MasInfoWidgetDesigner1.prototype = {
    /* --------------------------------- set up and tear down --------------------------------- */
    initialize: function () {
        /* Here you can attach to events or do other initialization */
        SitefinityWebApp.WidgetDesigners.MasInfoWidgetDesigner1.callBaseMethod(this, 'initialize');
    },
    dispose: function () {
        /* this is the place to unbind/dispose the event handlers created in the initialize method */
        SitefinityWebApp.WidgetDesigners.MasInfoWidgetDesigner1.callBaseMethod(this, 'dispose');
    },

    /* --------------------------------- public methods ---------------------------------- */

    findElement: function (id) {
        var result = jQuery(this.get_element()).find("#" + id).get(0);
        return result;
    },

    /* Called when the designer window gets opened and here is place to "bind" your designer to the control properties */
    refreshUI: function () {
        var controlData = this._propertyEditor.get_control().Settings; /* JavaScript clone of your control - all the control properties will be properties of the controlData too */

        /* RefreshUI titulo */
        jQuery(this.get_titulo()).val(controlData.titulo);

        /* RefreshUI texto */
        jQuery(this.get_texto()).val(controlData.texto);

        /* RefreshUI link */
        jQuery(this.get_link()).val(controlData.link);

        /* RefreshUI texto_link */
        jQuery(this.get_texto_link()).val(controlData.texto_link);
    },

    /* Called when the "Save" button is clicked. Here you can transfer the settings from the designer to the control */
    applyChanges: function () {
        var controlData = this._propertyEditor.get_control().Settings;

        /* ApplyChanges titulo */
        controlData.titulo = jQuery(this.get_titulo()).val();

        /* ApplyChanges texto */
        controlData.texto = jQuery(this.get_texto()).val();

        /* ApplyChanges link */
        controlData.link = jQuery(this.get_link()).val();

        /* ApplyChanges texto_link */
        controlData.texto_link = jQuery(this.get_texto_link()).val();
    },

    /* --------------------------------- event handlers ---------------------------------- */

    /* --------------------------------- private methods --------------------------------- */

    /* --------------------------------- properties -------------------------------------- */

    /* titulo properties */
    get_titulo: function () { return this._titulo; }, 
    set_titulo: function (value) { this._titulo = value; },

    /* texto properties */
    get_texto: function () { return this._texto; }, 
    set_texto: function (value) { this._texto = value; },

    /* link properties */
    get_link: function () { return this._link; }, 
    set_link: function (value) { this._link = value; },

    /* texto_link properties */
    get_texto_link: function () { return this._texto_link; }, 
    set_texto_link: function (value) { this._texto_link = value; }
}

SitefinityWebApp.WidgetDesigners.MasInfoWidgetDesigner1.registerClass('SitefinityWebApp.WidgetDesigners.MasInfoWidgetDesigner1', Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesignerBase);
