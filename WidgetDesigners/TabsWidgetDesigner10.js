Type.registerNamespace("SitefinityWebApp.WidgetDesigners");

SitefinityWebApp.WidgetDesigners.TabsWidgetDesigner10 = function (element) {
    /* Initialize tab fields */
    this._tab = null;
    
    /* Initialize contenido fields */
    this._contenido = null;
    
    /* Calls the base constructor */
    SitefinityWebApp.WidgetDesigners.TabsWidgetDesigner10.initializeBase(this, [element]);
}

SitefinityWebApp.WidgetDesigners.TabsWidgetDesigner10.prototype = {
    /* --------------------------------- set up and tear down --------------------------------- */
    initialize: function () {
        /* Here you can attach to events or do other initialization */
        SitefinityWebApp.WidgetDesigners.TabsWidgetDesigner10.callBaseMethod(this, 'initialize');
    },
    dispose: function () {
        /* this is the place to unbind/dispose the event handlers created in the initialize method */
        SitefinityWebApp.WidgetDesigners.TabsWidgetDesigner10.callBaseMethod(this, 'dispose');
    },

    /* --------------------------------- public methods ---------------------------------- */

    findElement: function (id) {
        var result = jQuery(this.get_element()).find("#" + id).get(0);
        return result;
    },

    /* Called when the designer window gets opened and here is place to "bind" your designer to the control properties */
    refreshUI: function () {
        var controlData = this._propertyEditor.get_control().Settings; /* JavaScript clone of your control - all the control properties will be properties of the controlData too */

        /* RefreshUI tab */
        jQuery(this.get_tab()).val(controlData.tab);



        /* RefreshUI contenido */
        this.get_contenido().control.set_value(controlData.contenido);
    },

    /* Called when the "Save" button is clicked. Here you can transfer the settings from the designer to the control */
    applyChanges: function () {
        var controlData = this._propertyEditor.get_control().Settings;

        /* ApplyChanges tab */
        controlData.tab = jQuery(this.get_tab()).val();

      
        
        /* ApplyChanges contenido */
        controlData.contenido = this.get_contenido().control.get_value();
    },

    /* --------------------------------- event handlers ---------------------------------- */

    /* --------------------------------- private methods --------------------------------- */

    /* --------------------------------- properties -------------------------------------- */

    /* tab properties */
    get_tab: function () { return this._tab; }, 
    set_tab: function (value) { this._tab = value; },

    /* contenido properties */
    get_contenido: function () { return this._contenido; }, 
    set_contenido: function (value) { this._contenido = value; }
}

SitefinityWebApp.WidgetDesigners.TabsWidgetDesigner10.registerClass('SitefinityWebApp.WidgetDesigners.TabsWidgetDesigner10', Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesignerBase);
