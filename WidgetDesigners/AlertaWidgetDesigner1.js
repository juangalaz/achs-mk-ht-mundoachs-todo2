Type.registerNamespace("SitefinityWebApp.WidgetDesigners");

SitefinityWebApp.WidgetDesigners.AlertaWidgetDesigner1 = function (element) {
    /* Initialize texto fields */
    this._texto = null;
    
    /* Calls the base constructor */
    SitefinityWebApp.WidgetDesigners.AlertaWidgetDesigner1.initializeBase(this, [element]);
}

SitefinityWebApp.WidgetDesigners.AlertaWidgetDesigner1.prototype = {
    /* --------------------------------- set up and tear down --------------------------------- */
    initialize: function () {
        /* Here you can attach to events or do other initialization */
        SitefinityWebApp.WidgetDesigners.AlertaWidgetDesigner1.callBaseMethod(this, 'initialize');
    },
    dispose: function () {
        /* this is the place to unbind/dispose the event handlers created in the initialize method */
        SitefinityWebApp.WidgetDesigners.AlertaWidgetDesigner1.callBaseMethod(this, 'dispose');
    },

    /* --------------------------------- public methods ---------------------------------- */

    findElement: function (id) {
        var result = jQuery(this.get_element()).find("#" + id).get(0);
        return result;
    },

    /* Called when the designer window gets opened and here is place to "bind" your designer to the control properties */
    refreshUI: function () {
        var controlData = this._propertyEditor.get_control().Settings; /* JavaScript clone of your control - all the control properties will be properties of the controlData too */



        /* RefreshUI texto */
        this.get_texto().control.set_value(controlData.texto);
    },

    /* Called when the "Save" button is clicked. Here you can transfer the settings from the designer to the control */
    applyChanges: function () {
        var controlData = this._propertyEditor.get_control().Settings;



        /* ApplyChanges texto */
        controlData.texto = this.get_texto().control.get_value();
    },

    /* --------------------------------- event handlers ---------------------------------- */

    /* --------------------------------- private methods --------------------------------- */

    /* --------------------------------- properties -------------------------------------- */

    /* texto properties */
    get_texto: function () { return this._texto; }, 
    set_texto: function (value) { this._texto = value; }
}

SitefinityWebApp.WidgetDesigners.AlertaWidgetDesigner1.registerClass('SitefinityWebApp.WidgetDesigners.AlertaWidgetDesigner1', Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesignerBase);
