using System;
using System.Linq;
using System.Web.UI;
using Telerik.Sitefinity.Web.UI;
using Telerik.Sitefinity.Web.UI.ControlDesign;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Web;
using Telerik.Sitefinity.Localization;
using Telerik.Sitefinity.Modules.Pages;

namespace SitefinityWebApp.WidgetDesigners
{
    /// <summary>
    /// Represents a designer for the <typeparamref name="SitefinityWebApp.Mvc.Controllers.HT_CajasProgramasWidgetController"/> widget
    /// </summary>
    public class HT_CajasProgramasWidgetDesigner1 : ControlDesignerBase
    {
        #region Properties
        /// <summary>
        /// Obsolete. Use LayoutTemplatePath instead.
        /// </summary>
        protected override string LayoutTemplateName
        {
            get
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the layout template's relative or virtual path.
        /// </summary>
        public override string LayoutTemplatePath
        {
            get
            {
                if (string.IsNullOrEmpty(base.LayoutTemplatePath))
                    return HT_CajasProgramasWidgetDesigner1.layoutTemplatePath;
                return base.LayoutTemplatePath;
            }
            set
            {
                base.LayoutTemplatePath = value;
            }
        }

        protected override HtmlTextWriterTag TagKey
        {
            get
            {
                return HtmlTextWriterTag.Div;
            }
        }
        #endregion

        #region Control references
        /// <summary>
        /// Gets the control that is bound to the texto property
        /// </summary>
        protected virtual Control texto
        {
            get
            {
                return this.Container.GetControl<Control>("texto", true);
            }
        }

        /// <summary>
        /// Gets the control that is bound to the link property
        /// </summary>
        protected virtual Control link
        {
            get
            {
                return this.Container.GetControl<Control>("link", true);
            }
        }

        /// <summary>
        /// The LinkButton for selecting logo.
        /// </summary>
        /// <value>The page selector control.</value>
        protected internal virtual LinkButton SelectButtonlogo
        {
            get
            {
                return this.Container.GetControl<LinkButton>("selectButtonlogo", false);
            }
        }

        /// <summary>
        /// The LinkButton for deselecting logo.
        /// </summary>
        /// <value>The page selector control.</value>
        protected internal virtual LinkButton DeselectButtonlogo
        {
            get
            {
                return this.Container.GetControl<LinkButton>("deselectButtonlogo", false);
            }
        }

        /// <summary>
        /// Gets the RadEditor Manager dialog for inserting image, document or video for the logo property.
        /// </summary>
        /// <value>The RadEditor Manager dialog for inserting image, document or video.</value>
        protected EditorContentManagerDialog Selectorlogo
        {
            get
            {
                return this.Container.GetControl<EditorContentManagerDialog>("selectorlogo", false);
            }
        }

        /// <summary>
        /// The LinkButton for selecting fondo.
        /// </summary>
        /// <value>The page selector control.</value>
        protected internal virtual LinkButton SelectButtonfondo
        {
            get
            {
                return this.Container.GetControl<LinkButton>("selectButtonfondo", false);
            }
        }

        /// <summary>
        /// The LinkButton for deselecting fondo.
        /// </summary>
        /// <value>The page selector control.</value>
        protected internal virtual LinkButton DeselectButtonfondo
        {
            get
            {
                return this.Container.GetControl<LinkButton>("deselectButtonfondo", false);
            }
        }

        /// <summary>
        /// Gets the RadEditor Manager dialog for inserting image, document or video for the fondo property.
        /// </summary>
        /// <value>The RadEditor Manager dialog for inserting image, document or video.</value>
        protected EditorContentManagerDialog Selectorfondo
        {
            get
            {
                return this.Container.GetControl<EditorContentManagerDialog>("selectorfondo", false);
            }
        }

        #endregion

        #region Methods
        protected override void InitializeControls(Telerik.Sitefinity.Web.UI.GenericContainer container)
        {
            // Place your initialization logic here
        }
        #endregion

        #region IScriptControl implementation
        /// <summary>
        /// Gets a collection of script descriptors that represent ECMAScript (JavaScript) client components.
        /// </summary>
        public override System.Collections.Generic.IEnumerable<System.Web.UI.ScriptDescriptor> GetScriptDescriptors()
        {
            var scriptDescriptors = new List<ScriptDescriptor>(base.GetScriptDescriptors());
            var descriptor = (ScriptControlDescriptor)scriptDescriptors.Last();

            descriptor.AddElementProperty("texto", this.texto.ClientID);
            descriptor.AddElementProperty("link", this.link.ClientID);
            descriptor.AddElementProperty("selectButtonlogo", this.SelectButtonlogo.ClientID);
            descriptor.AddElementProperty("deselectButtonlogo", this.DeselectButtonlogo.ClientID);
            descriptor.AddComponentProperty("selectorlogo", this.Selectorlogo.ClientID);
            descriptor.AddElementProperty("selectButtonfondo", this.SelectButtonfondo.ClientID);
            descriptor.AddElementProperty("deselectButtonfondo", this.DeselectButtonfondo.ClientID);
            descriptor.AddComponentProperty("selectorfondo", this.Selectorfondo.ClientID);
            descriptor.AddProperty("imageServiceUrl", this.imageServiceUrl);

            return scriptDescriptors;
        }

        /// <summary>
        /// Gets a collection of ScriptReference objects that define script resources that the control requires.
        /// </summary>
        public override System.Collections.Generic.IEnumerable<System.Web.UI.ScriptReference> GetScriptReferences()
        {
            var scripts = new List<ScriptReference>(base.GetScriptReferences());
            scripts.Add(new ScriptReference(HT_CajasProgramasWidgetDesigner1.scriptReference));
            return scripts;
        }

        /// <summary>
        /// Gets the required by the control, core library scripts predefined in the <see cref="ScriptRef"/> enum.
        /// </summary>
        protected override ScriptRef GetRequiredCoreScripts()
        {
            return ScriptRef.JQuery | ScriptRef.JQueryUI;
        }
        #endregion

        #region Private members & constants
        public static readonly string layoutTemplatePath = "~/WidgetDesigners/HT_CajasProgramasWidgetDesigner1.ascx";
        public const string scriptReference = "~/WidgetDesigners/HT_CajasProgramasWidgetDesigner1.js";
        private string imageServiceUrl = VirtualPathUtility.ToAbsolute("~/Sitefinity/Services/Content/ImageService.svc/");
        #endregion
    }
}
 
