Type.registerNamespace("SitefinityWebApp.WidgetDesigners");

SitefinityWebApp.WidgetDesigners.AcordeonConArchivoWidgetDesigner5 = function (element) {
    /* Initialize titulo fields */
    this._titulo = null;
    
    /* Initialize texto fields */
    this._texto = null;
    
    /* Initialize documento fields */
    this._selectButtondocumento = null;
    this._selectButtondocumentoClickDelegate = null;
    this._deselectButtondocumento = null;
    this._deselectButtondocumentoClickDelegate = null;
    this._selectordocumentoCloseDelegate = null;
    this._selectordocumentoUploaderViewFileChangedDelegate = null;
    
    this._documentoDialog = null;
    this._selectordocumento = null;
    this._documentoId = null;
    
    /* Initialize texto2 fields */
    this._texto2 = null;
    
    /* Initialize documento2 fields */
    this._selectButtondocumento2 = null;
    this._selectButtondocumento2ClickDelegate = null;
    this._deselectButtondocumento2 = null;
    this._deselectButtondocumento2ClickDelegate = null;
    this._selectordocumento2CloseDelegate = null;
    this._selectordocumento2UploaderViewFileChangedDelegate = null;
    
    this._documento2Dialog = null;
    this._selectordocumento2 = null;
    this._documento2Id = null;
    
    /* Initialize texto3 fields */
    this._texto3 = null;
    
    /* Initialize documento3 fields */
    this._selectButtondocumento3 = null;
    this._selectButtondocumento3ClickDelegate = null;
    this._deselectButtondocumento3 = null;
    this._deselectButtondocumento3ClickDelegate = null;
    this._selectordocumento3CloseDelegate = null;
    this._selectordocumento3UploaderViewFileChangedDelegate = null;
    
    this._documento3Dialog = null;
    this._selectordocumento3 = null;
    this._documento3Id = null;
    
    /* Initialize texto4 fields */
    this._texto4 = null;
    
    /* Initialize documento4 fields */
    this._selectButtondocumento4 = null;
    this._selectButtondocumento4ClickDelegate = null;
    this._deselectButtondocumento4 = null;
    this._deselectButtondocumento4ClickDelegate = null;
    this._selectordocumento4CloseDelegate = null;
    this._selectordocumento4UploaderViewFileChangedDelegate = null;
    
    this._documento4Dialog = null;
    this._selectordocumento4 = null;
    this._documento4Id = null;
    
    /* Initialize texto5 fields */
    this._texto5 = null;
    
    /* Initialize documento5 fields */
    this._selectButtondocumento5 = null;
    this._selectButtondocumento5ClickDelegate = null;
    this._deselectButtondocumento5 = null;
    this._deselectButtondocumento5ClickDelegate = null;
    this._selectordocumento5CloseDelegate = null;
    this._selectordocumento5UploaderViewFileChangedDelegate = null;
    
    this._documento5Dialog = null;
    this._selectordocumento5 = null;
    this._documento5Id = null;
    
    /* Initialize the service url for the document thumbnails */
    this.documentServiceUrl = null;

    /* Calls the base constructor */
    SitefinityWebApp.WidgetDesigners.AcordeonConArchivoWidgetDesigner5.initializeBase(this, [element]);
}

SitefinityWebApp.WidgetDesigners.AcordeonConArchivoWidgetDesigner5.prototype = {
    /* --------------------------------- set up and tear down --------------------------------- */
    initialize: function () {
        /* Here you can attach to events or do other initialization */
        SitefinityWebApp.WidgetDesigners.AcordeonConArchivoWidgetDesigner5.callBaseMethod(this, 'initialize');

        /* Initialize documento */
        this._selectButtondocumentoClickDelegate = Function.createDelegate(this, this._selectButtondocumentoClicked);
        if (this._selectButtondocumento) {
            $addHandler(this._selectButtondocumento, "click", this._selectButtondocumentoClickDelegate);
        }

        this._deselectButtondocumentoClickDelegate = Function.createDelegate(this, this._deselectButtondocumentoClicked);
        if (this._deselectButtondocumento) {
            $addHandler(this._deselectButtondocumento, "click", this._deselectButtondocumentoClickDelegate);
        }

        if (this._selectordocumento) {
            this._documentoDialog = jQuery(this._selectordocumento.get_element()).dialog({
                autoOpen: false,
                modal: false,
                width: 655,
                height: "auto",
                closeOnEscape: true,
                resizable: false,
                draggable: false,
                zIndex: 5000,
                close: this._selectordocumentoCloseDelegate
            });
        } 

        jQuery("#previewdocumento").load(function () {
            dialogBase.resizeToContent();
        });

        this._selectordocumentoInsertDelegate = Function.createDelegate(this, this._selectordocumentoInsertHandler);
        this._selectordocumento.set_customInsertDelegate(this._selectordocumentoInsertDelegate);
        $addHandler(this._selectordocumento._cancelLink, "click", this._selectordocumentoCloseHandler);
        this._selectordocumentoCloseDelegate = Function.createDelegate(this, this._selectordocumentoCloseHandler);
        this._selectordocumentoUploaderViewFileChangedDelegate = Function.createDelegate(this, this._selectordocumentoUploaderViewFileChangedHandler);

        /* Initialize documento2 */
        this._selectButtondocumento2ClickDelegate = Function.createDelegate(this, this._selectButtondocumento2Clicked);
        if (this._selectButtondocumento2) {
            $addHandler(this._selectButtondocumento2, "click", this._selectButtondocumento2ClickDelegate);
        }

        this._deselectButtondocumento2ClickDelegate = Function.createDelegate(this, this._deselectButtondocumento2Clicked);
        if (this._deselectButtondocumento2) {
            $addHandler(this._deselectButtondocumento2, "click", this._deselectButtondocumento2ClickDelegate);
        }

        if (this._selectordocumento2) {
            this._documento2Dialog = jQuery(this._selectordocumento2.get_element()).dialog({
                autoOpen: false,
                modal: false,
                width: 655,
                height: "auto",
                closeOnEscape: true,
                resizable: false,
                draggable: false,
                zIndex: 5000,
                close: this._selectordocumento2CloseDelegate
            });
        } 

        jQuery("#previewdocumento2").load(function () {
            dialogBase.resizeToContent();
        });

        this._selectordocumento2InsertDelegate = Function.createDelegate(this, this._selectordocumento2InsertHandler);
        this._selectordocumento2.set_customInsertDelegate(this._selectordocumento2InsertDelegate);
        $addHandler(this._selectordocumento2._cancelLink, "click", this._selectordocumento2CloseHandler);
        this._selectordocumento2CloseDelegate = Function.createDelegate(this, this._selectordocumento2CloseHandler);
        this._selectordocumento2UploaderViewFileChangedDelegate = Function.createDelegate(this, this._selectordocumento2UploaderViewFileChangedHandler);

        /* Initialize documento3 */
        this._selectButtondocumento3ClickDelegate = Function.createDelegate(this, this._selectButtondocumento3Clicked);
        if (this._selectButtondocumento3) {
            $addHandler(this._selectButtondocumento3, "click", this._selectButtondocumento3ClickDelegate);
        }

        this._deselectButtondocumento3ClickDelegate = Function.createDelegate(this, this._deselectButtondocumento3Clicked);
        if (this._deselectButtondocumento3) {
            $addHandler(this._deselectButtondocumento3, "click", this._deselectButtondocumento3ClickDelegate);
        }

        if (this._selectordocumento3) {
            this._documento3Dialog = jQuery(this._selectordocumento3.get_element()).dialog({
                autoOpen: false,
                modal: false,
                width: 655,
                height: "auto",
                closeOnEscape: true,
                resizable: false,
                draggable: false,
                zIndex: 5000,
                close: this._selectordocumento3CloseDelegate
            });
        } 

        jQuery("#previewdocumento3").load(function () {
            dialogBase.resizeToContent();
        });

        this._selectordocumento3InsertDelegate = Function.createDelegate(this, this._selectordocumento3InsertHandler);
        this._selectordocumento3.set_customInsertDelegate(this._selectordocumento3InsertDelegate);
        $addHandler(this._selectordocumento3._cancelLink, "click", this._selectordocumento3CloseHandler);
        this._selectordocumento3CloseDelegate = Function.createDelegate(this, this._selectordocumento3CloseHandler);
        this._selectordocumento3UploaderViewFileChangedDelegate = Function.createDelegate(this, this._selectordocumento3UploaderViewFileChangedHandler);

        /* Initialize documento4 */
        this._selectButtondocumento4ClickDelegate = Function.createDelegate(this, this._selectButtondocumento4Clicked);
        if (this._selectButtondocumento4) {
            $addHandler(this._selectButtondocumento4, "click", this._selectButtondocumento4ClickDelegate);
        }

        this._deselectButtondocumento4ClickDelegate = Function.createDelegate(this, this._deselectButtondocumento4Clicked);
        if (this._deselectButtondocumento4) {
            $addHandler(this._deselectButtondocumento4, "click", this._deselectButtondocumento4ClickDelegate);
        }

        if (this._selectordocumento4) {
            this._documento4Dialog = jQuery(this._selectordocumento4.get_element()).dialog({
                autoOpen: false,
                modal: false,
                width: 655,
                height: "auto",
                closeOnEscape: true,
                resizable: false,
                draggable: false,
                zIndex: 5000,
                close: this._selectordocumento4CloseDelegate
            });
        } 

        jQuery("#previewdocumento4").load(function () {
            dialogBase.resizeToContent();
        });

        this._selectordocumento4InsertDelegate = Function.createDelegate(this, this._selectordocumento4InsertHandler);
        this._selectordocumento4.set_customInsertDelegate(this._selectordocumento4InsertDelegate);
        $addHandler(this._selectordocumento4._cancelLink, "click", this._selectordocumento4CloseHandler);
        this._selectordocumento4CloseDelegate = Function.createDelegate(this, this._selectordocumento4CloseHandler);
        this._selectordocumento4UploaderViewFileChangedDelegate = Function.createDelegate(this, this._selectordocumento4UploaderViewFileChangedHandler);

        /* Initialize documento5 */
        this._selectButtondocumento5ClickDelegate = Function.createDelegate(this, this._selectButtondocumento5Clicked);
        if (this._selectButtondocumento5) {
            $addHandler(this._selectButtondocumento5, "click", this._selectButtondocumento5ClickDelegate);
        }

        this._deselectButtondocumento5ClickDelegate = Function.createDelegate(this, this._deselectButtondocumento5Clicked);
        if (this._deselectButtondocumento5) {
            $addHandler(this._deselectButtondocumento5, "click", this._deselectButtondocumento5ClickDelegate);
        }

        if (this._selectordocumento5) {
            this._documento5Dialog = jQuery(this._selectordocumento5.get_element()).dialog({
                autoOpen: false,
                modal: false,
                width: 655,
                height: "auto",
                closeOnEscape: true,
                resizable: false,
                draggable: false,
                zIndex: 5000,
                close: this._selectordocumento5CloseDelegate
            });
        } 

        jQuery("#previewdocumento5").load(function () {
            dialogBase.resizeToContent();
        });

        this._selectordocumento5InsertDelegate = Function.createDelegate(this, this._selectordocumento5InsertHandler);
        this._selectordocumento5.set_customInsertDelegate(this._selectordocumento5InsertDelegate);
        $addHandler(this._selectordocumento5._cancelLink, "click", this._selectordocumento5CloseHandler);
        this._selectordocumento5CloseDelegate = Function.createDelegate(this, this._selectordocumento5CloseHandler);
        this._selectordocumento5UploaderViewFileChangedDelegate = Function.createDelegate(this, this._selectordocumento5UploaderViewFileChangedHandler);
    },
    dispose: function () {
        /* this is the place to unbind/dispose the event handlers created in the initialize method */
        SitefinityWebApp.WidgetDesigners.AcordeonConArchivoWidgetDesigner5.callBaseMethod(this, 'dispose');

        /* Dispose documento */
        if (this._selectButtondocumento) {
            $removeHandler(this._selectButtondocumento, "click", this._selectButtondocumentoClickDelegate);
        }
        if (this._selectButtondocumentoClickDelegate) {
            delete this._selectButtondocumentoClickDelegate;
        }
        
        if (this._deselectButtondocumento) {
            $removeHandler(this._deselectButtondocumento, "click", this._deselectButtondocumentoClickDelegate);
        }
        if (this._deselectButtondocumentoClickDelegate) {
            delete this._deselectButtondocumentoClickDelegate;
        }

        $removeHandler(this._selectordocumento._cancelLink, "click", this._selectordocumentoCloseHandler);

        if (this._selectordocumentoCloseDelegate) {
            delete this._selectordocumentoCloseDelegate;
        }

        if (this._selectordocumentoUploaderViewFileChangedDelegate) {
            this._selectordocumento._uploaderView.remove_onFileChanged(this._selectordocumentoUploaderViewFileChangedDelegate);
            delete this._selectordocumentoUploaderViewFileChangedDelegate;
        }

        /* Dispose documento2 */
        if (this._selectButtondocumento2) {
            $removeHandler(this._selectButtondocumento2, "click", this._selectButtondocumento2ClickDelegate);
        }
        if (this._selectButtondocumento2ClickDelegate) {
            delete this._selectButtondocumento2ClickDelegate;
        }
        
        if (this._deselectButtondocumento2) {
            $removeHandler(this._deselectButtondocumento2, "click", this._deselectButtondocumento2ClickDelegate);
        }
        if (this._deselectButtondocumento2ClickDelegate) {
            delete this._deselectButtondocumento2ClickDelegate;
        }

        $removeHandler(this._selectordocumento2._cancelLink, "click", this._selectordocumento2CloseHandler);

        if (this._selectordocumento2CloseDelegate) {
            delete this._selectordocumento2CloseDelegate;
        }

        if (this._selectordocumento2UploaderViewFileChangedDelegate) {
            this._selectordocumento2._uploaderView.remove_onFileChanged(this._selectordocumento2UploaderViewFileChangedDelegate);
            delete this._selectordocumento2UploaderViewFileChangedDelegate;
        }

        /* Dispose documento3 */
        if (this._selectButtondocumento3) {
            $removeHandler(this._selectButtondocumento3, "click", this._selectButtondocumento3ClickDelegate);
        }
        if (this._selectButtondocumento3ClickDelegate) {
            delete this._selectButtondocumento3ClickDelegate;
        }
        
        if (this._deselectButtondocumento3) {
            $removeHandler(this._deselectButtondocumento3, "click", this._deselectButtondocumento3ClickDelegate);
        }
        if (this._deselectButtondocumento3ClickDelegate) {
            delete this._deselectButtondocumento3ClickDelegate;
        }

        $removeHandler(this._selectordocumento3._cancelLink, "click", this._selectordocumento3CloseHandler);

        if (this._selectordocumento3CloseDelegate) {
            delete this._selectordocumento3CloseDelegate;
        }

        if (this._selectordocumento3UploaderViewFileChangedDelegate) {
            this._selectordocumento3._uploaderView.remove_onFileChanged(this._selectordocumento3UploaderViewFileChangedDelegate);
            delete this._selectordocumento3UploaderViewFileChangedDelegate;
        }

        /* Dispose documento4 */
        if (this._selectButtondocumento4) {
            $removeHandler(this._selectButtondocumento4, "click", this._selectButtondocumento4ClickDelegate);
        }
        if (this._selectButtondocumento4ClickDelegate) {
            delete this._selectButtondocumento4ClickDelegate;
        }
        
        if (this._deselectButtondocumento4) {
            $removeHandler(this._deselectButtondocumento4, "click", this._deselectButtondocumento4ClickDelegate);
        }
        if (this._deselectButtondocumento4ClickDelegate) {
            delete this._deselectButtondocumento4ClickDelegate;
        }

        $removeHandler(this._selectordocumento4._cancelLink, "click", this._selectordocumento4CloseHandler);

        if (this._selectordocumento4CloseDelegate) {
            delete this._selectordocumento4CloseDelegate;
        }

        if (this._selectordocumento4UploaderViewFileChangedDelegate) {
            this._selectordocumento4._uploaderView.remove_onFileChanged(this._selectordocumento4UploaderViewFileChangedDelegate);
            delete this._selectordocumento4UploaderViewFileChangedDelegate;
        }

        /* Dispose documento5 */
        if (this._selectButtondocumento5) {
            $removeHandler(this._selectButtondocumento5, "click", this._selectButtondocumento5ClickDelegate);
        }
        if (this._selectButtondocumento5ClickDelegate) {
            delete this._selectButtondocumento5ClickDelegate;
        }
        
        if (this._deselectButtondocumento5) {
            $removeHandler(this._deselectButtondocumento5, "click", this._deselectButtondocumento5ClickDelegate);
        }
        if (this._deselectButtondocumento5ClickDelegate) {
            delete this._deselectButtondocumento5ClickDelegate;
        }

        $removeHandler(this._selectordocumento5._cancelLink, "click", this._selectordocumento5CloseHandler);

        if (this._selectordocumento5CloseDelegate) {
            delete this._selectordocumento5CloseDelegate;
        }

        if (this._selectordocumento5UploaderViewFileChangedDelegate) {
            this._selectordocumento5._uploaderView.remove_onFileChanged(this._selectordocumento5UploaderViewFileChangedDelegate);
            delete this._selectordocumento5UploaderViewFileChangedDelegate;
        }
    },

    /* --------------------------------- public methods ---------------------------------- */

    findElement: function (id) {
        var result = jQuery(this.get_element()).find("#" + id).get(0);
        return result;
    },

    /* Called when the designer window gets opened and here is place to "bind" your designer to the control properties */
    refreshUI: function () {
        var controlData = this._propertyEditor.get_control().Settings; /* JavaScript clone of your control - all the control properties will be properties of the controlData too */

        /* RefreshUI titulo */
        jQuery(this.get_titulo()).val(controlData.titulo);

        /* RefreshUI texto */
        jQuery(this.get_texto()).val(controlData.texto);

        /* RefreshUI documento */
        this.get_selecteddocumento().innerHTML = controlData.documento;
        if (controlData.documento && controlData.documento != "00000000-0000-0000-0000-000000000000") {
            this.get_selectButtondocumento().innerHTML = "<span class=\"sfLinkBtnIn\">Change</span>";
            jQuery(this.get_deselectButtondocumento()).show()
            var url = this.documentServiceUrl + controlData.documento + "/?published=true";
            jQuery.ajax({
                url: url,
                type: "GET",
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    var cssClass = "sf" + data.Item.Extension.substring(1);
                    jQuery("#previewdocumento").removeClass().addClass(cssClass).text(data.Item.Title.Value);
                    dialogBase.resizeToContent();
                }
            });
        }
        else {
            jQuery(this.get_deselectButtondocumento()).hide()
        }

        /* RefreshUI texto2 */
        jQuery(this.get_texto2()).val(controlData.texto2);

        /* RefreshUI documento2 */
        this.get_selecteddocumento2().innerHTML = controlData.documento2;
        if (controlData.documento2 && controlData.documento2 != "00000000-0000-0000-0000-000000000000") {
            this.get_selectButtondocumento2().innerHTML = "<span class=\"sfLinkBtnIn\">Change</span>";
            jQuery(this.get_deselectButtondocumento2()).show()
            var url = this.documentServiceUrl + controlData.documento2 + "/?published=true";
            jQuery.ajax({
                url: url,
                type: "GET",
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    var cssClass = "sf" + data.Item.Extension.substring(1);
                    jQuery("#previewdocumento2").removeClass().addClass(cssClass).text(data.Item.Title.Value);
                    dialogBase.resizeToContent();
                }
            });
        }
        else {
            jQuery(this.get_deselectButtondocumento2()).hide()
        }

        /* RefreshUI texto3 */
        jQuery(this.get_texto3()).val(controlData.texto3);

        /* RefreshUI documento3 */
        this.get_selecteddocumento3().innerHTML = controlData.documento3;
        if (controlData.documento3 && controlData.documento3 != "00000000-0000-0000-0000-000000000000") {
            this.get_selectButtondocumento3().innerHTML = "<span class=\"sfLinkBtnIn\">Change</span>";
            jQuery(this.get_deselectButtondocumento3()).show()
            var url = this.documentServiceUrl + controlData.documento3 + "/?published=true";
            jQuery.ajax({
                url: url,
                type: "GET",
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    var cssClass = "sf" + data.Item.Extension.substring(1);
                    jQuery("#previewdocumento3").removeClass().addClass(cssClass).text(data.Item.Title.Value);
                    dialogBase.resizeToContent();
                }
            });
        }
        else {
            jQuery(this.get_deselectButtondocumento3()).hide()
        }

        /* RefreshUI texto4 */
        jQuery(this.get_texto4()).val(controlData.texto4);

        /* RefreshUI documento4 */
        this.get_selecteddocumento4().innerHTML = controlData.documento4;
        if (controlData.documento4 && controlData.documento4 != "00000000-0000-0000-0000-000000000000") {
            this.get_selectButtondocumento4().innerHTML = "<span class=\"sfLinkBtnIn\">Change</span>";
            jQuery(this.get_deselectButtondocumento4()).show()
            var url = this.documentServiceUrl + controlData.documento4 + "/?published=true";
            jQuery.ajax({
                url: url,
                type: "GET",
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    var cssClass = "sf" + data.Item.Extension.substring(1);
                    jQuery("#previewdocumento4").removeClass().addClass(cssClass).text(data.Item.Title.Value);
                    dialogBase.resizeToContent();
                }
            });
        }
        else {
            jQuery(this.get_deselectButtondocumento4()).hide()
        }

        /* RefreshUI texto5 */
        jQuery(this.get_texto5()).val(controlData.texto5);

        /* RefreshUI documento5 */
        this.get_selecteddocumento5().innerHTML = controlData.documento5;
        if (controlData.documento5 && controlData.documento5 != "00000000-0000-0000-0000-000000000000") {
            this.get_selectButtondocumento5().innerHTML = "<span class=\"sfLinkBtnIn\">Change</span>";
            jQuery(this.get_deselectButtondocumento5()).show()
            var url = this.documentServiceUrl + controlData.documento5 + "/?published=true";
            jQuery.ajax({
                url: url,
                type: "GET",
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    var cssClass = "sf" + data.Item.Extension.substring(1);
                    jQuery("#previewdocumento5").removeClass().addClass(cssClass).text(data.Item.Title.Value);
                    dialogBase.resizeToContent();
                }
            });
        }
        else {
            jQuery(this.get_deselectButtondocumento5()).hide()
        }
    },

    /* Called when the "Save" button is clicked. Here you can transfer the settings from the designer to the control */
    applyChanges: function () {
        var controlData = this._propertyEditor.get_control().Settings;

        /* ApplyChanges titulo */
        controlData.titulo = jQuery(this.get_titulo()).val();

        /* ApplyChanges texto */
        controlData.texto = jQuery(this.get_texto()).val();

        /* ApplyChanges documento */
        controlData.documento = this.get_selecteddocumento().innerHTML;

        /* ApplyChanges texto2 */
        controlData.texto2 = jQuery(this.get_texto2()).val();

        /* ApplyChanges documento2 */
        controlData.documento2 = this.get_selecteddocumento2().innerHTML;

        /* ApplyChanges texto3 */
        controlData.texto3 = jQuery(this.get_texto3()).val();

        /* ApplyChanges documento3 */
        controlData.documento3 = this.get_selecteddocumento3().innerHTML;

        /* ApplyChanges texto4 */
        controlData.texto4 = jQuery(this.get_texto4()).val();

        /* ApplyChanges documento4 */
        controlData.documento4 = this.get_selecteddocumento4().innerHTML;

        /* ApplyChanges texto5 */
        controlData.texto5 = jQuery(this.get_texto5()).val();

        /* ApplyChanges documento5 */
        controlData.documento5 = this.get_selecteddocumento5().innerHTML;
    },

    /* --------------------------------- event handlers ---------------------------------- */

    /* documento event handlers */
    _selectButtondocumentoClicked: function (sender, args) {
        this._selectordocumento._uploaderView.add_onFileChanged(this._selectordocumentoUploaderViewFileChangedDelegate);
        this._documentoDialog.dialog("open");
        jQuery("#designerLayoutRoot").hide();
        this._documentoDialog.dialog().parent().css("min-width", "655px");
        dialogBase.resizeToContent();
        try {
            this._selectordocumento.get_uploaderView().get_altTextField().set_value("");
        }
        catch (ex) { }
        jQuery(this._selectordocumento.get_uploaderView().get_settingsPanel()).hide();
        return false;
    },

    _deselectButtondocumentoClicked: function (sender, args) {
        jQuery("#previewdocumento").removeClass().attr("src", "").text("");
        this.get_selecteddocumento().innerHTML = "00000000-0000-0000-0000-000000000000";
        this.get_selectButtondocumento().innerHTML = "<span class=\"sfLinkBtnIn\">Select...</span>";
        jQuery(this.get_deselectButtondocumento()).hide()
		dialogBase.resizeToContent();
        return false;
    },

    /* documento2 event handlers */
    _selectButtondocumento2Clicked: function (sender, args) {
        this._selectordocumento2._uploaderView.add_onFileChanged(this._selectordocumento2UploaderViewFileChangedDelegate);
        this._documento2Dialog.dialog("open");
        jQuery("#designerLayoutRoot").hide();
        this._documento2Dialog.dialog().parent().css("min-width", "655px");
        dialogBase.resizeToContent();
        try {
            this._selectordocumento2.get_uploaderView().get_altTextField().set_value("");
        }
        catch (ex) { }
        jQuery(this._selectordocumento2.get_uploaderView().get_settingsPanel()).hide();
        return false;
    },

    _deselectButtondocumento2Clicked: function (sender, args) {
        jQuery("#previewdocumento2").removeClass().attr("src", "").text("");
        this.get_selecteddocumento2().innerHTML = "00000000-0000-0000-0000-000000000000";
        this.get_selectButtondocumento2().innerHTML = "<span class=\"sfLinkBtnIn\">Select...</span>";
        jQuery(this.get_deselectButtondocumento2()).hide()
		dialogBase.resizeToContent();
        return false;
    },

    /* documento3 event handlers */
    _selectButtondocumento3Clicked: function (sender, args) {
        this._selectordocumento3._uploaderView.add_onFileChanged(this._selectordocumento3UploaderViewFileChangedDelegate);
        this._documento3Dialog.dialog("open");
        jQuery("#designerLayoutRoot").hide();
        this._documento3Dialog.dialog().parent().css("min-width", "655px");
        dialogBase.resizeToContent();
        try {
            this._selectordocumento3.get_uploaderView().get_altTextField().set_value("");
        }
        catch (ex) { }
        jQuery(this._selectordocumento3.get_uploaderView().get_settingsPanel()).hide();
        return false;
    },

    _deselectButtondocumento3Clicked: function (sender, args) {
        jQuery("#previewdocumento3").removeClass().attr("src", "").text("");
        this.get_selecteddocumento3().innerHTML = "00000000-0000-0000-0000-000000000000";
        this.get_selectButtondocumento3().innerHTML = "<span class=\"sfLinkBtnIn\">Select...</span>";
        jQuery(this.get_deselectButtondocumento3()).hide()
		dialogBase.resizeToContent();
        return false;
    },

    /* documento4 event handlers */
    _selectButtondocumento4Clicked: function (sender, args) {
        this._selectordocumento4._uploaderView.add_onFileChanged(this._selectordocumento4UploaderViewFileChangedDelegate);
        this._documento4Dialog.dialog("open");
        jQuery("#designerLayoutRoot").hide();
        this._documento4Dialog.dialog().parent().css("min-width", "655px");
        dialogBase.resizeToContent();
        try {
            this._selectordocumento4.get_uploaderView().get_altTextField().set_value("");
        }
        catch (ex) { }
        jQuery(this._selectordocumento4.get_uploaderView().get_settingsPanel()).hide();
        return false;
    },

    _deselectButtondocumento4Clicked: function (sender, args) {
        jQuery("#previewdocumento4").removeClass().attr("src", "").text("");
        this.get_selecteddocumento4().innerHTML = "00000000-0000-0000-0000-000000000000";
        this.get_selectButtondocumento4().innerHTML = "<span class=\"sfLinkBtnIn\">Select...</span>";
        jQuery(this.get_deselectButtondocumento4()).hide()
		dialogBase.resizeToContent();
        return false;
    },

    /* documento5 event handlers */
    _selectButtondocumento5Clicked: function (sender, args) {
        this._selectordocumento5._uploaderView.add_onFileChanged(this._selectordocumento5UploaderViewFileChangedDelegate);
        this._documento5Dialog.dialog("open");
        jQuery("#designerLayoutRoot").hide();
        this._documento5Dialog.dialog().parent().css("min-width", "655px");
        dialogBase.resizeToContent();
        try {
            this._selectordocumento5.get_uploaderView().get_altTextField().set_value("");
        }
        catch (ex) { }
        jQuery(this._selectordocumento5.get_uploaderView().get_settingsPanel()).hide();
        return false;
    },

    _deselectButtondocumento5Clicked: function (sender, args) {
        jQuery("#previewdocumento5").removeClass().attr("src", "").text("");
        this.get_selecteddocumento5().innerHTML = "00000000-0000-0000-0000-000000000000";
        this.get_selectButtondocumento5().innerHTML = "<span class=\"sfLinkBtnIn\">Select...</span>";
        jQuery(this.get_deselectButtondocumento5()).hide()
		dialogBase.resizeToContent();
        return false;
    },

    /* --------------------------------- private methods --------------------------------- */

    /* documento private methods */
    _selectordocumentoInsertHandler: function (selectedItem) {

        if (selectedItem) {
            this._documentoId = selectedItem.Id;
            this.get_selecteddocumento().innerHTML = this._documentoId;
            jQuery(this.get_deselectButtondocumento()).show()
            this.get_selectButtondocumento().innerHTML = "<span class=\"sfLinkBtnIn\">Change</span>";
            jQuery("#previewdocumento").removeClass().addClass("sf" + selectedItem.Extension.substring(1)).text(selectedItem.Title);
        }
        this._documentoDialog.dialog("close");
        jQuery("#designerLayoutRoot").show();
        dialogBase.resizeToContent();
    },

    _selectordocumentoCloseHandler: function () {
        if(this._documentoDialog){
            this._documentoDialog.dialog("close");
        }
        jQuery("#designerLayoutRoot").show();
        dialogBase.resizeToContent();
    },

    _selectordocumentoUploaderViewFileChangedHandler: function () {
        dialogBase.resizeToContent();
    },

    /* documento2 private methods */
    _selectordocumento2InsertHandler: function (selectedItem) {

        if (selectedItem) {
            this._documento2Id = selectedItem.Id;
            this.get_selecteddocumento2().innerHTML = this._documento2Id;
            jQuery(this.get_deselectButtondocumento2()).show()
            this.get_selectButtondocumento2().innerHTML = "<span class=\"sfLinkBtnIn\">Change</span>";
            jQuery("#previewdocumento2").removeClass().addClass("sf" + selectedItem.Extension.substring(1)).text(selectedItem.Title);
        }
        this._documento2Dialog.dialog("close");
        jQuery("#designerLayoutRoot").show();
        dialogBase.resizeToContent();
    },

    _selectordocumento2CloseHandler: function () {
        if(this._documento2Dialog){
            this._documento2Dialog.dialog("close");
        }
        jQuery("#designerLayoutRoot").show();
        dialogBase.resizeToContent();
    },

    _selectordocumento2UploaderViewFileChangedHandler: function () {
        dialogBase.resizeToContent();
    },

    /* documento3 private methods */
    _selectordocumento3InsertHandler: function (selectedItem) {

        if (selectedItem) {
            this._documento3Id = selectedItem.Id;
            this.get_selecteddocumento3().innerHTML = this._documento3Id;
            jQuery(this.get_deselectButtondocumento3()).show()
            this.get_selectButtondocumento3().innerHTML = "<span class=\"sfLinkBtnIn\">Change</span>";
            jQuery("#previewdocumento3").removeClass().addClass("sf" + selectedItem.Extension.substring(1)).text(selectedItem.Title);
        }
        this._documento3Dialog.dialog("close");
        jQuery("#designerLayoutRoot").show();
        dialogBase.resizeToContent();
    },

    _selectordocumento3CloseHandler: function () {
        if(this._documento3Dialog){
            this._documento3Dialog.dialog("close");
        }
        jQuery("#designerLayoutRoot").show();
        dialogBase.resizeToContent();
    },

    _selectordocumento3UploaderViewFileChangedHandler: function () {
        dialogBase.resizeToContent();
    },

    /* documento4 private methods */
    _selectordocumento4InsertHandler: function (selectedItem) {

        if (selectedItem) {
            this._documento4Id = selectedItem.Id;
            this.get_selecteddocumento4().innerHTML = this._documento4Id;
            jQuery(this.get_deselectButtondocumento4()).show()
            this.get_selectButtondocumento4().innerHTML = "<span class=\"sfLinkBtnIn\">Change</span>";
            jQuery("#previewdocumento4").removeClass().addClass("sf" + selectedItem.Extension.substring(1)).text(selectedItem.Title);
        }
        this._documento4Dialog.dialog("close");
        jQuery("#designerLayoutRoot").show();
        dialogBase.resizeToContent();
    },

    _selectordocumento4CloseHandler: function () {
        if(this._documento4Dialog){
            this._documento4Dialog.dialog("close");
        }
        jQuery("#designerLayoutRoot").show();
        dialogBase.resizeToContent();
    },

    _selectordocumento4UploaderViewFileChangedHandler: function () {
        dialogBase.resizeToContent();
    },

    /* documento5 private methods */
    _selectordocumento5InsertHandler: function (selectedItem) {

        if (selectedItem) {
            this._documento5Id = selectedItem.Id;
            this.get_selecteddocumento5().innerHTML = this._documento5Id;
            jQuery(this.get_deselectButtondocumento5()).show()
            this.get_selectButtondocumento5().innerHTML = "<span class=\"sfLinkBtnIn\">Change</span>";
            jQuery("#previewdocumento5").removeClass().addClass("sf" + selectedItem.Extension.substring(1)).text(selectedItem.Title);
        }
        this._documento5Dialog.dialog("close");
        jQuery("#designerLayoutRoot").show();
        dialogBase.resizeToContent();
    },

    _selectordocumento5CloseHandler: function () {
        if(this._documento5Dialog){
            this._documento5Dialog.dialog("close");
        }
        jQuery("#designerLayoutRoot").show();
        dialogBase.resizeToContent();
    },

    _selectordocumento5UploaderViewFileChangedHandler: function () {
        dialogBase.resizeToContent();
    },

    /* --------------------------------- properties -------------------------------------- */

    /* titulo properties */
    get_titulo: function () { return this._titulo; }, 
    set_titulo: function (value) { this._titulo = value; },

    /* texto properties */
    get_texto: function () { return this._texto; }, 
    set_texto: function (value) { this._texto = value; },

    /* documento properties */
    get_selectordocumento: function () {
        return this._selectordocumento;
    },
    set_selectordocumento: function (value) {
        this._selectordocumento = value;
    },
    get_selectButtondocumento: function () {
        return this._selectButtondocumento;
    },
    set_selectButtondocumento: function (value) {
        this._selectButtondocumento = value;
    },
    get_deselectButtondocumento: function () {
        return this._deselectButtondocumento;
    },
    set_deselectButtondocumento: function (value) {
        this._deselectButtondocumento = value;
    },
    get_selecteddocumento: function () {
        if (this._selecteddocumento == null) {
            this._selecteddocumento = this.findElement("selecteddocumento");
        }
        return this._selecteddocumento;
    },

    /* texto2 properties */
    get_texto2: function () { return this._texto2; }, 
    set_texto2: function (value) { this._texto2 = value; },

    /* documento2 properties */
    get_selectordocumento2: function () {
        return this._selectordocumento2;
    },
    set_selectordocumento2: function (value) {
        this._selectordocumento2 = value;
    },
    get_selectButtondocumento2: function () {
        return this._selectButtondocumento2;
    },
    set_selectButtondocumento2: function (value) {
        this._selectButtondocumento2 = value;
    },
    get_deselectButtondocumento2: function () {
        return this._deselectButtondocumento2;
    },
    set_deselectButtondocumento2: function (value) {
        this._deselectButtondocumento2 = value;
    },
    get_selecteddocumento2: function () {
        if (this._selecteddocumento2 == null) {
            this._selecteddocumento2 = this.findElement("selecteddocumento2");
        }
        return this._selecteddocumento2;
    },

    /* texto3 properties */
    get_texto3: function () { return this._texto3; }, 
    set_texto3: function (value) { this._texto3 = value; },

    /* documento3 properties */
    get_selectordocumento3: function () {
        return this._selectordocumento3;
    },
    set_selectordocumento3: function (value) {
        this._selectordocumento3 = value;
    },
    get_selectButtondocumento3: function () {
        return this._selectButtondocumento3;
    },
    set_selectButtondocumento3: function (value) {
        this._selectButtondocumento3 = value;
    },
    get_deselectButtondocumento3: function () {
        return this._deselectButtondocumento3;
    },
    set_deselectButtondocumento3: function (value) {
        this._deselectButtondocumento3 = value;
    },
    get_selecteddocumento3: function () {
        if (this._selecteddocumento3 == null) {
            this._selecteddocumento3 = this.findElement("selecteddocumento3");
        }
        return this._selecteddocumento3;
    },

    /* texto4 properties */
    get_texto4: function () { return this._texto4; }, 
    set_texto4: function (value) { this._texto4 = value; },

    /* documento4 properties */
    get_selectordocumento4: function () {
        return this._selectordocumento4;
    },
    set_selectordocumento4: function (value) {
        this._selectordocumento4 = value;
    },
    get_selectButtondocumento4: function () {
        return this._selectButtondocumento4;
    },
    set_selectButtondocumento4: function (value) {
        this._selectButtondocumento4 = value;
    },
    get_deselectButtondocumento4: function () {
        return this._deselectButtondocumento4;
    },
    set_deselectButtondocumento4: function (value) {
        this._deselectButtondocumento4 = value;
    },
    get_selecteddocumento4: function () {
        if (this._selecteddocumento4 == null) {
            this._selecteddocumento4 = this.findElement("selecteddocumento4");
        }
        return this._selecteddocumento4;
    },

    /* texto5 properties */
    get_texto5: function () { return this._texto5; }, 
    set_texto5: function (value) { this._texto5 = value; },

    /* documento5 properties */
    get_selectordocumento5: function () {
        return this._selectordocumento5;
    },
    set_selectordocumento5: function (value) {
        this._selectordocumento5 = value;
    },
    get_selectButtondocumento5: function () {
        return this._selectButtondocumento5;
    },
    set_selectButtondocumento5: function (value) {
        this._selectButtondocumento5 = value;
    },
    get_deselectButtondocumento5: function () {
        return this._deselectButtondocumento5;
    },
    set_deselectButtondocumento5: function (value) {
        this._deselectButtondocumento5 = value;
    },
    get_selecteddocumento5: function () {
        if (this._selecteddocumento5 == null) {
            this._selecteddocumento5 = this.findElement("selecteddocumento5");
        }
        return this._selecteddocumento5;
    }
}

SitefinityWebApp.WidgetDesigners.AcordeonConArchivoWidgetDesigner5.registerClass('SitefinityWebApp.WidgetDesigners.AcordeonConArchivoWidgetDesigner5', Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesignerBase);
