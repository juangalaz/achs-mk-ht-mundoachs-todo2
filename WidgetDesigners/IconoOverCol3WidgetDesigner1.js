Type.registerNamespace("SitefinityWebApp.WidgetDesigners");

SitefinityWebApp.WidgetDesigners.IconoOverCol3WidgetDesigner1 = function (element) {
    /* Initialize titulo fields */
    this._titulo = null;
    
    /* Initialize texto fields */
    this._texto = null;
    
    /* Initialize imagen fields */
    this._selectButtonimagen = null;
    this._selectButtonimagenClickDelegate = null;
    this._deselectButtonimagen = null;
    this._deselectButtonimagenClickDelegate = null;
    this._selectorimagenCloseDelegate = null;
    this._selectorimagenUploaderViewFileChangedDelegate = null;
    
    this._imagenDialog = null;
    this._selectorimagen = null;
    this._imagenId = null;
    
    /* Initialize the service url for the image thumbnails */
    this.imageServiceUrl = null;

    /* Calls the base constructor */
    SitefinityWebApp.WidgetDesigners.IconoOverCol3WidgetDesigner1.initializeBase(this, [element]);
}

SitefinityWebApp.WidgetDesigners.IconoOverCol3WidgetDesigner1.prototype = {
    /* --------------------------------- set up and tear down --------------------------------- */
    initialize: function () {
        /* Here you can attach to events or do other initialization */
        SitefinityWebApp.WidgetDesigners.IconoOverCol3WidgetDesigner1.callBaseMethod(this, 'initialize');

        /* Initialize imagen */
        this._selectButtonimagenClickDelegate = Function.createDelegate(this, this._selectButtonimagenClicked);
        if (this._selectButtonimagen) {
            $addHandler(this._selectButtonimagen, "click", this._selectButtonimagenClickDelegate);
        }

        this._deselectButtonimagenClickDelegate = Function.createDelegate(this, this._deselectButtonimagenClicked);
        if (this._deselectButtonimagen) {
            $addHandler(this._deselectButtonimagen, "click", this._deselectButtonimagenClickDelegate);
        }

        if (this._selectorimagen) {
            this._imagenDialog = jQuery(this._selectorimagen.get_element()).dialog({
                autoOpen: false,
                modal: false,
                width: 655,
                height: "auto",
                closeOnEscape: true,
                resizable: false,
                draggable: false,
                zIndex: 5000,
                close: this._selectorimagenCloseDelegate
            });
        } 

        jQuery("#previewimagen").load(function () {
            dialogBase.resizeToContent();
        });

        this._selectorimagenInsertDelegate = Function.createDelegate(this, this._selectorimagenInsertHandler);
        this._selectorimagen.set_customInsertDelegate(this._selectorimagenInsertDelegate);
        $addHandler(this._selectorimagen._cancelLink, "click", this._selectorimagenCloseHandler);
        this._selectorimagenCloseDelegate = Function.createDelegate(this, this._selectorimagenCloseHandler);
        this._selectorimagenUploaderViewFileChangedDelegate = Function.createDelegate(this, this._selectorimagenUploaderViewFileChangedHandler);
    },
    dispose: function () {
        /* this is the place to unbind/dispose the event handlers created in the initialize method */
        SitefinityWebApp.WidgetDesigners.IconoOverCol3WidgetDesigner1.callBaseMethod(this, 'dispose');

        /* Dispose imagen */
        if (this._selectButtonimagen) {
            $removeHandler(this._selectButtonimagen, "click", this._selectButtonimagenClickDelegate);
        }
        if (this._selectButtonimagenClickDelegate) {
            delete this._selectButtonimagenClickDelegate;
        }
        
        if (this._deselectButtonimagen) {
            $removeHandler(this._deselectButtonimagen, "click", this._deselectButtonimagenClickDelegate);
        }
        if (this._deselectButtonimagenClickDelegate) {
            delete this._deselectButtonimagenClickDelegate;
        }

        $removeHandler(this._selectorimagen._cancelLink, "click", this._selectorimagenCloseHandler);

        if (this._selectorimagenCloseDelegate) {
            delete this._selectorimagenCloseDelegate;
        }

        if (this._selectorimagenUploaderViewFileChangedDelegate) {
            this._selectorimagen._uploaderView.remove_onFileChanged(this._selectorimagenUploaderViewFileChangedDelegate);
            delete this._selectorimagenUploaderViewFileChangedDelegate;
        }
    },

    /* --------------------------------- public methods ---------------------------------- */

    findElement: function (id) {
        var result = jQuery(this.get_element()).find("#" + id).get(0);
        return result;
    },

    /* Called when the designer window gets opened and here is place to "bind" your designer to the control properties */
    refreshUI: function () {
        var controlData = this._propertyEditor.get_control().Settings; /* JavaScript clone of your control - all the control properties will be properties of the controlData too */

        /* RefreshUI titulo */
        jQuery(this.get_titulo()).val(controlData.titulo);

        /* RefreshUI texto */
        jQuery(this.get_texto()).val(controlData.texto);

        /* RefreshUI imagen */
        this.get_selectedimagen().innerHTML = controlData.imagen;
        if (controlData.imagen && controlData.imagen != "00000000-0000-0000-0000-000000000000") {
            this.get_selectButtonimagen().innerHTML = "<span class=\"sfLinkBtnIn\">Change</span>";
            jQuery(this.get_deselectButtonimagen()).show()
            var url = this.imageServiceUrl + controlData.imagen + "/?published=true";
            jQuery.ajax({
                url: url,
                type: "GET",
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    jQuery("#previewimagen").show();
                    jQuery("#previewimagen").attr("src", data.Item.ThumbnailUrl);
                    dialogBase.resizeToContent();
                }
            });
        }
        else {
            jQuery(this.get_deselectButtonimagen()).hide()
        }
    },

    /* Called when the "Save" button is clicked. Here you can transfer the settings from the designer to the control */
    applyChanges: function () {
        var controlData = this._propertyEditor.get_control().Settings;

        /* ApplyChanges titulo */
        controlData.titulo = jQuery(this.get_titulo()).val();

        /* ApplyChanges texto */
        controlData.texto = jQuery(this.get_texto()).val();

        /* ApplyChanges imagen */
        controlData.imagen = this.get_selectedimagen().innerHTML;
    },

    /* --------------------------------- event handlers ---------------------------------- */

    /* imagen event handlers */
    _selectButtonimagenClicked: function (sender, args) {
        this._selectorimagen._uploaderView.add_onFileChanged(this._selectorimagenUploaderViewFileChangedDelegate);
        this._imagenDialog.dialog("open");
        jQuery("#designerLayoutRoot").hide();
        this._imagenDialog.dialog().parent().css("min-width", "655px");
        dialogBase.resizeToContent();
        try {
            this._selectorimagen.get_uploaderView().get_altTextField().set_value("");
        }
        catch (ex) { }
        jQuery(this._selectorimagen.get_uploaderView().get_settingsPanel()).hide();
        return false;
    },

    _deselectButtonimagenClicked: function (sender, args) {
        jQuery("#previewimagen").hide();
                    jQuery("#previewimagen").attr("src", "");
        this.get_selectedimagen().innerHTML = "00000000-0000-0000-0000-000000000000";
        this.get_selectButtonimagen().innerHTML = "<span class=\"sfLinkBtnIn\">Select...</span>";
        jQuery(this.get_deselectButtonimagen()).hide()
		dialogBase.resizeToContent();
        return false;
    },

    /* --------------------------------- private methods --------------------------------- */

    /* imagen private methods */
    _selectorimagenInsertHandler: function (selectedItem) {

        if (selectedItem) {
            this._imagenId = selectedItem.Id;
            this.get_selectedimagen().innerHTML = this._imagenId;
            jQuery(this.get_deselectButtonimagen()).show()
            this.get_selectButtonimagen().innerHTML = "<span class=\"sfLinkBtnIn\">Change</span>";
            jQuery("#previewimagen").show();
                    jQuery("#previewimagen").attr("src", selectedItem.ThumbnailUrl);
        }
        this._imagenDialog.dialog("close");
        jQuery("#designerLayoutRoot").show();
        dialogBase.resizeToContent();
    },

    _selectorimagenCloseHandler: function () {
        if(this._imagenDialog){
            this._imagenDialog.dialog("close");
        }
        jQuery("#designerLayoutRoot").show();
        dialogBase.resizeToContent();
    },

    _selectorimagenUploaderViewFileChangedHandler: function () {
        dialogBase.resizeToContent();
    },

    /* --------------------------------- properties -------------------------------------- */

    /* titulo properties */
    get_titulo: function () { return this._titulo; }, 
    set_titulo: function (value) { this._titulo = value; },

    /* texto properties */
    get_texto: function () { return this._texto; }, 
    set_texto: function (value) { this._texto = value; },

    /* imagen properties */
    get_selectorimagen: function () {
        return this._selectorimagen;
    },
    set_selectorimagen: function (value) {
        this._selectorimagen = value;
    },
    get_selectButtonimagen: function () {
        return this._selectButtonimagen;
    },
    set_selectButtonimagen: function (value) {
        this._selectButtonimagen = value;
    },
    get_deselectButtonimagen: function () {
        return this._deselectButtonimagen;
    },
    set_deselectButtonimagen: function (value) {
        this._deselectButtonimagen = value;
    },
    get_selectedimagen: function () {
        if (this._selectedimagen == null) {
            this._selectedimagen = this.findElement("selectedimagen");
        }
        return this._selectedimagen;
    }
}

SitefinityWebApp.WidgetDesigners.IconoOverCol3WidgetDesigner1.registerClass('SitefinityWebApp.WidgetDesigners.IconoOverCol3WidgetDesigner1', Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesignerBase);
