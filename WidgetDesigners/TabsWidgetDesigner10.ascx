<%@ Control %>
<%@ Register Assembly="Telerik.Sitefinity" TagPrefix="sf" Namespace="Telerik.Sitefinity.Web.UI" %>
<%@ Register Assembly="Telerik.Sitefinity" TagPrefix="sitefinity" Namespace="Telerik.Sitefinity.Web.UI" %>
<%@ Register Assembly="Telerik.Sitefinity" TagPrefix="sfFields" Namespace="Telerik.Sitefinity.Web.UI.Fields" %>

<sitefinity:ResourceLinks ID="resourcesLinks" runat="server">
    <sitefinity:ResourceFile Name="Styles/Ajax.css" />
</sitefinity:ResourceLinks>
<div id="designerLayoutRoot" class="sfContentViews sfSingleContentView" style="max-height: 400px; overflow: auto; ">
<ol>        
    <li class="sfFormCtrl">
    <asp:Label runat="server" AssociatedControlID="tab" CssClass="sfTxtLbl">Id (1,2,4...)</asp:Label>
    <asp:TextBox ID="tab" runat="server" CssClass="sfTxt" />
    <div class="sfExample"></div>
    </li>
    
    <li class="sfFormCtrl">
    <asp:Label runat="server" AssociatedControlID="contenido" CssClass="sfTxtLbl">Contenido</asp:Label>
    
        <sitefinity:HtmlField ID="contenido" runat="server" ClientIDMode="Static" Width="99%" Height="370px" EditorContentFilters="DefaultFilters" EditorStripFormattingOptions="MSWord,Css,Font,Span,ConvertWordLists" DisplayMode="Write" FixCursorIssue="True"> </sitefinity:HtmlField>
    <div class="sfExample"></div>
    </li>
    
</ol>
</div>
