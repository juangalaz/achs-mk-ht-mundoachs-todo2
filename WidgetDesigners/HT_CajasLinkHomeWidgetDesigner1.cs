using System;
using System.Linq;
using System.Web.UI;
using Telerik.Sitefinity.Web.UI;
using Telerik.Sitefinity.Web.UI.ControlDesign;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Web;
using Telerik.Sitefinity.Localization;
using Telerik.Sitefinity.Modules.Pages;

namespace SitefinityWebApp.WidgetDesigners
{
    /// <summary>
    /// Represents a designer for the <typeparamref name="SitefinityWebApp.Mvc.Controllers.HT_CajasLinkHomeWidgetController"/> widget
    /// </summary>
    public class HT_CajasLinkHomeWidgetDesigner1 : ControlDesignerBase
    {
        #region Properties
        /// <summary>
        /// Obsolete. Use LayoutTemplatePath instead.
        /// </summary>
        protected override string LayoutTemplateName
        {
            get
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the layout template's relative or virtual path.
        /// </summary>
        public override string LayoutTemplatePath
        {
            get
            {
                if (string.IsNullOrEmpty(base.LayoutTemplatePath))
                    return HT_CajasLinkHomeWidgetDesigner1.layoutTemplatePath;
                return base.LayoutTemplatePath;
            }
            set
            {
                base.LayoutTemplatePath = value;
            }
        }

        protected override HtmlTextWriterTag TagKey
        {
            get
            {
                return HtmlTextWriterTag.Div;
            }
        }
        #endregion

        #region Control references
        /// <summary>
        /// Gets the control that is bound to the titulo property
        /// </summary>
        protected virtual Control titulo
        {
            get
            {
                return this.Container.GetControl<Control>("titulo", true);
            }
        }

        /// <summary>
        /// Gets the control that is bound to the link property
        /// </summary>
        protected virtual Control link
        {
            get
            {
                return this.Container.GetControl<Control>("link", true);
            }
        }

        /// <summary>
        /// The LinkButton for selecting imagen.
        /// </summary>
        /// <value>The page selector control.</value>
        protected internal virtual LinkButton SelectButtonimagen
        {
            get
            {
                return this.Container.GetControl<LinkButton>("selectButtonimagen", false);
            }
        }

        /// <summary>
        /// The LinkButton for deselecting imagen.
        /// </summary>
        /// <value>The page selector control.</value>
        protected internal virtual LinkButton DeselectButtonimagen
        {
            get
            {
                return this.Container.GetControl<LinkButton>("deselectButtonimagen", false);
            }
        }

        /// <summary>
        /// Gets the RadEditor Manager dialog for inserting image, document or video for the imagen property.
        /// </summary>
        /// <value>The RadEditor Manager dialog for inserting image, document or video.</value>
        protected EditorContentManagerDialog Selectorimagen
        {
            get
            {
                return this.Container.GetControl<EditorContentManagerDialog>("selectorimagen", false);
            }
        }

        #endregion

        #region Methods
        protected override void InitializeControls(Telerik.Sitefinity.Web.UI.GenericContainer container)
        {
            // Place your initialization logic here
        }
        #endregion

        #region IScriptControl implementation
        /// <summary>
        /// Gets a collection of script descriptors that represent ECMAScript (JavaScript) client components.
        /// </summary>
        public override System.Collections.Generic.IEnumerable<System.Web.UI.ScriptDescriptor> GetScriptDescriptors()
        {
            var scriptDescriptors = new List<ScriptDescriptor>(base.GetScriptDescriptors());
            var descriptor = (ScriptControlDescriptor)scriptDescriptors.Last();

            descriptor.AddElementProperty("titulo", this.titulo.ClientID);
            descriptor.AddElementProperty("link", this.link.ClientID);
            descriptor.AddElementProperty("selectButtonimagen", this.SelectButtonimagen.ClientID);
            descriptor.AddElementProperty("deselectButtonimagen", this.DeselectButtonimagen.ClientID);
            descriptor.AddComponentProperty("selectorimagen", this.Selectorimagen.ClientID);
            descriptor.AddProperty("imageServiceUrl", this.imageServiceUrl);

            return scriptDescriptors;
        }

        /// <summary>
        /// Gets a collection of ScriptReference objects that define script resources that the control requires.
        /// </summary>
        public override System.Collections.Generic.IEnumerable<System.Web.UI.ScriptReference> GetScriptReferences()
        {
            var scripts = new List<ScriptReference>(base.GetScriptReferences());
            scripts.Add(new ScriptReference(HT_CajasLinkHomeWidgetDesigner1.scriptReference));
            return scripts;
        }

        /// <summary>
        /// Gets the required by the control, core library scripts predefined in the <see cref="ScriptRef"/> enum.
        /// </summary>
        protected override ScriptRef GetRequiredCoreScripts()
        {
            return ScriptRef.JQuery | ScriptRef.JQueryUI;
        }
        #endregion

        #region Private members & constants
        public static readonly string layoutTemplatePath = "~/WidgetDesigners/HT_CajasLinkHomeWidgetDesigner1.ascx";
        public const string scriptReference = "~/WidgetDesigners/HT_CajasLinkHomeWidgetDesigner1.js";
        private string imageServiceUrl = VirtualPathUtility.ToAbsolute("~/Sitefinity/Services/Content/ImageService.svc/");
        #endregion
    }
}
 
