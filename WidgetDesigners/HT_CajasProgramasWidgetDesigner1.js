Type.registerNamespace("SitefinityWebApp.WidgetDesigners");

SitefinityWebApp.WidgetDesigners.HT_CajasProgramasWidgetDesigner1 = function (element) {
    /* Initialize texto fields */
    this._texto = null;
    
    /* Initialize link fields */
    this._link = null;
    
    /* Initialize logo fields */
    this._selectButtonlogo = null;
    this._selectButtonlogoClickDelegate = null;
    this._deselectButtonlogo = null;
    this._deselectButtonlogoClickDelegate = null;
    this._selectorlogoCloseDelegate = null;
    this._selectorlogoUploaderViewFileChangedDelegate = null;
    
    this._logoDialog = null;
    this._selectorlogo = null;
    this._logoId = null;
    
    /* Initialize fondo fields */
    this._selectButtonfondo = null;
    this._selectButtonfondoClickDelegate = null;
    this._deselectButtonfondo = null;
    this._deselectButtonfondoClickDelegate = null;
    this._selectorfondoCloseDelegate = null;
    this._selectorfondoUploaderViewFileChangedDelegate = null;
    
    this._fondoDialog = null;
    this._selectorfondo = null;
    this._fondoId = null;
    
    /* Initialize the service url for the image thumbnails */
    this.imageServiceUrl = null;

    /* Calls the base constructor */
    SitefinityWebApp.WidgetDesigners.HT_CajasProgramasWidgetDesigner1.initializeBase(this, [element]);
}

SitefinityWebApp.WidgetDesigners.HT_CajasProgramasWidgetDesigner1.prototype = {
    /* --------------------------------- set up and tear down --------------------------------- */
    initialize: function () {
        /* Here you can attach to events or do other initialization */
        SitefinityWebApp.WidgetDesigners.HT_CajasProgramasWidgetDesigner1.callBaseMethod(this, 'initialize');

        /* Initialize logo */
        this._selectButtonlogoClickDelegate = Function.createDelegate(this, this._selectButtonlogoClicked);
        if (this._selectButtonlogo) {
            $addHandler(this._selectButtonlogo, "click", this._selectButtonlogoClickDelegate);
        }

        this._deselectButtonlogoClickDelegate = Function.createDelegate(this, this._deselectButtonlogoClicked);
        if (this._deselectButtonlogo) {
            $addHandler(this._deselectButtonlogo, "click", this._deselectButtonlogoClickDelegate);
        }

        if (this._selectorlogo) {
            this._logoDialog = jQuery(this._selectorlogo.get_element()).dialog({
                autoOpen: false,
                modal: false,
                width: 655,
                height: "auto",
                closeOnEscape: true,
                resizable: false,
                draggable: false,
                zIndex: 5000,
                close: this._selectorlogoCloseDelegate
            });
        } 

        jQuery("#previewlogo").load(function () {
            dialogBase.resizeToContent();
        });

        this._selectorlogoInsertDelegate = Function.createDelegate(this, this._selectorlogoInsertHandler);
        this._selectorlogo.set_customInsertDelegate(this._selectorlogoInsertDelegate);
        $addHandler(this._selectorlogo._cancelLink, "click", this._selectorlogoCloseHandler);
        this._selectorlogoCloseDelegate = Function.createDelegate(this, this._selectorlogoCloseHandler);
        this._selectorlogoUploaderViewFileChangedDelegate = Function.createDelegate(this, this._selectorlogoUploaderViewFileChangedHandler);

        /* Initialize fondo */
        this._selectButtonfondoClickDelegate = Function.createDelegate(this, this._selectButtonfondoClicked);
        if (this._selectButtonfondo) {
            $addHandler(this._selectButtonfondo, "click", this._selectButtonfondoClickDelegate);
        }

        this._deselectButtonfondoClickDelegate = Function.createDelegate(this, this._deselectButtonfondoClicked);
        if (this._deselectButtonfondo) {
            $addHandler(this._deselectButtonfondo, "click", this._deselectButtonfondoClickDelegate);
        }

        if (this._selectorfondo) {
            this._fondoDialog = jQuery(this._selectorfondo.get_element()).dialog({
                autoOpen: false,
                modal: false,
                width: 655,
                height: "auto",
                closeOnEscape: true,
                resizable: false,
                draggable: false,
                zIndex: 5000,
                close: this._selectorfondoCloseDelegate
            });
        } 

        jQuery("#previewfondo").load(function () {
            dialogBase.resizeToContent();
        });

        this._selectorfondoInsertDelegate = Function.createDelegate(this, this._selectorfondoInsertHandler);
        this._selectorfondo.set_customInsertDelegate(this._selectorfondoInsertDelegate);
        $addHandler(this._selectorfondo._cancelLink, "click", this._selectorfondoCloseHandler);
        this._selectorfondoCloseDelegate = Function.createDelegate(this, this._selectorfondoCloseHandler);
        this._selectorfondoUploaderViewFileChangedDelegate = Function.createDelegate(this, this._selectorfondoUploaderViewFileChangedHandler);
    },
    dispose: function () {
        /* this is the place to unbind/dispose the event handlers created in the initialize method */
        SitefinityWebApp.WidgetDesigners.HT_CajasProgramasWidgetDesigner1.callBaseMethod(this, 'dispose');

        /* Dispose logo */
        if (this._selectButtonlogo) {
            $removeHandler(this._selectButtonlogo, "click", this._selectButtonlogoClickDelegate);
        }
        if (this._selectButtonlogoClickDelegate) {
            delete this._selectButtonlogoClickDelegate;
        }
        
        if (this._deselectButtonlogo) {
            $removeHandler(this._deselectButtonlogo, "click", this._deselectButtonlogoClickDelegate);
        }
        if (this._deselectButtonlogoClickDelegate) {
            delete this._deselectButtonlogoClickDelegate;
        }

        $removeHandler(this._selectorlogo._cancelLink, "click", this._selectorlogoCloseHandler);

        if (this._selectorlogoCloseDelegate) {
            delete this._selectorlogoCloseDelegate;
        }

        if (this._selectorlogoUploaderViewFileChangedDelegate) {
            this._selectorlogo._uploaderView.remove_onFileChanged(this._selectorlogoUploaderViewFileChangedDelegate);
            delete this._selectorlogoUploaderViewFileChangedDelegate;
        }

        /* Dispose fondo */
        if (this._selectButtonfondo) {
            $removeHandler(this._selectButtonfondo, "click", this._selectButtonfondoClickDelegate);
        }
        if (this._selectButtonfondoClickDelegate) {
            delete this._selectButtonfondoClickDelegate;
        }
        
        if (this._deselectButtonfondo) {
            $removeHandler(this._deselectButtonfondo, "click", this._deselectButtonfondoClickDelegate);
        }
        if (this._deselectButtonfondoClickDelegate) {
            delete this._deselectButtonfondoClickDelegate;
        }

        $removeHandler(this._selectorfondo._cancelLink, "click", this._selectorfondoCloseHandler);

        if (this._selectorfondoCloseDelegate) {
            delete this._selectorfondoCloseDelegate;
        }

        if (this._selectorfondoUploaderViewFileChangedDelegate) {
            this._selectorfondo._uploaderView.remove_onFileChanged(this._selectorfondoUploaderViewFileChangedDelegate);
            delete this._selectorfondoUploaderViewFileChangedDelegate;
        }
    },

    /* --------------------------------- public methods ---------------------------------- */

    findElement: function (id) {
        var result = jQuery(this.get_element()).find("#" + id).get(0);
        return result;
    },

    /* Called when the designer window gets opened and here is place to "bind" your designer to the control properties */
    refreshUI: function () {
        var controlData = this._propertyEditor.get_control().Settings; /* JavaScript clone of your control - all the control properties will be properties of the controlData too */

        /* RefreshUI texto */
        jQuery(this.get_texto()).val(controlData.texto);

        /* RefreshUI link */
        jQuery(this.get_link()).val(controlData.link);

        /* RefreshUI logo */
        this.get_selectedlogo().innerHTML = controlData.logo;
        if (controlData.logo && controlData.logo != "00000000-0000-0000-0000-000000000000") {
            this.get_selectButtonlogo().innerHTML = "<span class=\"sfLinkBtnIn\">Change</span>";
            jQuery(this.get_deselectButtonlogo()).show()
            var url = this.imageServiceUrl + controlData.logo + "/?published=true";
            jQuery.ajax({
                url: url,
                type: "GET",
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    jQuery("#previewlogo").show();
                    jQuery("#previewlogo").attr("src", data.Item.ThumbnailUrl);
                    dialogBase.resizeToContent();
                }
            });
        }
        else {
            jQuery(this.get_deselectButtonlogo()).hide()
        }

        /* RefreshUI fondo */
        this.get_selectedfondo().innerHTML = controlData.fondo;
        if (controlData.fondo && controlData.fondo != "00000000-0000-0000-0000-000000000000") {
            this.get_selectButtonfondo().innerHTML = "<span class=\"sfLinkBtnIn\">Change</span>";
            jQuery(this.get_deselectButtonfondo()).show()
            var url = this.imageServiceUrl + controlData.fondo + "/?published=true";
            jQuery.ajax({
                url: url,
                type: "GET",
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    jQuery("#previewfondo").show();
                    jQuery("#previewfondo").attr("src", data.Item.ThumbnailUrl);
                    dialogBase.resizeToContent();
                }
            });
        }
        else {
            jQuery(this.get_deselectButtonfondo()).hide()
        }
    },

    /* Called when the "Save" button is clicked. Here you can transfer the settings from the designer to the control */
    applyChanges: function () {
        var controlData = this._propertyEditor.get_control().Settings;

        /* ApplyChanges texto */
        controlData.texto = jQuery(this.get_texto()).val();

        /* ApplyChanges link */
        controlData.link = jQuery(this.get_link()).val();

        /* ApplyChanges logo */
        controlData.logo = this.get_selectedlogo().innerHTML;

        /* ApplyChanges fondo */
        controlData.fondo = this.get_selectedfondo().innerHTML;
    },

    /* --------------------------------- event handlers ---------------------------------- */

    /* logo event handlers */
    _selectButtonlogoClicked: function (sender, args) {
        this._selectorlogo._uploaderView.add_onFileChanged(this._selectorlogoUploaderViewFileChangedDelegate);
        this._logoDialog.dialog("open");
        jQuery("#designerLayoutRoot").hide();
        this._logoDialog.dialog().parent().css("min-width", "655px");
        dialogBase.resizeToContent();
        try {
            this._selectorlogo.get_uploaderView().get_altTextField().set_value("");
        }
        catch (ex) { }
        jQuery(this._selectorlogo.get_uploaderView().get_settingsPanel()).hide();
        return false;
    },

    _deselectButtonlogoClicked: function (sender, args) {
        jQuery("#previewlogo").hide();
                    jQuery("#previewlogo").attr("src", "");
        this.get_selectedlogo().innerHTML = "00000000-0000-0000-0000-000000000000";
        this.get_selectButtonlogo().innerHTML = "<span class=\"sfLinkBtnIn\">Select...</span>";
        jQuery(this.get_deselectButtonlogo()).hide()
		dialogBase.resizeToContent();
        return false;
    },

    /* fondo event handlers */
    _selectButtonfondoClicked: function (sender, args) {
        this._selectorfondo._uploaderView.add_onFileChanged(this._selectorfondoUploaderViewFileChangedDelegate);
        this._fondoDialog.dialog("open");
        jQuery("#designerLayoutRoot").hide();
        this._fondoDialog.dialog().parent().css("min-width", "655px");
        dialogBase.resizeToContent();
        try {
            this._selectorfondo.get_uploaderView().get_altTextField().set_value("");
        }
        catch (ex) { }
        jQuery(this._selectorfondo.get_uploaderView().get_settingsPanel()).hide();
        return false;
    },

    _deselectButtonfondoClicked: function (sender, args) {
        jQuery("#previewfondo").hide();
                    jQuery("#previewfondo").attr("src", "");
        this.get_selectedfondo().innerHTML = "00000000-0000-0000-0000-000000000000";
        this.get_selectButtonfondo().innerHTML = "<span class=\"sfLinkBtnIn\">Select...</span>";
        jQuery(this.get_deselectButtonfondo()).hide()
		dialogBase.resizeToContent();
        return false;
    },

    /* --------------------------------- private methods --------------------------------- */

    /* logo private methods */
    _selectorlogoInsertHandler: function (selectedItem) {

        if (selectedItem) {
            this._logoId = selectedItem.Id;
            this.get_selectedlogo().innerHTML = this._logoId;
            jQuery(this.get_deselectButtonlogo()).show()
            this.get_selectButtonlogo().innerHTML = "<span class=\"sfLinkBtnIn\">Change</span>";
            jQuery("#previewlogo").show();
                    jQuery("#previewlogo").attr("src", selectedItem.ThumbnailUrl);
        }
        this._logoDialog.dialog("close");
        jQuery("#designerLayoutRoot").show();
        dialogBase.resizeToContent();
    },

    _selectorlogoCloseHandler: function () {
        if(this._logoDialog){
            this._logoDialog.dialog("close");
        }
        jQuery("#designerLayoutRoot").show();
        dialogBase.resizeToContent();
    },

    _selectorlogoUploaderViewFileChangedHandler: function () {
        dialogBase.resizeToContent();
    },

    /* fondo private methods */
    _selectorfondoInsertHandler: function (selectedItem) {

        if (selectedItem) {
            this._fondoId = selectedItem.Id;
            this.get_selectedfondo().innerHTML = this._fondoId;
            jQuery(this.get_deselectButtonfondo()).show()
            this.get_selectButtonfondo().innerHTML = "<span class=\"sfLinkBtnIn\">Change</span>";
            jQuery("#previewfondo").show();
                    jQuery("#previewfondo").attr("src", selectedItem.ThumbnailUrl);
        }
        this._fondoDialog.dialog("close");
        jQuery("#designerLayoutRoot").show();
        dialogBase.resizeToContent();
    },

    _selectorfondoCloseHandler: function () {
        if(this._fondoDialog){
            this._fondoDialog.dialog("close");
        }
        jQuery("#designerLayoutRoot").show();
        dialogBase.resizeToContent();
    },

    _selectorfondoUploaderViewFileChangedHandler: function () {
        dialogBase.resizeToContent();
    },

    /* --------------------------------- properties -------------------------------------- */

    /* texto properties */
    get_texto: function () { return this._texto; }, 
    set_texto: function (value) { this._texto = value; },

    /* link properties */
    get_link: function () { return this._link; }, 
    set_link: function (value) { this._link = value; },

    /* logo properties */
    get_selectorlogo: function () {
        return this._selectorlogo;
    },
    set_selectorlogo: function (value) {
        this._selectorlogo = value;
    },
    get_selectButtonlogo: function () {
        return this._selectButtonlogo;
    },
    set_selectButtonlogo: function (value) {
        this._selectButtonlogo = value;
    },
    get_deselectButtonlogo: function () {
        return this._deselectButtonlogo;
    },
    set_deselectButtonlogo: function (value) {
        this._deselectButtonlogo = value;
    },
    get_selectedlogo: function () {
        if (this._selectedlogo == null) {
            this._selectedlogo = this.findElement("selectedlogo");
        }
        return this._selectedlogo;
    },

    /* fondo properties */
    get_selectorfondo: function () {
        return this._selectorfondo;
    },
    set_selectorfondo: function (value) {
        this._selectorfondo = value;
    },
    get_selectButtonfondo: function () {
        return this._selectButtonfondo;
    },
    set_selectButtonfondo: function (value) {
        this._selectButtonfondo = value;
    },
    get_deselectButtonfondo: function () {
        return this._deselectButtonfondo;
    },
    set_deselectButtonfondo: function (value) {
        this._deselectButtonfondo = value;
    },
    get_selectedfondo: function () {
        if (this._selectedfondo == null) {
            this._selectedfondo = this.findElement("selectedfondo");
        }
        return this._selectedfondo;
    }
}

SitefinityWebApp.WidgetDesigners.HT_CajasProgramasWidgetDesigner1.registerClass('SitefinityWebApp.WidgetDesigners.HT_CajasProgramasWidgetDesigner1', Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesignerBase);
