Type.registerNamespace("SitefinityWebApp.WidgetDesigners");

SitefinityWebApp.WidgetDesigners.CajaDescargasWidgetDesigner1 = function (element) {
    /* Initialize titulo fields */
    this._titulo = null;
    
    /* Initialize documento fields */
    this._selectButtondocumento = null;
    this._selectButtondocumentoClickDelegate = null;
    this._deselectButtondocumento = null;
    this._deselectButtondocumentoClickDelegate = null;
    this._selectordocumentoCloseDelegate = null;
    this._selectordocumentoUploaderViewFileChangedDelegate = null;
    
    this._documentoDialog = null;
    this._selectordocumento = null;
    this._documentoId = null;
    
    /* Initialize imagen fields */
    this._selectButtonimagen = null;
    this._selectButtonimagenClickDelegate = null;
    this._deselectButtonimagen = null;
    this._deselectButtonimagenClickDelegate = null;
    this._selectorimagenCloseDelegate = null;
    this._selectorimagenUploaderViewFileChangedDelegate = null;
    
    this._imagenDialog = null;
    this._selectorimagen = null;
    this._imagenId = null;
    
    /* Initialize the service url for the image thumbnails */
    this.imageServiceUrl = null;

    /* Initialize the service url for the document thumbnails */
    this.documentServiceUrl = null;

    /* Calls the base constructor */
    SitefinityWebApp.WidgetDesigners.CajaDescargasWidgetDesigner1.initializeBase(this, [element]);
}

SitefinityWebApp.WidgetDesigners.CajaDescargasWidgetDesigner1.prototype = {
    /* --------------------------------- set up and tear down --------------------------------- */
    initialize: function () {
        /* Here you can attach to events or do other initialization */
        SitefinityWebApp.WidgetDesigners.CajaDescargasWidgetDesigner1.callBaseMethod(this, 'initialize');

        /* Initialize documento */
        this._selectButtondocumentoClickDelegate = Function.createDelegate(this, this._selectButtondocumentoClicked);
        if (this._selectButtondocumento) {
            $addHandler(this._selectButtondocumento, "click", this._selectButtondocumentoClickDelegate);
        }

        this._deselectButtondocumentoClickDelegate = Function.createDelegate(this, this._deselectButtondocumentoClicked);
        if (this._deselectButtondocumento) {
            $addHandler(this._deselectButtondocumento, "click", this._deselectButtondocumentoClickDelegate);
        }

        if (this._selectordocumento) {
            this._documentoDialog = jQuery(this._selectordocumento.get_element()).dialog({
                autoOpen: false,
                modal: false,
                width: 655,
                height: "auto",
                closeOnEscape: true,
                resizable: false,
                draggable: false,
                zIndex: 5000,
                close: this._selectordocumentoCloseDelegate
            });
        } 

        jQuery("#previewdocumento").load(function () {
            dialogBase.resizeToContent();
        });

        this._selectordocumentoInsertDelegate = Function.createDelegate(this, this._selectordocumentoInsertHandler);
        this._selectordocumento.set_customInsertDelegate(this._selectordocumentoInsertDelegate);
        $addHandler(this._selectordocumento._cancelLink, "click", this._selectordocumentoCloseHandler);
        this._selectordocumentoCloseDelegate = Function.createDelegate(this, this._selectordocumentoCloseHandler);
        this._selectordocumentoUploaderViewFileChangedDelegate = Function.createDelegate(this, this._selectordocumentoUploaderViewFileChangedHandler);

        /* Initialize imagen */
        this._selectButtonimagenClickDelegate = Function.createDelegate(this, this._selectButtonimagenClicked);
        if (this._selectButtonimagen) {
            $addHandler(this._selectButtonimagen, "click", this._selectButtonimagenClickDelegate);
        }

        this._deselectButtonimagenClickDelegate = Function.createDelegate(this, this._deselectButtonimagenClicked);
        if (this._deselectButtonimagen) {
            $addHandler(this._deselectButtonimagen, "click", this._deselectButtonimagenClickDelegate);
        }

        if (this._selectorimagen) {
            this._imagenDialog = jQuery(this._selectorimagen.get_element()).dialog({
                autoOpen: false,
                modal: false,
                width: 655,
                height: "auto",
                closeOnEscape: true,
                resizable: false,
                draggable: false,
                zIndex: 5000,
                close: this._selectorimagenCloseDelegate
            });
        } 

        jQuery("#previewimagen").load(function () {
            dialogBase.resizeToContent();
        });

        this._selectorimagenInsertDelegate = Function.createDelegate(this, this._selectorimagenInsertHandler);
        this._selectorimagen.set_customInsertDelegate(this._selectorimagenInsertDelegate);
        $addHandler(this._selectorimagen._cancelLink, "click", this._selectorimagenCloseHandler);
        this._selectorimagenCloseDelegate = Function.createDelegate(this, this._selectorimagenCloseHandler);
        this._selectorimagenUploaderViewFileChangedDelegate = Function.createDelegate(this, this._selectorimagenUploaderViewFileChangedHandler);
    },
    dispose: function () {
        /* this is the place to unbind/dispose the event handlers created in the initialize method */
        SitefinityWebApp.WidgetDesigners.CajaDescargasWidgetDesigner1.callBaseMethod(this, 'dispose');

        /* Dispose documento */
        if (this._selectButtondocumento) {
            $removeHandler(this._selectButtondocumento, "click", this._selectButtondocumentoClickDelegate);
        }
        if (this._selectButtondocumentoClickDelegate) {
            delete this._selectButtondocumentoClickDelegate;
        }
        
        if (this._deselectButtondocumento) {
            $removeHandler(this._deselectButtondocumento, "click", this._deselectButtondocumentoClickDelegate);
        }
        if (this._deselectButtondocumentoClickDelegate) {
            delete this._deselectButtondocumentoClickDelegate;
        }

        $removeHandler(this._selectordocumento._cancelLink, "click", this._selectordocumentoCloseHandler);

        if (this._selectordocumentoCloseDelegate) {
            delete this._selectordocumentoCloseDelegate;
        }

        if (this._selectordocumentoUploaderViewFileChangedDelegate) {
            this._selectordocumento._uploaderView.remove_onFileChanged(this._selectordocumentoUploaderViewFileChangedDelegate);
            delete this._selectordocumentoUploaderViewFileChangedDelegate;
        }

        /* Dispose imagen */
        if (this._selectButtonimagen) {
            $removeHandler(this._selectButtonimagen, "click", this._selectButtonimagenClickDelegate);
        }
        if (this._selectButtonimagenClickDelegate) {
            delete this._selectButtonimagenClickDelegate;
        }
        
        if (this._deselectButtonimagen) {
            $removeHandler(this._deselectButtonimagen, "click", this._deselectButtonimagenClickDelegate);
        }
        if (this._deselectButtonimagenClickDelegate) {
            delete this._deselectButtonimagenClickDelegate;
        }

        $removeHandler(this._selectorimagen._cancelLink, "click", this._selectorimagenCloseHandler);

        if (this._selectorimagenCloseDelegate) {
            delete this._selectorimagenCloseDelegate;
        }

        if (this._selectorimagenUploaderViewFileChangedDelegate) {
            this._selectorimagen._uploaderView.remove_onFileChanged(this._selectorimagenUploaderViewFileChangedDelegate);
            delete this._selectorimagenUploaderViewFileChangedDelegate;
        }
    },

    /* --------------------------------- public methods ---------------------------------- */

    findElement: function (id) {
        var result = jQuery(this.get_element()).find("#" + id).get(0);
        return result;
    },

    /* Called when the designer window gets opened and here is place to "bind" your designer to the control properties */
    refreshUI: function () {
        var controlData = this._propertyEditor.get_control().Settings; /* JavaScript clone of your control - all the control properties will be properties of the controlData too */

        /* RefreshUI titulo */
        jQuery(this.get_titulo()).val(controlData.titulo);

        /* RefreshUI documento */
        this.get_selecteddocumento().innerHTML = controlData.documento;
        if (controlData.documento && controlData.documento != "00000000-0000-0000-0000-000000000000") {
            this.get_selectButtondocumento().innerHTML = "<span class=\"sfLinkBtnIn\">Change</span>";
            jQuery(this.get_deselectButtondocumento()).show()
            var url = this.documentServiceUrl + controlData.documento + "/?published=true";
            jQuery.ajax({
                url: url,
                type: "GET",
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    var cssClass = "sf" + data.Item.Extension.substring(1);
                    jQuery("#previewdocumento").removeClass().addClass(cssClass).text(data.Item.Title.Value);
                    dialogBase.resizeToContent();
                }
            });
        }
        else {
            jQuery(this.get_deselectButtondocumento()).hide()
        }

        /* RefreshUI imagen */
        this.get_selectedimagen().innerHTML = controlData.imagen;
        if (controlData.imagen && controlData.imagen != "00000000-0000-0000-0000-000000000000") {
            this.get_selectButtonimagen().innerHTML = "<span class=\"sfLinkBtnIn\">Change</span>";
            jQuery(this.get_deselectButtonimagen()).show()
            var url = this.imageServiceUrl + controlData.imagen + "/?published=true";
            jQuery.ajax({
                url: url,
                type: "GET",
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    jQuery("#previewimagen").show();
                    jQuery("#previewimagen").attr("src", data.Item.ThumbnailUrl);
                    dialogBase.resizeToContent();
                }
            });
        }
        else {
            jQuery(this.get_deselectButtonimagen()).hide()
        }
    },

    /* Called when the "Save" button is clicked. Here you can transfer the settings from the designer to the control */
    applyChanges: function () {
        var controlData = this._propertyEditor.get_control().Settings;

        /* ApplyChanges titulo */
        controlData.titulo = jQuery(this.get_titulo()).val();

        /* ApplyChanges documento */
        controlData.documento = this.get_selecteddocumento().innerHTML;

        /* ApplyChanges imagen */
        controlData.imagen = this.get_selectedimagen().innerHTML;
    },

    /* --------------------------------- event handlers ---------------------------------- */

    /* documento event handlers */
    _selectButtondocumentoClicked: function (sender, args) {
        this._selectordocumento._uploaderView.add_onFileChanged(this._selectordocumentoUploaderViewFileChangedDelegate);
        this._documentoDialog.dialog("open");
        jQuery("#designerLayoutRoot").hide();
        this._documentoDialog.dialog().parent().css("min-width", "655px");
        dialogBase.resizeToContent();
        try {
            this._selectordocumento.get_uploaderView().get_altTextField().set_value("");
        }
        catch (ex) { }
        jQuery(this._selectordocumento.get_uploaderView().get_settingsPanel()).hide();
        return false;
    },

    _deselectButtondocumentoClicked: function (sender, args) {
        jQuery("#previewdocumento").removeClass().attr("src", "").text("");
        this.get_selecteddocumento().innerHTML = "00000000-0000-0000-0000-000000000000";
        this.get_selectButtondocumento().innerHTML = "<span class=\"sfLinkBtnIn\">Select...</span>";
        jQuery(this.get_deselectButtondocumento()).hide()
		dialogBase.resizeToContent();
        return false;
    },

    /* imagen event handlers */
    _selectButtonimagenClicked: function (sender, args) {
        this._selectorimagen._uploaderView.add_onFileChanged(this._selectorimagenUploaderViewFileChangedDelegate);
        this._imagenDialog.dialog("open");
        jQuery("#designerLayoutRoot").hide();
        this._imagenDialog.dialog().parent().css("min-width", "655px");
        dialogBase.resizeToContent();
        try {
            this._selectorimagen.get_uploaderView().get_altTextField().set_value("");
        }
        catch (ex) { }
        jQuery(this._selectorimagen.get_uploaderView().get_settingsPanel()).hide();
        return false;
    },

    _deselectButtonimagenClicked: function (sender, args) {
        jQuery("#previewimagen").hide();
                    jQuery("#previewimagen").attr("src", "");
        this.get_selectedimagen().innerHTML = "00000000-0000-0000-0000-000000000000";
        this.get_selectButtonimagen().innerHTML = "<span class=\"sfLinkBtnIn\">Select...</span>";
        jQuery(this.get_deselectButtonimagen()).hide()
		dialogBase.resizeToContent();
        return false;
    },

    /* --------------------------------- private methods --------------------------------- */

    /* documento private methods */
    _selectordocumentoInsertHandler: function (selectedItem) {

        if (selectedItem) {
            this._documentoId = selectedItem.Id;
            this.get_selecteddocumento().innerHTML = this._documentoId;
            jQuery(this.get_deselectButtondocumento()).show()
            this.get_selectButtondocumento().innerHTML = "<span class=\"sfLinkBtnIn\">Change</span>";
            jQuery("#previewdocumento").removeClass().addClass("sf" + selectedItem.Extension.substring(1)).text(selectedItem.Title);
        }
        this._documentoDialog.dialog("close");
        jQuery("#designerLayoutRoot").show();
        dialogBase.resizeToContent();
    },

    _selectordocumentoCloseHandler: function () {
        if(this._documentoDialog){
            this._documentoDialog.dialog("close");
        }
        jQuery("#designerLayoutRoot").show();
        dialogBase.resizeToContent();
    },

    _selectordocumentoUploaderViewFileChangedHandler: function () {
        dialogBase.resizeToContent();
    },

    /* imagen private methods */
    _selectorimagenInsertHandler: function (selectedItem) {

        if (selectedItem) {
            this._imagenId = selectedItem.Id;
            this.get_selectedimagen().innerHTML = this._imagenId;
            jQuery(this.get_deselectButtonimagen()).show()
            this.get_selectButtonimagen().innerHTML = "<span class=\"sfLinkBtnIn\">Change</span>";
            jQuery("#previewimagen").show();
                    jQuery("#previewimagen").attr("src", selectedItem.ThumbnailUrl);
        }
        this._imagenDialog.dialog("close");
        jQuery("#designerLayoutRoot").show();
        dialogBase.resizeToContent();
    },

    _selectorimagenCloseHandler: function () {
        if(this._imagenDialog){
            this._imagenDialog.dialog("close");
        }
        jQuery("#designerLayoutRoot").show();
        dialogBase.resizeToContent();
    },

    _selectorimagenUploaderViewFileChangedHandler: function () {
        dialogBase.resizeToContent();
    },

    /* --------------------------------- properties -------------------------------------- */

    /* titulo properties */
    get_titulo: function () { return this._titulo; }, 
    set_titulo: function (value) { this._titulo = value; },

    /* documento properties */
    get_selectordocumento: function () {
        return this._selectordocumento;
    },
    set_selectordocumento: function (value) {
        this._selectordocumento = value;
    },
    get_selectButtondocumento: function () {
        return this._selectButtondocumento;
    },
    set_selectButtondocumento: function (value) {
        this._selectButtondocumento = value;
    },
    get_deselectButtondocumento: function () {
        return this._deselectButtondocumento;
    },
    set_deselectButtondocumento: function (value) {
        this._deselectButtondocumento = value;
    },
    get_selecteddocumento: function () {
        if (this._selecteddocumento == null) {
            this._selecteddocumento = this.findElement("selecteddocumento");
        }
        return this._selecteddocumento;
    },

    /* imagen properties */
    get_selectorimagen: function () {
        return this._selectorimagen;
    },
    set_selectorimagen: function (value) {
        this._selectorimagen = value;
    },
    get_selectButtonimagen: function () {
        return this._selectButtonimagen;
    },
    set_selectButtonimagen: function (value) {
        this._selectButtonimagen = value;
    },
    get_deselectButtonimagen: function () {
        return this._deselectButtonimagen;
    },
    set_deselectButtonimagen: function (value) {
        this._deselectButtonimagen = value;
    },
    get_selectedimagen: function () {
        if (this._selectedimagen == null) {
            this._selectedimagen = this.findElement("selectedimagen");
        }
        return this._selectedimagen;
    }
}

SitefinityWebApp.WidgetDesigners.CajaDescargasWidgetDesigner1.registerClass('SitefinityWebApp.WidgetDesigners.CajaDescargasWidgetDesigner1', Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesignerBase);
