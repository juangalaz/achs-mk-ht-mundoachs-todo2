﻿using IdentityModel.Client;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

namespace SitefinityWebApp.Helper
{
    public class MetodosApi
    {
        //https://www.progress.com/documentation/sitefinity-cms/request-access-token-for-calling-web-services
        //https://www.progress.com/documentation/sitefinity-cms/example-filter-dynamic-content-items-by-dynamic-field
        //https://www.progress.com/documentation/sitefinity-cms/request-access-token-for-calling-web-services
        //https://app.quicktype.io/?l=csharp

        // GET: Home

        private static TokenClient tokenClient;
       
        public static TokenResponse RequestToken()
        {
            tokenClient = new TokenClient(TokenEndpoint, ClientId, ClientSecret, AuthenticationStyle.PostValues);
            //This is call to the token endpoint with the parameters that are set
            TokenResponse tokenResponse = tokenClient.RequestResourceOwnerPasswordAsync(Username, Password, Scopes, AdditionalParameters).Result;

            if (tokenResponse.IsError)
            {
                throw new ApplicationException("Couldn't get access token. Error: " + tokenResponse.Error);
            }

            return tokenResponse;
        }

        public static string CallApi(string WebApiEndPoint)
        {

            Uri uri = new Uri(WebApiEndPoint);

            tokenClient = new TokenClient(uri.Scheme + ":" + TokenEndpoint, ClientId, ClientSecret, AuthenticationStyle.PostValues);
            //This is call to the token endpoint with the parameters that are set
            TokenResponse tokenResponse = tokenClient.RequestResourceOwnerPasswordAsync(Username, Password, Scopes, AdditionalParameters).Result;
            if (tokenResponse.IsError)
            {
                throw new ApplicationException("Couldn't get access token. Error: " + tokenResponse.Error);
            }
            string accessToken = tokenResponse.AccessToken;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(WebApiEndPoint);
            request.ContentType = "application/json";
            request.Method = "GET";
            request.Headers.Add("Authorization", "Bearer " + accessToken);

            string html = string.Empty;
            WebResponse response = request.GetResponse();
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                html = reader.ReadToEnd();
            }

            return html;
        }


        public static string AddApi(string WebApiEndPoint)
        {

            try
            {
                var client = new RestClient(WebApiEndPoint);
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("Authorization", "{{token}}");
                request.AddHeader("x-sf-service-request", "true");
                request.AddParameter("application/json", "{\r\n  \"ExpirationDate\": \"2016-04-15T07:57:36.671168Z\",\r\n  \"UrlName\": \"software-engineer\",\r\n  \"Title\": \"Software Engineer\",\r\n  \"Summary\": \"Develops information systems by designing, developing, and installing software solutions.\",\r\n  \"JobDescription\": \"Analyzing Information , General Programming Skills, Software Design, Software Debugging, Software Documentation, Software Testing, Problem Solving, Teamwork, Software Development Fundamentals, Software Development Process, Software Requirements\",\r\n  \"Parent\": { \"@odata.id\":\"http://servicessandbox.cloudapp.net/api/default/offices(689cd9da-0cb8-4cde-9bde-1bfd6ce4244b)\" } \r\n}", ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                return response.Content;
            } catch(Exception ex)
            {
                return ex.ToString();
            }

        }



        public static T CallApiObject<T>(string WebApiEndPoint)
        {

            Uri uri = new Uri(WebApiEndPoint);
            
            tokenClient = new TokenClient(uri.Scheme + ":" + TokenEndpoint, ClientId, ClientSecret, AuthenticationStyle.PostValues);
            //This is call to the token endpoint with the parameters that are set
            TokenResponse tokenResponse = tokenClient.RequestResourceOwnerPasswordAsync(Username, Password, Scopes, AdditionalParameters).Result;
            if (tokenResponse.IsError)
            {
                throw new ApplicationException("Couldn't get access token. Error: " + tokenResponse.Error);
            }
            string accessToken = tokenResponse.AccessToken;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(WebApiEndPoint);
            request.ContentType = "application/json";
            request.Method = "GET";
            request.Headers.Add("Authorization", "Bearer " + accessToken);

            string html = string.Empty;
            WebResponse response = request.GetResponse();
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                html = reader.ReadToEnd();
            }
            var json = JsonConvert.DeserializeObject<T>(html);
            return json;
        }
        public static T CallApiPublic<T>(string WebApiEndPoint)
        {

            Uri uri = new Uri(WebApiEndPoint);

            //tokenClient = new TokenClient(uri.Scheme + ":" + TokenEndpoint, ClientId, ClientSecret, AuthenticationStyle.PostValues);

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(WebApiEndPoint);
            request.ContentType = "application/json";
            request.Method = "GET";

            string html = string.Empty;
            WebResponse response = request.GetResponse();
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                html = reader.ReadToEnd();
            }
            var json = JsonConvert.DeserializeObject<T>(html);
            return json;
        }

        public static TokenResponse RefreshToken(string refreshToken)
        {
            tokenClient = new TokenClient(TokenEndpoint, ClientId, ClientSecret, AuthenticationStyle.PostValues);
            //This is call to the token endpoint that can retrieve new access and refresh token from the current refresh token
            return tokenClient.RequestRefreshTokenAsync(refreshToken).Result;
        }
   
        public  static string ClientId = Common.GetWebConfigKey("ClientId");
        public static string ClientSecret = Common.GetWebConfigKey("ClientSecret");
        public static string TokenEndpoint = Common.GetWebConfigKey("TokenEndpoint");
        public static string Username = Common.GetWebConfigKey("Username");
        public static string Password = Common.GetWebConfigKey("Password");
        public static string Scopes = Common.GetWebConfigKey("Scopes");
        public static readonly Dictionary<string, string> AdditionalParameters = new Dictionary<string, string>()
        {
            { "membershipProvider",  Common.GetWebConfigKey("membershipProvider") }
        };

       
    }
}