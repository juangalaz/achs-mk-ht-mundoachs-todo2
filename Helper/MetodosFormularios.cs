﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Telerik.Sitefinity.Data;
using Telerik.Sitefinity.Data.ContentLinks;
using Telerik.Sitefinity.Forms.Model;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Libraries.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Model.ContentLinks;
using Telerik.Sitefinity.Modules.Forms;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Services;

namespace SitefinityWebApp.Helper
{
    public class MetodosFormularios
    {

        public static string insertar_formulario(string nombre_formulario, Dictionary<string, string> dict, string IpAddress, HttpPostedFileBase file, string campo, string libreria)
        {
            try
            {

                var manager = FormsManager.GetManager();
                var form = manager.GetFormByName(nombre_formulario);

                FormEntry entry = manager.CreateFormEntry(form.EntriesTypeName);
                foreach (var item in dict)
                {
                    entry.SetValue(item.Key, item.Value);
                }

                if (file != null)
                {
                    ContentLink[] sitefinity_file = upload_document(entry, file, libreria);
                    entry.SetValue(campo, sitefinity_file);
                    ContentLink sitefinity_file0 = sitefinity_file[0];
                    string x = sitefinity_file[0].ToString();
                }

                Guid g = new Guid("00fe59fc-25a7-4cfa-8500-97c4cd50a18d");
                entry.UserId = Telerik.Sitefinity.Security.Claims.ClaimsManager.GetCurrentUserId();
                entry.Language = System.Globalization.CultureInfo.CurrentUICulture.Name;
                entry.IpAddress = IpAddress;
                if (SystemManager.CurrentContext.AppSettings.Multilingual)
                {
                    entry.Language = CultureInfo.CurrentUICulture.Name;
                }
                entry.ReferralCode = manager.Provider.GetNextReferralCode(entry.ToString()).ToString();
                entry.SubmittedOn = System.DateTime.UtcNow;

                manager.SaveChanges();
                return "1";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }


        private static ContentLink[] upload_document(FormEntry entry, HttpPostedFileBase newFile, string libreria)
        {

            var contentLinksManager = ContentLinksManager.GetManager();
            LibrariesManager libraryManager = LibrariesManager.GetManager();
            //DocumentLibrary documentLibrary = libraryManager.GetDocumentLibraries().SingleOrDefault();
            DocumentLibrary documentLibrary = libraryManager.GetDocumentLibraries().Where(x => x.Title == libreria).SingleOrDefault();

            string fecha = DateTime.Now.ToString("yyyyMMddHHmmss");

            string fileExtension = Path.GetExtension(newFile.FileName);
            string fileName = Path.GetFileName(newFile.FileName);
            fileName = fileName + fecha;
            string urlName = get_unique_slug_url(documentLibrary, fileName);

            Document document = libraryManager.CreateDocument();
            document.Author = string.Empty;
            document.DateCreated = DateTime.UtcNow;
            document.Owner = Guid.Empty;
            document.Parent = documentLibrary;
            document.Title = generar_slug(urlName);
            document.UrlName = generar_slug(urlName);

            libraryManager.Upload(document, newFile.InputStream, fileExtension);
            libraryManager.RecompileItemUrls(document);
            libraryManager.Lifecycle.Publish(document);
            document.ApprovalWorkflowState = "Published";
            libraryManager.SaveChanges();


            ContentLink contentLink = new ContentLink();
            contentLink.Id = Guid.NewGuid();
            contentLink.ParentItemId = entry.Id;
            contentLink.ParentItemType = entry.GetType().ToString();
            contentLink.ChildItemId = document.Id;
            contentLink.ComponentPropertyName = entry.Id.ToString();
            contentLink.ChildItemAdditionalInfo = "additional info";
            contentLink.ChildItemProviderName = (((IDataItem)document).Provider as DataProviderBase).Name;
            contentLink.ChildItemType = document.GetType().FullName;
            contentLink.ApplicationName = contentLinksManager.Provider.ApplicationName;

            ContentLink[] contentLinks = new ContentLink[0];
            if (contentLinks == null)
            {
                contentLinks = new ContentLink[0];
            }

            var assetsFieldList = contentLinks.ToList();
            assetsFieldList.Insert(0, contentLink);
            return assetsFieldList.ToArray();
        }

        public static string insertar_formulario_return_id(string nombre_formulario, Dictionary<string, string> dict, string IpAddress, HttpPostedFileBase file, string campo, string libreria)
        {
            try
            {
                var manager = FormsManager.GetManager();
                var form = manager.GetFormByName(nombre_formulario);
                FormEntry entry = manager.CreateFormEntry(form.EntriesTypeName);
                foreach (var item in dict)
                {
                    entry.SetValue(item.Key, item.Value);
                }

                if (file != null)
                {
                    ContentLink[] sitefinity_file = upload_document(entry, file, libreria);
                    entry.SetValue(campo, sitefinity_file);
                    ContentLink sitefinity_file0 = sitefinity_file[0];
                    string x = sitefinity_file[0].ToString();
                }

                Guid g = new Guid("00fe59fc-25a7-4cfa-8500-97c4cd50a18d");
                entry.UserId = Telerik.Sitefinity.Security.Claims.ClaimsManager.GetCurrentUserId();
                entry.Language = System.Globalization.CultureInfo.CurrentUICulture.Name;
                entry.IpAddress = IpAddress;
                if (SystemManager.CurrentContext.AppSettings.Multilingual)
                {
                    entry.Language = CultureInfo.CurrentUICulture.Name;
                }
                //form.FormEntriesSeed++;
                //entry.ReferralCode = form.FormEntriesSeed.ToString();
                entry.ReferralCode = manager.Provider.GetNextReferralCode(entry.ToString()).ToString();
                entry.SubmittedOn = System.DateTime.UtcNow;

                manager.SaveChanges();

                return entry.ReferralCode;
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }




        private static string generar_slug(string phrase)
        {
            string str = remover_acentos(phrase).ToLower();
            // invalid chars           
            str = Regex.Replace(str, @"[^a-z0-9\s-]", "");
            // convert multiple spaces into one space   
            str = Regex.Replace(str, @"\s+", " ").Trim();
            // cut and trim 
            str = str.Substring(0, str.Length <= 45 ? str.Length : 45).Trim();
            str = Regex.Replace(str, @"\s", "-"); // hyphens   
            return str;
        }

        private static string remover_acentos(string txt)
        {
            byte[] bytes = System.Text.Encoding.GetEncoding("Cyrillic").GetBytes(txt);
            return System.Text.Encoding.ASCII.GetString(bytes);
        }

        private static string get_unique_slug_url(DocumentLibrary documentLibrary, string fileName)
        {
            List<string> existingUrlNames = documentLibrary.ChildContentItems
                .Where(c => c.Status == ContentLifecycleStatus.Live && c.Visible)
                .Select(c => c.UrlName.ToString()).ToList();
            string safeTitle = fileName;
            while (existingUrlNames.Contains(safeTitle))
            {
                safeTitle += "-";
            }
            return safeTitle;
        }
    }
}