﻿using SitefinityWebApp.Mvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SitefinityWebApp.Helper
{

    


    public class MetodosEstadistica
    {
        public List<grafico_empresa> getNEmpresas()
        {
            List<grafico_empresa> Gempresas = new List<grafico_empresa>();
            using (var db = new DataClassesAchsEstadisticaDataContext())
            {
                Gempresas = db.grafico_empresas.ToList();
            }
            return Gempresas;
        }

        public List<grafico_evolucion_tasa> getEvolucionTasaReposos()
        {
            List<grafico_evolucion_tasa> evotasa = new List<grafico_evolucion_tasa>();
            using (var db = new DataClassesAchsEstadisticaDataContext())
            {
                evotasa = db.grafico_evolucion_tasas.ToList();
            }
            return evotasa;
        }

        public List<grafico_tasas_sector> getTasaSector()
        {
            List<grafico_tasas_sector> tasasector = new List<grafico_tasas_sector>();
            using (var db = new DataClassesAchsEstadisticaDataContext())
            {
                tasasector = db.grafico_tasas_sectors.OrderBy(w => w.orden).ToList();
            }
            return tasasector;
        }

        public List<grafico_participacion> getParticipacion()
        {
            List<grafico_participacion> participacion = new List<grafico_participacion>();
            using (var db = new DataClassesAchsEstadisticaDataContext())
            {
                participacion = db.grafico_participacions.ToList();
            }
            return participacion;
        }

        public List<graficos_hombre_mujer> getHombreMujer()
        {
            List<graficos_hombre_mujer> hm = new List<graficos_hombre_mujer>();
            using (var db = new DataClassesAchsEstadisticaDataContext())
            {
                hm = db.graficos_hombre_mujers.OrderBy(w => w.orden).ToList();
            }
            return hm;
        }
        public List<listasubsidio> gestListadoSubsidios()
        {
            List<listasubsidio> ls = new List<listasubsidio>();
            using (var db = new DataClassesAchsEstadisticaDataContext())
            {
                ls = db.listasubsidios.OrderBy(w => w.orden).ToList();
            }
            return ls;
        }

        public List<listapensione> getListadoPensiones()
        {
            List<listapensione> lp = new List<listapensione>();
            using (var db = new DataClassesAchsEstadisticaDataContext())
            {
                lp = db.listapensiones.ToList();
            }
            return lp;
        }

        public List<listaindemnizacione> getListadoIndemnizaciones()
        {
            List<listaindemnizacione> li = new List<listaindemnizacione>();
            using (var db = new DataClassesAchsEstadisticaDataContext())
            {
                li = db.listaindemnizaciones.OrderBy(w => w.orden).ToList();
            }
            return li;
        }

        public List<estadistica_grafico_conf_titulo> getGraficoTitulos()
        {
            List<estadistica_grafico_conf_titulo> lgt = new List<estadistica_grafico_conf_titulo>();
            using (var db = new DataClassesAchsEstadisticaDataContext())
            {
                lgt = db.estadistica_grafico_conf_titulos.ToList();
            }
            return lgt;
        }
    }
}