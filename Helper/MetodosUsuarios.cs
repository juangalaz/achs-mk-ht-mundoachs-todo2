﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using Telerik.Sitefinity.Data;
using Telerik.Sitefinity.Security;
using Telerik.Sitefinity.Security.Model;

namespace SitefinityWebApp.Helper
{
    public class MetodosUsuarios
    {
        public void WorkWithUsers()
        {
            UserManager userManager = UserManager.GetManager();
            UserProfileManager profileManager = UserProfileManager.GetManager();

            //custom logic goes here

            userManager.SaveChanges();
            profileManager.SaveChanges();
        }



        public static User GetUser(Guid userId)
        {
            UserManager userManager = UserManager.GetManager();

            User user = userManager.GetUser(userId);

            return user;
        }



        public static List<User> GetAllUsers()
        {
            UserManager userManager = UserManager.GetManager("external");

            List<User> users = userManager.GetUsers().ToList();



            return users;
        }

        public static List<UsuarioCompleto> GetAllUsers2()
        {
            UserManager userManager = UserManager.GetManager("external");

            List<User> users = userManager.GetUsers().ToList();

            List<UsuarioCompleto> listaUsuarios = new List<UsuarioCompleto>();

            foreach (var item in users)
            {
                UsuarioCompleto usuario = new UsuarioCompleto();

                usuario.id = item.Id;
                string userId = item.Id.ToString();
                usuario.username = item.UserName;
                usuario.aprobado = item.IsApproved;
                usuario.nombre = GetUserProfileByUserId(userId).FirstName + " " + GetUserProfileByUserId(userId).LastName;
                usuario.correo = item.Email;
                usuario.fecha = item.CreationDate.ToString().Substring(0, 10);
                usuario.roles = GetRolesForUser(item.UserName);
                listaUsuarios.Add(usuario);
            }


            return listaUsuarios;
        }

        public static SitefinityProfile GetUserProfileByUserId(string id)
        {
            UserProfileManager profileManager = UserProfileManager.GetManager();
            UserManager userManager = UserManager.GetManager("external");



            User user = userManager.GetUser(Guid.Parse(id));

            SitefinityProfile profile = null;

            if (user != null)
            {
                profile = profileManager.GetUserProfile<SitefinityProfile>(user);
            }

            return profile;
        }

        public static int AddUserToRoles(string userName, List<string> rolesToAdd)
        {

            int resp = 0;
            UserManager userManager = UserManager.GetManager("external");
            RoleManager roleManager = RoleManager.GetManager("external");
            UserProfileManager profileManager = UserProfileManager.GetManager();


            userManager.Provider.SuppressSecurityChecks = true;
            roleManager.Provider.SuppressSecurityChecks = true;
            profileManager.Provider.SuppressSecurityChecks = true;


            roleManager.Provider.SuppressSecurityChecks = true;

            if (userManager.UserExists(userName))
            {
                List<string> listaborrar = GetAllRolesString();
                RemoveUserFromRoles(userName, listaborrar);

                User user = userManager.GetUser(userName);

                foreach (var roleName in rolesToAdd)
                {
                    if (roleManager.RoleExists(roleName))
                    {
                        Role role = roleManager.GetRole(roleName);
                        roleManager.AddUserToRole(user, role);
                        resp = 1;
                    }
                }
            }

            roleManager.SaveChanges();
            roleManager.Provider.SuppressSecurityChecks = false;
            userManager.Provider.SuppressSecurityChecks = false;
            profileManager.Provider.SuppressSecurityChecks = false;
            return resp;


        }



        public static List<rolUsuario> GetRolesForUser(string userName)
        {
            List<rolUsuario> rolesusuario = new List<rolUsuario>();
            List<Role> roles = new List<Role>();

            RoleManager roleManager = RoleManager.GetManager("external");
            UserManager userManager = UserManager.GetManager("external");

            bool userExists = userManager.UserExists(userName);

            if (userExists)
            {


                User user = userManager.GetUser(userName);
                roles = roleManager.GetRolesForUser(user.Id).ToList();

                foreach (var item in roles)
                {
                    rolUsuario rol = new rolUsuario();
                    rol.nombrerol = item.Name;
                    rolesusuario.Add(rol);
                }
            }

            return rolesusuario;
        }

        public class rolUsuario
        {
            public string nombrerol { get; set; }

        }


        public class UsuarioCompleto
        {
            public Guid id { get; set; }
            public string username { get; set; }
            public string nombre { get; set; }
            public string correo { get; set; }
            public bool aprobado { get; set; }
            public string fecha { get; set; }
            public List<rolUsuario> roles { get; set; }

        }

        public static List<string> GetAllRolesString()
        {
            RoleManager roleManager = RoleManager.GetManager("external");

            List<string> lista = new List<string>();

            foreach (var item in roleManager.GetRoles().ToList())
            {
                lista.Add(item.Name);
            }
            return lista;
        }

        public static List<Role> GetAllRoles()
        {
            RoleManager roleManager = RoleManager.GetManager("external");

            return roleManager.GetRoles().ToList();
        }

        public static void ValidarUsuario(string username)
        {
            UserManager userManager = UserManager.GetManager("external");
            UserProfileManager profileManager = UserProfileManager.GetManager();

            User user = userManager.GetUsers().Where(u => u.UserName == username).SingleOrDefault();

            if (user != null)
            {
                user.IsApproved = true;

                userManager.SaveChanges();
            }
        }
        public static MembershipCreateStatus CreateUser(string username, string password, string firstName, string lastName, string mail, string secretQuestion, string secretAnswer, bool isApproved)
        {

            UserManager userManager = UserManager.GetManager("external");
            UserProfileManager profileManager = UserProfileManager.GetManager();
            RoleManager roleManager = RoleManager.GetManager();


            userManager.Provider.SuppressSecurityChecks = true;
            roleManager.Provider.SuppressSecurityChecks = true;
            profileManager.Provider.SuppressSecurityChecks = true;

            System.Web.Security.MembershipCreateStatus status;

            User user = userManager.CreateUser(username, password, mail, secretQuestion, secretAnswer, isApproved, null, out status);

            if (status == MembershipCreateStatus.Success)
            {
                SitefinityProfile sfProfile = profileManager.CreateProfile(user, Guid.NewGuid(), typeof(SitefinityProfile)) as SitefinityProfile;

                if (sfProfile != null)
                {
                    sfProfile.FirstName = firstName;
                    sfProfile.LastName = lastName;
                }
                StringBuilder body = new StringBuilder();
                body.Append("<span style='font-family:Courier New;font-size:13px'> Estimado ");
                body.Append(firstName + " " + lastName);
                body.Append(":<br /><br />");
                body.Append("Su cuenta se ha creado, el administrador activará su cuenta lo antes posible.<br />");
                body.Append("<br />");
                string mails = "juan@lafabricaimaginaria.cl," + mail;
                Common.envio_correo(mail, "Bienvenido a Marketing ACHS", body, "Admin Marketing Achs");


                userManager.SaveChanges();


                profileManager.RecompileItemUrls(sfProfile);
                profileManager.SaveChanges();

                roleManager.Provider.SuppressSecurityChecks = false;
                userManager.Provider.SuppressSecurityChecks = false;
                profileManager.Provider.SuppressSecurityChecks = false;
            }

            return status;



        }


        public static int DesactivarUsuario(string username)
        {
            int resp = 0;
            UserManager userManager = UserManager.GetManager("external");
            UserProfileManager profileManager = UserProfileManager.GetManager();

            User user = userManager.GetUsers().Where(u => u.UserName == username).SingleOrDefault();

            if (user != null)
            {
                List<string> listaborrar = GetAllRolesString();
                RemoveUserFromRoles(username, listaborrar);

                user.IsApproved = false;
                resp = 1;
                userManager.SaveChanges();
            }
            return resp;
        }

        public static void RemoveUserFromRoles(string userName, List<string> rolesToRemove)
        {
            UserManager userManager = UserManager.GetManager("external");
            RoleManager roleManager = RoleManager.GetManager("external");
            roleManager.Provider.SuppressSecurityChecks = true;

            if (userManager.UserExists(userName))
            {
                User user = userManager.GetUser(userName);

                foreach (var roleName in rolesToRemove)
                {
                    if (roleManager.RoleExists(roleName))
                    {
                        Role role = roleManager.GetRole(roleName);
                        roleManager.RemoveUserFromRole(user, role);
                    }
                }
            }

            roleManager.SaveChanges();
            roleManager.Provider.SuppressSecurityChecks = false;
        }
    }
}




//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Web;
//using System.Web.Security;
//using Telerik.Sitefinity.Data;
//using Telerik.Sitefinity.Security;
//using Telerik.Sitefinity.Security.Model;

//namespace SitefinityWebApp.Helper
//{
//    public class MetodosUsuarios
//    {
//        public void WorkWithUsers()
//        {
//            UserManager userManager = UserManager.GetManager();
//            UserProfileManager profileManager = UserProfileManager.GetManager();

//            //custom logic goes here

//            userManager.SaveChanges();
//            profileManager.SaveChanges();
//        }



//        public static User GetUser(Guid userId)
//        {
//            UserManager userManager = UserManager.GetManager();

//            User user = userManager.GetUser(userId);

//            return user;
//        }



//        public static List<User> GetAllUsers()
//        {
//            UserManager userManager = UserManager.GetManager("external");

//            List<User> users = userManager.GetUsers().ToList();



//            return users;
//        }

//        public static List<UsuarioCompleto> GetAllUsers2()
//        {
//            UserManager userManager = UserManager.GetManager("external");

//            List<User> users = userManager.GetUsers().ToList();

//            List<UsuarioCompleto> listaUsuarios = new List<UsuarioCompleto>();

//            foreach (var item in users)
//            {
//                UsuarioCompleto usuario = new UsuarioCompleto();

//                usuario.id = item.Id;
//                string userId = item.Id.ToString();
//                usuario.username = item.UserName;
//                usuario.aprobado = item.IsApproved;
//                usuario.nombre = GetUserProfileByUserId(userId).FirstName+" " + GetUserProfileByUserId(userId).LastName;
//                usuario.correo = item.Email;
//                usuario.fecha = item.CreationDate.ToString().Substring(0,10);
//                usuario.roles = GetRolesForUser(item.UserName);
//                listaUsuarios.Add(usuario);
//            }


//            return listaUsuarios;
//        }

//        public static SitefinityProfile GetUserProfileByUserId(string id)
//        {
//            UserProfileManager profileManager = UserProfileManager.GetManager();
//            UserManager userManager = UserManager.GetManager("external");



//            User user = userManager.GetUser(Guid.Parse(id));

//            SitefinityProfile profile = null;

//            if (user != null)
//            {
//                profile = profileManager.GetUserProfile<SitefinityProfile>(user);
//            }

//            return profile;
//        }

//        public static int AddUserToRoles(string userName, List<string> rolesToAdd)
//        {

//            int resp = 0;
//            UserManager userManager = UserManager.GetManager("external");
//            RoleManager roleManager = RoleManager.GetManager("external");
//            UserProfileManager profileManager = UserProfileManager.GetManager();


//            userManager.Provider.SuppressSecurityChecks = true;
//            roleManager.Provider.SuppressSecurityChecks = true;
//            profileManager.Provider.SuppressSecurityChecks = true;




//                    if (userManager.UserExists(userName))
//                    {
//                        List<string> listaborrar = GetAllRolesString();
//                        RemoveUserFromRoles(userName, listaborrar);

//                        User user = userManager.GetUser(userName);

//                        foreach (var roleName in rolesToAdd)
//                        {
//                            if (roleManager.RoleExists(roleName))
//                            {
//                                Role role = roleManager.GetRole(roleName);
//                                roleManager.AddUserToRole(user, role);
//                                resp = 1;
//                            }
//                        }
//                    }

//                    roleManager.SaveChanges();
//                    roleManager.Provider.SuppressSecurityChecks = false;
//               userManager.Provider.SuppressSecurityChecks = false;
//            profileManager.Provider.SuppressSecurityChecks = false;
//                    return resp;


//        }



//        public static List<rolUsuario> GetRolesForUser(string userName)
//        {
//            List<rolUsuario> rolesusuario = new List<rolUsuario>();
//            List<Role> roles = new List<Role>();

//            RoleManager roleManager = RoleManager.GetManager("external");
//            UserManager userManager = UserManager.GetManager("external");

//            bool userExists = userManager.UserExists(userName);

//            if (userExists)
//            {


//                User user = userManager.GetUser(userName);
//                roles = roleManager.GetRolesForUser(user.Id).ToList();

//                foreach (var item in roles)
//                {
//                    rolUsuario rol = new rolUsuario();
//                    rol.nombrerol = item.Name;
//                    rolesusuario.Add(rol);
//                }
//            }

//            return rolesusuario;
//        }

//        public class rolUsuario
//        {
//            public string nombrerol { get; set; }

//        }


//        public class UsuarioCompleto
//        {
//            public Guid id { get; set; }
//            public string username { get; set; }
//            public string nombre { get; set; }
//            public string correo { get; set; }
//            public bool aprobado { get; set; }
//            public string fecha { get; set; }
//            public List<rolUsuario> roles { get; set; }

//        }

//        public static List<string> GetAllRolesString()
//        {
//            RoleManager roleManager = RoleManager.GetManager("external");

//            List<string> lista = new List<string>();

//            foreach (var item in roleManager.GetRoles().ToList())
//            {
//                lista.Add(item.Name);
//            }
//            return lista;
//        }

//        public static List<Role> GetAllRoles()
//        {
//            RoleManager roleManager = RoleManager.GetManager("external");

//            return roleManager.GetRoles().Where(u => u.Name != "Admin").ToList();
//        }

//        public static void ValidarUsuario(string username)
//        {
//            UserManager userManager = UserManager.GetManager("external");
//            UserProfileManager profileManager = UserProfileManager.GetManager();
//            RoleManager roleManager = RoleManager.GetManager();

//            userManager.Provider.SuppressSecurityChecks = true;
//            roleManager.Provider.SuppressSecurityChecks = true;
//            profileManager.Provider.SuppressSecurityChecks = true;


//            User user = userManager.GetUsers().Where(u => u.UserName == username).SingleOrDefault();

//            if (user != null)
//            {
//                user.IsApproved = true;

//                userManager.SaveChanges();
//                roleManager.Provider.SuppressSecurityChecks = false;
//                userManager.Provider.SuppressSecurityChecks = false;
//                profileManager.Provider.SuppressSecurityChecks = false;
//            }
//        }
//        public static MembershipCreateStatus CreateUser(string username, string password, string firstName, string lastName, string mail, string secretQuestion, string secretAnswer, bool isApproved)
//        {

//            UserManager userManager = UserManager.GetManager("external");
//            UserProfileManager profileManager = UserProfileManager.GetManager();
//            RoleManager roleManager = RoleManager.GetManager();


//            userManager.Provider.SuppressSecurityChecks = true;
//            roleManager.Provider.SuppressSecurityChecks = true;
//            profileManager.Provider.SuppressSecurityChecks = true;

//            System.Web.Security.MembershipCreateStatus status;

//            User user = userManager.CreateUser(username, password, mail, secretQuestion, secretAnswer, isApproved, null, out status);

//            if (status == MembershipCreateStatus.Success)
//            {
//                SitefinityProfile sfProfile = profileManager.CreateProfile(user, Guid.NewGuid(), typeof(SitefinityProfile)) as SitefinityProfile;

//                if (sfProfile != null)
//                {
//                    sfProfile.FirstName = firstName;
//                    sfProfile.LastName = lastName;
//                }
//                StringBuilder body = new StringBuilder();
//                body.Append("<span style='font-family:Courier New;font-size:13px'> Estimado ");
//                body.Append(firstName+" "+lastName);
//                body.Append(":<br /><br />");
//                body.Append("Su cuenta se ha creado, el administrador activará su cuenta lo antes posible.<br />");
//                body.Append("<br />");
//                string mails = "juan@lafabricaimaginaria.cl," + mail;
//                Common.envio_correo(mail,"Bienvenido a Marketing ACHS", body, "Admin Marketing Achs");


//                userManager.SaveChanges();


//                profileManager.RecompileItemUrls(sfProfile);
//                profileManager.SaveChanges();

//                roleManager.Provider.SuppressSecurityChecks = false;
//                userManager.Provider.SuppressSecurityChecks = false;
//                profileManager.Provider.SuppressSecurityChecks = false;
//            }

//            return status;



//        }


//        public static int DesactivarUsuario(string username)
//        {
//            int resp = 0;
//            UserManager userManager = UserManager.GetManager("external");
//            UserProfileManager profileManager = UserProfileManager.GetManager();
//            RoleManager roleManager = RoleManager.GetManager();

//            userManager.Provider.SuppressSecurityChecks = true;
//            roleManager.Provider.SuppressSecurityChecks = true;
//            profileManager.Provider.SuppressSecurityChecks = true;

//            User user = userManager.GetUsers().Where(u => u.UserName == username).SingleOrDefault();

//            if (user != null)
//            {
//                List<string> listaborrar = GetAllRolesString();
//                RemoveUserFromRoles(username, listaborrar);

//                user.IsApproved = false;
//                resp = 1;
//                userManager.SaveChanges();

//                roleManager.Provider.SuppressSecurityChecks = false;
//                userManager.Provider.SuppressSecurityChecks = false;
//                profileManager.Provider.SuppressSecurityChecks = false;
//            }
//            return resp;
//        }

//        public static void RemoveUserFromRoles(string userName, List<string> rolesToRemove)
//        {
//            UserManager userManager = UserManager.GetManager("external");
//            RoleManager roleManager = RoleManager.GetManager("external");
//            UserProfileManager profileManager = UserProfileManager.GetManager();

//            userManager.Provider.SuppressSecurityChecks = true;
//            roleManager.Provider.SuppressSecurityChecks = true;
//            profileManager.Provider.SuppressSecurityChecks = true;

//            if (userManager.UserExists(userName))
//            {
//                User user = userManager.GetUser(userName);

//                foreach (var roleName in rolesToRemove)
//                {
//                    if (roleManager.RoleExists(roleName))
//                    {
//                        Role role = roleManager.GetRole(roleName);
//                        roleManager.RemoveUserFromRole(user, role);
//                    }
//                }
//            }

//            roleManager.SaveChanges();
//            roleManager.Provider.SuppressSecurityChecks = false;
//            userManager.Provider.SuppressSecurityChecks = false;
//            profileManager.Provider.SuppressSecurityChecks = false;
//        }
//    }
//}
