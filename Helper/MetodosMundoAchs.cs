﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;
using SitefinityWebApp.Mvc.Models;
using System.Collections.Generic;
using SitefinityWebApp.Helper;
using Telerik.Sitefinity.DynamicModules;
using Telerik.Sitefinity.Utilities.TypeConverters;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Data;
using Telerik.Sitefinity.Lifecycle;
using Telerik.Sitefinity.Versioning;

using System.Linq.Dynamic;

using Telerik.Sitefinity.RelatedData;
using Telerik.Sitefinity.Libraries.Model;
using Telerik.Sitefinity.Taxonomies;
using Telerik.Sitefinity.Taxonomies.Model;
using Telerik.OpenAccess;
using RestSharp;

namespace SitefinityWebApp.Helper
{
    public class Descuento
    {
        public string title { get; set; }
        public string nombre { get; set; }
        public string summary { get; set; }
        public DateTime? fechaInicio { get; set; }
        public DateTime? fechaTermino { get; set; }
        public string detalle { get; set; }
        public string instruccionesDescuento { get; set; }
        public string mensajeEmail { get; set; }
        public string restriccion { get; set; }
        public string region { get; set; }
        public string letraChica { get; set; }
        public string textoDescuento { get; set; }
        public string valorDescuento { get; set; }
        public string imagen { get; set; }
        public string titleEmpresa { get; set; }
        public string rutEmpresa { get; set; }
        public string direccionEmpresa { get; set; }
        public string telefonoEmpresa { get; set; }
        public string sitioWebEmpresa { get; set; }
        public string instagramEmpresa { get; set; }
        public string twitterEmpresa { get; set; }
        public string facebookEmpresa { get; set; }
        public string emailEmpresa { get; set; }
        public string descripcionEmpresa { get; set; }
        public string imagenEmpresa { get; set; }
    }

    public class MetodosMundoAchs
    {

   

        public bool validarRut(string rut, ref string error)
        {
            bool resultado = false;
            try
            {

                var client = new RestClient(Helper.Common.GetWebConfigKey("url_api_verifica_rut") + "?id=" + rut );
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                IRestResponse response = client.Execute(request);

                string data = response.Content;
                if (data.Length > 4)
                {
                    resultado = true;
                }
                else
                {
                    resultado = false;
                }
                error = "";
                return resultado;

            }
            catch (Exception ex)
            {
                error = ex.Message.ToString();
                return resultado;
                throw;
            }


        }



        public static Descuento get_datos_descuento (string urlName)
        {

            Descuento descuento = new Descuento();
            var providerName = Helper.Common.GetWebConfigKey("ProviderDescuentos");
            TaxonomyManager taxonomyManager = TaxonomyManager.GetManager();
            var transactionName = "someTransactionName" + DateTime.Now.Ticks.ToString();

            DynamicModuleManager dynamicModuleManager = DynamicModuleManager.GetManager(providerName, transactionName);

            Type descuentoType = TypeResolutionService.ResolveType("Telerik.Sitefinity.DynamicTypes.Model.DescuentosMundoAchs.DescuentoMundoAchs");

            DynamicContent descuentoItem = dynamicModuleManager.GetDataItems(descuentoType).Where(dynItem => dynItem.UrlName == urlName && dynItem.Status == ContentLifecycleStatus.Live && dynItem.Visible == true).SingleOrDefault();

            if (descuentoItem != null)
            {
                string Title = descuentoItem.GetValue("Title") != null ? descuentoItem.GetValue("Title").ToString() : "";
                string Nombre = descuentoItem.GetValue("Nombre") != null ? descuentoItem.GetValue("Nombre").ToString() : "";

                string Summary = descuentoItem.GetValue("Summary") != null ? descuentoItem.GetValue("Summary").ToString() : "";
                DateTime? FechaInicio = descuentoItem.GetValue("FechaInicio") != null ? (DateTime?)descuentoItem.GetValue("FechaInicio") : null;
                DateTime? FechaTermino = descuentoItem.GetValue("FechaTermino") != null ? (DateTime?)descuentoItem.GetValue("FechaTermino") : null;

                string Detalle = descuentoItem.GetValue("Detalle") != null ? descuentoItem.GetValue("Detalle").ToString() : "";
                string InstruccionesDescuento = descuentoItem.GetValue("InstruccionesDescuento") != null ? descuentoItem.GetValue("InstruccionesDescuento").ToString() : "";
                string MensajeEmail = descuentoItem.GetValue("MensajeEmail") != null ? descuentoItem.GetValue("MensajeEmail").ToString() : "";
                string Restriccion = descuentoItem.GetValue("Restriccion") != null ? descuentoItem.GetValue("Restriccion").ToString() : "";
                //string Region = descuentoItem.GetValue("Region") != null ? descuentoItem.GetValue<ChoiceOption>("Region").PersistedValue : "";
                string LetraChica = descuentoItem.GetValue("LetraChica") != null ? descuentoItem.GetValue("LetraChica").ToString() : "";
                string TextoDescuento = descuentoItem.GetValue("TextoDescuento") != null ? descuentoItem.GetValue("TextoDescuento").ToString() : "";
                string ValorDescuento = descuentoItem.GetValue("ValorDescuento") != null ? descuentoItem.GetValue("ValorDescuento").ToString() : "";

                //FlatTaxon taxon = new FlatTaxon();
                //Guid gTaxon = new Guid();
                //gTaxon = descuentoItem.GetValue<TrackedList<Guid>>("categoriasdescuentos").SingleOrDefault();
                //taxon = taxonomyManager.GetTaxa<FlatTaxon>().Where(t => t.Id.Equals(g)).SingleOrDefault();
                //string categoriasdescuentos_texto = string.Empty;
                //string categoriasdescuentos_slug = string.Empty;
                //if (taxon != null)
                //{
                //    categoriasdescuentos_texto = taxon.Title;
                //    categoriasdescuentos_slug = taxon.UrlName.ToString();
                //}


                var empresaItem = descuentoItem.GetRelatedItems<DynamicContent>("Empresa").SingleOrDefault();
                string Title_Empresa = string.Empty;
                string RutEmpresa = string.Empty;
                string DireccionEmpresa = string.Empty;
                string TelefonoEmpresa = string.Empty;
                string SitioWebEmpresa = string.Empty;
                string InstagramEmpresa = string.Empty;
                string TwitterEmpresa = string.Empty;
                string FacebookEmpresa = string.Empty;
                string EmailEmpresa = string.Empty;
                string DescripcionEmpresa = string.Empty;
                string ImagenEmpresa = string.Empty;

                if (empresaItem != null)
                {
                    Title_Empresa = empresaItem.GetValue("Title") != null ? empresaItem.GetValue("Title").ToString() : "";
                    RutEmpresa = empresaItem.GetValue("RutEmpresa") != null ? empresaItem.GetValue("RutEmpresa").ToString() : "";
                    DireccionEmpresa = empresaItem.GetValue("DireccionEmpresa") != null ? empresaItem.GetValue("DireccionEmpresa").ToString() : "";
                    TelefonoEmpresa = empresaItem.GetValue("TelefonoEmpresa") != null ? empresaItem.GetValue("TelefonoEmpresa").ToString() : "";
                    SitioWebEmpresa = empresaItem.GetValue("SitioWebEmpresa") != null ? empresaItem.GetValue("SitioWebEmpresa").ToString() : "";
                    InstagramEmpresa = empresaItem.GetValue("InstagramEmpresa") != null ? empresaItem.GetValue("InstagramEmpresa").ToString() : "";
                    TwitterEmpresa = empresaItem.GetValue("TwitterEmpresa") != null ? empresaItem.GetValue("TwitterEmpresa").ToString() : "";
                    FacebookEmpresa = empresaItem.GetValue("FacebookEmpresa") != null ? empresaItem.GetValue("FacebookEmpresa").ToString() : "";
                    EmailEmpresa = empresaItem.GetValue("EmailEmpresa") != null ? empresaItem.GetValue("EmailEmpresa").ToString() : "";
                    DescripcionEmpresa = empresaItem.GetValue("DescripcionEmpresa") != null ? empresaItem.GetValue("DescripcionEmpresa").ToString() : "";

                    Image imageFieldEmpresa = empresaItem.GetRelatedItems<Image>("ImagenEmpresa").SingleOrDefault();
                    ImagenEmpresa = imageFieldEmpresa != null ? imageFieldEmpresa.MediaUrl : string.Empty;
                }


                Image imageField = descuentoItem.GetRelatedItems<Image>("Imagen").SingleOrDefault();
                string imagen = imageField != null ? imageField.MediaUrl : string.Empty;


                descuento.title = Title;
                descuento.nombre = Nombre;
                descuento.summary = Summary;
                descuento.fechaInicio = FechaInicio;
                descuento.fechaTermino = FechaTermino;
                descuento.detalle = Detalle;
                descuento.instruccionesDescuento = InstruccionesDescuento;
                descuento.mensajeEmail = MensajeEmail;
                descuento.restriccion = Restriccion;
                //descuento.region = Region;
                descuento.letraChica = LetraChica;
                descuento.textoDescuento = TextoDescuento;
                descuento.valorDescuento = ValorDescuento;
                descuento.imagen = imagen;
                descuento.titleEmpresa = Title_Empresa;
                descuento.rutEmpresa = RutEmpresa;
                descuento.direccionEmpresa = DireccionEmpresa;
                descuento.telefonoEmpresa = TelefonoEmpresa;
                descuento.sitioWebEmpresa = SitioWebEmpresa;
                descuento.instagramEmpresa = InstagramEmpresa;
                descuento.twitterEmpresa = TwitterEmpresa;
                descuento.facebookEmpresa = FacebookEmpresa;
                descuento.emailEmpresa = EmailEmpresa;
                descuento.descripcionEmpresa = DescripcionEmpresa;
                descuento.imagenEmpresa = ImagenEmpresa;



                try
                {

                    dynamicModuleManager.Provider.SuppressSecurityChecks = true;

                    DynamicContent descuentoItemUpdate = dynamicModuleManager.GetDataItem(descuentoType, descuentoItem.OriginalContentId);
                    var versionManager = VersionManager.GetManager(null, transactionName);
                    //// Create a version
                    versionManager.CreateVersion(descuentoItemUpdate, true);

                    // Then we check it out
                    DynamicContent checkOutContentoneItem = dynamicModuleManager.Lifecycle.CheckOut(descuentoItemUpdate) as DynamicContent;



                    string visitas = checkOutContentoneItem.GetValue("Visitas") != null ? checkOutContentoneItem.GetValue("Visitas").ToString() : "0";
                    int vista_descuento = 0;
                    try
                    {
                        vista_descuento = Convert.ToInt32(visitas) + 1;
                        if (vista_descuento < 0)
                        {
                            vista_descuento = 0;
                        }
                    }
                    catch
                    {
                        vista_descuento = 0;
                    }
                    checkOutContentoneItem.SetValue("Visitas", vista_descuento);



                    // Now we need to check in, so the changes apply
                    ILifecycleDataItem checkInContentoneItem = dynamicModuleManager.Lifecycle.CheckIn(checkOutContentoneItem);

                    // Create a version
                    versionManager.CreateVersion(checkInContentoneItem, false);

                    //Finnaly we publish the item again
                    dynamicModuleManager.Lifecycle.Publish(checkInContentoneItem);



                    // Create a version and commit the transaction in order changes to be persisted to data store
                    versionManager.CreateVersion(checkInContentoneItem, true);
                    TransactionManager.CommitTransaction(transactionName);



                    dynamicModuleManager.Provider.SuppressSecurityChecks = false;


                }
                catch (Exception ex)
                {
                   
                }

            }
            return descuento;


        }
    }
}