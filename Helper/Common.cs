﻿using Microsoft.IdentityModel.Protocols;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Libraries.Model;
using System.Text.RegularExpressions;
using Telerik.Sitefinity.Taxonomies.Model;
using Telerik.Sitefinity.Modules.Forms;
using Telerik.Sitefinity.Forms.Model;
using Telerik.Sitefinity.Services;
using System.Globalization;
using Telerik.Sitefinity.Model;
using System.Net.Mail;
using Telerik.Sitefinity.Configuration;
using System.Text;
using Telerik.Sitefinity.Web.Mail;
using System.Threading;
using System.Net;
using Telerik.Sitefinity.Taxonomies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Telerik.Sitefinity.Taxonomies;
using Telerik.Sitefinity.Taxonomies.Model;
using Telerik.Sitefinity.Model.ContentLinks;
using Telerik.Sitefinity.Data.ContentLinks;
using System.IO;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Data;
using Telerik.Sitefinity.DynamicModules;
using Telerik.Sitefinity.Utilities.TypeConverters;
using Telerik.Sitefinity.Modules.GenericContent;

namespace SitefinityWebApp.Helper
{



    public class Common
    {

        private static string getYoutubeEmbedCode(string youtubeId)
        {
            string youtubeEmbedFormat = "https://www.youtube.com/embed/{0}?rel=0";


            return string.Format(youtubeEmbedFormat, youtubeId);
        }


        public static string UrlToEmbedCode(string url)
        {
            System.Text.RegularExpressions.Regex YoutubeVideoRegex = new System.Text.RegularExpressions.Regex(@"youtu(?:\.be|be\.com)/(?:(.*)v(/|=)|(.*/)?)([a-zA-Z0-9-_]+)", System.Text.RegularExpressions.RegexOptions.IgnoreCase);

            if (!string.IsNullOrEmpty(url))
            {
                var youtubeMatch = YoutubeVideoRegex.Match(url);

                if (youtubeMatch.Success)
                {
                    return getYoutubeEmbedCode(youtubeMatch.Groups[youtubeMatch.Groups.Count - 1].Value);
                }



            }

            return null;
        }


        private class ResponseToken
        {
            public DateTime challenge_ts { get; set; }
            public float score { get; set; }
            public List<string> ErrorCodes { get; set; }
            public bool Success { get; set; }
            public string hostname { get; set; }
        }


        public static IQueryable<FormEntry> GetFormResponsesForSpecificForm(string formName)
        {
            FormsManager formsManager = FormsManager.GetManager();

            //here you can also use the GetForm(Guid id) method of the formsManager the get a specific form
            var form = formsManager.GetFormByName(formName);

            if (form != null)
            {
                return formsManager.GetFormEntries(new FormDescription(form.Name));
            }

            return null;
        }


        public static bool RecaptchaVerifyBackend(string recaptchaToken)
        {
            string apiAddress = "https://www.google.com/recaptcha/api/siteverify";
            string recaptchaSecret = Common.GetWebConfigKey("RECAPTCHA_SECRET_ASSA");
            string url = $"{apiAddress}?secret={recaptchaSecret}&response={recaptchaToken}";
            using (var httpClient = new System.Net.Http.HttpClient())
            {
                try
                {
                    string responseString = httpClient.GetStringAsync(url).Result;
                    ResponseToken response = new ResponseToken();
                    response = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseToken>(responseString);
                    return response.Success;
                }
                catch (Exception ex)
                {
                    return false;
                    throw new Exception(ex.Message);
                }
            }
        }

        public static bool validarRut(string rut)
        {

            bool validacion = false;
            try
            {
                rut = rut.ToUpper();
                rut = rut.Replace(".", "");
                rut = rut.Replace("-", "");
                int rutAux = int.Parse(rut.Substring(0, rut.Length - 1));

                char dv = char.Parse(rut.Substring(rut.Length - 1, 1));

                int m = 0, s = 1;
                for (; rutAux != 0; rutAux /= 10)
                {
                    s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
                }
                if (dv == (char)(s != 0 ? s + 47 : 75))
                {
                    validacion = true;
                }
            }
            catch (Exception)
            {
            }
            return validacion;
        }


        public class Value
        {
            public string name { get; set; }
            public string value { get; set; }
        }


        public static string get_html_raw(string slugname,string providerName)
        {
            // Set the provider name for the DynamicModuleManager here. All available providers are listed in
            // Administration -> Settings -> Advanced -> DynamicModules -> Providers
          

            // Set a transaction name
            var transactionName = "";


            // Set the culture name for the multilingual fields
            var cultureName = "es";
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(cultureName);

            DynamicModuleManager dynamicModuleManager = DynamicModuleManager.GetManager(providerName, transactionName);
            Type htmlRawType = TypeResolutionService.ResolveType("Telerik.Sitefinity.DynamicTypes.Model.HTMLRAW.html_raw");
         
            // This is how we get the htmlRaw items through filtering
            var itemHtmlRaw = dynamicModuleManager.GetDataItems(htmlRawType).Where(x=>x.UrlName.Equals(slugname)).FirstOrDefault();
            string html = "";
            if (itemHtmlRaw!=null)
            {
                html  = itemHtmlRaw.GetValue("cuerpo_html") != null ? itemHtmlRaw.GetValue("cuerpo_html").ToString() : string.Empty;
            }

            // At this point myFilteredCollection contains the items that match the lambda expression passed to the Where extension method
            // If you want only the first matching element you can freely get it by ".First()" extension method like this:
            // var myFirstFilteredItem = myFilteredCollection.First();
            return html;

        }


        public static IQueryable<Value> get_filter_list(string taxo)
        {
            IList<Value> model = new List<Value>();
            TaxonomyManager taxonomyManager = TaxonomyManager.GetManager();
            var lista_tag_activo = taxonomyManager.GetTaxa<FlatTaxon>().Where(t => t.Taxonomy.Name == taxo).OrderBy(t => t.Name).ToList();
            foreach (var item_tag in lista_tag_activo)
            {
                Value d = new Value
                {
                    name = item_tag.Title,
                    value = Common.generar_slug(item_tag.Title.ToLower())
                };
                model.Add(d);
            }
            IQueryable<Value> l = model.AsQueryable();
            return l;
        }


        public static Random rnd = new Random();
        

        public static int rd()
        {

            int id = rnd.Next(1, Int32.MaxValue);
            return id;
        }
        //public static MedicoContenidoCAS get_medicoContenidoPublico(string IdCas)
        //{

        //    MedicoContenidoCAS m = MetodosMedicos.get_medicoContenidoPublico(IdCas);

        //    return m;
        //}


    


        public static string GetWebConfigKey(string nombre)
        {
            if ((!String.IsNullOrEmpty(ConfigurationManager.AppSettings[nombre]) && ConfigurationManager.AppSettings[nombre] != null))
            {
                return ConfigurationManager.AppSettings[nombre].ToString();
            }
            else
            {
                return string.Empty;
            }
        }

        public static string replace_n_to_br(string texto)
        {
            if (!string.IsNullOrEmpty(texto))
                return texto.Replace("\n", "<br>");
            return string.Empty;
        }

        public static string remover_acentos(string txt)
        {
            byte[] bytes = System.Text.Encoding.GetEncoding("Cyrillic").GetBytes(txt);
            return System.Text.Encoding.ASCII.GetString(bytes);
        }


        public static string generar_slug(string phrase)
        {
            string str = remover_acentos(phrase).ToLower();
            // invalid chars           
            str = Regex.Replace(str, @"[^a-z0-9\s-]", "");
            // convert multiple spaces into one space   
            str = Regex.Replace(str, @"\s+", " ").Trim();
            // cut and trim 
            str = str.Substring(0, str.Length <= 45 ? str.Length : 45).Trim();
            str = Regex.Replace(str, @"\s", "-"); // hyphens   
            return str;
        }

        public static string ResolveThumbnailUrl(Telerik.Sitefinity.Frontend.Mvc.Models.ItemViewModel relatedItem, string thumbnail_name)
        {

            var manager = Telerik.Sitefinity.Modules.Libraries.LibrariesManager.GetManager();
            Image image = manager.GetImages().FirstOrDefault(i => i.Id == relatedItem.DataItem.Id);
            if (image != null)
            {
                bool flag = image.Thumbnails.Where(x => x.Name.Equals(thumbnail_name)).Any();
                if (flag)
                {
                    string media_url = image.ResolveThumbnailUrl(thumbnail_name);
                    if (!String.IsNullOrEmpty(media_url))
                    {
                        return media_url;
                    }
                    else
                    {
                        return image.ResolveMediaUrl();
                    }
                }
                else
                {
                    return image.ResolveMediaUrl();
                }
            }
            else
            {
                return image.ResolveMediaUrl();

            }

        }

        private static string get_unique_slug_url(DocumentLibrary documentLibrary, string fileName)
        {
            List<string> existingUrlNames = documentLibrary.ChildContentItems
                .Where(c => c.Status == ContentLifecycleStatus.Live && c.Visible)
                .Select(c => c.UrlName.ToString()).ToList();
            string safeTitle = fileName;
            while (existingUrlNames.Contains(safeTitle))
            {
                safeTitle += "-";
            }
            return safeTitle;

        }

        private static ContentLink[] upload_document(FormEntry entry, HttpPostedFileBase newFile, string libreria,string provider)
        {

            var contentLinksManager = ContentLinksManager.GetManager();
            LibrariesManager libraryManager = LibrariesManager.GetManager();
            using (new ElevatedModeRegion(libraryManager))
            {
                //DocumentLibrary documentLibrary = libraryManager.GetDocumentLibraries().SingleOrDefault();
                DocumentLibrary documentLibrary = libraryManager.GetDocumentLibraries().Where(x => x.Title == libreria).FirstOrDefault();

                string fecha = DateTime.Now.ToString("yyyyMMddHHmmss");

                string fileExtension = Path.GetExtension(newFile.FileName);
                string fileName = Path.GetFileName(newFile.FileName);
                fileName = fileName + fecha;
                string urlName = get_unique_slug_url(documentLibrary, fileName);

                Document document = libraryManager.CreateDocument();
                document.Author = string.Empty;
                document.DateCreated = DateTime.UtcNow;
                document.Owner = Guid.Empty;
                document.Parent = documentLibrary;
                document.Title = generar_slug(urlName);
                document.UrlName = generar_slug(urlName);

                libraryManager.Upload(document, newFile.InputStream, fileExtension);
                libraryManager.RecompileItemUrls(document);
                libraryManager.Lifecycle.Publish(document);
                document.ApprovalWorkflowState = "Published";
                libraryManager.SaveChanges();


                ContentLink contentLink = new ContentLink();
                contentLink.Id = Guid.NewGuid();
                contentLink.ParentItemId = entry.Id;
                contentLink.ParentItemType = entry.GetType().ToString();
                contentLink.ChildItemId = document.Id;
                contentLink.ComponentPropertyName = entry.Id.ToString();
                contentLink.ChildItemAdditionalInfo = "additional info";
                contentLink.ChildItemProviderName = (((IDataItem)document).Provider as DataProviderBase).Name;
                contentLink.ChildItemType = document.GetType().FullName;
                contentLink.ApplicationName = contentLinksManager.Provider.ApplicationName;

                ContentLink[] contentLinks = new ContentLink[0];
                if (contentLinks == null)
                {
                    contentLinks = new ContentLink[0];
                }

                var assetsFieldList = contentLinks.ToList();
                assetsFieldList.Insert(0, contentLink);
                return assetsFieldList.ToArray();
            }
        }
        public static string insertar_formulario_return_id(string nombre_formulario, Dictionary<string, string> dict, string UserHostAddress)
        {
            try
            {

                var manager = FormsManager.GetManager();
                var form = manager.GetFormByName(nombre_formulario);

                FormEntry entry = manager.CreateFormEntry(form.EntriesTypeName);
                foreach (var item in dict)
                {
                    entry.SetValue(item.Key, item.Value);
                }


                //Guid g = new Guid("00fe59fc-25a7-4cfa-8500-97c4cd50a18d");
                entry.UserId = Telerik.Sitefinity.Security.Claims.ClaimsManager.GetCurrentUserId();
                entry.Language = System.Globalization.CultureInfo.CurrentUICulture.Name;
                entry.IpAddress = UserHostAddress;
                if (SystemManager.CurrentContext.AppSettings.Multilingual)
                {
                    entry.Language = CultureInfo.CurrentUICulture.Name;
                }
                entry.ReferralCode = manager.Provider.GetNextReferralCode(entry.ToString()).ToString();
                //form.FormEntriesSeed++;
                //entry.ReferralCode = form.FormEntriesSeed.ToString();
                entry.SubmittedOn = System.DateTime.UtcNow;

                manager.SaveChanges();
                return entry.ReferralCode;
              
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        public static string insertar_formulario_siniestro_return_id(string nombre_formulario, Dictionary<string, string> dict, HttpPostedFileBase file , string UserHostAddress)
        {
            try
            {
                string libreria = "Adjuntos";
                string campo = "adjunto";
                string provider = "librariesProvider7";
                var manager = FormsManager.GetManager();
                var form = manager.GetFormByName(nombre_formulario);

                FormEntry entry = manager.CreateFormEntry(form.EntriesTypeName);
                foreach (var item in dict)
                {
                    entry.SetValue(item.Key, item.Value);
                }


                string fileName = Path.GetFileName(file.FileName);
                if (!string.IsNullOrEmpty(fileName))
                {
                    if (file != null)
                    {
                        ContentLink[] sitefinity_file = upload_document(entry, file, libreria, provider);
                        entry.SetValue(campo, sitefinity_file);
                        ContentLink sitefinity_file0 = sitefinity_file[0];
                        string x = sitefinity_file[0].ToString();
                    }

                } 


                Guid g = new Guid("00fe59fc-25a7-4cfa-8500-97c4cd50a18d");
                entry.UserId = Telerik.Sitefinity.Security.Claims.ClaimsManager.GetCurrentUserId();
                entry.Language = System.Globalization.CultureInfo.CurrentUICulture.Name;
                entry.IpAddress = UserHostAddress;
                if (SystemManager.CurrentContext.AppSettings.Multilingual)
                {
                    entry.Language = CultureInfo.CurrentUICulture.Name;
                }
                 
                form.FormEntriesSeed++;
                entry.ReferralCode = form.FormEntriesSeed.ToString();
                 
                entry.SubmittedOn = System.DateTime.UtcNow;

                manager.SaveChanges();
                return entry.ReferralCode;

            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        public static string ImagenThumbnailUrl(Image image, string thumbnail_name)
        {

            var manager = Telerik.Sitefinity.Modules.Libraries.LibrariesManager.GetManager();
           // Image ImageThumbnail = manager.GetImages().FirstOrDefault(i => i.Id == image.Id);
            if (image != null)
            {
                bool flag = image.Thumbnails.Where(x => x.Name.Equals(thumbnail_name)).Any();
                if (flag)
                {
                    string media_url = image.ResolveThumbnailUrl(thumbnail_name);
                    if (!String.IsNullOrEmpty(media_url))
                    {
                        return media_url;
                    }
                    else
                    {
                        return image.ResolveMediaUrl()+"&e=1";
                    }
                }
                else
                {
                    return image.ResolveMediaUrl() + "&e=2"; ;
                }
            }
            else
            {
                return image.ResolveMediaUrl() + "&e=3"; ;

            }

        }


        public static string envio_correo(string email_destinatario, string subject, StringBuilder sb,string nombre_correo)
        {    //construct mail message

            try
            {
                string DefaultSenderEmailAddress = Common.GetWebConfigKey("EMAIL_DESCUENTO");
                var smtpSettings = Config.Get<SystemConfig>().SmtpSettings;
                MailMessage message = new MailMessage();
                message.From = new MailAddress(DefaultSenderEmailAddress, nombre_correo);
                foreach (string email1 in email_destinatario.Split(',').Select(x => x.ToString()).ToList())
                {
                    message.To.Add(new MailAddress(email1));
                }
                message.Subject = subject;
                message.Body = sb.ToString();
                message.IsBodyHtml = true;
                message.BodyEncoding = Encoding.Unicode;
                message.SubjectEncoding = Encoding.Unicode;
                
                //send the notification
                EmailSender.Get().Send(message);
                return "";
            }  catch (Exception ex)
            {
                return ex.ToString();
            }

        }

      

        public static string  send_email_smtp_file(string to, string cc, string subject, StringBuilder body,string file)
        {
            string salida = "";
            Thread email = new Thread(delegate ()
            {
                try
                {
                    string from = ConfigurationManager.AppSettings["EMAIL_ENVIADOR_MG"].ToString();
                    string password = ConfigurationManager.AppSettings["PASS_ENVIADOR_MG"].ToString();
                    string nombre = ConfigurationManager.AppSettings["NOMBRE_ENVIADOR_MG"].ToString();
                    string from_cliente = ConfigurationManager.AppSettings["EMAIL_CLIENTE_MG"].ToString();
                    using (MailMessage mm = new MailMessage())
                    {
                        string[] array_to = to.Split(',');
                        foreach (string email_to in array_to)
                            mm.To.Add(email_to);

                        if (!String.IsNullOrEmpty(cc))
                        {
                            string[] array_cc = cc.Split(',');
                            foreach (string email_cc in array_cc)
                                mm.Bcc.Add(email_cc);
                        }
                        mm.From = new MailAddress(from_cliente, nombre);
                        mm.Subject = subject;
                        mm.Body = body.ToString();
                        mm.IsBodyHtml = true;
                        mm.ReplyToList.Add(new MailAddress(from_cliente));
                        mm.Attachments.Add(new Attachment(file));
                        mm.BodyEncoding = System.Text.Encoding.UTF8;
                        SmtpClient smtp = new SmtpClient();
                        smtp.Host = ConfigurationManager.AppSettings["SERVER_MAIL_MG"].ToString();

                        smtp.EnableSsl = true;

                        NetworkCredential NetworkCred = new NetworkCredential(from, password);

                        smtp.Credentials = NetworkCred;
                        smtp.Port = Convert.ToInt32(ConfigurationManager.AppSettings["PORT_MG"].ToString());
                        smtp.Send(mm);
                       

                    }
                }
                catch(Exception ex)
                {
                    salida = ex.ToString();
                }
            });

            email.IsBackground = true;
            email.Start();
            return salida;
        }


        public static string RemoveQueryStringByKey(string url, string key)
        {
            var uri = new Uri(url);

            // this gets all the query string key value pairs as a collection
            var newQueryString = HttpUtility.ParseQueryString(uri.Query);

            // this removes the key if exists
            newQueryString.Remove(key);

            // this gets the page path from root without QueryString
            string pagePathWithoutQueryString = uri.GetLeftPart(UriPartial.Path);

            return newQueryString.Count > 0
                ? String.Format("{0}?{1}", pagePathWithoutQueryString, newQueryString)
                : pagePathWithoutQueryString;
        }




        public static string  GetContentItemByTitleNative(string Title)
        {
            ContentManager manager = ContentManager.GetManager();
            var contentItem = manager.GetContent().Where(cI => (cI.Title == Title && cI.Status == ContentLifecycleStatus.Live)).FirstOrDefault();
            string content = contentItem.Content;
            return content;
        }


        public static string send_email_smtp_custom_file(string email_account, string email_passowrd,string to, string cc, string subject, StringBuilder body, string file)
        {
            string salida = "";
            Thread email = new Thread(delegate ()
            {
                try
                {
                   
                     email_account = !String.IsNullOrEmpty(email_passowrd) ? email_account : ConfigurationManager.AppSettings["EMAIL_ENVIADOR_MG"].ToString();
                    string email_account_producto = !String.IsNullOrEmpty(email_account) ? email_account : ConfigurationManager.AppSettings["EMAIL_CLIENTE_MG"].ToString();
                    email_passowrd = !String.IsNullOrEmpty(email_passowrd) ? email_passowrd : ConfigurationManager.AppSettings["PASS_ENVIADOR_MG"].ToString();
                    string from = email_account;
                    string password = email_passowrd;
                    string nombre = ConfigurationManager.AppSettings["NOMBRE_ENVIADOR_MG"].ToString();
                    string from_email_producto = email_account_producto;
                  
                    using (MailMessage mm = new MailMessage())
                    {
                        string[] array_to = to.Split(',');
                        foreach (string email_to in array_to)
                            mm.To.Add(email_to);
                        
                        if (!String.IsNullOrEmpty(cc))
                        {
                           
                            string[] array_cc = cc.Split(',');
                            foreach (string email_cc in array_cc)
                                mm.Bcc.Add(email_cc);
                        }
                        mm.From = new MailAddress(from_email_producto, nombre);
                        mm.Subject = subject;
                        mm.Body = body.ToString();
                        mm.IsBodyHtml = true;
                        mm.ReplyToList.Add(new MailAddress(from_email_producto));
                        if (!String.IsNullOrEmpty(file))
                        {
                            var stream = new WebClient().OpenRead(file);
                            file = Common.RemoveQueryStringByKey(file, "sfvrsn");
                            string filename = System.IO.Path.GetFileName(file);
                            mm.Attachments.Add(new Attachment(stream, filename));
                        }
                        mm.BodyEncoding = System.Text.Encoding.UTF8;
                        SmtpClient smtp = new SmtpClient();
                        smtp.Host = ConfigurationManager.AppSettings["SERVER_MAIL_MG"].ToString();

                        smtp.EnableSsl = true;

                        NetworkCredential NetworkCred = new NetworkCredential(from, password);

                        smtp.Credentials = NetworkCred;
                        smtp.Port = Convert.ToInt32(ConfigurationManager.AppSettings["PORT_MG"].ToString());
                        smtp.Send(mm);


                    }
                }
                catch (Exception ex)
                {
                    salida = ex.ToString();
                }
            });

            email.IsBackground = true;
            email.Start();
            return salida;
        }


        public static string envio_correo_attachment(string email_destinatario, string subject, StringBuilder sb, string nombre_correo,string file)
        {    //construct mail message

            try
            {
                var smtpSettings = Config.Get<SystemConfig>().SmtpSettings;
                
                MailMessage message = new MailMessage();
                message.From = new MailAddress(smtpSettings.DefaultSenderEmailAddress, nombre_correo);
                foreach (string email1 in email_destinatario.Split(',').Select(x => x.ToString()).ToList())
                {
                    message.To.Add(new MailAddress(email1));
                }
                message.Subject = subject;
                message.Body = sb.ToString();
                message.IsBodyHtml = true;
                message.BodyEncoding = Encoding.Unicode;
                message.SubjectEncoding = Encoding.Unicode;
                message.Attachments.Add(new Attachment(file));
                //send the notification
                EmailSender.Get().Send(message);
                return "";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }

        }
    }
}