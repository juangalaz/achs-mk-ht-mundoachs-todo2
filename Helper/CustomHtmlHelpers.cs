﻿using System.Web.Mvc;
using Telerik.Sitefinity.Frontend.Mvc.Helpers;
using Telerik.Sitefinity.Modules.Pages;

namespace SitefinityWebApp.Helper
{
    public static class CustomHtmlHelpers
    {
        public static MvcHtmlString DeferredScript(this HtmlHelper helper, ScriptRef scriptReference, string section)
        {
            if(string.IsNullOrEmpty(section))
            {
                section = null;
            }
            var markup = ResourceHelper.Script(helper, scriptReference, section, false, true);
            markup = new MvcHtmlString(markup.ToHtmlString().Replace("src", "defer src"));
            return markup;
        }

        public static MvcHtmlString DeferredScript(this HtmlHelper helper, string scriptPath,string section)
        {
            if (string.IsNullOrEmpty(section))
            {
                section = null;
            }
            var markup = ResourceHelper.Script(helper, scriptPath, section, false, true);
            markup = new MvcHtmlString(markup.ToHtmlString().Replace("src", "defer src"));
            return markup;
        }
    }
}