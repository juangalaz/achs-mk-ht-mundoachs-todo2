﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using Telerik.OpenAccess;
using Telerik.Sitefinity.DynamicModules;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Taxonomies;
using Telerik.Sitefinity.Taxonomies.Model;
using Telerik.Sitefinity.Utilities.TypeConverters;

using Telerik.Sitefinity.RelatedData;
using Telerik.Sitefinity.Libraries.Model;
using Telerik.Sitefinity.Versioning;
using System.Net;
using System.IO;
using Telerik.Sitefinity.Modules.Libraries;
using System.Drawing.Imaging;
using Telerik.Sitefinity.Lifecycle;
using Telerik.Sitefinity;
using System.Threading;
using System.Globalization;
using Telerik.Sitefinity.Data;
using System.Text.RegularExpressions;
using Telerik.Sitefinity.Security;
using Telerik.Sitefinity.Workflow;

namespace SitefinityWebApp.Helper
{
    public class MetodosImportar
    {

        public class NoticiaImportada
        {
            public string url_vieja { get; set; }
            public string url_nueva { get; set; }
            public string error { get; set; }

        }


        public static string Get400(string cuerpo)
        {

            cuerpo = Regex.Replace(cuerpo, "<.*?>", String.Empty);
            if (cuerpo.Length > 400)
            {
                cuerpo = cuerpo.Substring(0, 400);
                return cuerpo;
            }
            else
            {
                return cuerpo;
            }



        }

        private List<string> GetImagesInHTMLString(string htmlString)
        {
            List<string> images = new List<string>();
            string pattern = @"<(img)\b[^>]*>";

            Regex rgx = new Regex(pattern, RegexOptions.IgnoreCase);
            MatchCollection matches = rgx.Matches(htmlString);

            for (int i = 0, l = matches.Count; i < l; i++)
            {
                images.Add(matches[i].Value);
            }

            return images;
        }

        private static string get_unique_slug_url(Album album, string fileName)
        {
            List<string> existingUrlNames = album.ChildContentItems
                .Where(c => c.Status == ContentLifecycleStatus.Live && c.Visible)
                .Select(c => c.UrlName.ToString()).ToList();
            string safeTitle = fileName;
            while (existingUrlNames.Contains(safeTitle))
            {
                safeTitle += "-";
            }
            return safeTitle;
        }



        private static Album crete_albun(string title, string urlName)
        {
            LibrariesManager librariesManager = LibrariesManager.GetManager();
            Album album = librariesManager.GetAlbums().Where(a => a.UrlName == urlName).FirstOrDefault();

            if (album == null)
            {
                //Create the album.
                album = librariesManager.CreateAlbum(Guid.NewGuid());

                //Set the properties of the album.
                album.Title = title;
                album.DateCreated = DateTime.UtcNow;
                album.LastModified = DateTime.UtcNow;
                album.UrlName = urlName;

                //Recompiles and validates the url of the album.
                librariesManager.RecompileAndValidateUrls(album);

                //Save the changes.
                librariesManager.SaveChanges();
            }
            return album;
        }

        public static Image upload_imagen(string url_imagen, string nombre_album, string url_album)
        {
            string image_name = string.Empty;
            string image_ext = string.Empty;
            System.Drawing.Image img;
            try
            {

                WebClient wc = new WebClient();
                byte[] bytes = wc.DownloadData(url_imagen);
                MemoryStream ms = new MemoryStream(bytes);
                img = System.Drawing.Image.FromStream(ms);
                image_name = Path.GetFileNameWithoutExtension(url_imagen);

                image_ext = Path.GetExtension(url_imagen);
                image_ext = ".jpg";
            }
            catch
            {

                url_imagen = "http://lfi.lfi.cl/achs2020/achs.jpg";
                WebClient wc = new WebClient();
                byte[] bytes = wc.DownloadData(url_imagen);
                MemoryStream ms = new MemoryStream(bytes);
                img = System.Drawing.Image.FromStream(ms);
                image_name = Path.GetFileNameWithoutExtension(url_imagen);
                image_ext = Path.GetExtension(url_imagen);
            }


            //var cultureName = "es";
            //var cultureNameEN = "en";
            //Thread.CurrentThread.CurrentUICulture = new CultureInfo(cultureName);



            Album album = crete_albun(nombre_album, url_album);
            if (album != null)
            {
                LibrariesManager libraryManager = LibrariesManager.GetManager();

                image_name = get_unique_slug_url(album, image_name);
                Image image = libraryManager.CreateImage();
                image.Title = Common.remover_acentos(Regex.Replace(image_name.ToLower(), @"[^\w\-\!\$\'\(\)\=\@\d_]+", "-")); ;

                image.UrlName = Common.remover_acentos(Regex.Replace(image_name.ToLower(), @"[^\w\-\!\$\'\(\)\=\@\d_]+", "-")); ;

                image.MediaFileUrlName = Common.remover_acentos(Regex.Replace(image_name.ToLower(), @"[^\w\-\!\$\'\(\)\=\@\d_]+", "-")); ;

                image.Parent = album;
                image.Extension = image_ext;
                image.DateCreated = DateTime.UtcNow;
                image.PublicationDate = DateTime.UtcNow;
                image.LastModified = DateTime.UtcNow;
                libraryManager.RecompileItemUrls<Telerik.Sitefinity.Libraries.Model.Image>(image);
                libraryManager.Upload(image, ToStream(img, ImageFormat.Jpeg), image_ext);
                //Publish
                libraryManager.Lifecycle.Publish(image);

                libraryManager.SaveChanges();

                return image;
            }
            else
            {
                return null;
            }

            //var bag = new Dictionary<string, string>();
            //bag.Add("ContentType", typeof(Image).FullName);
            //WorkflowManager.MessageWorkflow(image.Id, typeof(Image), null, "Publish", false, bag);
        }

        private static Stream ToStream(System.Drawing.Image image, ImageFormat format)
        {
            var stream = new System.IO.MemoryStream();
            image.Save(stream, format);
            stream.Position = 0;
            return stream;
        }
    }
}