using System;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;
using SitefinityWebApp.Mvc.Models;

namespace SitefinityWebApp.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "Cifras", Title = "Cifras", SectionName = "HT")]
    public class HT_CifrasWidgetController : Controller
    {
        [Category("Propiedades")]
        [DisplayName("Texto")]
        public string texto { get; set; }

        [Category("Propiedades")]
        [DisplayName("Cifra")]
        public string cifra { get; set; }

        /// <summary>
        /// This is the default Action.
        /// </summary>
        public ActionResult Index()
        {
            ViewBag.texto = String.IsNullOrEmpty(this.texto) ? "Hospital de Trabajador" : this.texto;
            ViewBag.cifra = String.IsNullOrEmpty(this.cifra) ? "13" : this.cifra;
            return View("Default"); ;
        }
    }
}