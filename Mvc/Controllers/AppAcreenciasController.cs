using System;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;
using SitefinityWebApp.Mvc.Models;
using IdentityModel.Client;
using SitefinityWebApp.Helper;

namespace SitefinityWebApp.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "Acreencias", Title = "Acreencias", SectionName = "Aplicativo")]

    public class AppAcreenciasController : Controller
    {
       
        /// <summary>
        /// This is the default Action.
        /// </summary>
        public ActionResult Index()
        {
            return View("Default");
        }

        public ActionResult acreencias(string rut)
        {
            if (!String.IsNullOrEmpty(rut))
            {
                string dominio = Request.Url.Scheme + System.Uri.SchemeDelimiter + Request.Url.Host + (Request.Url.IsDefaultPort ? "" : ":" + Request.Url.Port);
                
                string WebApiEndPoint = $"{dominio}/api/acreencias/acreencias?$filter=contains(Rut,'{rut}')&$orderby=Fecha";
       
                var reponseHtml = MetodosApi.CallApiPublic<Acreencias>(WebApiEndPoint);
                var jsonResult = Json(reponseHtml.Value, JsonRequestBehavior.AllowGet);
       
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
            else
            {
                Acreencias a = new Acreencias();
                var jsonResult = Json(a.Value, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
        }

    }
}