using System;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;
using SitefinityWebApp.Mvc.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System.Collections.Generic;
using SitefinityWebApp.Helper;

namespace SitefinityWebApp.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "Modulo Requimiento", Title = "Modulo Requimiento", SectionName = "Aplicativo")]
    public class AppModuloRequimientoWidget1Controller : Controller
    {
        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [Category("String Properties")]
        public string Message { get; set; }

        /// <summary>
        /// This is the default Action.
        /// </summary>
        public ActionResult Index()
        {
            var model = new AppModuloRequimientoWidget1Model();
            if (string.IsNullOrEmpty(this.Message))
            {
                model.Message = "Hello, World!";
            }
            else
            {
                model.Message = this.Message;
            }

            return View("Default");
        }





        public JsonResult ValidateEmpresaACHS(string run)
        {
            string jsonFormatted = string.Empty;
            try
            {
                var client = new RestClient("https://api.achs.cl/normativateletrabajo?IP_RUT_EMPRESA=" + run.ToString().ToUpper());
                var request = new RestRequest(Method.GET);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                request.AddHeader("Ocp-Apim-Subscription-Key", "2115d938ac604743a402a363fa6f90b5");
                IRestResponse response = client.Execute(request);

                string data = response.Content;

                JObject joResponse = JObject.Parse(data);
                JObject ojObject = (JObject)joResponse["d"];
                JArray array = (JArray)ojObject["results"];
                List<Empresateletrabajo> lst = array.ToObject<List<Empresateletrabajo>>();
                jsonFormatted = JsonConvert.SerializeObject(lst, Newtonsoft.Json.Formatting.Indented);
            }
            catch (Exception ex)
            {
                jsonFormatted = ex.ToString();
            }

            return Json(jsonFormatted, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult insertar_sf(FormularioRequerimiento objinsert)
        {

            try
            {
                //objinsert.ambito = "01";
                //objinsert.tiporequerimiento = "01";
                //objinsert.TipoGestion = "01";


                var ambito = getambito(objinsert.ambito);
                var tipo = gettipoxambito(objinsert.ambito, objinsert.tiporequerimiento);
                var solicitud = gettiporequerimiento(objinsert.TipoGestion);


                Dictionary<string, string> dict = new Dictionary<string, string>();
                dict.Add("ambito", ambito);
                dict.Add("tipo", tipo);
                dict.Add("rutEmpresa", objinsert.rutempresa);
                dict.Add("razonSocial", objinsert.RazonSocial);
                dict.Add("segmento", objinsert.segmento);
                dict.Add("comuna", objinsert.comuna);
                dict.Add("nombreSolicitante", objinsert.NombreCompletoSolicitante);
                dict.Add("cargoSolicitante", objinsert.CargoSolicitante);
                dict.Add("emailSolicitante", objinsert.EmailSolicitante);
                dict.Add("telefonoSolicitante", objinsert.TelefonoSolicitante);
                dict.Add("NombreCompleto", objinsert.NombreCompleto);
                dict.Add("Cargo", objinsert.Cargo);
                dict.Add("Email", objinsert.Email);
                dict.Add("fono", objinsert.Telefono);
                dict.Add("detalleRequerimiento", objinsert.DetalleRequerimiento);
                dict.Add("tipoGestion", gettiporequerimiento(objinsert.TipoGestion));

                string salida = MetodosFormularios.insertar_formulario_return_id("sf_modulorequerimiento", dict, Request.UserHostAddress, null, "", "");

                var jsonResult = Json(salida, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            } catch (Exception ex)
            {
                var jsonResult = Json(ex.ToString(), JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
        }



        private string gettiporequerimiento(string tipo)
        {
            var nombretipo = "";
            switch (tipo)
            {
                case "01":
                    nombretipo = "Requerimiento";
                    break;
                case "02":
                    nombretipo = "Reclamo";
                    break;

            }
            return nombretipo;
        }
        private string gettipoxambito(string ambito, string tipo)
        {

            var nombretipo = "";
            if (ambito == "01")
            {
                switch (tipo)
                {
                    case "01":
                        nombretipo = "Asesor�a Prevencionista";
                        break;
                    case "02":
                        nombretipo = "Asesor�a de Especialista";
                        break;
                    case "03":
                        nombretipo = "Cursos de capacitaci�n";
                        break;
                    case "05":
                        nombretipo = "Campa�as preventivas";
                        break;
                    case "06":
                        nombretipo = "Materiales y se�aleticas";
                        break;
                    case "04":
                        nombretipo = "Otros";
                        break;
                }
            }
            else if (ambito == "02")
            {
                switch (tipo)
                {
                    case "01":
                        nombretipo = "Informaci�n de paciente";
                        break;
                    case "02":
                        nombretipo = "Calidad de atenci�n";
                        break;
                    case "03":
                        nombretipo = "Apelaci�n de d�as perdidos";
                        break;
                    case "05":
                        nombretipo = "Apelaci�n de calificaci�n";
                        break;
                    case "04":
                        nombretipo = "Otros";
                        break;
                }

            }
            else if (ambito == "03")
            {
                switch (tipo)
                {
                    case "01":
                        nombretipo = "Visita de ejecutivo/a";
                        break;
                    case "02":
                        nombretipo = "Informaci�n de subsidio";
                        break;
                    case "03":
                        nombretipo = "Actualizaci�n de datos";
                        break;
                    case "05":
                        nombretipo = "Certificados";
                        break;
                    case "07":
                        nombretipo = "Servicio Complementario";
                        break;
                    case "06":
                        nombretipo = "Servicio SEL";
                        break;
                    case "04":
                        nombretipo = "Otros";
                        break;
                }
            }
            else if (ambito == "04")
            {
                switch (tipo)
                {
                    case "05":
                        nombretipo = " Informaci�n proceso DS67";
                        break;
                    case "01":
                        nombretipo = "Solicitud de Simulaci�n de Tasa";
                        break;
                    case "02":
                        nombretipo = "Solicitud de Rebaja de Tasa";
                        break;
                    case "03":
                        nombretipo = "Apelaci�n a Resoluci�n";
                        break;
                    case "04":
                        nombretipo = "Otros";
                        break;

                }
            }
            return nombretipo;
        }
        private string getambito(string idambito)
        {
            var nombreambito = "";
            switch (idambito)
            {
                case "01":
                    nombreambito = "Preventivas";
                    break;
                case "02":
                    nombreambito = "Salud";
                    break;

                case "03":
                    nombreambito = "Comerciales";
                    break;
                case "04":
                    nombreambito = "DS67";
                    break;
            }
            return nombreambito;
        }



        public class FormularioRequerimiento
        {

            public string ambito { get; set; }
         
            public string tiporequerimiento { get; set; }
            public string rutempresa { get; set; }
            public string RazonSocial { get; set; }
            public string segmento { get; set; }
            public string comuna { get; set; }
            public string NombreCompletoSolicitante { get; set; }
            public string CargoSolicitante { get; set; }
            public string EmailSolicitante { get; set; }
            public string TelefonoSolicitante { get; set; }
            public string NombreCompleto { get; set; }
            public string Telefono { get; set; }
            public string Email { get; set; }
            public string DetalleRequerimiento { get; set; }
            public string Cargo { get; set; }
            public string TipoGestion { get; set; }

        }

        public class __metadata
        {
            public string type { get; set; }
            public string uri { get; set; }

        }
        public class Results1
        {
            public __metadata __metadata { get; set; }
            public string Key_Rut_Empresa { get; set; }
            public string BP_MATRIZ_FINANZAS { get; set; }
            public string RAZON_SOCIAL { get; set; }
            public string NOMBRE_FANTASIA { get; set; }
            public string SUBSEGMENTO { get; set; }
            public string TASA_VIGENTE { get; set; }
            public string CANTIDAD_TRABAJADORES_BP { get; set; }

            public string EMPRESA_VIG_DESDE { get; set; }
            public string EMPRESA_VIG_HASTA { get; set; }
            public string ESTATUS_EMPRESA_DESC { get; set; }
            public string TELEFONO_EMPRESA { get; set; }
            public string EMAIL_EMPRESA { get; set; }

            public string VIGENCIA_CONTACTO { get; set; }
            public string REGIONAL { get; set; }
            public string TERRITORIO { get; set; }
            public string REGION { get; set; }
            public string REGION_DESC { get; set; }
            public string COMUNA { get; set; }
            public string COMUNA_DESC { get; set; }
            public string CALLE { get; set; }
            public string NUMERO_CALLE { get; set; }
            public string DIRECCION_EMPRESA { get; set; }
            public string CIIU { get; set; }
            public string CIIU_DESC { get; set; }
            public string AGENCIA { get; set; }
            public string AGENCIA_DESC { get; set; }
            public string SEDE { get; set; }
            public string SEDE_DESC { get; set; }
            public string FORMA_JURIDICA { get; set; }
            public string FORMA_JURIDICA_DESC { get; set; }
            public string RAZON_SOCIAL_ { get; set; }
            public string RAZON_SOCIAL2 { get; set; }
            public string RAZON_SOCIAL3 { get; set; }
            public string RAZON_SOCIAL4 { get; set; }
            public string CRITICIDAD_EMPRESA { get; set; }
            public string SECTOR_EMP_PUB_PRIV { get; set; }
            public string SECTOR_EMP_PUB_PRIV_DESC { get; set; }
            public string SECTOR_ECONOMICO_ACHS { get; set; }
            public string SECTOR_ECONOMICO_ACHS_DESC { get; set; }
            public string RUBRO_ACHS { get; set; }
            public string RUBRO_ACHS_DESC { get; set; }
            public string TIENE_DEPTO_PREVENCION { get; set; }
            public string TIENE_DEPTO_PREVENCION_DESC { get; set; }
            public string NIVEL_PREVENTIVO { get; set; }
            public string NIVEL_PREVENTIVO_DESC { get; set; }
            public string TIENE_CAJA_COMPENSACION { get; set; }
            public string TIENE_CAJA_COMPENSACION_DESC { get; set; }
            public string MUTUALIDAD_ANTERIOR { get; set; }
            public string MUTUALIDAD_ANTERIOR_DESC { get; set; }
            public string COMITE_PARITARIO { get; set; }
            public string COMITE_PARITARIO_DESC { get; set; }

            public string VALIDA_EMAIL_CONTACTO { get; set; }
            public string EMPRESA_USUARIO_CORREO { get; set; }
            public string EMPRESA_INSTITUCION_CORREO { get; set; }
            public string EMPRESA_DOMINIO_CORREO { get; set; }
            public string VALIDA_EMAIL_EMPRESA { get; set; }

            public string VALIDA_TELEFONO_CONTACTO { get; set; }
            public string LARGO_TEL_EMPRESA { get; set; }
            public string VALIDA_TELEFONO_EMPRESA { get; set; }

            public string LOGICA_TEL_EMPRESA_SIGLA { get; set; }
            public string LOGICA_EMAIL_EMPRESA_SIGLA { get; set; }
            public string LOGICA_TEL_CONTACTO_SIGLA { get; set; }
            public string LOGICA_EMAIL_CONTACTO_SIGLA { get; set; }
            public string RUT_EMPRESA { get; set; }
            public int RN_CONTACTOS { get; set; }
            public int RN_MATRIZ_FINANZAS { get; set; }

        }

        public class Empresateletrabajo
        {
            public string BP_MATRIZ_FINANZAS { get; set; }
            public string RUT_EMPRESA { get; set; }
            public string RAZON_SOCIAL { get; set; }
            public string NOMBRE_FANTASIA { get; set; }
            public string SUBSEGMENTO { get; set; }
            public string TASA_VIGENTE { get; set; }
            public string CANTIDAD_TRABAJADORES_BP { get; set; }
            public string ULTIMA_MASA_COMERCIAL { get; set; }
            public string ULTIMO_PERIODO_COTIZADO { get; set; }
            public string EMPRESA_VIG_DESDE { get; set; }
            public string EMPRESA_VIG_HASTA { get; set; }
            public string ESTATUS_EMPRESA_DESC { get; set; }
            public string TELEFONO_EMPRESA { get; set; }
            public string EMAIL_EMPRESA { get; set; }
            public string BP_CONTACTO { get; set; }
            public string TIPO_CONTACTO { get; set; }
            public string VIGENCIA_CONTACTO { get; set; }
            public string CONTACTO_VIG_DESDE { get; set; }
            public string CONTACTO_VIG_HASTA { get; set; }
            public string NOMBRE_COMPLETO_CONTACTO { get; set; }
            public string RUT_CONTACTO { get; set; }
            public string SEXO_CONTACTO { get; set; }
            public string FEC_NACIMIENTO_CONTACTO { get; set; }
            public string EMAIL_CONTACTO { get; set; }
            public string TELEFONO_CONTACTO { get; set; }
            public string NOMBRES_CONTACTO { get; set; }
            public string APELLIDO_PATERNO_CONTACTO { get; set; }
            public string APELLIDO_MATERNO_CONTACTO { get; set; }
            public string BP_GRUPO_EMPRESA { get; set; }
            public string NOMBRE_GRUPO_EMP { get; set; }
            public string BP_CASA_MATRIZ_DATOS_COM { get; set; }
            public string REGIONAL { get; set; }
            public string TERRITORIO { get; set; }
            public string REGION { get; set; }
            public string REGION_DESC { get; set; }
            public string COMUNA { get; set; }
            public string COMUNA_DESC { get; set; }
            public string CALLE { get; set; }
            public string NUMERO_CALLE { get; set; }
            public string DIRECCION_EMPRESA { get; set; }
            public string CIIU { get; set; }
        }
        public class D
        {
            public IList<Results1> results { get; set; }

        }
        public class Application
        {
            public D d { get; set; }

        }


      

       
    }
}