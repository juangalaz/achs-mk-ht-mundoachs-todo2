using System;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;
using SitefinityWebApp.Mvc.Models;

namespace SitefinityWebApp.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "Afiliate Aqui Independiente", Title = "Afiliate Aqui Independiente", SectionName = "Aplicativo")]
    public class AppAfiliateAquiIndependienteWidget1Controller : Controller
    {
        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [Category("Propiedades")]
        [DisplayName("Exito")]
        public string exito { get; set; }


        /// <summary>
        /// This is the default Action.
        /// </summary>
        public ActionResult Index()
        {
            ViewBag.exito = String.IsNullOrEmpty(this.exito) ? "https://wa-qa-formulario-achs-sf.azurewebsites.net/test.aspx" : this.exito;


            return View("Default");
        }
    }
}