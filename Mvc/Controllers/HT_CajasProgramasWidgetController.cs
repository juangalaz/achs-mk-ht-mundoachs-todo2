using System;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;
using SitefinityWebApp.Mvc.Models;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Libraries.Model;
using SitefinityWebApp.Helper;

namespace SitefinityWebApp.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "Cajas Programas", Title = "Cajas Programas", SectionName = "HT")]
    [Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesigner(typeof(WidgetDesigners.HT_CajasProgramasWidgetDesigner1))]
    public class HT_CajasProgramasWidgetController : Controller
    {
        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [Category("Propiedades")]
        [DisplayName("Texto")]
        public string texto { get; set; }

        [Category("Propiedades")]
        [DisplayName("Lik")]
        public string link { get; set; }

        [Category("Propiedades")]
        [DisplayName("Fondo")]
        public Guid fondo { get; set; }


        [Category("Propiedades")]
        [DisplayName("Logo")]
        public Guid logo { get; set; }

        /// <summary>
        /// This is the default Action.
        /// </summary>
        public ActionResult Index()
        {
            ViewBag.texto = String.IsNullOrEmpty(this.texto) ? "Hospital de Trabajador" : this.texto;
            ViewBag.link = String.IsNullOrEmpty(this.link) ? "javascript:void(0)" : this.link;
            LibrariesManager librariesManager = LibrariesManager.GetManager();
            Image imageFieldFondo = librariesManager.GetImages().Where(i => i.Id == this.fondo).FirstOrDefault();
            ViewBag.fondo = imageFieldFondo != null ? imageFieldFondo.MediaUrl : "/ResourcePackages/HospitalDelTrabajador/assets/dist/images/img-pro1.png";

            Image imageFieldLogo = librariesManager.GetImages().Where(i => i.Id == this.logo).FirstOrDefault();
            ViewBag.logo = imageFieldLogo != null ? imageFieldLogo.MediaUrl : "/ResourcePackages/HospitalDelTrabajador/assets/dist/images/logo-pro1.png";



            return View("Default"); ;
        }
    }
}