using System;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;
using SitefinityWebApp.Mvc.Models;

namespace SitefinityWebApp.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "Acordeon Link", Title = "Acordeon Link", SectionName = "ACHS")]
    public class AcordeonConLinkWidgetController : Controller
    {

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [Category("Propiedades")]
        [DisplayName("Titulo")]
        public string titulo { get; set; }


        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [Category("Propiedades")]
        [DisplayName("Texto1")]
        public string texto1 { get; set; }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [Category("Propiedades")]
        [DisplayName("Link1")]
        public string Link1 { get; set; }




        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [Category("Propiedades")]
        [DisplayName("Texto2")]
        public string texto2 { get; set; }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [Category("Propiedades")]
        [DisplayName("Link2")]
        public string Link2 { get; set; }





        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [Category("Propiedades")]
        [DisplayName("Texto3")]
        public string texto3 { get; set; }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [Category("Propiedades")]
        [DisplayName("Link3")]
        public string Link3 { get; set; }


        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [Category("Propiedades")]
        [DisplayName("Texto4")]
        public string texto4 { get; set; }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [Category("Propiedades")]
        [DisplayName("Link4")]
        public string Link4 { get; set; }



        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [Category("Propiedades")]
        [DisplayName("Texto5")]
        public string texto5 { get; set; }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [Category("Propiedades")]
        [DisplayName("Link5")]
        public string Link5 { get; set; }



        /// <summary>
        /// This is the default Action.
        /// </summary>
        public ActionResult Index()
        {
            ViewBag.titulo = String.IsNullOrEmpty(this.titulo) ? "ACHS" : this.titulo;
            ViewBag.texto1 = String.IsNullOrEmpty(this.texto1) ? "Link ACHS" : this.texto1;
            ViewBag.Link1 = String.IsNullOrEmpty(this.Link1) ? "#" : this.Link1;



            ViewBag.texto2 = String.IsNullOrEmpty(this.texto2) ? "" : this.texto2;
            ViewBag.Link2 = String.IsNullOrEmpty(this.Link2) ? "#" : this.Link2;

            ViewBag.texto3 = String.IsNullOrEmpty(this.texto3) ? "" : this.texto3;
            ViewBag.Link3 = String.IsNullOrEmpty(this.Link3) ? "#" : this.Link3;

            ViewBag.texto4 = String.IsNullOrEmpty(this.texto4) ? "" : this.texto4;
            ViewBag.Link4 = String.IsNullOrEmpty(this.Link4) ? "#" : this.Link4;

            ViewBag.texto5 = String.IsNullOrEmpty(this.texto5) ? "" : this.texto5;
            ViewBag.Link5 = String.IsNullOrEmpty(this.Link5) ? "#" : this.Link5;

            return View("Default");
        }
    }
}