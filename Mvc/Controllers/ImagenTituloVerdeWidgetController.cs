using System;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;
using SitefinityWebApp.Mvc.Models;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Libraries.Model;

namespace SitefinityWebApp.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "Imagen Titulo Verde", Title = "Imagen Titulo Verde", SectionName = "ACHS")]
    [Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesigner(typeof(WidgetDesigners.ImagenTituloVerdeWidgetDesigner1))]
    public class ImagenTituloVerdeWidgetController : Controller
    {
        [Category("Propiedades")]
        [DisplayName("Texto")]
        public string texto { get; set; }


        [Category("Propiedades")]
        [DisplayName("Imagen")]
        public Guid imagen { get; set; }
        /// <summary>
        /// This is the default Action.
        /// </summary>
        public ActionResult Index()
        {
            ViewBag.texto = String.IsNullOrEmpty(this.texto) ? "ACHS" : this.texto;
            LibrariesManager librariesManager = LibrariesManager.GetManager();
            Image imageField = librariesManager.GetImages().Where(i => i.Id == this.imagen).FirstOrDefault();
            ViewBag.imagen = imageField != null ? imageField.MediaUrl : "/ResourcePackages/AchsCorporativo/assets/dist/images/achs1.png";
            return View("Default");
        }
    }
}