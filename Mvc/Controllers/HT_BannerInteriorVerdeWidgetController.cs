using System;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;
using SitefinityWebApp.Mvc.Models;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Libraries.Model;
using SitefinityWebApp.Helper;

namespace SitefinityWebApp.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "Banner Interior Verde", Title = "Banner Interior Verde", SectionName = "HT")]
    [Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesigner(typeof(WidgetDesigners.HT_BannerInteriorVerdeWidgetDesigner1))]
    public class HT_BannerInteriorVerdeWidgetController : Controller
    {
        protected override void HandleUnknownAction(string actionName)
        {
            Index().ExecuteResult(ControllerContext);
        }
        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [Category("Propiedades")]
        [DisplayName("Titulo")]
        public string titulo { get; set; }




        [Category("Propiedades")]
        [DisplayName("Imagen")]
        public Guid imagen { get; set; }


        /// <summary>
        /// This is the default Action.
        /// </summary>
        public ActionResult Index()
        {
            ViewBag.titulo = String.IsNullOrEmpty(this.titulo) ? "Hospital de Trabajador" : this.titulo;
            LibrariesManager librariesManager = LibrariesManager.GetManager();
            Image imageField = librariesManager.GetImages().Where(i => i.Id == this.imagen).FirstOrDefault();
            ViewBag.imagen = imageField != null ? imageField.MediaUrl : "/ResourcePackages/HospitalDelTrabajador/assets/dist/images/banner1.jpg";
            return View("Default"); ;
        }
    }
}