using System;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;
using SitefinityWebApp.Mvc.Models;

namespace SitefinityWebApp.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "Alerta", Title = "Alerta", SectionName = "ACHS")]
    [Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesigner(typeof(WidgetDesigners.AlertaWidgetDesigner1))]
    public class AlertaController : Controller
    {
        [Category("Propiedades")]
        [DisplayName("texto")]
        public string texto { get; set; }

        /// <summary>
        /// This is the default Action.
        /// </summary>
        public ActionResult Index()
        {

            ViewBag.texto = String.IsNullOrEmpty(this.texto) ? "<p>Inf�rmate sobre ... <a href='#'>AQU�.</a></p>" : this.texto;

            return View("Default");
        }
    }
}