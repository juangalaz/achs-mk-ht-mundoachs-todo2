using System;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;
using SitefinityWebApp.Mvc.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.Data;
using SitefinityWebApp.Helper;

namespace SitefinityWebApp.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "Mandato Digital", Title = "Mandato Digital", SectionName = "Aplicativo")]
    public class AppMandatoDigitalWidgetController : Controller
    {
        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [Category("String Properties")]
        public string Message { get; set; }

        /// <summary>
        /// This is the default Action.
        /// </summary>
        public ActionResult Index()
        {
           

            return View("Default");
        }

        [HttpPost]
        public JsonResult insertar_sf(FormularioMandatoDigital obj)
        {

            try
            {
                Dictionary<string, string> dict = new Dictionary<string, string>();



                dict.Add("nombre_rol", obj.nombre_rol);
                dict.Add("apellido_p_rol", obj.apellido_p_rol);
                dict.Add("apellido_m_rol", obj.apellido_m_rol);
                dict.Add("rut_rol", obj.rut_rol);
                dict.Add("cargo_rol", obj.cargo_rol);
                dict.Add("email_rol", obj.email_rol);
                //dict.Add("email_r_rol", obj.email_r_rol);
                dict.Add("movil_rol", obj.movil_rol);
                dict.Add("fijo_rol", obj.fijo_rol);
                dict.Add("direccion_rol", obj.direccion_rol);
                dict.Add("numeracion_rol", obj.numeracion_rol);
                dict.Add("comuna_rol", obj.comuna_rol);
                dict.Add("region_rol", obj.region_rol);
                dict.Add("rut_empresa", obj.rutempresa);
                dict.Add("nombre_representante", obj.nombre_representante);
                dict.Add("apellido_p_representante", obj.apellido_p_representante);
                dict.Add("apellido_m_representante", obj.apellido_m_representante);
                dict.Add("rut_representante", obj.rut_representante);
                dict.Add("cargo_representante", obj.cargo_representante);
                dict.Add("email_representante", obj.email_representante);
                //dict.Add("email_r_representante", obj.email_r_representante);
                dict.Add("movil_representante", obj.movil_representante);
                dict.Add("fijo_representante", obj.fijo_representante);
                dict.Add("direccion_representante", obj.direccion_representante);
                dict.Add("numeracion_representante", obj.numeracion_representante);
                dict.Add("comuna_representante", obj.comuna_representante);
                dict.Add("region_representante", obj.region_representante);
                string salida = MetodosFormularios.insertar_formulario_return_id("sf_mandatodigital", dict, Request.UserHostAddress, null, "", "");

                var jsonResult = Json(salida, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            } catch (Exception ex)
            {
                var jsonResult = Json(ex.ToString(), JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
        }

        public JsonResult ValidateEmpresaACHS(string run)
        {
            string jsonFormatted = string.Empty;
            try
            {
                var client = new RestClient("https://api.achs.cl/normativateletrabajo?IP_RUT_EMPRESA=" + run.ToString().ToUpper());
                var request = new RestRequest(Method.GET);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                request.AddHeader("Ocp-Apim-Subscription-Key", "2115d938ac604743a402a363fa6f90b5");
                IRestResponse response = client.Execute(request);

                string data = response.Content;

                JObject joResponse = JObject.Parse(data);
                JObject ojObject = (JObject)joResponse["d"];
                JArray array = (JArray)ojObject["results"];
                List<Empresateletrabajo> lst = array.ToObject<List<Empresateletrabajo>>();
                jsonFormatted = JsonConvert.SerializeObject(lst, Newtonsoft.Json.Formatting.Indented);

                Session["DATOSCLIENTE"] = lst;

                var datosCliente = Session["DATOSCLIENTE"] as List<Empresateletrabajo>;
            }
            catch (Exception ex)
            {
                jsonFormatted = ex.ToString();
            }

            return Json(jsonFormatted, JsonRequestBehavior.AllowGet);
        }


        public int validarUsuario(string run)
        {
            int res = 0;
            try
            {
                if (Session["DATOSCLIENTE"] != null)
                {
                    var datosCliente = Session["DATOSCLIENTE"] as List<Empresateletrabajo>;
                    res = datosCliente.Count();
                }
            }
            catch (Exception)
            {

                throw;
            }
            return res;
        }

        public JsonResult EsValidoDocumento(string run, string serie)
        {

            //Validaci�n de documentos

            //Inicializa Variables
            var auxautoriza = 0;
            var auxexiste = 0;
            var msj = "";

            //run = "183585967";
            //serie = "108809331";
            //Reemplaza dentro del RUT los guiones por vacio
            var runaux = run.Replace("-", "");
            var respuesta = "";
            try
            {


                //QA

                //URL del proceso de validaci�n standarizado
                //var client2 = new RestClient("https://ams-qa-midleware.azure-api.net/Pequifax/EntregaPreguntas");

                //PRODUCTIVO
                var client2 = new RestClient("https://api.achs.cl/Pequifax/EntregaPreguntas");
                var request2 = new RestRequest(Method.POST);
                request2.AddHeader("Content-Type", "application/json;charset=UTF-8");
                request2.AddHeader("Ocp-Apim-Subscription-Key", "2115d938ac604743a402a363fa6f90b5");
                //request2.AddHeader("Ocp-Apim-Subscription-Key", "4ee8e4bc367f43808807ef0669faaaee");

                //Confeccion del JSON que se envia en el body del METODO POST de la consulta
                request2.AddParameter("undefined", "{\"Rut\": \"" + runaux + "\",\"NroSerie\": \"" + serie + "\",\"NombreProyecto\": \"Mandato Digital\"}", ParameterType.RequestBody);
                IRestResponse response2 = client2.Execute(request2);

                respuesta = response2.Content;


                if (respuesta == "\"Resultado de verificacion rechazado\"" || respuesta == "\"Error de servicio Equifax\"" || respuesta == "\"Usuario sin cuestionario en sistema\"")
                {
                    if (respuesta == "\"Usuario sin cuestionario en sistema\"")
                    {
                        auxexiste = 1;
                        auxautoriza = 1;
                        msj = "";
                    }
                    else
                    {
                        auxexiste = -1;
                        auxautoriza = -1;
                        msj = "No se pudo verificar su identidad favor contactarse al 600 600 22 47.";
                    }
                }
                else
                {
                    //Primera Edici�n Si se obtiene Q se entiende como Validaci�n correcta
                    Dictionary<string, object> values2 = JsonConvert.DeserializeObject<Dictionary<string, object>>(response2.Content);

                    if (values2["transactionStatus"].ToString() == "Q")
                    {
                        //Si es validaci�n correcta se responde 1 existe y 1 autoriza
                        auxexiste = 1;
                        auxautoriza = 1;
                        msj = "";

                    }
                    else
                    {
                        //En el caso contrario se devuelven -1 y un mensaje de error
                        auxexiste = -1;
                        auxautoriza = -1;
                        msj = "No se pudo verificar su identidad favor contactarse al 600 600 22 47.";
                    }
                }

            }
            catch (Exception e)
            {
                //En el caso de cualquier error inesperado se deniega el acceso a la pagina y envia mensaje generico
                auxexiste = -1;
                auxautoriza = -1;
                msj = "No se pudo verificar su identidad favor contactarse al 600 600 22 47. | " + respuesta;
            }
            var estructurapregunta = respuesta;
            return Json(new { existe = auxexiste, autoriza = auxautoriza, msj = msj, estructura = respuesta }, JsonRequestBehavior.AllowGet);




        }
        public JsonResult GetRegiones()
        {

            List<RegionComuna> objlistregion = new List<RegionComuna>();
            using (SqlConnection conn = new SqlConnection(getConnection()))
            {

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT distinct cod_region,nom_region from mandato_region_comuna";

                    try
                    {
                        conn.Open();
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            RegionComuna obj = new RegionComuna();
                            obj.cod_region = int.Parse(reader["cod_region"].ToString());
                            obj.nom_region = reader["nom_region"].ToString();
                            objlistregion.Add(obj);
                        }
                    }
                    catch (SqlException e)
                    {

                    }

                }
                conn.Close();

            }
            var jsonFormatted = JsonConvert.SerializeObject(objlistregion, Formatting.None);
            return Json(new { regionlist = jsonFormatted }, JsonRequestBehavior.AllowGet);

        }
        public JsonResult GetComunaFilter(string codcomuna)
        {
            List<RegionComuna> objlistregion = new List<RegionComuna>();
            using (SqlConnection conn = new SqlConnection(getConnection()))
            {

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT distinct cod_comuna,nom_comuna from mandato_region_comuna where cod_region=" + codcomuna;

                    try
                    {
                        conn.Open();
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            RegionComuna obj = new RegionComuna();
                            obj.cod_comuna = int.Parse(reader["cod_comuna"].ToString());
                            obj.nom_comuna = reader["nom_comuna"].ToString();
                            objlistregion.Add(obj);
                        }
                    }
                    catch (SqlException e)
                    {

                    }

                }
                conn.Close();

            }
            var jsonFormatted = JsonConvert.SerializeObject(objlistregion, Formatting.None);
            return Json(new { comunalist = jsonFormatted }, JsonRequestBehavior.AllowGet);

        }
        public class EnvioRespuestas
        {
            public string p1 { get; set; }
            public string p2 { get; set; }
            public string p3 { get; set; }
            public string p4 { get; set; }
            public string tk { get; set; }

        }
        public JsonResult EnvioRespuesta(EnvioRespuestas objrespuesta)
        {
            var esvalido = -1;
            var msj = "";

            try
            {

                //QA

                //URL del proceso de validaci�n standarizado
                //var client2 = new RestClient("https://ams-qa-midleware.azure-api.net/Pequifax/ValidaRespuestas");

                //PRODUCTIVO
                var client2 = new RestClient("https://api.achs.cl/Pequifax/ValidaRespuestas");
                var request2 = new RestRequest(Method.POST);
                request2.AddHeader("Content-Type", "application/json;charset=UTF-8");
                request2.AddHeader("Ocp-Apim-Subscription-Key", "2115d938ac604743a402a363fa6f90b5");
                // request2.AddHeader("Ocp-Apim-Subscription-Key", "4ee8e4bc367f43808807ef0669faaaee");

                //Confeccion del JSON que se envia en el body del METODO POST de la consulta
                request2.AddParameter("undefined", "{\"iQAnswerRequest\": {\"interactiveQueryId\": 1,\"interactiveQueryResponse\": {\"answers\": [{\"answerId\":\"1\" ,\"questionId\":" + objrespuesta.p1 + "},{\"answerId\":\"2\" ,\"questionId\":" + objrespuesta.p2 + "},{\"answerId\":\"3\" ,\"questionId\":" + objrespuesta.p3 + "},{\"answerId\":\"4\" ,\"questionId\":" + objrespuesta.p4 + "}]},\"transactionKey\": \"" + objrespuesta.tk + "\"}}", ParameterType.RequestBody);
                IRestResponse response2 = client2.Execute(request2);

                var respuesta = response2.Content;
                if (respuesta == "\"Resultado de verificacion rechazado\"" || respuesta == "\"Error de servicio Equifax\"" || respuesta == "\"Usuario sin cuestionario en sistema\"" || respuesta == null || respuesta == "")
                {

                    esvalido = -1;
                    msj = "No se pudo verificar su identidad favor contactarse al 600 600 22 47.";

                }
                else
                {
                    esvalido = 1;
                    msj = "";
                }



            }
            catch (Exception e)
            {
                //En el caso de cualquier error inesperado se deniega el acceso a la pagina y envia mensaje generico
                esvalido = -1;

                msj = "No se pudo verificar su identidad favor contactarse al 600 600 22 47.";
            }

            return Json(new { esvalido = esvalido, msj = msj }, JsonRequestBehavior.AllowGet);


        }
        public class RegionComuna
        {
            public int cod_region { get; set; }
            public string nom_region { get; set; }
            public int cod_comuna { get; set; }
            public string nom_comuna { get; set; }
        }
        public class ListadoCargo
        {
            public string codigocargo { get; set; }
            public string cargo { get; set; }
        }
        private string getConnection()
        {
            //return @"Server=aplicativosachs.lfi.cl;Initial Catalog=bd_mandatodigitalachs;Persist Security Info=False;User ID=sa_mandatodigitalachs;Password=Nrgwwb5oBe;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=True;Connection Timeout=30;";
            string bd_mandato = "@"+Common.GetWebConfigKey("conexion_mandato");
            return bd_mandato;
        }
        private string GetListCargos(string cod)
        {
            List<ListadoCargo> objlist = new List<ListadoCargo>();
            ListadoCargo obj = new ListadoCargo();
            obj.codigocargo = "0001";
            obj.cargo = "Trabajador";
            objlist.Add(obj);

            ListadoCargo obj1 = new ListadoCargo();
            obj1.codigocargo = "0002";
            obj1.cargo = "Monitor en Prevenci�n de Riesgo";
            objlist.Add(obj1);

            ListadoCargo obj2 = new ListadoCargo();
            obj2.codigocargo = "0003";
            obj2.cargo = "Supervisor";
            objlist.Add(obj2);

            ListadoCargo obj3 = new ListadoCargo();
            obj3.codigocargo = "0004";
            obj3.cargo = "Subgerente de �rea";
            objlist.Add(obj3);

            ListadoCargo obj4 = new ListadoCargo();
            obj4.codigocargo = "0005";
            obj4.cargo = "Empleado Administrativo";
            objlist.Add(obj4);

            ListadoCargo obj5 = new ListadoCargo();
            obj5.codigocargo = "0006";
            obj5.cargo = "Jefe �rea";
            objlist.Add(obj5);

            ListadoCargo obj6 = new ListadoCargo();
            obj6.codigocargo = "0007";
            obj6.cargo = "Gerente �rea";
            objlist.Add(obj6);

            ListadoCargo obj7 = new ListadoCargo();
            obj7.codigocargo = "0008";
            obj7.cargo = "Gerente General";
            objlist.Add(obj7);

            ListadoCargo obj8 = new ListadoCargo();
            obj8.codigocargo = "0009";
            obj8.cargo = "Director";
            objlist.Add(obj8);

            ListadoCargo obj9 = new ListadoCargo();
            obj9.codigocargo = "0010";
            obj9.cargo = "Socio Empresa";
            objlist.Add(obj9);

            ListadoCargo obj10 = new ListadoCargo();
            obj10.codigocargo = "0011";
            obj10.cargo = "Prevencionista";
            objlist.Add(obj10);
            ListadoCargo obj11 = new ListadoCargo();
            obj11.codigocargo = "0012";
            obj11.cargo = "Dirigente";
            objlist.Add(obj11);
            ListadoCargo obj12 = new ListadoCargo();
            obj12.codigocargo = "0013";
            obj12.cargo = "Integrante CPHS";
            objlist.Add(obj12);
            ListadoCargo obj13 = new ListadoCargo();
            obj13.codigocargo = "0014";
            obj13.cargo = "Otro";
            objlist.Add(obj13);
            ListadoCargo obj14 = new ListadoCargo();
            obj14.codigocargo = "0030";
            obj14.cargo = "Comercial Autorizado";
            objlist.Add(obj14);
            ListadoCargo obj15 = new ListadoCargo();
            obj15.codigocargo = "0040";
            obj15.cargo = "Presidente Comit� Paritario";
            objlist.Add(obj15);
            ListadoCargo obj16 = new ListadoCargo();
            obj16.codigocargo = "0041";
            obj16.cargo = "Secretario Comit� Paritario";
            objlist.Add(obj16);
            ListadoCargo obj17 = new ListadoCargo();
            obj17.codigocargo = "0042";
            obj17.cargo = "Experto Empresa";
            objlist.Add(obj17);
            ListadoCargo obj18 = new ListadoCargo();
            obj18.codigocargo = "0043";
            obj18.cargo = "Encargado de Recursos Humanos";
            objlist.Add(obj18);
            ListadoCargo obj19 = new ListadoCargo();
            obj19.codigocargo = "0044";
            obj19.cargo = "Encargado de �rea";
            objlist.Add(obj19);

            ListadoCargo objsingle = objlist.Find(x => x.codigocargo == cod);
            return objsingle.cargo;
        }
        public JsonResult GetListCargos()
        {
            List<ListadoCargo> objlist = new List<ListadoCargo>();
            ListadoCargo obj = new ListadoCargo();
            obj.codigocargo = "0001";
            obj.cargo = "Trabajador";
            objlist.Add(obj);

            ListadoCargo obj1 = new ListadoCargo();
            obj1.codigocargo = "0002";
            obj1.cargo = "Monitor en Prevenci�n de Riesgo";
            objlist.Add(obj1);

            ListadoCargo obj2 = new ListadoCargo();
            obj2.codigocargo = "0003";
            obj2.cargo = "Supervisor";
            objlist.Add(obj2);

            ListadoCargo obj3 = new ListadoCargo();
            obj3.codigocargo = "0004";
            obj3.cargo = "Subgerente de �rea";
            objlist.Add(obj3);

            ListadoCargo obj4 = new ListadoCargo();
            obj4.codigocargo = "0005";
            obj4.cargo = "Empleado Administrativo";
            objlist.Add(obj4);

            ListadoCargo obj5 = new ListadoCargo();
            obj5.codigocargo = "0006";
            obj5.cargo = "Jefe �rea";
            objlist.Add(obj5);

            ListadoCargo obj6 = new ListadoCargo();
            obj6.codigocargo = "0007";
            obj6.cargo = "Gerente �rea";
            objlist.Add(obj6);

            ListadoCargo obj7 = new ListadoCargo();
            obj7.codigocargo = "0008";
            obj7.cargo = "Gerente General";
            objlist.Add(obj7);

            ListadoCargo obj8 = new ListadoCargo();
            obj8.codigocargo = "0009";
            obj8.cargo = "Director";
            objlist.Add(obj8);

            ListadoCargo obj9 = new ListadoCargo();
            obj9.codigocargo = "0010";
            obj9.cargo = "Socio Empresa";
            objlist.Add(obj9);

            ListadoCargo obj10 = new ListadoCargo();
            obj10.codigocargo = "0011";
            obj10.cargo = "Prevencionista";
            objlist.Add(obj10);
            ListadoCargo obj11 = new ListadoCargo();
            obj11.codigocargo = "0012";
            obj11.cargo = "Dirigente";
            objlist.Add(obj11);
            ListadoCargo obj12 = new ListadoCargo();
            obj12.codigocargo = "0013";
            obj12.cargo = "Integrante CPHS";
            objlist.Add(obj12);
            ListadoCargo obj13 = new ListadoCargo();
            obj13.codigocargo = "0014";
            obj13.cargo = "Otro";
            objlist.Add(obj13);
            ListadoCargo obj14 = new ListadoCargo();
            obj14.codigocargo = "0030";
            obj14.cargo = "Comercial Autorizado";
            objlist.Add(obj14);
            ListadoCargo obj15 = new ListadoCargo();
            obj15.codigocargo = "0040";
            obj15.cargo = "Presidente Comit� Paritario";
            objlist.Add(obj15);
            ListadoCargo obj16 = new ListadoCargo();
            obj16.codigocargo = "0041";
            obj16.cargo = "Secretario Comit� Paritario";
            objlist.Add(obj16);
            ListadoCargo obj17 = new ListadoCargo();
            obj17.codigocargo = "0042";
            obj17.cargo = "Experto Empresa";
            objlist.Add(obj17);
            ListadoCargo obj18 = new ListadoCargo();
            obj18.codigocargo = "0043";
            obj18.cargo = "Encargado de Recursos Humanos";
            objlist.Add(obj18);
            ListadoCargo obj19 = new ListadoCargo();
            obj19.codigocargo = "0044";
            obj19.cargo = "Encargado de �rea";
            objlist.Add(obj19);

            var jsonFormatted = JsonConvert.SerializeObject(objlist, Formatting.Indented);
            return Json(jsonFormatted, JsonRequestBehavior.AllowGet);


        }
        ////METODOS PARA GUARDAR EN LA BASE DE DATOS
        private void InsertBDMandatoDigital(FormatoClassRepresentante obj)
        {
            try
            {
                String ConStr = getConnection();
                using (SqlConnection conn = new SqlConnection(ConStr))
                {
                    var querystring = @"INSERT INTO [dbo].[mandato_digita_registroheleido]
           ([idregistrounico]
           ,[nombre_rol]
           ,[apellido_p_rol]
           ,[apellido_m_rol]
           ,[rut_rol]
           ,[cargo_rol]
           ,[email_rol]
           ,[email_r_rol]
           ,[movil_rol]
           ,[fijo_rol]
           ,[direccion_rol]
           ,[numeracion_rol]
           ,[comuna_rol]
           ,[region_rol]
           ,[rut_empresa]
           ,[nombre_representante]
           ,[apellido_p_representante]
           ,[apellido_m_representante]
           ,[rut_representante]
           ,[cargo_representante]
           ,[email_representante]
           ,[email_r_representante]
           ,[movil_representante]
           ,[fijo_representante]
           ,[direccion_representante]
           ,[numeracion_representante]
           ,[comuna_representante]
           ,[region_representante],[fecha_registroheleido])
     VALUES
           (@idregistrounico
           , @nombre_rol
           , @apellido_p_rol
           , @apellido_m_rol
           , @rut_rol
           , @cargo_rol
           , @email_rol
           , @email_r_rol
           , @movil_rol
           , @fijo_rol
           , @direccion_rol
           , @numeracion_rol
           , @comuna_rol
           , @region_rol
           , @rut_empresa
           , @nombre_representante
           , @apellido_p_representante
           , @apellido_m_representante
           , @rut_representante
           , @cargo_representante
           , @email_representante
           , @email_r_representante
           , @movil_representante
           , @fijo_representante
           , @direccion_representante
           , @numeracion_representante
           , @comuna_representante
           , @region_representante,@fecha_registroheleido)";
                    using (SqlCommand cmd = new SqlCommand(querystring, conn))
                    {
                        conn.Open();
                        cmd.Parameters.Clear();
                        cmd.Parameters.AddWithValue("@idregistrounico", Guid.NewGuid().ToString());
                        cmd.Parameters.AddWithValue("@nombre_rol", obj.nombre_rol);
                        cmd.Parameters.AddWithValue("@apellido_p_rol", obj.apellido_p_rol);
                        cmd.Parameters.AddWithValue("@apellido_m_rol", obj.apellido_m_rol);
                        cmd.Parameters.AddWithValue("@rut_rol", obj.rut_rol);
                        cmd.Parameters.AddWithValue("@cargo_rol", obj.cargo_rol);
                        cmd.Parameters.AddWithValue("@email_rol", obj.email_rol);
                        cmd.Parameters.AddWithValue("@email_r_rol", obj.email_r_rol);
                        cmd.Parameters.AddWithValue("@movil_rol", obj.movil_rol);
                        cmd.Parameters.AddWithValue("@fijo_rol", obj.fijo_rol);
                        cmd.Parameters.AddWithValue("@direccion_rol", obj.direccion_rol);
                        cmd.Parameters.AddWithValue("@numeracion_rol", obj.numeracion_rol);
                        cmd.Parameters.AddWithValue("@comuna_rol", obj.comuna_rol);
                        cmd.Parameters.AddWithValue("@region_rol", obj.region_rol);
                        cmd.Parameters.AddWithValue("@rut_empresa", obj.rutempresa);
                        cmd.Parameters.AddWithValue("@nombre_representante", obj.nombre_representante);
                        cmd.Parameters.AddWithValue("@apellido_p_representante", obj.apellido_p_representante);
                        cmd.Parameters.AddWithValue("@apellido_m_representante", obj.apellido_m_representante);
                        cmd.Parameters.AddWithValue("@rut_representante", obj.rut_representante);
                        cmd.Parameters.AddWithValue("@cargo_representante", obj.cargo_representante);
                        cmd.Parameters.AddWithValue("@email_representante", obj.email_representante);
                        cmd.Parameters.AddWithValue("@email_r_representante", obj.email_r_representante);
                        cmd.Parameters.AddWithValue("@movil_representante", obj.movil_representante);
                        cmd.Parameters.AddWithValue("@fijo_representante", obj.fijo_representante);
                        cmd.Parameters.AddWithValue("@direccion_representante", obj.direccion_representante);
                        cmd.Parameters.AddWithValue("@numeracion_representante", obj.numeracion_representante);
                        cmd.Parameters.AddWithValue("@comuna_representante", obj.comuna_representante);
                        cmd.Parameters.AddWithValue("@region_representante", obj.region_representante);
                        cmd.Parameters.AddWithValue("@fecha_registroheleido", DateTime.Now);
                        cmd.ExecuteNonQuery();
                    }
                    conn.Close();

                }
            }
            catch (SqlException ex)
            {
                ex.Message.ToString();
            }
            catch (Exception ex)
            {
                ex.Message.ToString();

            }

        }
        [HttpPost]
        public JsonResult AddFormularioRepresentante(FormatoClassRepresentante objformulario)
        {
            var returnok = "0";
            try
            {
                InsertBDMandatoDigital(objformulario);

                var linkdescarga = @"http://www.achs.cl/portal/Documents/mandato_digital/Mandato_%20Admin_ACHS%20Virtual.pdf";

                var client = new RestClient("https://ams-qa-midleware.azure-api.net/achsvirtual/api/mail/send");
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Ocp-Apim-Subscription-Key", "2b4beff42a5b433cad5bb1bc67b81fc3");
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("undefined", "{\"personalizations\": [{\"to\": [{\"email\": \"" + objformulario.email_rol + "\",\"name\":\"" + objformulario.nombre_rol + " " + objformulario.apellido_p_rol + "\"}],\"subject\": \"Asginaci�n de Adminstraci�n en  ACHS Virtual\",\"dynamic_template_data\": {\"nombreUsuario\": \"Estimado(a) " + objformulario.nombre_rol + " " + objformulario.apellido_p_rol + "\"}}],\"from\": {\"email\":\"automatizacion@achs.cl\",\"name\": \"Asociacion Chilena de Seguridad\"},\"template_id\": \"d-67433295d42342989ca191eda5ed5aca\"}", ParameterType.RequestBody); //correodelasginado
                IRestResponse response = client.Execute(request);



                var client2 = new RestClient("https://ams-qa-midleware.azure-api.net/achsvirtual/api/mail/send");
                var request2 = new RestRequest(Method.POST);
                request2.AddHeader("cache-control", "no-cache");
                request2.AddHeader("Ocp-Apim-Subscription-Key", "2b4beff42a5b433cad5bb1bc67b81fc3");
                request2.AddHeader("Content-Type", "application/json");
                request2.AddParameter("undefined", "{\"personalizations\": [{\"to\": [{\"email\": \"" + objformulario.email_r_representante + "\",\"name\":\"" + objformulario.nombre_representante + " " + objformulario.apellido_p_representante + "\"}],\"subject\": \"Confirmaci�n de Adminstraci�n en  ACHS Virtual\",\"dynamic_template_data\": {\"nombreUsuario\": \"Estimado(a) " + objformulario.nombre_representante + " " + objformulario.apellido_p_representante + "\",\"razonsocial\":\"" + objformulario.razonempresa + "\",\"rutempresa\":\"" + objformulario.rutempresa + "\",\"descargarArchivo\":\"" + linkdescarga + "\"}}],\"from\": {\"email\":\"automatizacion@achs.cl\",\"name\": \"Asociacion Chilena de Seguridad\"},\"template_id\": \"d-a0b2f5b31d6748e3a179052ed80ce0b2\"}", ParameterType.RequestBody); //correodelasginado
                IRestResponse response2 = client2.Execute(request2);
                returnok = "1";

            }
            catch (Exception r)
            {

                returnok = r.Message.ToString();
            }

            return Json(returnok, JsonRequestBehavior.AllowGet);
        }

        public class FormatoClassRepresentante
        {
            public string id { get; set; }
            public string idregistrounico { get; set; }
            public string nombre_rol { get; set; }
            public string apellido_p_rol { get; set; }
            public string apellido_m_rol { get; set; }
            public string rut_rol { get; set; }
            public string cargo_rol { get; set; }
            public string email_rol { get; set; }
            public string email_r_rol { get; set; }
            public string movil_rol { get; set; }
            public string fijo_rol { get; set; }
            public string direccion_rol { get; set; }
            public string numeracion_rol { get; set; }
            public string comuna_rol { get; set; }
            public string region_rol { get; set; }
            public string nombre_representante { get; set; }
            public string apellido_p_representante { get; set; }
            public string apellido_m_representante { get; set; }
            public string rut_representante { get; set; }
            public string cargo_representante { get; set; }
            public string email_representante { get; set; }
            public string email_r_representante { get; set; }
            public string movil_representante { get; set; }
            public string fijo_representante { get; set; }
            public string direccion_representante { get; set; }
            public string numeracion_representante { get; set; }
            public string comuna_representante { get; set; }
            public string region_representante { get; set; }
            public string razonempresa { get; set; }
            public string rutempresa { get; set; }
            public string bpmatrizfinanzas { get; set; }
            public string subsegmento { get; set; }

        }

        public class Empresateletrabajo
        {
            public string BP_MATRIZ_FINANZAS { get; set; }
            public string RUT_EMPRESA { get; set; }
            public string RAZON_SOCIAL { get; set; }
            public string NOMBRE_FANTASIA { get; set; }
            public string SUBSEGMENTO { get; set; }
            public string TASA_VIGENTE { get; set; }
            public string CANTIDAD_TRABAJADORES_BP { get; set; }
            public string ULTIMA_MASA_COMERCIAL { get; set; }
            public string ULTIMO_PERIODO_COTIZADO { get; set; }
            public string EMPRESA_VIG_DESDE { get; set; }
            public string EMPRESA_VIG_HASTA { get; set; }
            public string ESTATUS_EMPRESA_DESC { get; set; }
            public string TELEFONO_EMPRESA { get; set; }
            public string EMAIL_EMPRESA { get; set; }
            public string BP_CONTACTO { get; set; }
            public string TIPO_CONTACTO { get; set; }
            public string VIGENCIA_CONTACTO { get; set; }
            public string CONTACTO_VIG_DESDE { get; set; }
            public string CONTACTO_VIG_HASTA { get; set; }
            public string NOMBRE_COMPLETO_CONTACTO { get; set; }
            public string RUT_CONTACTO { get; set; }
            public string SEXO_CONTACTO { get; set; }
            public string FEC_NACIMIENTO_CONTACTO { get; set; }
            public string EMAIL_CONTACTO { get; set; }
            public string TELEFONO_CONTACTO { get; set; }
            public string NOMBRES_CONTACTO { get; set; }
            public string APELLIDO_PATERNO_CONTACTO { get; set; }
            public string APELLIDO_MATERNO_CONTACTO { get; set; }
            public string BP_GRUPO_EMPRESA { get; set; }
            public string NOMBRE_GRUPO_EMP { get; set; }
            public string BP_CASA_MATRIZ_DATOS_COM { get; set; }
            public string REGIONAL { get; set; }
            public string TERRITORIO { get; set; }
            public string REGION { get; set; }
            public string REGION_DESC { get; set; }
            public string COMUNA { get; set; }
            public string COMUNA_DESC { get; set; }
            public string CALLE { get; set; }
            public string NUMERO_CALLE { get; set; }
            public string DIRECCION_EMPRESA { get; set; }
            public string CIIU { get; set; }
        }


        public class FormularioMandatoDigital
        {
       
            public string nombre_rol { get; set; }
            public string apellido_p_rol { get; set; }
            public string apellido_m_rol { get; set; }
            public string rut_rol { get; set; }
            public string cargo_rol { get; set; }
            public string email_rol { get; set; }
            public string email_r_rol { get; set; }
            public string movil_rol { get; set; }
            public string fijo_rol { get; set; }
            public string direccion_rol { get; set; }
            public string numeracion_rol { get; set; }
            public string comuna_rol { get; set; }
            public string region_rol { get; set; }
            public string nombre_representante { get; set; }
            public string apellido_p_representante { get; set; }
            public string apellido_m_representante { get; set; }
            public string rut_representante { get; set; }
            public string cargo_representante { get; set; }
            public string email_representante { get; set; }
            public string email_r_representante { get; set; }
            public string movil_representante { get; set; }
            public string fijo_representante { get; set; }
            public string direccion_representante { get; set; }
            public string numeracion_representante { get; set; }
            public string comuna_representante { get; set; }
            public string region_representante { get; set; }
            public string razonempresa { get; set; }
            public string rutempresa { get; set; }
            public string bpmatrizfinanzas { get; set; }
            public string subsegmento { get; set; }
        }
    }
}