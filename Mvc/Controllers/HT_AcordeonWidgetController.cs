using System;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;
using SitefinityWebApp.Mvc.Models;

namespace SitefinityWebApp.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "Acordeon", Title = "Acordeon", SectionName = "HT")]
    [Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesigner(typeof(WidgetDesigners.AcordeonWidgetDesigner1))]

    public class HT_AcordeonWidgetController : Controller
    {
        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [Category("Propiedades")]
        [DisplayName("Titulo")]
        public string titulo { get; set; }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [Category("Propiedades")]
        [DisplayName("Contenido")]
        public string contenido { get; set; }


        /// <summary>
        /// This is the default Action.
        /// </summary>
        public ActionResult Index()
        {
            ViewBag.titulo = String.IsNullOrEmpty(this.titulo) ? "ACHS" : this.titulo;
            ViewBag.contenido = String.IsNullOrEmpty(this.contenido) ? "<p>ACHS</p>" : this.contenido;
            return View("Default");
        }
    }
}