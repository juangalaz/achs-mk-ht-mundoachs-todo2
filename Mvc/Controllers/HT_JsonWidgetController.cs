using System;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;
using SitefinityWebApp.Mvc.Models;
using System.Linq;
using System.Text.RegularExpressions;
using Telerik.Sitefinity.Modules.News;
using Telerik.Sitefinity.News.Model;
using Telerik.Sitefinity.Workflow;
using System.Collections.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Telerik.Sitefinity.News.Model;
using Telerik.Sitefinity.Workflow;
using Telerik.Sitefinity;
using SitefinityWebApp.Helper;
using Telerik.Sitefinity.Libraries.Model;
using Telerik.Sitefinity.RelatedData;
using System.Net;
using Newtonsoft.Json.Linq;
using Telerik.Sitefinity.Modules.Newsletters;
using Telerik.Sitefinity.Newsletters.Model;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.DynamicModules;
using Telerik.Sitefinity.Utilities.TypeConverters;
using Telerik.Sitefinity.Data.Linq.Dynamic;

using Telerik.Sitefinity;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.DynamicModules;
using Telerik.Sitefinity.Data.Linq.Dynamic;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Utilities.TypeConverters;
using Telerik.Sitefinity.Security;
using Telerik.Sitefinity.Lifecycle;
using Telerik.Sitefinity.Data;
using Telerik.Sitefinity.Versioning;

using Telerik.Sitefinity.Taxonomies;
using Telerik.Sitefinity.Taxonomies.Model;

using Telerik.Sitefinity.Modules.Libraries;

using Telerik.Sitefinity.Locations.Configuration;
using Telerik.Sitefinity.GeoLocations.Model;
using Telerik.Sitefinity.Configuration;
using Telerik.Sitefinity.Locations;
using RestSharp;
using System;
using System.Collections.Generic;

using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using IdentityModel.Client;

namespace SitefinityWebApp.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "Json", Title = "Json", SectionName = "HT")]
    public class HT_JsonWidgetController : Controller
    {
        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [Category("String Properties")]
        public string Message { get; set; }

        /// <summary>
        /// This is the default Action.
        /// </summary>
        public ActionResult Index()
        {
       

            return View("Default");
        }

        public ActionResult ImportarHT(string nombre_json)
        {
            string domainName = HttpContext.Request.Url.GetLeftPart(UriPartial.Authority);
            string RSSURL = domainName + $"/archivos_json_ht/{nombre_json}.json";
            List<MetodosImportar.NoticiaImportada> l = new List<MetodosImportar.NoticiaImportada>();
            WebClient wclient = new WebClient();
            string RSSData = wclient.DownloadString(RSSURL);
            var htmlData = wclient.DownloadData(RSSURL);
            string json = Encoding.UTF8.GetString(htmlData);
            JArray jsonArray = JArray.Parse(json);
            // dynamic data = JObject.Parse(jsonArray[0].ToString());
            IList<JToken> results = jsonArray.ToObject<IList<JToken>>();
            string providerName = "newsProvider2";
            providerName = "";
            foreach (JToken result in results)
            {
                string Title = "";
                string Fecha = "";
                string ContenidoPagina = "";
                string ImagenPagina = "";
                string Urlproductiva = "";
                MetodosImportar.NoticiaImportada noticia = new MetodosImportar.NoticiaImportada();

                try
                {

                    Guid masterNewsId = Guid.NewGuid();
                    Title = result["Title"].ToString();


                    //if (Title.Length > 1)
                    //{

                    //    Title = char.ToUpper(Title[0]) + Title.ToLower().Substring(1);
                    //    Title = Title.Replace("achs", "ACHS");
                    //    Title = Title.Replace("Achs", "ACHS");
                    //}

                    Fecha = result["Fecha"].ToString();
                    string Fecha_aux = result["Fecha"].ToString();

                    ContenidoPagina = "<div class='importado'>" + result["ContenidoPagina"].ToString() + "</div>";



                    ContenidoPagina = ContenidoPagina.Replace("src=\"/Comunidad/GuiaSalud/Salud/PublishingImages", "src=\"/img-noticia-ht/Comunidad/GuiaSalud/Salud/PublishingImages");
                    ContenidoPagina = ContenidoPagina.Replace("src=\"/Comunidad/Noticias/PublishingImages", "src=\"/img-noticia-ht/Comunidad/Noticias/PublishingImages");


                    ContenidoPagina = ContenidoPagina.Replace("src=\"/ht/Comunidad/GuiaSalud/Salud/PublishingImages", "src=\"/img-noticia-ht/Comunidad/GuiaSalud/Salud/PublishingImages");
                    ContenidoPagina = ContenidoPagina.Replace("src=\"/ht/Comunidad/Noticias/PublishingImages", "src=\"/img-noticia-ht/Comunidad/Noticias/PublishingImages");


                    ContenidoPagina = ContenidoPagina.Replace("src=\"/Comunidad/Noticias/Documents", "src=\"/img-noticia-ht/Comunidad/GuiaSalud/Salud/PublishingImages");
                    ContenidoPagina = ContenidoPagina.Replace("src=\"/ht/Comunidad/Noticias/Documents", "src=\"/img-noticia-ht/ht/Comunidad/GuiaSalud/Salud/PublishingImages");




                    ImagenPagina = "https://www.hospitaldeltrabajador.cl/ht/" + result["ImagenPagina"].ToString();
                    Urlproductiva = result["Url"].ToString();
                    if (Fecha.Split(' ').Count() > 0)
                    {
                        Fecha = Fecha.Split(' ')[0].ToString().Replace("-", "/");
                    }
                    else
                    {
                        Fecha = "01/01/2021";
                    }
                    DateTime fecha = new DateTime();
                    //try
                    //{
                    //    fecha = DateTime.ParseExact(Fecha, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                    //}
                    //catch
                    //{
                    //    string[] faux = Fecha.Split('/');
                    //    Fecha = faux[0].PadLeft(2, '0') + "/" + faux[1].PadLeft(2, '0') + "/" + faux[2];
                    //    fecha = DateTime.ParseExact(Fecha, "MM/dd/yyyy", CultureInfo.InvariantCulture);

                    //}

                    try
                    {
                        fecha = DateTime.ParseExact(Fecha, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    }
                    catch
                    {
                        string[] faux = Fecha.Split('/');
                        Fecha = faux[0].PadLeft(2, '0') + "/" + faux[1].PadLeft(2, '0') + "/" + faux[2];
                        fecha = DateTime.ParseExact(Fecha, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                    }



                    fecha = new DateTime(fecha.Year, fecha.Month, fecha.Day, 12, 0, 0);

                    var count = 0;
                    //App.WorkWith().NewsItems().Where(newsItem => newsItem.Id == masterNewsId).Count(out count);
                    string albun = nombre_json.ToLower().Replace("json", "");
                    Image imagen = MetodosImportar.upload_imagen(ImagenPagina, albun, albun);
                    if (count == 0 && imagen != null)
                    {


                        DateTime DateCreated = DateTime.SpecifyKind(fecha, DateTimeKind.Utc);
                        DateTime PublicationDate = DateTime.SpecifyKind(fecha.AddMinutes(15), DateTimeKind.Utc);
                        DateTime LastModified = DateTime.UtcNow;

                        string slug = Common.remover_acentos(Regex.Replace(Title.ToLower(), @"[^\w\-\!\$\'\(\)\=\@\d_]+", "-"));

                        //The news item is created as a master. The newsId is assigned to the master.
                        App.WorkWith().NewsItem().CreateNew(masterNewsId)
                        //Set the properties of the news item.
                        .Do(newsItem =>
                        {
                            newsItem.Title = Title;
                            newsItem.DateCreated = DateCreated;
                            newsItem.PublicationDate = PublicationDate;
                            newsItem.LastModified = LastModified;
                            newsItem.Content = ContenidoPagina;
                            newsItem.Summary = MetodosImportar.Get400(ContenidoPagina) + "...";

                            newsItem.UrlName = slug;
                            newsItem.CreateRelation(imagen, "Imagen");
                        })
                        //Save the changes.
                        .SaveChanges();

                        //Publish the news item. The published version acquires new ID.
                        var bag = new Dictionary<string, string>();
                        bag.Add("ContentType", typeof(NewsItem).FullName);
                        WorkflowManager.MessageWorkflow(masterNewsId, typeof(NewsItem), providerName, "Publish", false, bag);

                        App.WorkWith().NewsItems().Where(newsItem => newsItem.Id == masterNewsId).Count(out count);
                        string url = "";
                        if (count > 0)
                        {
                            //Modify the news item.
                            App.WorkWith().NewsItem(masterNewsId).CheckOut().Do(item =>
                            {
                                item.PublicationDate = PublicationDate;
                                item.Urls.Clear();
                                item.UrlName = slug;
                            }).CheckIn().SaveChanges();

                            //Publish the news item.
                            WorkflowManager.MessageWorkflow(masterNewsId, typeof(NewsItem), null, "Publish", false, bag);
                        }






                        noticia.url_vieja = Urlproductiva;
                        noticia.url_nueva = App.WorkWith().NewsItem(masterNewsId).CheckOut().Get().ItemDefaultUrl;
                        noticia.error = "";

                    }
                    else
                    {
                        noticia.url_vieja = Urlproductiva;
                        noticia.url_nueva = "";
                        noticia.error = "Imagen Null";
                    }
                }
                catch (Exception ex)
                {
                    noticia.url_vieja = Urlproductiva;
                    noticia.url_nueva = Fecha;
                    noticia.error = ex.ToString();
                }
                l.Add(noticia);
            }


            var jsonResult = Json(l, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
    }
}