using System;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;
using SitefinityWebApp.Mvc.Models;
using System.Collections.Generic;
using SitefinityWebApp.Helper;
using System.Text;
using Telerik.Sitefinity;

namespace SitefinityWebApp.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "Registro Interes Evento", Title = "Registro Interes Evento", SectionName = "MA")]
    public class MA_RegistroInteresEventoWidgetController : Controller
    {




        [HttpPost]
        public ActionResult Index(FormCollection f)
        {
            ViewBag.id = "";
            ViewBag.salida1 = "";
            ViewBag.error = "";
            try
            {


                string nombre_evento = f["nombre_evento"].ToString();
                string email = f["email"].ToString();
                string fecha = DateTime.Now.ToSitefinityUITime().ToString();

               

                    string dominio = Request.Url.Scheme + System.Uri.SchemeDelimiter + Request.Url.Host + (Request.Url.IsDefaultPort ? "" : ":" + Request.Url.Port);
                    StringBuilder sbCliente = new StringBuilder();
                    sbCliente.Append(System.IO.File.ReadAllText(HttpContext.Server.MapPath("~\\ResourcePackages\\MundoAchs\\EmailTemplate\\descuento\\index.html")));

                    sbCliente = sbCliente.Replace("{{dominio}}", dominio);
            

                    Dictionary<string, string> dict = new Dictionary<string, string>();
                    dict.Add("nombre_evento", nombre_evento);
                    dict.Add("email", email);
                    dict.Add("fecha", fecha);
                    string id = Common.insertar_formulario_return_id("sf_interesevento", dict, Request.UserHostAddress);
                    ViewBag.id = id;
                    string email_envio = "felipe.morales@lfi.cl," + email;
                    string email_nombre = Common.GetWebConfigKey("EMAIL_NOMBRE_DESCUENTO");
                    string email_subject = Common.GetWebConfigKey("EMAIL_ASUNTO_DESCUENTO");
                    //string salida1 = Common.envio_correo(email_envio, email_subject, sbCliente, email_nombre);
                    ViewBag.salida1 = "";

            }
            catch (Exception ex)
            {
                ViewBag.error = ex.ToString();
            }
            return View("Default");
        }

    }
}