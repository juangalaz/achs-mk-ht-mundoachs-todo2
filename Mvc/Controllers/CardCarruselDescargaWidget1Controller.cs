using System;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;
using SitefinityWebApp.Mvc.Models;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Libraries.Model;
using SitefinityWebApp.Helper;
namespace SitefinityWebApp.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "Carrusel Descarga", Title = "Carrusel Descarga", SectionName = "ACHS")]
    [Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesigner(typeof(WidgetDesigners.CardCarruselDescargaWidgetDesigner1))]
    public class CardCarruselDescargaWidget1Controller : Controller
    {
        [Category("Propiedades")]
        [DisplayName("Titulo")]
        public string titulo { get; set; }
        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [Category("Propiedades")]
        [DisplayName("Documento")]
        public Guid documento { get; set; }
        /// <summary>
        /// This is the default Action.
        /// </summary>
        public ActionResult Index()
        {
            ViewBag.titulo = String.IsNullOrEmpty(this.titulo) ? "Nombre Documento" : this.titulo;
            LibrariesManager librariesManager = LibrariesManager.GetManager();
            Document document = librariesManager.GetDocuments().Where(i => i.Id == this.documento).FirstOrDefault();
            ViewBag.document = document != null ? document.MediaUrl : "#";
            return View("Default");
        }
    }
}