using System;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;
using SitefinityWebApp.Mvc.Models;
using System.Collections.Generic;
using SitefinityWebApp.Helper;
using Telerik.Sitefinity.DynamicModules;
using Telerik.Sitefinity.Utilities.TypeConverters;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Data;
using Telerik.Sitefinity.Lifecycle;
using Telerik.Sitefinity.Versioning;

using System.Linq.Dynamic;

using Telerik.Sitefinity.RelatedData;
using Telerik.Sitefinity.Libraries.Model;
using Telerik.Sitefinity.Taxonomies;
using Telerik.Sitefinity.Taxonomies.Model;
using Telerik.OpenAccess;
using System.Text;

namespace SitefinityWebApp.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "Envio Descuento", Title = "Envio Descuento", SectionName = "MA")]

    public class MA_EnvioDeDescuentoWidgetController : Controller
    {
 


        [HttpPost]
        public ActionResult Index(FormCollection f)
        {
            ViewBag.id = "";
            ViewBag.salida1 = "";
            ViewBag.error = "";
            try
            {

            
            string nombre_completo = f["nombre_completo"].ToString();
            string rut1 = f["rut_1"].ToString();
            string rut2 = f["rut_2"].ToString();
            string rut = rut1 + "-" + rut2;
            string email = f["email"].ToString();
            string url_name = f["url_name"].ToString();

            Descuento descuento = new Descuento();

            descuento = MetodosMundoAchs.get_datos_descuento(url_name);
                if (descuento != null)
                {

                    string dominio = Request.Url.Scheme + System.Uri.SchemeDelimiter + Request.Url.Host + (Request.Url.IsDefaultPort ? "" : ":" + Request.Url.Port);
                    StringBuilder sbCliente = new StringBuilder();
                    sbCliente.Append(System.IO.File.ReadAllText(HttpContext.Server.MapPath("~\\ResourcePackages\\MundoAchs\\EmailTemplate\\descuento\\index.html")));

                    sbCliente = sbCliente.Replace("{{dominio}}", dominio);
                    sbCliente = sbCliente.Replace("{{textoDescuento}}", descuento.textoDescuento);
                    sbCliente = sbCliente.Replace("{{titleEmpresa}}", descuento.titleEmpresa);
                    sbCliente = sbCliente.Replace("{{nombre_completo}}", nombre_completo);
                    sbCliente = sbCliente.Replace("{{rut}}", rut);
                    sbCliente = sbCliente.Replace("{{mensajeEmail}}", descuento.mensajeEmail);
                    descuento.imagenEmpresa = "http://placeimg.com/143/143/any";
                    sbCliente = sbCliente.Replace("{{imagenEmpresa}}", descuento.imagenEmpresa);
                    sbCliente = sbCliente.Replace("{{summary}}", descuento.summary);


                    Dictionary<string, string> dict = new Dictionary<string, string>();
                    dict.Add("txt_mombre", nombre_completo);
                    dict.Add("txt_email", email);
                    dict.Add("txt_rut", rut);
                    dict.Add("txt_descuento", descuento.textoDescuento);
                    dict.Add("txt_nombre_empresa", descuento.titleEmpresa);
                    string id = Common.insertar_formulario_return_id("sf_descuentos", dict, Request.UserHostAddress);
                    ViewBag.id = id;
                    string email_envio = "felipe.morales@lfi.cl,"+email;
                    string email_nombre = Common.GetWebConfigKey("EMAIL_NOMBRE_DESCUENTO");
                    string email_subject = Common.GetWebConfigKey("EMAIL_ASUNTO_DESCUENTO");
                    string salida1 = Common.envio_correo(email_envio, email_subject, sbCliente, email_nombre);
                    ViewBag.salida1 = salida1;

                }
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.ToString();
            }
            return View("Default");
        }


        

        //public ActionResult Test(string urlName)
        //{


        //    var providerName = Helper.Common.GetWebConfigKey("ProviderDescuentos");
        //    TaxonomyManager taxonomyManager = TaxonomyManager.GetManager();
        //    var transactionName = "someTransactionName" + DateTime.Now.Ticks.ToString();

        //    DynamicModuleManager dynamicModuleManager = DynamicModuleManager.GetManager(providerName, transactionName);

        //    Type descuentoType = TypeResolutionService.ResolveType("Telerik.Sitefinity.DynamicTypes.Model.DescuentosMundoAchs.DescuentoMundoAchs");

        //    DynamicContent descuentoItem = dynamicModuleManager.GetDataItems(descuentoType).Where(dynItem => dynItem.UrlName == urlName && dynItem.Status == ContentLifecycleStatus.Live && dynItem.Visible == true).SingleOrDefault();

        //    string Title = descuentoItem.GetValue("Title") != null ? descuentoItem.GetValue("Title").ToString() : "";
        //    string Nombre = descuentoItem.GetValue("Nombre") != null ? descuentoItem.GetValue("Nombre").ToString() : "";

        //    string Summary = descuentoItem.GetValue("Summary") != null ? descuentoItem.GetValue("Summary").ToString() : "";
        //    DateTime? FechaInicio = descuentoItem.GetValue("FechaInicio") != null ? (DateTime?)descuentoItem.GetValue("FechaInicio") : null;
        //    DateTime? FechaTermino = descuentoItem.GetValue("FechaTermino") != null ? (DateTime?)descuentoItem.GetValue("FechaTermino") : null;

        //    string Detalle = descuentoItem.GetValue("Detalle") != null ? descuentoItem.GetValue("Detalle").ToString() : "";
        //    string InstruccionesDescuento = descuentoItem.GetValue("InstruccionesDescuento") != null ? descuentoItem.GetValue("InstruccionesDescuento").ToString() : "";
        //    string MensajeEmail = descuentoItem.GetValue("MensajeEmail") != null ? descuentoItem.GetValue("MensajeEmail").ToString() : "";
        //    string Restriccion = descuentoItem.GetValue("Restriccion") != null ? descuentoItem.GetValue("Restriccion").ToString() : "";
        //    //string Region = descuentoItem.GetValue("Region") != null ? descuentoItem.GetValue<ChoiceOption>("Region").PersistedValue : "";
        //    string LetraChica = descuentoItem.GetValue("LetraChica") != null ? descuentoItem.GetValue("LetraChica").ToString() : "";
        //    string TextoDescuento = descuentoItem.GetValue("TextoDescuento") != null ? descuentoItem.GetValue("TextoDescuento").ToString() : "";
        //    string ValorDescuento = descuentoItem.GetValue("ValorDescuento") != null ? descuentoItem.GetValue("ValorDescuento").ToString() : "";

        //    //FlatTaxon taxon = new FlatTaxon();
        //    //Guid gTaxon = new Guid();
        //    //gTaxon = descuentoItem.GetValue<TrackedList<Guid>>("categoriasdescuentos").SingleOrDefault();
        //    //taxon = taxonomyManager.GetTaxa<FlatTaxon>().Where(t => t.Id.Equals(g)).SingleOrDefault();
        //    //string categoriasdescuentos_texto = string.Empty;
        //    //string categoriasdescuentos_slug = string.Empty;
        //    //if (taxon != null)
        //    //{
        //    //    categoriasdescuentos_texto = taxon.Title;
        //    //    categoriasdescuentos_slug = taxon.UrlName.ToString();
        //    //}


        //    var empresaItem = descuentoItem.GetRelatedItems<DynamicContent>("Empresa").SingleOrDefault();
        //    string Title_Empresa = string.Empty;
        //    string RutEmpresa = string.Empty;
        //    string DireccionEmpresa = string.Empty;
        //    string TelefonoEmpresa = string.Empty;
        //    string SitioWebEmpresa = string.Empty;
        //    string InstagramEmpresa = string.Empty;
        //    string TwitterEmpresa = string.Empty;
        //    string FacebookEmpresa = string.Empty;
        //    string EmailEmpresa = string.Empty;
        //    string DescripcionEmpresa = string.Empty;
        //    string ImagenEmpresa = string.Empty;

        //    if (empresaItem!=null)
        //    {
        //        Title_Empresa = empresaItem.GetValue("Title") != null ? empresaItem.GetValue("Title").ToString() : "";
        //        RutEmpresa = empresaItem.GetValue("RutEmpresa") != null ? empresaItem.GetValue("RutEmpresa").ToString() : "";
        //        DireccionEmpresa = empresaItem.GetValue("DireccionEmpresa") != null ? empresaItem.GetValue("DireccionEmpresa").ToString() : "";
        //        TelefonoEmpresa = empresaItem.GetValue("TelefonoEmpresa") != null ? empresaItem.GetValue("TelefonoEmpresa").ToString() : "";
        //        SitioWebEmpresa = empresaItem.GetValue("SitioWebEmpresa") != null ? empresaItem.GetValue("SitioWebEmpresa").ToString() : "";
        //        InstagramEmpresa = empresaItem.GetValue("InstagramEmpresa") != null ? empresaItem.GetValue("InstagramEmpresa").ToString() : "";
        //        TwitterEmpresa = empresaItem.GetValue("TwitterEmpresa") != null ? empresaItem.GetValue("TwitterEmpresa").ToString() : "";
        //        FacebookEmpresa = empresaItem.GetValue("FacebookEmpresa") != null ? empresaItem.GetValue("FacebookEmpresa").ToString() : "";
        //        EmailEmpresa = empresaItem.GetValue("EmailEmpresa") != null ? empresaItem.GetValue("EmailEmpresa").ToString() : "";
        //        DescripcionEmpresa = empresaItem.GetValue("DescripcionEmpresa") != null ? empresaItem.GetValue("DescripcionEmpresa").ToString() : "";

        //        Image imageFieldEmpresa = empresaItem.GetRelatedItems<Image>("ImagenEmpresa").SingleOrDefault();
        //        ImagenEmpresa = imageFieldEmpresa != null ? imageFieldEmpresa.MediaUrl : string.Empty;
        //    }


        //    Image imageField = descuentoItem.GetRelatedItems<Image>("Imagen").SingleOrDefault();
        //    string imagen = imageField != null ? imageField.MediaUrl : string.Empty;

        //    ViewBag.Title = Title;
        //    ViewBag.Nombre = Nombre;
        //    ViewBag.Summary = Summary;
        //    ViewBag.FechaInicio = FechaInicio;
        //    ViewBag.FechaTermino = FechaTermino;
        //    ViewBag.Detalle = Detalle;
        //    ViewBag.InstruccionesDescuento = InstruccionesDescuento;
        //    ViewBag.MensajeEmail = MensajeEmail;
        //    ViewBag.Restriccion = Restriccion;
        //    //ViewBag.Region = Region;
        //    ViewBag.LetraChica = LetraChica;
        //    ViewBag.TextoDescuento = TextoDescuento;
        //    ViewBag.ValorDescuento = ValorDescuento;
        //    ViewBag.Imagen = imagen;

        //    ViewBag.Title_Empresa = Title_Empresa;
        //    ViewBag.RutEmpresa = RutEmpresa;
        //    ViewBag.DireccionEmpresa = DireccionEmpresa;
        //    ViewBag.TelefonoEmpresa = TelefonoEmpresa;
        //    ViewBag.SitioWebEmpresa = SitioWebEmpresa;
        //    ViewBag.InstagramEmpresa = InstagramEmpresa;
        //    ViewBag.TwitterEmpresa = TwitterEmpresa;
        //    ViewBag.FacebookEmpresa = FacebookEmpresa;
        //    ViewBag.EmailEmpresa = EmailEmpresa;
        //    ViewBag.DescripcionEmpresa = DescripcionEmpresa;
        //    ViewBag.ImagenEmpresa = ImagenEmpresa;




        //    try
        //    {

        //        dynamicModuleManager.Provider.SuppressSecurityChecks = true;

        //        DynamicContent descuentoItemUpdate = dynamicModuleManager.GetDataItem(descuentoType, descuentoItem.OriginalContentId);
        //        var versionManager = VersionManager.GetManager(null, transactionName);
        //        //// Create a version
        //        versionManager.CreateVersion(descuentoItemUpdate, true);

        //        // Then we check it out
        //        DynamicContent checkOutContentoneItem = dynamicModuleManager.Lifecycle.CheckOut(descuentoItemUpdate) as DynamicContent;



        //        string visitas = checkOutContentoneItem.GetValue("Visitas") != null ? checkOutContentoneItem.GetValue("Visitas").ToString() : "0";
        //        int vista_descuento = 0;
        //        try
        //        {
        //            vista_descuento = Convert.ToInt32(visitas) +  1;
        //            if (vista_descuento < 0)
        //            {
        //                vista_descuento = 0;
        //            }
        //        }
        //        catch
        //        {
        //            vista_descuento = 0;
        //        }
        //        checkOutContentoneItem.SetValue("Visitas", vista_descuento);



        //        // Now we need to check in, so the changes apply
        //        ILifecycleDataItem checkInContentoneItem = dynamicModuleManager.Lifecycle.CheckIn(checkOutContentoneItem);

        //        // Create a version
        //        versionManager.CreateVersion(checkInContentoneItem, false);

        //        //Finnaly we publish the item again
        //        dynamicModuleManager.Lifecycle.Publish(checkInContentoneItem);



        //        // Create a version and commit the transaction in order changes to be persisted to data store
        //        versionManager.CreateVersion(checkInContentoneItem, true);
        //        TransactionManager.CommitTransaction(transactionName);


              
        //        dynamicModuleManager.Provider.SuppressSecurityChecks = false;


        //    } catch (Exception ex)
        //    {
        //        ViewBag.Errror = ex.ToString();
        //    }
        //    return View("Test");

        //}




    }
}