using System;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;
using SitefinityWebApp.Mvc.Models;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Libraries.Model;
using SitefinityWebApp.Helper;


namespace SitefinityWebApp.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "Documento Compartir", Title = "Documento Compartir", SectionName = "ACHS")]
    [Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesigner(typeof(WidgetDesigners.DocumentoSharedWidgetDesigner1))]
    public class DocumentoSharedWidgetController : Controller
    {
        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [Category("Propiedades")]
        [DisplayName("Titulo")]
        public string titulo { get; set; }


        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [Category("Propiedades")]
        [DisplayName("Texto")]
        public string texto { get; set; }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [Category("Propiedades")]
        [DisplayName("Documento")]
        public Guid documento { get; set; }


        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [Category("Propiedades")]
        [DisplayName("Icono")]
        public Guid imagen { get; set; }


        /// <summary>
        /// This is the default Action.
        /// </summary>
        public ActionResult Index()
        {
            ViewBag.titulo = String.IsNullOrEmpty(this.titulo) ? "ACHS" : this.titulo;
            ViewBag.texto = String.IsNullOrEmpty(this.titulo) ? "Documento ACHS" : this.texto;

            LibrariesManager librariesManager = LibrariesManager.GetManager();
            Document document = librariesManager.GetDocuments().Where(i => i.Id == this.documento).FirstOrDefault();
            ViewBag.document = document != null ? document.MediaUrl : "#";

            Image imageField = librariesManager.GetImages().Where(i => i.Id == this.imagen).FirstOrDefault();
            ViewBag.imagen = imageField != null ? imageField.MediaUrl : "/ResourcePackages/AchsCorporativo/assets/dist/images/ic-doc.png";


            return View("Default");
        }
    }
}