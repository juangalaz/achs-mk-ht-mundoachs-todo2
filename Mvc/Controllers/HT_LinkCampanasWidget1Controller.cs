using System;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;
using SitefinityWebApp.Mvc.Models;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Libraries.Model;
using SitefinityWebApp.Helper;


namespace SitefinityWebApp.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "Link Campa�as", Title = "Link Campa�as", SectionName = "HT")]
    [Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesigner(typeof(WidgetDesigners.HT_LinkCampanasWidgetDesigner1))]
    public class HT_LinkCampanasWidget1Controller : Controller
    {
        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [Category("Propiedades")]
        [DisplayName("Titulo")]
        public string titulo { get; set; }


        [Category("Propiedades")]
        [DisplayName("Icono")]
        public Guid imagen { get; set; }

        [Category("Propiedades")]
        [DisplayName("Link")]
        public string link { get; set; }


        /// <summary>
        /// This is the default Action.
        /// </summary>
        public ActionResult Index()
        {
            ViewBag.titulo = String.IsNullOrEmpty(this.titulo) ? "Hospital de Trabajador" : this.titulo;
            ViewBag.alt = String.IsNullOrEmpty(this.titulo) ? "Hospital de Trabajador" : this.titulo;
            ViewBag.link = String.IsNullOrEmpty(this.link) ? "" : this.link;
            LibrariesManager librariesManager = LibrariesManager.GetManager();
            Image imageField = librariesManager.GetImages().Where(i => i.Id == this.imagen).FirstOrDefault();
            ViewBag.imagen = imageField != null ? imageField.MediaUrl : "/images/librariesprovider4/default-album/i1f49150c0ca144aa59dee9db58b8ea3fa.png?sfvrsn=a3851a9f_2";
            return View("Default"); ;
        }
    }
}