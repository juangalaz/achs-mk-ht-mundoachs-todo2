using System;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;
using SitefinityWebApp.Mvc.Models;
using SitefinityWebApp.Helper;
using Newtonsoft.Json;
using System.Collections.Generic;
using Telerik.Sitefinity.Security.Model;
using System.Text;
using static SitefinityWebApp.Helper.MetodosUsuarios;

namespace SitefinityWebApp.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "Listado Usuarios", Title = "Listado Usuarios", SectionName = "MK Registro")]
    public class MK_ListadoUsuariosWidgetController : Controller
    {

        public ActionResult Index()
        {
            ViewBag.lista = MetodosUsuarios.GetAllUsers2();
            ViewBag.listaroles = MetodosUsuarios.GetAllRoles();

            //     //

            ////   foreach (var item in ViewBag.lista)
            //   //  {
            // //        SitefinityProfile perfil = new SitefinityProfile();

            //         string id = item.id;
            //         perfil = 

            //         nombres.Add(perfil.FirstName +' '+perfil.LastName);
            //     }

            ViewBag.Perfiles = MetodosUsuarios.GetUserProfileByUserId("d729e964-1b92-41da-9666-4a48b25420f7");
            ViewBag.json = JsonConvert.SerializeObject(ViewBag.lista);

            return View("Default");
        }



        [HttpGet]
        public JsonResult listarUserJson()
        {

            List<UsuarioCompleto> resp = new List<UsuarioCompleto>();
            try
            {
                resp = MetodosUsuarios.GetAllUsers2();
            }
            catch (Exception)
            {
                return Json(resp, JsonRequestBehavior.AllowGet);

            }
            return Json(resp, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult desactivarUsuario(string username)
        {

            int resp = 0;
            try
            {
                resp = MetodosUsuarios.DesactivarUsuario(username);
                if (resp != 0)
                {
                    MetodosUsuarios.ValidarUsuario(username);
                    StringBuilder sbCliente = new StringBuilder();
                    sbCliente.Append(System.IO.File.ReadAllText(HttpContext.Server.MapPath("~\\ResourcePackages\\AchsMarketing\\EmailTemplate\\tu-acceso-a-marketing-ACHS-ha-sido-rechazado.html")));

                    string email_nombre = Common.GetWebConfigKey("EMAIL_NOMBRE_MK");     

                    Common.envio_correo(username, "Su acceso ha sido rechazado", sbCliente, email_nombre);

                }
            }
            catch (Exception)
            {
                return Json(resp, JsonRequestBehavior.AllowGet);

            }
            return Json(resp, JsonRequestBehavior.AllowGet);

        }


        [HttpPost]
        public JsonResult insertar_rol(string idRol, string username)
        {
            int resp = 0;
            try
            {
                List<string> listaroles = new List<string>();
                string rol1 = idRol;
                listaroles.Add(rol1);

                resp = MetodosUsuarios.AddUserToRoles(username, listaroles);
                if (resp != 0)
                {
                    MetodosUsuarios.ValidarUsuario(username);
                    StringBuilder sbCliente = new StringBuilder();
                    sbCliente.Append(System.IO.File.ReadAllText(HttpContext.Server.MapPath("~\\ResourcePackages\\AchsMarketing\\EmailTemplate\\tu-acceso-a-marketing-ACHS-ha-sido-aprobado.html")));

                    string email_nombre = Common.GetWebConfigKey("EMAIL_NOMBRE_MK");
                    string email_subject = Common.GetWebConfigKey("EMAIL_ASUNTO_MK");

                    Common.envio_correo(username, email_subject, sbCliente, email_nombre);

                }
            }
            catch (Exception)
            {
                return Json(resp, JsonRequestBehavior.AllowGet);

            }
            return Json(resp, JsonRequestBehavior.AllowGet);
        }

    }
}