using System;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;
using SitefinityWebApp.Mvc.Models;

namespace SitefinityWebApp.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "Titulo Bajda  Verde", Title = "Titulo Bajda Verde", SectionName = "ACHS")]
    public class TituloBajadaVerdeWidgetController : Controller
    {
        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [Category("Propiedades")]
        [DisplayName("Titulo")]
        public string titulo { get; set; }


        [Category("Propiedades")]
        [DisplayName("Bajada")]
        public string bajada { get; set; }

        /// <summary>
        /// This is the default Action.
        /// </summary>
        public ActionResult Index()
        {
            ViewBag.titulo = String.IsNullOrEmpty(this.titulo) ? "ACHS" : this.titulo;
            ViewBag.bajada = String.IsNullOrEmpty(this.titulo) ? "" : this.bajada;

            return View("Default");
        }
    }
}