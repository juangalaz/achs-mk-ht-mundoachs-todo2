using System;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;
using SitefinityWebApp.Mvc.Models;

namespace SitefinityWebApp.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "Estadistica ACHS", Title = "Estadistica ACHS", SectionName = "Aplicativo")]

    public class AppEstadisticaWidget1Controller : Controller
    {
     

        /// <summary>
        /// This is the default Action.
        /// </summary>
        public ActionResult Index()
        {
            var model = new AppEstadisticaWidget1Model();
           

            return View("Default");
        }


        public JsonResult getgraficoEmpresa()
        {

          


            return Json("Ok", JsonRequestBehavior.AllowGet);
        }
    }
}