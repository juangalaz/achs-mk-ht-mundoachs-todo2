using System;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;
using SitefinityWebApp.Mvc.Models;
using System.Web;
using System.Collections.Generic;
using SitefinityWebApp.Helper;

namespace SitefinityWebApp.Mvc.Controllers
{
    
    [ControllerToolboxItem(Name = "Comite Paritario", Title = "Comite Paritario", SectionName = "Aplicativo")]
    public class AppComiteParitarioWidget1Controller : Controller
    {
       

        /// <summary>
        /// This is the default Action.
        /// </summary>
        public ActionResult Index()
        {
          
            return View("Default");
        }


        [HttpPost]
        public JsonResult insertar_sf(FormCollection f)
        {




            try
            {


                string nombreEmpresa = f["nombreEmpresa"].ToString();
                string rutEmpresa = f["rutEmpresa"].ToString();
                string NroAdherenteACHS = f["NroAdherenteACHS"].ToString();
                string tipoCPHS = f["tipoCPHS"].ToString();
                string direccionCasaMatriz = f["direccionCasaMatriz"].ToString();
                string direccionSucursal = f["direccionSucursal"].ToString();
                string sector = f["sector"].ToString();
                string fechaConstitucion = f["fechaConstitucion"].ToString();
                string fechaUltimaRenovacion = f["fechaUltimaRenovacion"].ToString();
                string nombrePresidenteCHPS = f["nombrePresidenteCHPS"].ToString();
                string rutPresidenteCPHS = f["rutPresidenteCPHS"].ToString();
                string nombreSecretarioCPHS = f["nombreSecretarioCPHS"].ToString();
                string rutSecretaioCPHS = f["rutSecretaioCPHS"].ToString();
                string telefonoSecretarioCPHS = f["telefonoSecretarioCPHS"].ToString();

                string titularEmpresa1 = f["titularEmpresa1"].ToString();
                string titularEmpresa2 = f["titularEmpresa2"].ToString();
                string titularEmpresa3 = f["titularEmpresa3"].ToString();
                string suplenteEmpresa1 = f["suplenteEmpresa1"].ToString();
                string suplenteEmpresa2 = f["suplenteEmpresa2"].ToString();
                string suplenteEmpresa3 = f["suplenteEmpresa3"].ToString();
                string titularTrabajadores1 = f["titularTrabajadores1"].ToString();
                string titularTrabajadores2 = f["titularTrabajadores2"].ToString();
                string titularTrabajadores3 = f["titularTrabajadores3"].ToString();
                string suplenteTrabajadores1 = f["suplenteTrabajadores1"].ToString();
                string suplenteTrabajadores2 = f["suplenteTrabajadores2"].ToString();
                string suplenteTrabajadores3 = f["suplenteTrabajadores3"].ToString();
                string nombreExpertoAsesorACHS = f["nombreExpertoAsesorACHS"].ToString();
                string telefonoPresidenteCPHS = f["telefonoPresidenteCPHS"].ToString();
                string emailCorporativoComiteParitario = f["emailCorporativoComiteParitario"].ToString();
                string agencia = f["agencia"].ToString();
                string comuna = f["comuna"].ToString();
                string numeroCasaMatriz = f["numeroCasaMatriz"].ToString();
                string comunaCasaMatriz = f["comunaCasaMatriz"].ToString();
                string numeroSucursal = f["numeroSucursal"].ToString();
                string comunaSucursal = f["comunaSucursal"].ToString();








                HttpPostedFileBase file = Request.Files["file0"];


                Dictionary<string, string> dict = new Dictionary<string, string>();

                dict.Add("nombreEmpresa", nombreEmpresa);
                dict.Add("rutEmpresa", rutEmpresa);
                dict.Add("NroAdherenteACHS", NroAdherenteACHS);
                dict.Add("tipoCPHS", tipoCPHS);
                dict.Add("direccionCasaMatriz", direccionCasaMatriz);
                dict.Add("direccionSucursal", direccionSucursal);
                dict.Add("sector", sector);
                dict.Add("fechaConstitucion", fechaConstitucion);
                dict.Add("fechaUltimaRenovacion", fechaUltimaRenovacion);
                dict.Add("nombrePresidenteCHPS", nombrePresidenteCHPS);
                dict.Add("rutPresidenteCPHS", rutPresidenteCPHS);
                dict.Add("nombreSecretarioCPHS", nombreSecretarioCPHS);
                dict.Add("rutSecretaioCPHS", rutSecretaioCPHS);
                dict.Add("telefonoSecretarioCPHS", telefonoSecretarioCPHS);
                dict.Add("titularEmpresa1", titularEmpresa1);
                dict.Add("titularEmpresa2", titularEmpresa2);
                dict.Add("titularEmpresa3", titularEmpresa3);
                dict.Add("suplenteEmpresa1", suplenteEmpresa1);
                dict.Add("suplenteEmpresa2", suplenteEmpresa2);
                dict.Add("suplenteEmpresa3", suplenteEmpresa3);
                dict.Add("titularTrabajadores1", titularTrabajadores1);
                dict.Add("titularTrabajadores2", titularTrabajadores2);
                dict.Add("titularTrabajadores3", titularTrabajadores3);
                dict.Add("suplenteTrabajadores1", suplenteTrabajadores1);
                dict.Add("suplenteTrabajadores2", suplenteTrabajadores2);
                dict.Add("suplenteTrabajadores3", suplenteTrabajadores3);
                dict.Add("nombreExpertoAsesorACHS", nombreExpertoAsesorACHS);
                dict.Add("telefonoPresidenteCPHS", telefonoPresidenteCPHS);
                dict.Add("emailCorporativoComiteParitario", emailCorporativoComiteParitario);
                dict.Add("agencia", agencia);
                dict.Add("comuna", comuna);
                dict.Add("numeroCasaMatriz", numeroCasaMatriz);
                dict.Add("comunaCasaMatriz", comunaCasaMatriz);
                dict.Add("numeroSucursal", numeroSucursal);
                dict.Add("comunaSucursal", comunaSucursal);




                //string salida = "1";
                string salida = MetodosFormularios.insertar_formulario_return_id("sf_comiteparitario", dict, Request.UserHostAddress, file, "file0", "Comite Paritario");

                var jsonResult = Json(salida, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
            catch (Exception ex)
            {
                var jsonResult = Json(ex.ToString(), JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
        }
    }
}