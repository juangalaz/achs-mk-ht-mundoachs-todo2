using System;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;
using SitefinityWebApp.Mvc.Models;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Libraries.Model;
using SitefinityWebApp.Helper;

namespace SitefinityWebApp.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "Cajas Link Home", Title = "Cajas Link Home", SectionName = "HT")]
    [Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesigner(typeof(WidgetDesigners.HT_CajasLinkHomeWidgetDesigner1))]
    public class HT_CajasLinkHomeWidgetController : Controller
    {
        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [Category("Propiedades")]
        [DisplayName("Titulo")]
        public string titulo { get; set; }

   


        [Category("Propiedades")]
        [DisplayName("Imagen")]
        public Guid imagen { get; set; }

        [Category("Propiedades")]
        [DisplayName("Link")]
        public string link { get; set; }

   


        /// <summary>
        /// This is the default Action.
        /// </summary>
        public ActionResult Index()
        {
            ViewBag.titulo = String.IsNullOrEmpty(this.titulo) ? "Hospital de Trabajador" : Common.replace_n_to_br(this.titulo);
            ViewBag.alt = String.IsNullOrEmpty(this.titulo) ? "Hospital de Trabajador" : this.titulo;
            ViewBag.link = String.IsNullOrEmpty(this.link) ? "" : this.link;
            LibrariesManager librariesManager = LibrariesManager.GetManager();
            Image imageField = librariesManager.GetImages().Where(i => i.Id == this.imagen).FirstOrDefault();
            ViewBag.imagen = imageField != null ? imageField.MediaUrl : "/ResourcePackages/HospitalDelTrabajador/assets/dist/images/esp1.jpg";
            return View("Default"); ;
        }
    }
}