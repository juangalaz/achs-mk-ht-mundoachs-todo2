using System;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;
using SitefinityWebApp.Mvc.Models;
using SitefinityWebApp.Helper;
using System.Web.Security;
using System.Text;

namespace SitefinityWebApp.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "Formulario Registro", Title = "Formulario Registro", SectionName = "MK Registro")]
    public class MK_FormularioRegistroWidgetController : Controller
    {

        public ActionResult Index()
        {
            return View("Default");
        }

        [HttpPost]
        public JsonResult insertar_usuario(string nombre, string apellido, string email, string password)
        {
            int resp = 0;
            System.Web.Security.MembershipCreateStatus estado;


            try
            {



                estado = MetodosUsuarios.CreateUser(email, password, nombre, apellido, email, "", "", false);
                if (estado == MembershipCreateStatus.Success)
                {

                    StringBuilder sbCliente = new StringBuilder();
                    sbCliente.Append(System.IO.File.ReadAllText(HttpContext.Server.MapPath("~\\ResourcePackages\\AchsMarketing\\EmailTemplate\\bienvenido-a-la-pagina-de-marketing-ACHS.html")));

                    string email_nombre = Common.GetWebConfigKey("EMAIL_NOMBRE_MK");
                    string email_subject = Common.GetWebConfigKey("EMAIL_ASUNTO_MK");

                    Common.envio_correo(email, email_subject, sbCliente, email_nombre);

                    StringBuilder sbAdmin = new StringBuilder();
                    sbAdmin.Append(System.IO.File.ReadAllText(HttpContext.Server.MapPath("~\\ResourcePackages\\AchsMarketing\\EmailTemplate\\se-solicito-un-nuevo-acceso-a-marketing-ACHS.html")));
                    sbAdmin = sbAdmin.Replace("{{nombre}}", nombre + " " + apellido);
                    sbAdmin = sbAdmin.Replace("{{correo}}", email);

                    string email_subject_admin = Common.GetWebConfigKey("EMAIL_ASUNTO_MK_ADMIN");
                    string correo_admin = Common.GetWebConfigKey("CORREO_MK_ADMIN");

                    Common.envio_correo(correo_admin, email_subject_admin, sbAdmin, email_nombre);



                    resp = 1;


                }
            }
            catch (Exception)
            {
                return Json(resp, JsonRequestBehavior.AllowGet);

            }
            return Json(resp, JsonRequestBehavior.AllowGet);
        }

    }
}