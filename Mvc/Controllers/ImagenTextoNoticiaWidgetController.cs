using System;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;
using SitefinityWebApp.Mvc.Models;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Libraries.Model;
using SitefinityWebApp.Helper;

namespace SitefinityWebApp.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "Imgen/Texto Noticia", Title = "Imgen/Texto Noticia", SectionName = "ACHS")]
    [Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesigner(typeof(WidgetDesigners.ImagenTextoNoticiaWidgetDesigner1))]
    public class ImagenTextoNoticiaWidgetController : Controller
    {
        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [Category("Propiedades")]
        [DisplayName("Titulo")]
        public string titulo { get; set; }

        [Category("Propiedades")]
        [DisplayName("Texto")]
        public string texto { get; set; }


        [Category("Propiedades")]
        [DisplayName("Imagen")]
        public Guid imagen { get; set; }

        [Category("Propiedades")]
        [DisplayName("Link")]
        public string link { get; set; }

        [Category("Propiedades")]
        [DisplayName("Texto Link")]
        public string texto_link { get; set; }

        /// <summary>
        /// This is the default Action.
        /// </summary>
        public ActionResult Index()
        {
            ViewBag.titulo = String.IsNullOrEmpty(this.titulo) ? "ACHS" : this.titulo;
            ViewBag.texto = String.IsNullOrEmpty(this.texto) ? "ACHS" : Helper.Common.replace_n_to_br(this.texto);
            ViewBag.link = String.IsNullOrEmpty(this.link) ? "" : this.link;
            ViewBag.texto_link = String.IsNullOrEmpty(this.texto_link) ? "Ver m�s" : this.texto_link;


            LibrariesManager librariesManager = LibrariesManager.GetManager();
            Image imageField = librariesManager.GetImages().Where(i => i.Id == this.imagen).FirstOrDefault();
            ViewBag.imagen = imageField != null ? imageField.MediaUrl : "/ResourcePackages/AchsCorporativo/assets/dist/images/not1.jpg";
            return View("Default"); ;
        }
    }
}