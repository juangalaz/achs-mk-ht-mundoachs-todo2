using System;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;
using SitefinityWebApp.Mvc.Models;
using RestSharp;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace SitefinityWebApp.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "Teletrabajo", Title = "Teletrabajo ", SectionName = "Aplicativo")]
    public class AppTeleTrabajoWidget1Controller : Controller
    {
        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [Category("String Properties")]
        public string Message { get; set; }

        /// <summary>
        /// This is the default Action.
        /// </summary>
        public ActionResult Index()
        {
            var model = new AppTeleTrabajoWidget1Model();
           

            return View("Default");
        }


        public JsonResult validacion_Empresa_Achs(string run)
        {
            string respuesta = "";

            string jsonFormatted = string.Empty;

            var client = new RestClient("https://api.achs.cl/normativateletrabajo?IP_RUT_EMPRESA=" + run.ToString().ToUpper());
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Ocp-Apim-Subscription-Key", "2115d938ac604743a402a363fa6f90b5");
            IRestResponse response = client.Execute(request);

            string data = response.Content;

            JObject joResponse = JObject.Parse(data);
            JObject ojObject = (JObject)joResponse["d"];

            JArray array = (JArray)ojObject["results"];
            List<Results1> lst = array.ToObject<List<Results1>>();

            jsonFormatted = JsonConvert.SerializeObject(lst, Newtonsoft.Json.Formatting.Indented);


            if (array.Count >= 1)
            {
                //respuesta = 1;
                respuesta = lst[0].RAZON_SOCIAL + '|' + lst[0].VALIDA_EMAIL_CONTACTO;
            }
            else
            {
                //respuesta = 0;
                respuesta = "0";
            }


            return Json(respuesta, JsonRequestBehavior.AllowGet);




        }

        public JsonResult ValidateEmpresaACHS(string run)
        {
            string jsonFormatted = string.Empty;
            try
            {
                var client = new RestClient("https://api.achs.cl/normativateletrabajo?IP_RUT_EMPRESA=" + run.ToString().ToUpper());
                var request = new RestRequest(Method.GET);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                request.AddHeader("Ocp-Apim-Subscription-Key", "2115d938ac604743a402a363fa6f90b5");
                IRestResponse response = client.Execute(request);

                string data = response.Content;

                JObject joResponse = JObject.Parse(data);
                JObject ojObject = (JObject)joResponse["d"];
                JArray array = (JArray)ojObject["results"];
                List<Empresateletrabajo> lst = array.ToObject<List<Empresateletrabajo>>();
                jsonFormatted = JsonConvert.SerializeObject(lst, Newtonsoft.Json.Formatting.Indented);
            }
            catch (Exception ex)
            {
                jsonFormatted = ex.ToString();
            }

            return Json(jsonFormatted, JsonRequestBehavior.AllowGet);
        }
        public class __metadata
        {
            public string type { get; set; }
            public string uri { get; set; }

        }
        public class Results1
        {
            public __metadata __metadata { get; set; }
            public string Key_Rut_Empresa { get; set; }
            public string BP_MATRIZ_FINANZAS { get; set; }
            public string RAZON_SOCIAL { get; set; }
            public string NOMBRE_FANTASIA { get; set; }
            public string SUBSEGMENTO { get; set; }
            public string TASA_VIGENTE { get; set; }
            public string CANTIDAD_TRABAJADORES_BP { get; set; }

            public string EMPRESA_VIG_DESDE { get; set; }
            public string EMPRESA_VIG_HASTA { get; set; }
            public string ESTATUS_EMPRESA_DESC { get; set; }
            public string TELEFONO_EMPRESA { get; set; }
            public string EMAIL_EMPRESA { get; set; }

            public string VIGENCIA_CONTACTO { get; set; }
            public string REGIONAL { get; set; }
            public string TERRITORIO { get; set; }
            public string REGION { get; set; }
            public string REGION_DESC { get; set; }
            public string COMUNA { get; set; }
            public string COMUNA_DESC { get; set; }
            public string CALLE { get; set; }
            public string NUMERO_CALLE { get; set; }
            public string DIRECCION_EMPRESA { get; set; }
            public string CIIU { get; set; }
            public string CIIU_DESC { get; set; }
            public string AGENCIA { get; set; }
            public string AGENCIA_DESC { get; set; }
            public string SEDE { get; set; }
            public string SEDE_DESC { get; set; }
            public string FORMA_JURIDICA { get; set; }
            public string FORMA_JURIDICA_DESC { get; set; }
            public string RAZON_SOCIAL_ { get; set; }
            public string RAZON_SOCIAL2 { get; set; }
            public string RAZON_SOCIAL3 { get; set; }
            public string RAZON_SOCIAL4 { get; set; }
            public string CRITICIDAD_EMPRESA { get; set; }
            public string SECTOR_EMP_PUB_PRIV { get; set; }
            public string SECTOR_EMP_PUB_PRIV_DESC { get; set; }
            public string SECTOR_ECONOMICO_ACHS { get; set; }
            public string SECTOR_ECONOMICO_ACHS_DESC { get; set; }
            public string RUBRO_ACHS { get; set; }
            public string RUBRO_ACHS_DESC { get; set; }
            public string TIENE_DEPTO_PREVENCION { get; set; }
            public string TIENE_DEPTO_PREVENCION_DESC { get; set; }
            public string NIVEL_PREVENTIVO { get; set; }
            public string NIVEL_PREVENTIVO_DESC { get; set; }
            public string TIENE_CAJA_COMPENSACION { get; set; }
            public string TIENE_CAJA_COMPENSACION_DESC { get; set; }
            public string MUTUALIDAD_ANTERIOR { get; set; }
            public string MUTUALIDAD_ANTERIOR_DESC { get; set; }
            public string COMITE_PARITARIO { get; set; }
            public string COMITE_PARITARIO_DESC { get; set; }

            public string VALIDA_EMAIL_CONTACTO { get; set; }
            public string EMPRESA_USUARIO_CORREO { get; set; }
            public string EMPRESA_INSTITUCION_CORREO { get; set; }
            public string EMPRESA_DOMINIO_CORREO { get; set; }
            public string VALIDA_EMAIL_EMPRESA { get; set; }

            public string VALIDA_TELEFONO_CONTACTO { get; set; }
            public string LARGO_TEL_EMPRESA { get; set; }
            public string VALIDA_TELEFONO_EMPRESA { get; set; }

            public string LOGICA_TEL_EMPRESA_SIGLA { get; set; }
            public string LOGICA_EMAIL_EMPRESA_SIGLA { get; set; }
            public string LOGICA_TEL_CONTACTO_SIGLA { get; set; }
            public string LOGICA_EMAIL_CONTACTO_SIGLA { get; set; }
            public string RUT_EMPRESA { get; set; }
            public int RN_CONTACTOS { get; set; }
            public int RN_MATRIZ_FINANZAS { get; set; }

        }

        public class Empresateletrabajo
        {
            public string BP_MATRIZ_FINANZAS { get; set; }
            public string RUT_EMPRESA { get; set; }
            public string RAZON_SOCIAL { get; set; }
            public string NOMBRE_FANTASIA { get; set; }
            public string SUBSEGMENTO { get; set; }
            public string TASA_VIGENTE { get; set; }
            public string CANTIDAD_TRABAJADORES_BP { get; set; }
            public string ULTIMA_MASA_COMERCIAL { get; set; }
            public string ULTIMO_PERIODO_COTIZADO { get; set; }
            public string EMPRESA_VIG_DESDE { get; set; }
            public string EMPRESA_VIG_HASTA { get; set; }
            public string ESTATUS_EMPRESA_DESC { get; set; }
            public string TELEFONO_EMPRESA { get; set; }
            public string EMAIL_EMPRESA { get; set; }
            public string BP_CONTACTO { get; set; }
            public string TIPO_CONTACTO { get; set; }
            public string VIGENCIA_CONTACTO { get; set; }
            public string CONTACTO_VIG_DESDE { get; set; }
            public string CONTACTO_VIG_HASTA { get; set; }
            public string NOMBRE_COMPLETO_CONTACTO { get; set; }
            public string RUT_CONTACTO { get; set; }
            public string SEXO_CONTACTO { get; set; }
            public string FEC_NACIMIENTO_CONTACTO { get; set; }
            public string EMAIL_CONTACTO { get; set; }
            public string TELEFONO_CONTACTO { get; set; }
            public string NOMBRES_CONTACTO { get; set; }
            public string APELLIDO_PATERNO_CONTACTO { get; set; }
            public string APELLIDO_MATERNO_CONTACTO { get; set; }
            public string BP_GRUPO_EMPRESA { get; set; }
            public string NOMBRE_GRUPO_EMP { get; set; }
            public string BP_CASA_MATRIZ_DATOS_COM { get; set; }
            public string REGIONAL { get; set; }
            public string TERRITORIO { get; set; }
            public string REGION { get; set; }
            public string REGION_DESC { get; set; }
            public string COMUNA { get; set; }
            public string COMUNA_DESC { get; set; }
            public string CALLE { get; set; }
            public string NUMERO_CALLE { get; set; }
            public string DIRECCION_EMPRESA { get; set; }
            public string CIIU { get; set; }
        }
        public class D
        {
            public IList<Results1> results { get; set; }

        }
        public class Application
        {
            public D d { get; set; }

        }
        ////METODOS PARA GUARDAR EN LA BASE DE DATOS
    }
}