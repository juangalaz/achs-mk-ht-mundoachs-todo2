using System;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;
using SitefinityWebApp.Mvc.Models;

namespace SitefinityWebApp.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "Mas Info", Title = "Mas Info", SectionName = "ACHS")]
    [Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesigner(typeof(WidgetDesigners.MasInfoWidgetDesigner1))]
    public class MasInfoWidget1Controller : Controller
    {
        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [Category("Propiedades")]
        [DisplayName("Titulo")]
        public string titulo { get; set; }


  
        [Category("Propiedades")]
        [DisplayName("Texto")]
        public string texto { get; set; }


        [Category("Propiedades")]
        [DisplayName("Link")]
        public string link { get; set; }

        [Category("Propiedades")]
        [DisplayName("Texto Link")]
        public string texto_link { get; set; }




        public ActionResult Index()
        {

            ViewBag.titulo = String.IsNullOrEmpty(this.titulo) ? "ACHS" : this.titulo;
            ViewBag.texto = String.IsNullOrEmpty(this.texto) ? "ACHS" : Helper.Common.replace_n_to_br(this.texto);
            ViewBag.link = String.IsNullOrEmpty(this.link) ? "" : this.link;
            ViewBag.texto_link = String.IsNullOrEmpty(this.texto_link) ? "Ver m�s" : this.texto_link;
            
            return View("Default");
        }
    }
}