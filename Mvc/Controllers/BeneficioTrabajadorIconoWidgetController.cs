using System;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;
using SitefinityWebApp.Mvc.Models;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Libraries.Model;


namespace SitefinityWebApp.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "Beneficio Trabajador Icono", Title = "Beneficio Trabajador Icono", SectionName = "ACHS")]
    [Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesigner(typeof(WidgetDesigners.BeneficioTrabajadorIconoWidgetDesigner1))]
    public class BeneficioTrabajadorIconoWidgetController : Controller
    {


        [Category("Propiedades")]
        [DisplayName("Titulo")]
        public string titulo { get; set; }




        [Category("Propiedades")]
        [DisplayName("Link")]
        public string link { get; set; }

        [Category("Propiedades")]
        [DisplayName("Imagen")]
        public Guid imagen { get; set; }

        /// <summary>
        /// This is the default Action.
        /// </summary>
        public ActionResult Index()
        {

            ViewBag.titulo = String.IsNullOrEmpty(this.titulo) ? "ACHS" : this.titulo;
            ViewBag.link = String.IsNullOrEmpty(this.link) ? "" : this.link;

            LibrariesManager librariesManager = LibrariesManager.GetManager();
            Image imageField = librariesManager.GetImages().Where(i => i.Id == this.imagen).FirstOrDefault();
            ViewBag.imagen = imageField != null ? imageField.MediaUrl : "/ResourcePackages/AchsCorporativo/assets/dist/images/ic-b1.svg";

            return View("Default");
        }
    }
}