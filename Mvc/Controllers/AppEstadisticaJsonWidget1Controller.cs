using System;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;
using SitefinityWebApp.Mvc.Models;
using System.Collections.Generic;
using SitefinityWebApp.Helper;

namespace SitefinityWebApp.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "Estadistica Json", Title = "Estadistica Json", SectionName = "Aplicativo")]
    public class AppEstadisticaJsonWidget1Controller : Controller
    {
        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [Category("String Properties")]
        public string Message { get; set; }

        /// <summary>
        /// This is the default Action.
        /// </summary>
        public ActionResult Index()
        {
            var model = new AppEstadisticaJsonWidget1Model();
            if (string.IsNullOrEmpty(this.Message))
            {
                model.Message = "Hello, World!";
            }
            else
            {
                model.Message = this.Message;
            }

            return View("Default", model);
        }
        MetodosEstadistica est = new MetodosEstadistica();


        public JsonResult getNumeroEmpresas()
        {
            List<grafico_empresa> Gempresas = new List<grafico_empresa>();
            Gempresas = est.getNEmpresas();
            return Json(Gempresas, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getEvolucionTasaReposo()
        {
            List<grafico_evolucion_tasa> evotasa = new List<grafico_evolucion_tasa>();
            evotasa = est.getEvolucionTasaReposos();
            return Json(evotasa, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getValoresxIndustria()
        {
            List<grafico_tasas_sector> tasasector = new List<grafico_tasas_sector>();
            tasasector = est.getTasaSector();
            return Json(tasasector, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getParticipacion()
        {
            List<grafico_participacion> participacion = new List<grafico_participacion>();
            participacion = est.getParticipacion();
            return Json(participacion, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getNumeroTrabajadoresAfiliados()
        {
            List<graficos_hombre_mujer> hm = new List<graficos_hombre_mujer>();
            hm = est.getHombreMujer();
            return Json(hm, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getListadoSubsidios()
        {
            List<listasubsidio> listasubsidios = new List<listasubsidio>();
            listasubsidios = est.gestListadoSubsidios();
            return Json(listasubsidios, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getListadoPensiones()
        {
            List<listapensione> listapensiones = new List<listapensione>();
            listapensiones = est.getListadoPensiones();
            return Json(listapensiones, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getListadoIndemnizaciones()
        {
            List<listaindemnizacione> listaindemnizaciones = new List<listaindemnizacione>();
            listaindemnizaciones = est.getListadoIndemnizaciones();
            return Json(listaindemnizaciones, JsonRequestBehavior.AllowGet);
        }
    }
}