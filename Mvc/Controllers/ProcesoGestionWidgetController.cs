using System;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;
using SitefinityWebApp.Mvc.Models;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Libraries.Model;
using SitefinityWebApp.Helper;

namespace SitefinityWebApp.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "Proceso Gestion", Title = "Proceso Gestion", SectionName = "ACHS")]
    [Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesigner(typeof(WidgetDesigners.ProcesoGestionWidgetDesigner1))]
    public class ProcesoGestionWidgetController : Controller
    {
        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [Category("Propiedades")]
        [DisplayName("Titulo")]
        public string titulo { get; set; }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [Category("Propiedades")]
        [DisplayName("Link")]
        public string link { get; set; }


        [Category("Propiedades")]
        [DisplayName("Texto")]
        public string texto { get; set; }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [Category("Propiedades")]
        [DisplayName("Icono")]
        public Guid imagen { get; set; }

        /// <summary>
        /// This is the default Action.
        /// </summary>
        public ActionResult Index()
        {

            ViewBag.titulo = String.IsNullOrEmpty(this.titulo) ? "ACHS" : this.titulo;
            ViewBag.link = String.IsNullOrEmpty(this.link) ? "" : this.link;
            ViewBag.texto = String.IsNullOrEmpty(this.texto) ? "ACHS" : this.texto;
            LibrariesManager librariesManager = LibrariesManager.GetManager();
            Image imageField = librariesManager.GetImages().Where(i => i.Id == this.imagen).FirstOrDefault();
            ViewBag.imagen = imageField != null ? imageField.MediaUrl : "/ResourcePackages/AchsCorporativo/assets/dist/images/ic-g1.svg";
            return View("Default");
        }
    }
}