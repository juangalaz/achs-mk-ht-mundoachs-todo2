using System;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;
using SitefinityWebApp.Mvc.Models;
using System.Collections.Generic;
using SitefinityWebApp.Helper;
using Telerik.Sitefinity.DynamicModules;
using Telerik.Sitefinity.Utilities.TypeConverters;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Data;
using Telerik.Sitefinity.Lifecycle;
using Telerik.Sitefinity.Versioning;

namespace SitefinityWebApp.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "Formulario Exito", Title = "Formulario Exito", SectionName = "Capacitaciones")]
    public class CAP_FormularioExitoCapacitacionesWidgetController : Controller
    {
      
        /// <summary>
        /// This is the default Action.
        /// </summary>


            public ActionResult get_capacitaciones(string urlName)
        {
            string salida = "";
            try
            {
                var providerName = "dynamicProvider16";
                
                var transactionName = "someTransactionName";
                var versionManager = VersionManager.GetManager(null, transactionName);

                DynamicModuleManager dynamicModuleManager = DynamicModuleManager.GetManager(providerName, transactionName);

                Type capacitacionType = TypeResolutionService.ResolveType("Telerik.Sitefinity.DynamicTypes.Model.Capacitaciones.Capacitacion");
                DynamicContent vehiculoItem = dynamicModuleManager.GetDataItems(capacitacionType).Where(dynItem => dynItem.UrlName == urlName && dynItem.Status == ContentLifecycleStatus.Live && dynItem.Visible == true).SingleOrDefault();

                DynamicContent vehiculoItemUpdate = dynamicModuleManager.GetDataItem(capacitacionType, vehiculoItem.OriginalContentId);

                //// Create a version
                versionManager.CreateVersion(vehiculoItemUpdate, true);

                // Then we check it out
                DynamicContent checkOutContentoneItem = dynamicModuleManager.Lifecycle.CheckOut(vehiculoItemUpdate) as DynamicContent;

                string cupos_aux = checkOutContentoneItem.GetValue("Cupos") != null ? checkOutContentoneItem.GetValue("Cupos").ToString() : "0";
                int cupo = 0;
                try
                {
                    cupo = Convert.ToInt32(cupos_aux) - 1;
                    if (cupo < 0)
                    {
                        cupo = 0;
                    }
                }
                catch
                {
                    cupo = 0;
                }
                checkOutContentoneItem.SetValue("Cupos", cupo);


                // Now we need to check in, so the changes apply
                ILifecycleDataItem checkInContentoneItem = dynamicModuleManager.Lifecycle.CheckIn(checkOutContentoneItem);

                // Create a version
                versionManager.CreateVersion(checkInContentoneItem, false);

                //Finnaly we publish the item again
                dynamicModuleManager.Lifecycle.Publish(checkInContentoneItem);



                // Create a version and commit the transaction in order changes to be persisted to data store
                versionManager.CreateVersion(checkInContentoneItem, true);
                TransactionManager.CommitTransaction(transactionName);
            }catch (Exception ex)
            {
                salida = ex.ToString();
            }

            var jsonResult = Json(salida, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;


        }


        [HttpPost]
        public ActionResult Index(FormCollection f)
        {

            try
            {
                Dictionary<string, string> dict = new Dictionary<string, string>();
                string nombre = f["nombre"].ToString();
                string apellido = f["apellido"].ToString();
                string rut_solicitante = f["rut-solicitante"].ToString();
                string rut_empresa = f["rut-empresa"].ToString();
                string email = f["email"].ToString();
                string telefono = f["telefono"].ToString();
                string curso = f["curso"].ToString();
                string nombre_curso = f["nombre_curso"].ToString();
                string sector = f["sector"].ToString();
                string segmento = f["segmento"].ToString();
                string modalidad = f["modalidad"].ToString();
                string id = f["id"].ToString();
                string urlName = f["urlName"].ToString();
                string fecha = f["fecha"].ToString();
                get_capacitaciones(urlName);
                dict.Add("nombre", nombre);
                dict.Add("apellido", apellido);
                dict.Add("rut_solicitante", rut_solicitante);
                dict.Add("rut_empresa", rut_empresa);
                dict.Add("email", email);
                dict.Add("telefono", telefono);
                dict.Add("curso", nombre_curso);
                //dict.Add("nombre_curso", nombre_curso);
                dict.Add("sector", sector);
                dict.Add("segmento", segmento);
                dict.Add("modalidad", modalidad);
                string salida = MetodosFormularios.insertar_formulario_return_id("sf_registrocapacitaciones", dict, Request.UserHostAddress, null, "", "");
                ViewBag.error = "";
                ViewBag.salida = salida;
               


            } catch(Exception ex)
            {
                ViewBag.error = ex.ToString();
                ViewBag.salida = "";
            }
             return View("Default");

        }

    }
}