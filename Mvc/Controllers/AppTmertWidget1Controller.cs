using System;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;
using SitefinityWebApp.Mvc.Models;

namespace SitefinityWebApp.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "Tmert", Title = "Tmert", SectionName = "Aplicativo")]

    public class AppTmertWidget1Controller : Controller
    {


        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [Category("Propiedades")]
        [DisplayName("Instructivo ")]
        public string instructivo { get; set; }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [Category("Propiedades")]
        [DisplayName("Chequeo ")]
        public string chequeo { get; set; }


        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [Category("Propiedades")]
        [DisplayName("Planilla ")]
        public string planilla { get; set; }
        /// <summary>
        /// This is the default Action.
        /// </summary>
        public ActionResult Index()
        {

            ViewBag.chequeo = String.IsNullOrEmpty(this.chequeo) ? "/docs/librariesprovider2/tmert/lista_chequeo_tmert.pdf?sfvrsn=7f68e71a_2" : this.chequeo;
            ViewBag.instructivo = String.IsNullOrEmpty(this.instructivo) ? "/docs/librariesprovider2/tmert/instructivo_tmert.pdf?sfvrsn=30bc6160_2" : this.instructivo;
            ViewBag.planilla = String.IsNullOrEmpty(this.planilla) ? "/docs/librariesprovider2/tmert/herramienta_unica_tmertv.xlsx?sfvrsn=51f24071_2" : this.planilla;
            return View("Default");
        }
    }
}