using System;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;
using SitefinityWebApp.Mvc.Models;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Libraries.Model;
using SitefinityWebApp.Helper;


namespace SitefinityWebApp.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "Banner Chico", Title = "Banner Chico", SectionName = "HT")]
    [Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesigner(typeof(WidgetDesigners.HT_BannerChicoWidgetDesigner1))]
    public class Ht_BannerChicoWidgetController : Controller
    {
        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        protected override void HandleUnknownAction(string actionName)
        {
            Index().ExecuteResult(ControllerContext);
        }


        [Category("Propiedades")]
        [DisplayName("Imagen")]
        public Guid imagen { get; set; }

        [Category("Propiedades")]
        [DisplayName("Link")]
        public string link { get; set; }


        [Category("Propiedades")]
        [DisplayName("Padding Top")]
        public string padding_top { get; set; }

        [Category("Propiedades")]
        [DisplayName("Padding Bottom")]
        public string padding_bottom { get; set; }


        /// <summary>
        /// This is the default Action.
        /// </summary>
        public ActionResult Index()
        {
            ViewBag.link = String.IsNullOrEmpty(this.link) ? "" : this.link;
            padding_top = String.IsNullOrEmpty(this.padding_top) ? "0" : this.padding_top;
            padding_bottom = String.IsNullOrEmpty(this.padding_bottom) ? "0" : this.padding_bottom;
            LibrariesManager librariesManager = LibrariesManager.GetManager();
            Image imageField = librariesManager.GetImages().Where(i => i.Id == this.imagen).FirstOrDefault();
            ViewBag.imagen = imageField != null ? imageField.MediaUrl : "/images/librariesprovider4/default-album/banner-agendar.jpg?sfvrsn=a3851a9f_2";
            ViewBag.style = "";
            string style = "";
            if(!padding_top.Equals("0") && padding_bottom.Equals("0"))
            {
                style = "padding-top: " + padding_top;
            }
            else if (padding_top.Equals("0") && !padding_bottom.Equals("0"))
            {
                style = "padding-bottom: " + padding_bottom;

            } else if (!padding_top.Equals("0") && !padding_bottom.Equals("0"))
            {
                style = "padding-top: " + padding_top +"; padding-bottom: " + padding_bottom;
            }
            ViewBag.style = style;
            return View("Default"); ;
        }
    }
}