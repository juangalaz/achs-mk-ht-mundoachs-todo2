using System;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;
using SitefinityWebApp.Mvc.Models;

namespace SitefinityWebApp.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "Video Banner", Title = "Video Banner", SectionName = "ACHS")]
    public class VideoBannerWidget1Controller : Controller
    {
        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [Category("Propiedades")]
        [DisplayName("Youtube")]
        public string Youtube { get; set; }

        /// <summary>
        /// This is the default Action.
        /// </summary>
        public ActionResult Index()
        {
            string y = String.IsNullOrEmpty(this.Youtube) ? "https://www.youtube.com/watch?v=eP-mBP9TYvU" : this.Youtube;

            string embed = Helper.Common.UrlToEmbedCode(y);
            ViewBag.youtube = embed;

            return View("Default");
        }
    }
}