using System;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;
using SitefinityWebApp.Mvc.Models;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Libraries.Model;
using SitefinityWebApp.Helper;

namespace SitefinityWebApp.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "Iconos Consulta", Title = "Iconos Consulta", SectionName = "HT")]
    [Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesigner(typeof(WidgetDesigners.HT_IconoConsultaWidgetDesigner1))]
    public class HT_IconoConsultaWidgetController : Controller
    {
        [Category("Propiedades")]
        [DisplayName("Texto")]
        public string texto { get; set; }


        [Category("Propiedades")]
        [DisplayName("Imagen")]
        public Guid imagen { get; set; }

        /// <summary>
        /// This is the default Action.
        /// </summary>
        public ActionResult Index()
        {
            ViewBag.texto = String.IsNullOrEmpty(this.texto) ? "Hospital de Trabajador" : this.texto;
            LibrariesManager librariesManager = LibrariesManager.GetManager();
            Image imageField = librariesManager.GetImages().Where(i => i.Id == this.imagen).FirstOrDefault();
            ViewBag.imagen = imageField != null ? imageField.MediaUrl : "/ResourcePackages/HospitalDelTrabajador/assets/dist/images/ic-c2.svg";
            return View("Default");
        }
    }
}