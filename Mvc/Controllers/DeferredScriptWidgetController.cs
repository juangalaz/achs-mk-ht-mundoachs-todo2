using System;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;
using SitefinityWebApp.Mvc.Models;

namespace SitefinityWebApp.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "Deferred Script", Title = "Deferred Script", SectionName = "Helper")]
    public class DeferredScriptWidgetController : Controller
    {
        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [Category("Propiedades")]
        [DisplayName("JS")]
        public string js { get; set; }

        protected override void HandleUnknownAction(string actionName)
        {
            Index().ExecuteResult(ControllerContext);
        }


        /// <summary>
        /// This is the default Action.
        /// </summary>
        public ActionResult Index()
        {
            ViewBag.js = String.IsNullOrEmpty(this.js) ? "" : this.js;
            ViewBag.section = "";
            return View("Default");
        }
    }
}