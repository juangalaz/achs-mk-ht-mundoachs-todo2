using System;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;
using SitefinityWebApp.Mvc.Models;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Libraries.Model;
using SitefinityWebApp.Helper;

namespace SitefinityWebApp.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "Acordeon Archivo", Title = "Acordeon Archivo", SectionName = "ACHS")]
    [Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesigner(typeof(WidgetDesigners.AcordeonConArchivoWidgetDesigner5))]
    //[Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesigner(typeof(WidgetDesigners.AcordeonArchivoWidgetDesigner1))]
    public class AcordeonArchivoWidget1Controller : Controller
    {
        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [Category("Propiedades")]
        [DisplayName("Titulo")]
        public string titulo { get; set; }


        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [Category("Propiedades")]
        [DisplayName("Texto1")]
        public string texto { get; set; }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [Category("Propiedades")]
        [DisplayName("Documento1")]
        public Guid documento { get; set; }




        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [Category("Propiedades")]
        [DisplayName("Texto2")]
        public string texto2 { get; set; }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [Category("Propiedades")]
        [DisplayName("Documento2")]
        public Guid documento2 { get; set; }





        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [Category("Propiedades")]
        [DisplayName("Texto3")]
        public string texto3{ get; set; }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [Category("Propiedades")]
        [DisplayName("Documento3")]
        public Guid documento3 { get; set; }


        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [Category("Propiedades")]
        [DisplayName("Texto4")]
        public string texto4 { get; set; }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [Category("Propiedades")]
        [DisplayName("Documento4")]
        public Guid documento4 { get; set; }



        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [Category("Propiedades")]
        [DisplayName("Texto5")]
        public string texto5 { get; set; }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [Category("Propiedades")]
        [DisplayName("Documento5")]
        public Guid documento5 { get; set; }


        /// <summary>
        /// This is the default Action.
        /// </summary>
        public ActionResult Index()
        {
            ViewBag.titulo = String.IsNullOrEmpty(this.titulo) ? "ACHS" : this.titulo;
            ViewBag.texto = String.IsNullOrEmpty(this.texto) ? "Documento ACHS" : this.texto;
            LibrariesManager librariesManager = LibrariesManager.GetManager();
            Document document = librariesManager.GetDocuments().Where(i => i.Id == this.documento).FirstOrDefault();
            ViewBag.document = document != null ? document.MediaUrl : "#";


            ViewBag.texto2 = String.IsNullOrEmpty(this.texto2) ? "" : this.texto2;
            Document document2 = librariesManager.GetDocuments().Where(i => i.Id == this.documento2).FirstOrDefault();
            ViewBag.document2 = document2 != null ? document2.MediaUrl : "#";


            ViewBag.texto3 = String.IsNullOrEmpty(this.texto3) ? "" : this.texto3;
            Document document3 = librariesManager.GetDocuments().Where(i => i.Id == this.documento3).FirstOrDefault();
            ViewBag.document3 = document3 != null ? document3.MediaUrl : "#";

            ViewBag.texto4 = String.IsNullOrEmpty(this.texto4) ? "" : this.texto4;
            Document document4 = librariesManager.GetDocuments().Where(i => i.Id == this.documento4).FirstOrDefault();
            ViewBag.document4 = document4 != null ? document4.MediaUrl : "#";

            ViewBag.texto5= String.IsNullOrEmpty(this.texto5) ? "" : this.texto5;
            Document document5 = librariesManager.GetDocuments().Where(i => i.Id == this.documento5).FirstOrDefault();
            ViewBag.document5 = document5 != null ? document5.MediaUrl : "#";

            return View("Default");
        }
    }
}