using System;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;
using SitefinityWebApp.Mvc.Models;
using RestSharp;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Collections.Generic;
using SitefinityWebApp.Helper;
using System.Web;

namespace SitefinityWebApp.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "Pac", Title = "Pac", SectionName = "Aplicativo")]
    public class AppPacWidget1Controller : Controller
    {
    //    [Category("Propiedades")]
    //    [DisplayName("Servicio")]
    //    public string servicio { get; set; }


    //    [Category("Propiedades")]
    //    [DisplayName("key")]
    //    public string key { get; set; }

        /// <summary>
        /// This is the default Action.
        /// </summary>
        public ActionResult Index()
        {
           

            return View("Default");
        }



        [HttpPost]
        public JsonResult insertar_sf(FormCollection f)
        {

            try
            {
                //objinsert.ambito = "01";
                //objinsert.tiporequerimiento = "01";
                //objinsert.TipoGestion = "01";

                string TxtRut_Empresa = f["TxtRut_Empresa"].ToString();
                string TxtRazonsocialEmpresa = f["TxtRazonsocialEmpresa"].ToString();
                string TxtDireccionEmpresa = f["TxtDireccionEmpresa"].ToString();
                string TxtNumeroEmpresa = f["TxtNumeroEmpresa"].ToString();
                string DrpregionesEmpresa = f["DrpregionesEmpresa"].ToString();
                string ComunasEmpresas = f["ComunasEmpresas"].ToString();
                string FechaRecibo = f["FechaRecibo"].ToString();
                string NumeroResolucion = f["NumeroResolucion"].ToString();
                string TxtRutrepresentante = f["TxtRutrepresentante"].ToString();
                string TxtNombresrepresentante = f["TxtNombresrepresentante"].ToString();
                string TxtRut_ContactoEmpresa = f["TxtRut_ContactoEmpresa"].ToString();
                string TxtTelefonoContactoEmpresa = f["TxtTelefonoContactoEmpresa"].ToString();
                string TxtNombresContactoEmpresa = f["TxtNombresContactoEmpresa"].ToString();
                string TxtEmailContantoEmpresa = f["TxtEmailContantoEmpresa"].ToString();
                string TxtApellidosContactoEmpresa = f["TxtApellidosContactoEmpresa"].ToString();
                string TxtCargoContactoEmpresa = f["TxtCargoContactoEmpresa"].ToString();
                string TxtDireccionContactoEmpresa = f["TxtDireccionContactoEmpresa"].ToString();
                string TxtNumeroContactoEmpesa = f["TxtNumeroContactoEmpesa"].ToString();
                string DrpregionesContactoEmpresa = f["DrpregionesContactoEmpresa"].ToString();
                string ComunasContacto = f["ComunasContacto"].ToString();

                HttpPostedFileBase file = Request.Files["file0"];


                Dictionary<string, string> dict = new Dictionary<string, string>();
              
                dict.Add("TxtRut_Empresa", TxtRut_Empresa);
                dict.Add("TxtRazonsocialEmpresa", TxtRazonsocialEmpresa);
                dict.Add("TxtDireccionEmpresa", TxtDireccionEmpresa);
                dict.Add("TxtNumeroEmpresa", TxtNumeroEmpresa);
                dict.Add("DrpregionesEmpresa", DrpregionesEmpresa);
                dict.Add("ComunasEmpresas", ComunasEmpresas);
                dict.Add("FechaRecibo", FechaRecibo);
                dict.Add("NumeroResolucion", NumeroResolucion);
                dict.Add("TxtRutrepresentante", TxtRutrepresentante);
                dict.Add("TxtNombresrepresentante", TxtNombresrepresentante);
                dict.Add("TxtRut_ContactoEmpresa", TxtRut_ContactoEmpresa);
                dict.Add("TxtTelefonoContactoEmpresa", TxtTelefonoContactoEmpresa);
                dict.Add("TxtNombresContactoEmpresa", TxtNombresContactoEmpresa);
                dict.Add("TxtEmailContantoEmpresa", TxtEmailContantoEmpresa);
                dict.Add("TxtApellidosContactoEmpresa", TxtApellidosContactoEmpresa);
                dict.Add("TxtCargoContactoEmpresa", TxtCargoContactoEmpresa);
                dict.Add("TxtDireccionContactoEmpresa", TxtDireccionContactoEmpresa);
                dict.Add("TxtNumeroContactoEmpesa", TxtNumeroContactoEmpesa);
                dict.Add("DrpregionesContactoEmpresa", DrpregionesContactoEmpresa);
                dict.Add("ComunasContacto", ComunasContacto);





                string salida = MetodosFormularios.insertar_formulario_return_id("sf_pac", dict, Request.UserHostAddress, file, "file0", "PAC");

                var jsonResult = Json(salida, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
            catch (Exception ex)
            {
                var jsonResult = Json(ex.ToString(), JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
        }



        //public JsonResult ValidateEmpresaACHS(string run)
        //{

        //    string _servicio = String.IsNullOrEmpty(this.servicio) ? "https://api.achs.cl/normativateletrabajo" : this.servicio;
        //    string _key = String.IsNullOrEmpty(this.key) ? "2115d938ac604743a402a363fa6f90b5" : this.key;

        //    string jsonFormatted = string.Empty;
        //    try
        //    {
        //        var client = new RestClient($"{servicio}?IP_RUT_EMPRESA={run.ToString().ToUpper()}");
        //        var request = new RestRequest(Method.GET);
        //        request.AddHeader("cache-control", "no-cache");
        //        request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
        //        request.AddHeader("Ocp-Apim-Subscription-Key", key);
        //        IRestResponse response = client.Execute(request);

        //        string data = response.Content;

        //        JObject joResponse = JObject.Parse(data);
        //        JObject ojObject = (JObject)joResponse["d"];
        //        JArray array = (JArray)ojObject["results"];
        //        List<Empresateletrabajo> lst = array.ToObject<List<Empresateletrabajo>>();
        //        jsonFormatted = JsonConvert.SerializeObject(lst, Newtonsoft.Json.Formatting.Indented);
        //    }
        //    catch (Exception ex)
        //    {
        //        jsonFormatted = ex.ToString();
        //    }

        //    return Json(jsonFormatted, JsonRequestBehavior.AllowGet);
        //}

        //public class Empresateletrabajo
        //{
        //    public string BP_MATRIZ_FINANZAS { get; set; }
        //    public string RUT_EMPRESA { get; set; }
        //    public string RAZON_SOCIAL { get; set; }
        //    public string NOMBRE_FANTASIA { get; set; }
        //    public string SUBSEGMENTO { get; set; }
        //    public string TASA_VIGENTE { get; set; }
        //    public string CANTIDAD_TRABAJADORES_BP { get; set; }
        //    public string ULTIMA_MASA_COMERCIAL { get; set; }
        //    public string ULTIMO_PERIODO_COTIZADO { get; set; }
        //    public string EMPRESA_VIG_DESDE { get; set; }
        //    public string EMPRESA_VIG_HASTA { get; set; }
        //    public string ESTATUS_EMPRESA_DESC { get; set; }
        //    public string TELEFONO_EMPRESA { get; set; }
        //    public string EMAIL_EMPRESA { get; set; }
        //    public string BP_CONTACTO { get; set; }
        //    public string TIPO_CONTACTO { get; set; }
        //    public string VIGENCIA_CONTACTO { get; set; }
        //    public string CONTACTO_VIG_DESDE { get; set; }
        //    public string CONTACTO_VIG_HASTA { get; set; }
        //    public string NOMBRE_COMPLETO_CONTACTO { get; set; }
        //    public string RUT_CONTACTO { get; set; }
        //    public string SEXO_CONTACTO { get; set; }
        //    public string FEC_NACIMIENTO_CONTACTO { get; set; }
        //    public string EMAIL_CONTACTO { get; set; }
        //    public string TELEFONO_CONTACTO { get; set; }
        //    public string NOMBRES_CONTACTO { get; set; }
        //    public string APELLIDO_PATERNO_CONTACTO { get; set; }
        //    public string APELLIDO_MATERNO_CONTACTO { get; set; }
        //    public string BP_GRUPO_EMPRESA { get; set; }
        //    public string NOMBRE_GRUPO_EMP { get; set; }
        //    public string BP_CASA_MATRIZ_DATOS_COM { get; set; }
        //    public string REGIONAL { get; set; }
        //    public string TERRITORIO { get; set; }
        //    public string REGION { get; set; }
        //    public string REGION_DESC { get; set; }
        //    public string COMUNA { get; set; }
        //    public string COMUNA_DESC { get; set; }
        //    public string CALLE { get; set; }
        //    public string NUMERO_CALLE { get; set; }
        //    public string DIRECCION_EMPRESA { get; set; }
        //    public string CIIU { get; set; }
        //}
    }
}