using System;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;
using SitefinityWebApp.Mvc.Models;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Libraries.Model;
using SitefinityWebApp.Helper;

namespace SitefinityWebApp.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "Link Universidad", Title = "Link Universidad", SectionName = "HT")]
    [Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesigner(typeof(WidgetDesigners.HT_LinkUniversidadWidgetDesigner1))]
    public class HT_LinkUniversidadWidgetController : Controller
    {
        [Category("Propiedades")]
        [DisplayName("Link")]
        public string link { get; set;}

            [Category("Propiedades")]
            [DisplayName("Alt")]
            public string alt { get; set; }

        [Category("Propiedades")]
        [DisplayName("Imagen")]
        public Guid imagen { get; set; }

        /// <summary>
        /// This is the default Action.
        /// </summary>
        public ActionResult Index()
        {
            ViewBag.link = String.IsNullOrEmpty(this.link) ? "javascript:void(0)" : this.link;
            ViewBag.alt = String.IsNullOrEmpty(this.alt) ? "Hospital de Trabajador" : this.alt;
            LibrariesManager librariesManager = LibrariesManager.GetManager();
            Image imagenField = librariesManager.GetImages().Where(i => i.Id == this.imagen).FirstOrDefault();
            ViewBag.imagen = imagenField != null ? imagenField.MediaUrl : "/ResourcePackages/HospitalDelTrabajador/assets/dist/images/g-uni.png";
            return View("Default"); ;
        }
    }
}