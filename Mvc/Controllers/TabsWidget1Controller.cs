using System;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;
using SitefinityWebApp.Mvc.Models;

namespace SitefinityWebApp.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "Tab", Title = "Tab", SectionName = "ACHS")]
    [Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesigner(typeof(WidgetDesigners.TabsWidgetDesigner10))]
    public class TabsWidget1Controller : Controller
    {
        
        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [Category("Propiedades")]
        [DisplayName("Id (1,2,4...)")]
        public string tab { get; set; }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [Category("Propiedades")]
        [DisplayName("Contenido")]
        public string contenido { get; set; }


        /// <summary>
        /// This is the default Action.
        /// </summary>
        public ActionResult Index()
        {
            string id_aux = String.IsNullOrEmpty(this.tab) ? "1" : this.tab;
            ViewBag.tab = id_aux;
            ViewBag.clase = "";
            if (id_aux.Equals("1"))
            {
                ViewBag.clase = "visible";
            }
            ViewBag.contenido = String.IsNullOrEmpty(this.contenido) ? "<p>ACHS</p>" : this.contenido;
            return View("Default");
        }
    }
}