using System;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;
using SitefinityWebApp.Mvc.Models;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Libraries.Model;
using SitefinityWebApp.Helper;

namespace SitefinityWebApp.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "Cajas Servicios 2", Title = "Cajas Servicios 2", SectionName = "HT")]
    [Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesigner(typeof(WidgetDesigners.HT_CajaServicio2WidgetDesigner1))]
    public class HT_CajaServicio2WidgetController : Controller
    {
        [Category("Propiedades")]
        [DisplayName("Titulo")]
        public string titulo { get; set; }

        [Category("Propiedades")]
        [DisplayName("Bajada")]
        public string bajada { get; set; }

        [Category("Propiedades")]
        [DisplayName("Imagen")]
        public Guid imagen { get; set; }



        public ActionResult Index()
        {
            ViewBag.titulo = String.IsNullOrEmpty(this.titulo) ? "Hospital de Trabajador" : this.titulo;
            ViewBag.bajada = String.IsNullOrEmpty(this.bajada) ? "" : Common.replace_n_to_br(this.bajada);
            ViewBag.alt = String.IsNullOrEmpty(this.titulo) ? "Hospital de Trabajador" : this.titulo;
   
            LibrariesManager librariesManager = LibrariesManager.GetManager();
            Image imageField = librariesManager.GetImages().Where(i => i.Id == this.imagen).FirstOrDefault();
            ViewBag.imagen = imageField != null ? imageField.MediaUrl : "/images/librariesprovider4/default-album/ic-s2.svg?sfvrsn=32aab1b1_2.svg";
            return View("Default"); ;
        }
    }
}