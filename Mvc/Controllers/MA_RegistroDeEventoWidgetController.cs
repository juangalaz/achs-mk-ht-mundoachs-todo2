using SitefinityWebApp.Helper;
using SitefinityWebApp.Mvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Telerik.Sitefinity.Data;
using Telerik.Sitefinity.DynamicModules;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Lifecycle;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Mvc;
using Telerik.Sitefinity.RelatedData;
using Telerik.Sitefinity.Utilities.TypeConverters;
using Telerik.Sitefinity.Versioning;
using System.Linq.Dynamic;
using Telerik.OpenAccess;
using Telerik.Sitefinity.Taxonomies;
using Telerik.Sitefinity.Taxonomies.Model;
using System.Text;

namespace SitefinityWebApp.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "Registro Evento", Title = "Registro Evento", SectionName = "MA")]
    public class MA_RegistroDeEventoWidgetController : Controller
    {
        //17150277-2
        [HttpPost]
        public ActionResult Index(FormCollection f)
        {
            ViewBag.id = "";
            ViewBag.salida1 = "";
            ViewBag.error = "";
            try
            {

                string nombre_evento = f["nombre_evento"].ToString();
                string nombre = f["nombre"].ToString();
                string apellido = f["apellido"].ToString();
                string rut1 = f["rut_1"].ToString();
                string rut2 = f["rut_2"].ToString();
                string rut = rut1 + "-" + rut2;
                string email = f["email"].ToString();
                string empresa = f["empresa"].ToString();
                string cargo = f["cargo"].ToString();
                string urlname = f["urlname"].ToString();
                string fecha_incio = f["fecha_incio"].ToString();
                string ubicacion_evento = f["ubicacion_evento"].ToString();

                string salida = cupos_capacitaciones(urlname);
                if (String.IsNullOrEmpty(salida))
                {

                    string dominio = Request.Url.Scheme + System.Uri.SchemeDelimiter + Request.Url.Host + (Request.Url.IsDefaultPort ? "" : ":" + Request.Url.Port);
                    StringBuilder sbCliente = new StringBuilder();
                    sbCliente.Append(System.IO.File.ReadAllText(HttpContext.Server.MapPath("~\\ResourcePackages\\MundoAchs\\EmailTemplate\\eventos\\index.html")));

                    sbCliente = sbCliente.Replace("{{dominio}}", dominio);
                    sbCliente = sbCliente.Replace("{{nombre_evento}}", nombre_evento);
                    sbCliente = sbCliente.Replace("{{nombre}}", nombre);
                    sbCliente = sbCliente.Replace("{{apellido}}", apellido);
                    sbCliente = sbCliente.Replace("{{rut}}", rut);
                    sbCliente = sbCliente.Replace("{{email}}", email);
                    sbCliente = sbCliente.Replace("{{empresa}}", empresa);
                    sbCliente = sbCliente.Replace("{{cargo}}", cargo);
                    sbCliente = sbCliente.Replace("{{fecha_evento}}", fecha_incio);
                    sbCliente = sbCliente.Replace("{{ubicacion_evento}}", ubicacion_evento);

                    

                    Dictionary<string, string> dict = new Dictionary<string, string>();
                    dict.Add("nombre_evento", nombre_evento);
                    dict.Add("Fecha", fecha_incio);
                    dict.Add("Nombre", nombre);
                    dict.Add("apellido", apellido);
                    dict.Add("email", email);
                    dict.Add("rut", rut);
                    dict.Add("empresa", empresa);
                    dict.Add("cargo", cargo);
                    



                    string id = Common.insertar_formulario_return_id("sf_sf_eventos", dict, Request.UserHostAddress);
                    ViewBag.id = id;
                    string email_envio = "felipe.morales@lfi.cl," + email;
                    string email_nombre = Common.GetWebConfigKey("EMAIL_NOMBRE_EVENTOS");
                    string email_subject = Common.GetWebConfigKey("EMAIL_ASUNTO_EVENTOS");
                    string salida1 = Common.envio_correo(email_envio, email_subject, sbCliente, email_nombre);

                    //string salida1 = ""; 
                    ViewBag.salida1 = salida1;

                }
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.ToString();
            }
            return View("Default");
        }



        public string cupos_capacitaciones(string urlName)
        {
            string salida = "";
            try
            {
                var providerName = "dynamicProvider9";

                var transactionName = "someTransactionName";
                var versionManager = VersionManager.GetManager(null, transactionName);

                DynamicModuleManager dynamicModuleManager = DynamicModuleManager.GetManager(providerName, transactionName);
                using (new ElevatedModeRegion(dynamicModuleManager))
                {

                    Type eventosType = TypeResolutionService.ResolveType("Telerik.Sitefinity.DynamicTypes.Model.EventosMundoAchs.EventoMundoAchs");
                DynamicContent enventoItem = dynamicModuleManager.GetDataItems(eventosType).Where(dynItem => dynItem.UrlName == urlName && dynItem.Status == ContentLifecycleStatus.Live && dynItem.Visible == true).SingleOrDefault();

                DynamicContent eventosItemUpdate = dynamicModuleManager.GetDataItem(eventosType, enventoItem.OriginalContentId);

                //// Create a version
                versionManager.CreateVersion(eventosItemUpdate, true);

                // Then we check it out
                DynamicContent checkOutContentoneItem = dynamicModuleManager.Lifecycle.CheckOut(eventosItemUpdate) as DynamicContent;


               
                    string cupos_aux = checkOutContentoneItem.GetValue("Cupos") != null ? checkOutContentoneItem.GetValue("Cupos").ToString() : "0";
                    if (!cupos_aux.Equals("0"))
                    {
                        int cupo = 0;
                        try
                        {
                            cupo = Convert.ToInt32(cupos_aux) - 1;
                            if (cupo < 0)
                            {
                                salida = "error_cupo";
                                cupo = 0;
                            }
                        }
                        catch
                        {
                            cupo = 0;
                            salida = "error_cupo";
                        }
                        checkOutContentoneItem.SetValue("Cupos", cupo);


                        // Now we need to check in, so the changes apply
                        ILifecycleDataItem checkInContentoneItem = dynamicModuleManager.Lifecycle.CheckIn(checkOutContentoneItem);

                        // Create a version
                        versionManager.CreateVersion(checkInContentoneItem, false);

                        //Finnaly we publish the item again
                        dynamicModuleManager.Lifecycle.Publish(checkInContentoneItem);



                        // Create a version and commit the transaction in order changes to be persisted to data store
                        versionManager.CreateVersion(checkInContentoneItem, true);
                        TransactionManager.CommitTransaction(transactionName);
                    }
                    else
                    {
                        salida = "error_sin_cupo";
                    }
                }

            }
            catch (Exception ex)
            {
                salida = ex.ToString();
            }

            return salida;


        }
    }
}