using System;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;
using SitefinityWebApp.Mvc.Models;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Libraries.Model;

namespace SitefinityWebApp.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "Banner Titulo Verde", Title = "Banner Titulo Verde", SectionName = "ACHS")]
    [Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesigner(typeof(WidgetDesigners.BannerTituloVerdeWidgetDesigner1))]
    public class BannerTituloVerdeWidget1Controller : Controller
    {
        [Category("Propiedades")]
        [DisplayName("Titulo")]
        public string titulo { get; set; }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [Category("Propiedades")]
        [DisplayName("Texto")]
        public string texto { get; set; }



        [Category("Propiedades")]
        [DisplayName("Link")]
        public string link { get; set; }

        [Category("Propiedades")]
        [DisplayName("Texto Link")]
        public string texto_link { get; set; }
        public ActionResult Index()
        {
            ViewBag.titulo = String.IsNullOrEmpty(this.titulo) ? "ACHS" : this.titulo;
            ViewBag.texto = String.IsNullOrEmpty(this.texto) ? "ACHS" : this.texto;
            ViewBag.link = String.IsNullOrEmpty(this.link) ? "" : this.link;
            ViewBag.texto_link = String.IsNullOrEmpty(this.texto_link) ? "Ver m�s" : this.texto_link;


          
            return View("Default"); ;
        }
    }
}