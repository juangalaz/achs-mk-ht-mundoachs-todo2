using System;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;
using SitefinityWebApp.Mvc.Models;
using System.Linq;
using System.Text.RegularExpressions;
using Telerik.Sitefinity.Modules.News;
using Telerik.Sitefinity.News.Model;
using Telerik.Sitefinity.Workflow;
using System.Collections.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Telerik.Sitefinity.News.Model;
using Telerik.Sitefinity.Workflow;
using Telerik.Sitefinity;
using SitefinityWebApp.Helper;
using Telerik.Sitefinity.Libraries.Model;
using Telerik.Sitefinity.RelatedData;
using System.Net;
using Newtonsoft.Json.Linq;

namespace SitefinityWebApp.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "Json", Title = "Json", SectionName = "ACHS")]
    public class JsonWidget1Controller : Controller
    {
        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [Category("String Properties")]
        public string Message { get; set; }

        /// <summary>
        /// This is the default Action.
        /// </summary>
        public ActionResult Index()
        {
            var model = new JsonWidget1Model();
            if (string.IsNullOrEmpty(this.Message))
            {
                model.Message = "Hello, World!";
            }
            else
            {
                model.Message = this.Message;
            }

            return View("Default");
        }

        public ActionResult Importar(string nombre_json)
        {
            string domainName = HttpContext.Request.Url.GetLeftPart(UriPartial.Authority);
            string RSSURL = domainName + $"/archivos_json/{nombre_json}.json";
            List<MetodosImportar.NoticiaImportada> l = new List<MetodosImportar.NoticiaImportada>();
            WebClient wclient = new WebClient();
            string RSSData = wclient.DownloadString(RSSURL);
            var htmlData = wclient.DownloadData(RSSURL);
            string json = Encoding.UTF8.GetString(htmlData);
            JArray jsonArray = JArray.Parse(json);
            // dynamic data = JObject.Parse(jsonArray[0].ToString());
            IList<JToken> results = jsonArray.ToObject<IList<JToken>>();
            string providerName = "newsProvider2";
            providerName = "";
            foreach (JToken result in results)
            {
                string Title = "";
                string Fecha = "";
                string ContenidoPagina = "";
                string ImagenPagina = "";
                string Urlproductiva = "";
                MetodosImportar.NoticiaImportada noticia = new MetodosImportar.NoticiaImportada();
                try
                {

                    Guid masterNewsId = Guid.NewGuid();
                    Title = result["Title"].ToString();


                    if (Title.Length > 1)
                    {

                        Title = char.ToUpper(Title[0]) + Title.ToLower().Substring(1);
                        Title = Title.Replace("achs", "ACHS");
                        Title = Title.Replace("Achs", "ACHS");
                    }

                    Fecha = result["Fecha"].ToString();
                    ContenidoPagina = "<div class='importado'>" + result["ContenidoPagina"].ToString() + "</div>";

                    //ContenidoPagina = ContenidoPagina.Replace("src=\"/centro-de-noticias/PublishingImages", "src=\"/img-noticia/centro-de-noticias/PublishingImages");

                    ////achscorporativomediosachs
                    //ContenidoPagina = ContenidoPagina.Replace("src=\"/centro-de-noticias/PublishingImages", "src=\"/img-noticia/centro-de-noticias/PublishingImages");
                    //ContenidoPagina = ContenidoPagina.Replace("src=\"/centro-de-noticias/PublishingImages/galerias/achs-3465/El-Orientador-1", "src=\"/img-noticia/centro-de-noticias/PublishingImages/galerias/achs-3465/El-Orientador-1");
                    //ContenidoPagina = ContenidoPagina.Replace("src=\"/ACHS-Corporativo/MediosACHS/PublishingImages/", "src=\"/img-noticia/ACHS-Corporativo/MediosACHS/PublishingImages/");
                    //ContenidoPagina = ContenidoPagina.Replace("src=\"/news-intranet/PublishingImages", "src=\"/img-noticia/news-intranet/PublishingImages");
                    ////achscorporativomediosachs

                    //achsenlaprensa
                    ContenidoPagina = ContenidoPagina.Replace("src=\"/ACHS-Corporativo/achs-en-la-prensa/_layouts/images", "src=\"/img-noticia/ACHS-Corporativo/achs-en-la-prensa/_layouts/images");
                    ContenidoPagina = ContenidoPagina.Replace("src=\"/ACHS-Corporativo/achs-en-la-prensa/Documents", "src=\"/img-noticia/ACHS-Corporativo/achs-en-la-prensa/Documents");
                    ContenidoPagina = ContenidoPagina.Replace("src=\"/ACHS-Corporativo/achs-en-la-prensa/PublishingImages", "src=\"/img-noticia/ACHS-Corporativo/achs-en-la-prensa/PublishingImages");
                    //achsenlaprensa

                    //Articulospymes
                    ContenidoPagina = ContenidoPagina.Replace("src=\"/centro-de-noticias/PublishingImages", "src=\"/img-noticia/centro-de-noticias/PublishingImages");
                    ContenidoPagina = ContenidoPagina.Replace("src=\"/Empresas/articulos-pymes/PublishingImages", "src=\"/img-noticia/Empresas/articulos-pymes/PublishingImages");
                    //Articulospymes

                    //comiteparitario
                    ContenidoPagina = ContenidoPagina.Replace("src=\"/centro-de-noticias/PublishingImages", "src=\"/img-noticia/centro-de-noticias/PublishingImages");
                    ContenidoPagina = ContenidoPagina.Replace("src=\"/Comites-Paritarios/articulos-comites-paritarios/Documents", "src=\"/img-noticia/Comites-Paritarios/articulos-comites-paritarios/Documents");
                    ContenidoPagina = ContenidoPagina.Replace("src=\"/news-intranet/PublishingImages", "src=\"/img-noticia/news-intranet/PublishingImages");
                    //comiteparitario

                    //Mediosachs
                    ContenidoPagina = ContenidoPagina.Replace("src=\"/centro-de-noticias/PublishingImages", "src=\"/img-noticia/centro-de-noticias/PublishingImages");
                    ContenidoPagina = ContenidoPagina.Replace("src=\"/centro-de-noticias/PublishingImages/galerias/achs-3465/El-Orientador-1", "src=\"/img-noticia/centro-de-noticias/PublishingImages/galerias/achs-3465/El-Orientador-1");
                    ContenidoPagina = ContenidoPagina.Replace("src=\"/ACHS-Corporativo/MediosACHS/PublishingImages/", "src=\"/img-noticia/ACHS-Corporativo/MediosACHS/PublishingImages/");
                    ContenidoPagina = ContenidoPagina.Replace("src=\"/news-intranet/PublishingImages", "src=\"/img-noticia/news-intranet/PublishingImages");
                    //Mediosachs


                    //trabajadoresCapacitacioncursosachs
                    ContenidoPagina = ContenidoPagina.Replace("src=\"/_layouts/images", "src=\"/img-noticia/_layouts/images");
                    ContenidoPagina = ContenidoPagina.Replace("src=\"/ACHS-Corporativo/newsletters/infoempresas/PublishingImages", "src=\"/img-noticia/ACHS-Corporativo/newsletters/infoempresas/PublishingImages");
                    ContenidoPagina = ContenidoPagina.Replace("src=\"/centro-de-noticias/PublishingImages", "src=\"/img-noticia/centro-de-noticias/PublishingImages");
                    ContenidoPagina = ContenidoPagina.Replace("src=\"/centro-de-noticias/PublishingImages/galerias/achs-0516", "src=\"/img-noticia/centro-de-noticias/PublishingImages/galerias/achs-0516/");
                    ContenidoPagina = ContenidoPagina.Replace("src=\"/Comites-Paritarios/articulos-comites-paritarios/PublishingImages", "src=\"/img-noticia/Comites-Paritarios/articulos-comites-paritarios/PublishingImages");
                    ContenidoPagina = ContenidoPagina.Replace("src=\"/Empresas/UPC/Noticias/PublishingImages", "src=\"/img-noticia/Empresas/UPC/Noticias/PublishingImages");
                    ContenidoPagina = ContenidoPagina.Replace("src=\"/trabajadores/Capacitacion/CentrodeFichas/PublishingImages", "src=\"/trabajadores/Capacitacion/CentrodeFichas/PublishingImages");
                    ContenidoPagina = ContenidoPagina.Replace("src=\"/trabajadores/Capacitacion/cursosachs/centro-de-noticias/_layouts/images", "src=\"/trabajadores/Capacitacion/cursosachs/centro-de-noticias/_layouts/images");
                    ContenidoPagina = ContenidoPagina.Replace("src=\"/trabajadores/Capacitacion/cursosachs/centro-de-noticias/Documents", "src=\"/trabajadores/Capacitacion/cursosachs/centro-de-noticias/Documents");
                    ContenidoPagina = ContenidoPagina.Replace("src=\"/trabajadores/Capacitacion/cursosachs/centro-de-noticias/PublishingImages", "src=\"/trabajadores/Capacitacion/cursosachs/centro-de-noticias/PublishingImages");
                    ContenidoPagina = ContenidoPagina.Replace("src=\"/trabajadores/Capacitacion/cursosachs/centro-de-noticias/PublishingImages/achs-0516", "src=\"/trabajadores/Capacitacion/cursosachs/centro-de-noticias/PublishingImages/achs-0516");
                    //trabajadoresCapacitacioncursosachs


                    ///DOC
                    ///trabajadoresCapacitacioncursosachs
                    ContenidoPagina = ContenidoPagina.Replace("href=\"/Documents", "href=\"/doc-noticia/Documents");
                    ContenidoPagina = ContenidoPagina.Replace("href=\"/portal/ACHS-Corporativo/Documents", "href=\"/doc-noticia/portal/ACHS-Corporativo/Documents");
                    ContenidoPagina = ContenidoPagina.Replace("href=\"/trabajadores/Capacitacion/cursosachs/centro-de-noticias/Documents", "href=\"/doc-noticia/trabajadores/Capacitacion/cursosachs/centro-de-noticias/Documents");
                    ///trabajadoresCapacitacioncursosachs

                    //comiteparitario
                    ContenidoPagina = ContenidoPagina.Replace("href=\"/Comites-Paritarios/articulos-comites-paritarios/Documents", "href=\"/doc-noticia//Comites-Paritarios/articulos-comites-paritarios/Documents");
                    //comiteparitario

                    //Mediosachs
                    ContenidoPagina = ContenidoPagina.Replace("href=\"/ACHS-Corporativo/Documents", "href=\"/doc-noticia/ACHS-Corporativo/Documents");
                    ContenidoPagina = ContenidoPagina.Replace("href=\"/ACHS-Corporativo/MediosACHS/Documents", "href=/doc-noticia/ACHS-Corporativo/MediosACHS/Documents");
                    ContenidoPagina = ContenidoPagina.Replace("href=\"/centro-de-noticias/Documents", "href=\"/doc-noticia/centro-de-noticias/Documents");
                    //Mediosachs


                    //Articulospymes
                    ContenidoPagina = ContenidoPagina.Replace("href=\"/centro-de-noticias/Documents", "href=\"/doc-noticia/centro-de-noticias/Documents");
                    //Articulospymes

                    //achsenlaprensa
                    ContenidoPagina = ContenidoPagina.Replace("href=\"/ACHS-Corporativo/achs-en-la-prensa/Documents", "href=\"/doc-noticia/ACHS-Corporativo/achs-en-la-prensa/Documents");
                    //achsenlaprensa


                    ////achscorporativomediosachs
                    //ContenidoPagina = ContenidoPagina.Replace("href=\"/ACHS-Corporativo/Documents", "href=\"/doc-noticia/ACHS-Corporativo/Documents");
                    //ContenidoPagina = ContenidoPagina.Replace("href=\"/ACHS-Corporativo/MediosACHS/Documents", "href=/doc-noticia/ACHS-Corporativo/MediosACHS/Documents");
                    //ContenidoPagina = ContenidoPagina.Replace("href=\"/centro-de-noticias/Documents", "href=\"/doc-noticia/centro-de-noticias/Documents");
                    ////achscorporativomediosachs




                    //noticias 2020
                    ContenidoPagina = ContenidoPagina.Replace("src=\"/centro-de-noticias/PublishingImages", "src=\"/img-noticia/centro-de-noticias/PublishingImages");
                    ContenidoPagina = ContenidoPagina.Replace("src=\"/news-intranet/PublishingImages", "src=\"/img-noticia/news-intranet/PublishingImages");
                    ContenidoPagina = ContenidoPagina.Replace("href=\"/centro-de-noticias/PublishingImages", "href=\"/img-noticia/centro-de-noticias/PublishingImages");
                    ContenidoPagina = ContenidoPagina.Replace("href=\"/news-intranet/PublishingImages", "href=\"/img-noticia/news-intranet/PublishingImages");

                    ContenidoPagina = ContenidoPagina.Replace("href=\"/centro-de-noticias/Documents", "href=\"/doc-noticia/centro-de-noticias/Documents");
                    ContenidoPagina = ContenidoPagina.Replace("href=\"/ACHS-Corporativo/Documents", "href=\"/doc-noticia/ACHS-Corporativo/Documents");
                    ContenidoPagina = ContenidoPagina.Replace("href=\"/ACHS-Corporativo/DocTransparencia", "href=\"/doc-noticia/ACHS-Corporativo/DocTransparencia");
                    //noticias 2020

                    ImagenPagina = "https://www.achs.cl/portal/" + result["ImagenPagina"].ToString();
                    Urlproductiva = result["Urlproductiva"].ToString();
                    if (Fecha.Split(' ').Count() > 0)
                    {
                        Fecha = Fecha.Split(' ')[0].ToString();
                    }
                    else
                    {
                        Fecha = "01/01/" + nombre_json;
                    }
                    DateTime fecha = DateTime.ParseExact(Fecha, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    fecha = new DateTime(fecha.Year, fecha.Month, fecha.Day, 12, 0, 0);

                    var count = 0;
                    //App.WorkWith().NewsItems().Where(newsItem => newsItem.Id == masterNewsId).Count(out count);
                    Image imagen = MetodosImportar.upload_imagen(ImagenPagina, nombre_json, nombre_json);
                    if (count == 0 && imagen != null)
                    {


                        DateTime DateCreated = DateTime.SpecifyKind(fecha, DateTimeKind.Utc);
                        DateTime PublicationDate = DateTime.SpecifyKind(fecha.AddMinutes(15), DateTimeKind.Utc);
                        DateTime LastModified = DateTime.UtcNow;

                        string slug = Common.remover_acentos(Regex.Replace(Title.ToLower(), @"[^\w\-\!\$\'\(\)\=\@\d_]+", "-"));

                        //The news item is created as a master. The newsId is assigned to the master.
                        App.WorkWith().NewsItem().CreateNew(masterNewsId)
                        //Set the properties of the news item.
                        .Do(newsItem =>
                        {
                            newsItem.Title = Title;
                            newsItem.DateCreated = DateCreated;
                            newsItem.PublicationDate = PublicationDate;
                            newsItem.LastModified = LastModified;
                            newsItem.Content = ContenidoPagina;
                            newsItem.Summary = MetodosImportar.Get400(ContenidoPagina) + "...";

                            newsItem.UrlName = slug;
                            newsItem.CreateRelation(imagen, "Imagen");
                        })
                        //Save the changes.
                        .SaveChanges();

                        //Publish the news item. The published version acquires new ID.
                        var bag = new Dictionary<string, string>();
                        bag.Add("ContentType", typeof(NewsItem).FullName);
                        WorkflowManager.MessageWorkflow(masterNewsId, typeof(NewsItem), providerName, "Publish", false, bag);

                        App.WorkWith().NewsItems().Where(newsItem => newsItem.Id == masterNewsId).Count(out count);
                        string url = "";
                        if (count > 0)
                        {
                            //Modify the news item.
                            App.WorkWith().NewsItem(masterNewsId).CheckOut().Do(item =>
                            {
                                item.PublicationDate = PublicationDate;
                                item.Urls.Clear();
                                item.UrlName = slug;
                            }).CheckIn().SaveChanges();

                            //Publish the news item.
                            WorkflowManager.MessageWorkflow(masterNewsId, typeof(NewsItem), null, "Publish", false, bag);
                        }






                        noticia.url_vieja = Urlproductiva;
                        noticia.url_nueva = App.WorkWith().NewsItem(masterNewsId).CheckOut().Get().ItemDefaultUrl;
                        noticia.error = "";

                    }
                    else
                    {
                        noticia.url_vieja = Urlproductiva;
                        noticia.url_nueva = "";
                        noticia.error = "Imagen Null";
                    }
                }
                catch (Exception ex)
                {
                    noticia.url_vieja = Urlproductiva;
                    noticia.url_nueva = "";
                    noticia.error = ex.ToString();
                }
                l.Add(noticia);
            }


            var jsonResult = Json(l, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        //public ActionResult Importar(string year)
        //{
        //    string domainName = HttpContext.Request.Url.GetLeftPart(UriPartial.Authority);
        //    string RSSURL = domainName + $"/archivos_json/{year}.json";
        //    List<MetodosImportar.NoticiaImportada> l = new List<MetodosImportar.NoticiaImportada>();
        //    WebClient wclient = new WebClient();
        //    string RSSData = wclient.DownloadString(RSSURL);
        //    var htmlData = wclient.DownloadData(RSSURL);
        //    string json = Encoding.UTF8.GetString(htmlData);
        //    JArray jsonArray = JArray.Parse(json);
        //    // dynamic data = JObject.Parse(jsonArray[0].ToString());
        //    IList<JToken> results = jsonArray.ToObject<IList<JToken>>();
        //    string providerName = "newsProvider2";
        //    //providerName = "";
        //    foreach (JToken result in results)
        //    {
        //        string Title = "";
        //        string Fecha = "";
        //        string ContenidoPagina = "";
        //        string ImagenPagina = "";
        //        string Urlproductiva = "";
        //        MetodosImportar.NoticiaImportada noticia = new MetodosImportar.NoticiaImportada();
        //        try
        //        {

        //            Guid masterNewsId = Guid.NewGuid();
        //            Title = result["Title"].ToString();


        //            if (Title.Length > 1)
        //            {

        //                Title = char.ToUpper(Title[0]) + Title.ToLower().Substring(1);
        //                Title = Title.Replace("achs", "ACHS");
        //                Title = Title.Replace("Achs", "ACHS");
        //            }

        //            Fecha = result["Fecha"].ToString();
        //            ContenidoPagina = "<div class='importado'>" + result["ContenidoPagina"].ToString() + "</div>";

        //            ContenidoPagina = ContenidoPagina.Replace("src=\"/centro-de-noticias/PublishingImages", "src=\"/img-noticia/centro-de-noticias/PublishingImages");
        //            ContenidoPagina = ContenidoPagina.Replace("src=\"/news-intranet/PublishingImages", "src=\"/img-noticia/news-intranet/PublishingImages");
        //            ContenidoPagina = ContenidoPagina.Replace("href=\"/centro-de-noticias/PublishingImages", "href=\"/img-noticia/centro-de-noticias/PublishingImages");
        //            ContenidoPagina = ContenidoPagina.Replace("href=\"/news-intranet/PublishingImages", "href=\"/img-noticia/news-intranet/PublishingImages");

        //            ContenidoPagina = ContenidoPagina.Replace("href=\"/centro-de-noticias/Documents", "href=\"/doc-noticia/centro-de-noticias/Documents");
        //            ContenidoPagina = ContenidoPagina.Replace("href=\"/ACHS-Corporativo/Documents", "href=\"/doc-noticia/ACHS-Corporativo/Documents");
        //            ContenidoPagina = ContenidoPagina.Replace("href=\"/ACHS-Corporativo/DocTransparencia", "href=\"/doc-noticia/ACHS-Corporativo/DocTransparencia");




        //            ImagenPagina = "https://www.achs.cl/portal/" + result["ImagenPagina"].ToString();
        //            Urlproductiva = result["Urlproductiva"].ToString();
        //            if (Fecha.Split(' ').Count() > 0)
        //            {
        //                Fecha = Fecha.Split(' ')[0].ToString();
        //            }
        //            else
        //            {
        //                Fecha = "01/01/" + year;
        //            }
        //            DateTime fecha = DateTime.ParseExact(Fecha, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
        //            var count = 0;
        //            //App.WorkWith().NewsItems().Where(newsItem => newsItem.Id == masterNewsId).Count(out count);
        //            Image imagen = MetodosImportar.upload_imagen(ImagenPagina, year, year);
        //            if (count == 0 && imagen != null)
        //            {


        //                DateTime DateCreated = DateTime.SpecifyKind(fecha, DateTimeKind.Utc);
        //                DateTime PublicationDate = DateTime.SpecifyKind(fecha.AddMinutes(15), DateTimeKind.Utc);
        //                DateTime LastModified = DateTime.UtcNow;

        //                string slug = Common.remover_acentos(Regex.Replace(Title.ToLower(), @"[^\w\-\!\$\'\(\)\=\@\d_]+", "-"));

        //                //The news item is created as a master. The newsId is assigned to the master.
        //                App.WorkWith().NewsItem().CreateNew(masterNewsId)
        //                //Set the properties of the news item.
        //                .Do(newsItem =>
        //                {
        //                    newsItem.Title = Title;
        //                    newsItem.DateCreated = DateCreated;
        //                    newsItem.PublicationDate = PublicationDate;
        //                    newsItem.LastModified = LastModified;
        //                    newsItem.Content = ContenidoPagina;
        //                    newsItem.Summary = MetodosImportar.Get400(ContenidoPagina) + "...";

        //                    newsItem.UrlName = slug;
        //                    newsItem.CreateRelation(imagen, "Imagen");
        //                })
        //                //Save the changes.
        //                .SaveChanges();

        //                //Publish the news item. The published version acquires new ID.
        //                var bag = new Dictionary<string, string>();
        //                bag.Add("ContentType", typeof(NewsItem).FullName);
        //                WorkflowManager.MessageWorkflow(masterNewsId, typeof(NewsItem), providerName, "Publish", false, bag);

        //                App.WorkWith().NewsItems().Where(newsItem => newsItem.Id == masterNewsId).Count(out count);
        //                string url = "";
        //                if (count > 0)
        //                {
        //                    //Modify the news item.
        //                    App.WorkWith().NewsItem(masterNewsId).CheckOut().Do(item =>
        //                    {
        //                        item.PublicationDate = PublicationDate;
        //                        item.Urls.Clear();
        //                        item.UrlName = slug;
        //                    }).CheckIn().SaveChanges();

        //                    //Publish the news item.
        //                    WorkflowManager.MessageWorkflow(masterNewsId, typeof(NewsItem), null, "Publish", false, bag);
        //                }






        //                noticia.url_vieja = Urlproductiva;
        //                noticia.url_nueva = App.WorkWith().NewsItem(masterNewsId).CheckOut().Get().ItemDefaultUrl;
        //                noticia.error = "";

        //            }
        //            else
        //            {
        //                noticia.url_vieja = Urlproductiva;
        //                noticia.url_nueva = "";
        //                noticia.error = "Imagen Null";
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            noticia.url_vieja = Urlproductiva;
        //            noticia.url_nueva = "";
        //            noticia.error = ex.ToString();
        //        }
        //        l.Add(noticia);
        //    }


        //    var jsonResult = Json(l, JsonRequestBehavior.AllowGet);
        //    jsonResult.MaxJsonLength = int.MaxValue;
        //    return jsonResult;
        //}
    }
}