using System;
using System.Linq;

namespace SitefinityWebApp.Mvc.Models
{
    public class MA_RegistroDeEventoWidgetModel
    {
        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        public string Message { get; set; }
    }
}