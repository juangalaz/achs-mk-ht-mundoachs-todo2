using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SitefinityWebApp.Mvc.Models
{


    public partial class Acreencias
    {
        [JsonProperty("@odata.context")]
        public Uri OdataContext { get; set; }

        [JsonProperty("value")]
        public List<Value> Value { get; set; }
    }

    public partial class Value
    {
        [JsonProperty("Id")]
        public Guid Id { get; set; }

        [JsonProperty("LastModified")]
        public DateTimeOffset LastModified { get; set; }

        [JsonProperty("PublicationDate")]
        public DateTimeOffset PublicationDate { get; set; }

        [JsonProperty("DateCreated")]
        public DateTimeOffset DateCreated { get; set; }

        [JsonProperty("UrlName")]
        public string UrlName { get; set; }

        [JsonProperty("Fecha")]
        public DateTimeOffset Fecha { get; set; }

        [JsonProperty("Monto")]
        public long Monto { get; set; }

        [JsonProperty("Rut")]
        public string Rut { get; set; }


        [JsonProperty("Medio")]
        public string Medio { get; set; }

        [JsonProperty("Banco")]
        public string Banco { get; set; }

        [JsonProperty("Title")]
        public string Title { get; set; }
    }



    public class AppAcreenciasModel
    {
        /// <summary>
        /// Gets or sets the message.
        /// </summary>
       
    }
}