using System;
using System.Linq;

namespace SitefinityWebApp.Mvc.Models
{
    public class DocumentoSharedWidgetModel
    {
        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        public string Message { get; set; }
    }
}