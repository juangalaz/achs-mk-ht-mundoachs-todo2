using System;
using System.Linq;

namespace SitefinityWebApp.Mvc.Models
{
    public class MA_BannerInteriorWidgetModel
    {
        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        public string Message { get; set; }
    }
}