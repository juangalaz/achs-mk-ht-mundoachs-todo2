using System;
using System.Linq;

namespace SitefinityWebApp.Mvc.Models
{
    public class HT_CajasProgramasWidgetModel
    {
        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        public string Message { get; set; }
    }
}