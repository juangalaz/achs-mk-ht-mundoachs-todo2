using System;
using System.Linq;

namespace SitefinityWebApp.Mvc.Models
{
    public class AppHTImagenologiaWidgetModel
    {
        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        public string Message { get; set; }
    }
}