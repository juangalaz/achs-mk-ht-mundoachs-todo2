using System;
using System.Linq;

namespace SitefinityWebApp.Mvc.Models
{
    public class CAP_FormularioExitoCapacitacionesWidgetModel
    {
        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        public string Message { get; set; }
    }


    public class CAP_FechaCapacitaciones
    { 
        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        public DateTime fecha { get; set; }
        public string nombre { get; set; }
    }
}