using System;
using System.Linq;

namespace SitefinityWebApp.Mvc.Models
{
    public class MK_FormularioRegistroWidgetModel
    {
        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        public string Message { get; set; }
    }
}