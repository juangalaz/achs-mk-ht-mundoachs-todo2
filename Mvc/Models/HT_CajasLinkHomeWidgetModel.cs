using System;
using System.Linq;

namespace SitefinityWebApp.Mvc.Models
{
    public class HT_CajasLinkHomeWidgetModel
    {
        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        public string Message { get; set; }
    }
}