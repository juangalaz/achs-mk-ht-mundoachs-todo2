using System;
using System.Linq;

namespace SitefinityWebApp.Mvc.Models
{
    public class AcordeonConFotoWidgetModel
    {
        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        public string Message { get; set; }
    }
}