const cSlide = document.getElementsByClassName('c-home');
if(cSlide){
[].forEach.call(document.querySelectorAll('.c-home'), function (el) {
  var slider = tns({
    container: el,
    items: 1,
    controls: true,
    nav: true,
    navPosition: 'bottom',
    preventScrollOnTouch: "force"
    });
  });
}


const cSlideDos = document.getElementsByClassName('c-home-ma');
if(cSlideDos){
[].forEach.call(document.querySelectorAll('.c-home-ma'), function (el) {
  var slider = tns({
    autoplay: true,
    speed: 1500,
    container: el,
    items: 1,
    controls: true,
    nav: true,
    navPosition: 'bottom',
    preventScrollOnTouch: "force"
    });
  });
}

const cInformate = document.getElementsByClassName('c-informate');
if(cInformate){
[].forEach.call(document.querySelectorAll('.c-informate'), function (el) {
  var slider = tns({
    container: el,
    items: 1,
    controls: true,
    nav: true,
    navPosition: 'bottom',
    preventScrollOnTouch: "force"
    });
  });
}

const cGal = document.getElementsByClassName('c-gal');
if(cGal){
[].forEach.call(document.querySelectorAll('.c-gal'), function (el) {
  var slider = tns({
    container: el,
    items: 1,
    controls: true,
    nav: true,
    navPosition: 'bottom',
    preventScrollOnTouch: "force"
    });
  });
}
const c1 = document.getElementsByClassName('c-1');
if(c1){
[].forEach.call(document.querySelectorAll('.c-1'), function (el) {
  var slider = tns({
    container: el,
    items: 1,
    controls: true,
    nav: true,
    navPosition: 'bottom',
    preventScrollOnTouch: "force",
    mouseDrag: true
    });
  });
}
const carrusel3Ht = document.getElementsByClassName('c-3-ht');
if(carrusel3Ht){
[].forEach.call(document.querySelectorAll('.c-3-ht'), function (el) {
  var slider = tns({
    container: el,
    items: 3,
    controls: true,
    nav: true,
    gutter: 20,
    navPosition: 'bottom',
    preventScrollOnTouch: "force",
    responsive: {
      0 : {
        items: 1
      },
      480:{
        items: 2
      },
      640: {
        items: 3
      },
      767: {
        items: 3
      },
      900: {
        items: 3
      }
    }
    });
  });
}

const carruselPub = document.getElementsByClassName('c-pub-ht');
if(carruselPub){
[].forEach.call(document.querySelectorAll('.c-pub-ht'), function (el) {
  var slider = tns({
    container: el,
    items: 3,
    controls: true,
    nav: true,
    gutter: 20,
    navPosition: 'bottom',
    preventScrollOnTouch: "force",
    responsive: {
      0 : {
        items: 1
      },
      991: {
        items: 2
      },
      1300: {
        items: 3
      }
    }
    });
  });
}
const carrusel3 = document.getElementsByClassName('c-3');
if(carrusel3){
[].forEach.call(document.querySelectorAll('.c-3'), function (el) {
  var slider = tns({
    container: el,
    items: 3,
    controls: true,
    nav: true,
    gutter: 20,
    navPosition: 'bottom',
    preventScrollOnTouch: "force",
    responsive: {
      0 : {
        items: 1
      },
      480:{
        items: 2
      },
      640: {
        items: 3
      },
      767: {
        items: 3
      },
      900: {
        items: 3
      }
    }
    });
  });
}
const carrusel4 = document.getElementsByClassName('c-4');
if(carrusel4){
[].forEach.call(document.querySelectorAll('.c-4'), function (el) {
  var slider = tns({
    container: el,
    items: 4,
    controls: true,
    nav: true,
    gutter: 20,
    navPosition: 'bottom',
    preventScrollOnTouch: "force",
    responsive: {
      0 : {
        items: 1
      },
      480:{
        items: 2
      },
      640: {
        items: 3
      },
      767: {
        items: 3
      },
      900: {
        items: 4
      }
    }
    });
  });
}

/*--LAZY--*/
var lazyLoadInstance = new LazyLoad({
  elements_selector: ".lazy"
});
/*--WOW--*/
    wow = new WOW(
      {
        animateClass: 'animated',
        offset:       100,
        mobile: false,
        callback: function(box) {
        }
      }
    );
    wow.init();
/*--FIN WOW--*/
const acc = document.getElementsByClassName('accordion');
if(acc){
  for (let i = 0; i < acc.length; i++) {
      let item = acc[i];
      var accordion = new Accordion({
          element: item,
          oneOpen: true
      });
  }
}

$(document).ready(function() {
  $('.tabla-horarios').DataTable({
    "pageLength": 8,
    "language": {
      "search": "BUSCAR POR SEDE",
      "zeroRecords": "No hay datos disponibles",
      "paginate": {
        "first":      "Primera",
        "last":       "Última",
        "next":       "<i class='fas fa-chevron-right'></i>",
        "previous":   "<i class='fas fa-chevron-left'></i>"
      }
    },
    "order": [[ 0, 'asc' ], [ 1, 'asc' ]]
  });
  $('.table-licitacion').DataTable({
    "pageLength": 8,
    "language": {
      "search": "Buscar por licitación",
      "zeroRecords": "No hay datos disponibles",
      "paginate": {
        "first":      "Primera",
        "last":       "Última",
        "next":       "<i class='fas fa-chevron-right'></i>",
        "previous":   "<i class='fas fa-chevron-left'></i>"
      }
    },
    "order": [[ 0, 'asc' ], [ 1, 'asc' ]]
  });
});

$(function(){
  
  $('.menu-lateral a[href*=\\#]').click(function() {

  if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
      && location.hostname == this.hostname) {

          var $target = $(this.hash);

          $target = $target.length && $target || $('[name=' + this.hash.slice(1) +']');

          if ($target.length) {

              var targetOffset = $target.offset().top;

              $('html,body').animate({scrollTop: targetOffset}, 1000);

              return false;

         }

    }

});

});