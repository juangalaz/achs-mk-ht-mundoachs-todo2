
/*--LAZY--*/
var lazyLoadInstance = new LazyLoad({
  elements_selector: ".lazy"
});
$(document).ready(function() {
  $( ".btn-popup" ).click(function() {
  $( "#dvPopup" ).css( "display", "none" );
});
});


$(document).ready(function(){
  $('.menu-tabs a').click(function(e){
    e.preventDefault();
    var p = $(this).parent().parent();
    $(p).find('.menu-tabs a').removeClass('activo');
    $(this).addClass('activo');
    var d = $(this).data('rel');
    $(p).find('.panel-item').removeClass('visible');
    $(p).find('.panel-item#panel-' + d).addClass('visible');
  });

  $('.franja-ac').click(function(e){
    e.preventDefault();
    $(this).toggleClass('visible');
    $('.contenido-que-son').toggleClass('visible');
  });

  $('.btn-menu').click(function(){
    $('.head nav').toggleClass('visible');
    $(this).toggleClass('open');
  });

 if($(window).width()<768){
    $('.head nav .fas').click(function(e){
      e.preventDefault();
      $(this).parent().next().toggleClass('look');
      $(this).parent().toggleClass('icon');
    });
  }

  $('.informate .carrusel button[data-nav="0"]').html('1');
  $('.informate .carrusel button[data-nav="1"]').html('2');
  $('.informate .carrusel button[data-nav="2"]').html('3');
  $('.informate .carrusel button[data-nav="3"]').html('4');
  $('.informate .carrusel button[data-nav="4"]').html('5');

  s = 0;
  $('.fuente-mayor').click(function(e){
    e.preventDefault();
    if(s == 0){
      $('html').addClass('size-lg');
      s = 1;
    } else if( s == -1){
      $('html').removeClass('size-sm');
      s = 0;
    }
  });
  $('.fuente-menor').click(function(e){
    e.preventDefault();
    if(s == 0){
      $('html').addClass('size-sm');
      s = -1;
    } else if( s == 1){
      $('html').removeClass('size-lg');
      s = 0;
    }
  });

  $('.tab-auditoria a').click(function(e){
    e.preventDefault();
    var t = $(this).data('rel');
    $('.panel-auditoria').removeClass('visible');
    $('.panel-auditoria#panel-' + t).addClass('visible');
  });


  $('.item-proy').click(function(){
    $(this).toggleClass('activo');
  });
    $('.contraste').click(function(e){
    e.preventDefault();
    $('body').toggleClass('dark');
  });


      var ml = 0;
  $('.boton-menu-lateral').click(function(){
    if(ml == 0){
      $('aside.sidebar').addClass('visible');
      $(this).find('i').removeClass('fa-ellipsis-h');
      $(this).find('i').addClass('fa-times');
      ml = 1;
    } else {
      $('aside.sidebar').removeClass('visible');
      $(this).find('i').removeClass('fa-times');
      $(this).find('i').addClass('fa-ellipsis-h');
      ml = 0;
    }
  });
  

});

 $('#btn-buscar').click(function(e){
    e.preventDefault();
     var texto = $('#txt-buscar').val();
   var url = '/resultado-de-busqueda/?indexCatalogue=sitio-web&wordsMode=AllWords&searchQuery=' + texto;
   window.location.href = url;
  });

  
