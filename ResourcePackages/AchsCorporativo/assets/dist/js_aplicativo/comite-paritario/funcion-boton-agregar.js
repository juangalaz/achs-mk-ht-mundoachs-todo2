﻿
//TITULARES EMPRESA

$('#btnAgregarIntegranteTitularEmpresa').on('click', function () {
    var largoTitEmpresa1 = $('#txtIntTitEmpresa1').html().length;
    var largoTitEmpresa2 = $('#txtIntTitEmpresa2').html().length;
    var largoTitEmpresa3 = $('#txtIntTitEmpresa3').html().length;
    if (largoTitEmpresa1 ==0 ) {
        $('#txtIntTitEmpresa1').html($('#txtTitularEmpresa').val())
        $('#txtTitularEmpresa').val('')
    }

    if (largoTitEmpresa1 != 0 && largoTitEmpresa2 == 0) {
        $('#txtIntTitEmpresa2').html($('#txtTitularEmpresa').val())
        $('#txtTitularEmpresa').val('')
    }
    if (largoTitEmpresa1 != 0 && largoTitEmpresa2 != 0 && largoTitEmpresa3 == 0) {
        $('#txtIntTitEmpresa3').html($('#txtTitularEmpresa').val())
        $('#txtTitularEmpresa').val('')
    }
})
$('#txtIntTitEmpresa1').on('click', function () {
    $('#txtIntTitEmpresa1').html('')
})
$('#txtIntTitEmpresa2').on('click', function () {
    $('#txtIntTitEmpresa2').html('')
})
$('#txtIntTitEmpresa3').on('click', function () {
    $('#txtIntTitEmpresa3').html('')
})

// Suplentes Empresa
$('#btnAgregarIntegranteSuplenteEmpresa').on('click', function () {
    var largoSupEmpresa1 = $('#txtIntSupEmpresa1').html().length;
    var largoSupEmpresa2 = $('#txtIntSupEmpresa2').html().length;
    var largoSupEmpresa3 = $('#txtIntSupEmpresa3').html().length;
    if (largoSupEmpresa1 == 0) {
        $('#txtIntSupEmpresa1').html($('#txtSuplenteEmpresa').val())
        $('#txtSuplenteEmpresa').val('')
    }

    if (largoSupEmpresa1 != 0 && largoSupEmpresa2 == 0) {
        $('#txtIntSupEmpresa2').html($('#txtSuplenteEmpresa').val())
        $('#txtSuplenteEmpresa').val('')
    }
    if (largoSupEmpresa1 != 0 && largoSupEmpresa2 != 0 && largoSupEmpresa3 == 0) {
        $('#txtIntSupEmpresa3').html($('#txtSuplenteEmpresa').val())
        $('#txtSuplenteEmpresa').val('')
    }
})
$('#txtIntSupEmpresa1').on('click', function () {
    $('#txtIntSupEmpresa1').html('')
})
$('#txtIntSupEmpresa2').on('click', function () {
    $('#txtIntSupEmpresa2').html('')
})
$('#txtIntSupEmpresa3').on('click', function () {
    $('#txtIntSupEmpresa3').html('')
})

//Titulares de los Trabajadores

$('#btnAgregarIntegranteTitularesTrabajadores').on('click', function () {
    var largTitTrabajadores1 = $('#txtIntTitTrabajadores1').html().length;
    var largTitTrabajadores2 = $('#txtIntTitTrabajadores2').html().length;
    var largTitTrabajadores3 = $('#txtIntTitTrabajadores3').html().length;
    if (largTitTrabajadores1 == 0) {
        $('#txtIntTitTrabajadores1').html($('#txtTitTrabajadores').val())
        $('#txtTitTrabajadores').val('')
    }

    if (largTitTrabajadores1 != 0 && largTitTrabajadores2 == 0) {
        $('#txtIntTitTrabajadores2').html($('#txtTitTrabajadores').val())
        $('#txtTitTrabajadores').val('')
    }
    if (largTitTrabajadores1 != 0 && largTitTrabajadores2 != 0 && largTitTrabajadores3 == 0) {
        $('#txtIntTitTrabajadores3').html($('#txtTitTrabajadores').val())
        $('#txtTitTrabajadores').val('')
    }
})
$('#txtIntTitTrabajadores1').on('click', function () {
    $('#txtIntTitTrabajadores1').html('')
})
$('#txtIntTitTrabajadores2').on('click', function () {
    $('#txtIntTitTrabajadores2').html('')
})
$('#txtIntTitTrabajadores3').on('click', function () {
    $('#txtIntTitTrabajadores3').html('')
})

//Suplentes de los Trabajadores

$('#btnAgregarIntegranteSuplentesTrabajadores').on('click', function () {
    var largSupTrabajadores1 = $('#txtIntSupTrabajadores1').html().length;
    var largSupTrabajadores2 = $('#txtIntSupTrabajadores2').html().length;
    var largSupTrabajadores3 = $('#txtIntSupTrabajadores3').html().length;

    if (largSupTrabajadores1 == 0) {
        $('#txtIntSupTrabajadores1').html($('#txtSupTrabajadores').val())
        $('#txtSupTrabajadores').val('')
    }

    if (largSupTrabajadores1 != 0 && largSupTrabajadores2 == 0) {
        $('#txtIntSupTrabajadores2').html($('#txtSupTrabajadores').val())
        $('#txtSupTrabajadores').val('')
    }
    if (largSupTrabajadores1 != 0 && largSupTrabajadores2 != 0 && largSupTrabajadores3 == 0) {
        $('#txtIntSupTrabajadores3').html($('#txtSupTrabajadores').val())
        $('#txtSupTrabajadores').val('')
    }
})
$('#txtIntSupTrabajadores1').on('click', function () {
    $('#txtIntSupTrabajadores1').html('')
})
$('#txtIntSupTrabajadores2').on('click', function () {
    $('#txtIntSupTrabajadores2').html('')
})
$('#txtIntSupTrabajadores3').on('click', function () {
    $('#txtIntSupTrabajadores3').html('')
})
