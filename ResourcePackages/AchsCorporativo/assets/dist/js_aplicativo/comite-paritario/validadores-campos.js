﻿//CARGA COMUNAS
getComunas()
getsector()
// VALIDADOR DE RUT
$('#txtRutEmpresa').on('change', function () {
    var rut = $('#txtRutEmpresa').val();
    var error = 0;
    var valor = rut.split('.').join('');
    valor = valor.replace('-', '');
    cuerpo = valor.slice(0, -1);
    dv = valor.slice(-1).toUpperCase();
    rut.value = cuerpo + '-' + dv;
    if (cuerpo.length < 7) { error = 1; }
    suma = 0;
    multiplo = 2;
    for (i = 1; i <= cuerpo.length; i++) {
        index = multiplo * valor.charAt(cuerpo.length - i);
        suma = suma + index;
        if (multiplo < 7) { multiplo = multiplo + 1; } else { multiplo = 2; }
    }
    dvEsperado = 11 - (suma % 11);
    dv = (dv == 'K') ? 10 : dv;
    dv = (dv == 0) ? 11 : dv;
    if (dvEsperado != dv) { error = 1; }
    if (error == 1) {
        $('#txtRutEmpresa').val('');
        rutREFOK = document.getElementById('txtRutEmpresaOk');
        rutREFOK.innerText = "RUT no válido";
        $('#txtRutEmpresa').focus();

        return false;
    } else {
        if (dv == 10) {
            $('#txtRutEmpresaOk').html("")
            $('#txtRutEmpresa').css("border", "")
            $('#txtRutEmpresa').css("border-radius", "")
            $('#txtRutEmpresa').val((cuerpo + '-' + 'K').trim())

        } else if (dv == 11) {
            $('#txtRutEmpresaOk').html("")
            $('#txtRutEmpresa').css("border", "")
            $('#txtRutEmpresa').css("border-radius", "")
            $('#txtRutEmpresa').val((cuerpo + '-' + '0').trim())
        }
        else {
            $('#txtRutEmpresaOk').html("")
            $('#txtRutEmpresa').css("border", "")
            $('#txtRutEmpresa').css("border-radius", "")
            $('#txtRutEmpresa').val((cuerpo + '-' + dv).trim())
        }
        rutREFOK.innerText = "";
        return true;
    }
});
// VALIDADOR DE RUT
$('#txtRutPresidente').on('change', function () {
    var rut = $('#txtRutPresidente').val();
    var error = 0;
    var valor = rut.split('.').join('');
    valor = valor.replace('-', '');
    cuerpo = valor.slice(0, -1);
    dv = valor.slice(-1).toUpperCase();
    rut.value = cuerpo + '-' + dv;
    if (cuerpo.length < 7) { error = 1; }
    suma = 0;
    multiplo = 2;
    for (i = 1; i <= cuerpo.length; i++) {
        index = multiplo * valor.charAt(cuerpo.length - i);
        suma = suma + index;
        if (multiplo < 7) { multiplo = multiplo + 1; } else { multiplo = 2; }
    }
    dvEsperado = 11 - (suma % 11);
    dv = (dv == 'K') ? 10 : dv;
    dv = (dv == 0) ? 11 : dv;
    if (dvEsperado != dv) { error = 1; }
    if (error == 1) {
        $('#txtRutPresidente').val('');
        rutREFOK = document.getElementById('txtRutPresidenteOk');
        rutREFOK.innerText = "RUT no válido";
        $('#txtRutPresidente').focus();

        return false;
    } else {
        if (dv == 10) {
            $('#txtRutPresidenteOk').html("")
            $('#txtRutPresidente').css("border", "")
            $('#txtRutPresidente').css("border-radius", "")
            $('#txtRutPresidente').val((cuerpo + '-' + 'K').trim())

        } else if (dv == 11) {
            $('#txtRutPresidenteOk').html("")
            $('#txtRutPresidente').css("border", "")
            $('#txtRutPresidente').css("border-radius", "")
            $('#txtRutPresidente').val((cuerpo + '-' + '0').trim())
        }
        else {
            $('#txtRutPresidenteOk').html("")
            $('#txtRutPresidente').css("border", "")
            $('#txtRutPresidente').css("border-radius", "")
            $('#txtRutPresidente').val((cuerpo + '-' + dv).trim())
        }
        rutREFOK.innerText = "";
        return true;
    }
});

// VALIDADOR DE RUT
$('#txtRutSecretarioCPHS').on('change', function () {
    var rut = $('#txtRutSecretarioCPHS').val();
    var error = 0;
    var valor = rut.split('.').join('');
    valor = valor.replace('-', '');
    cuerpo = valor.slice(0, -1);
    dv = valor.slice(-1).toUpperCase();
    rut.value = cuerpo + '-' + dv;
    if (cuerpo.length < 7) { error = 1; }
    suma = 0;
    multiplo = 2;
    for (i = 1; i <= cuerpo.length; i++) {
        index = multiplo * valor.charAt(cuerpo.length - i);
        suma = suma + index;
        if (multiplo < 7) { multiplo = multiplo + 1; } else { multiplo = 2; }
    }
    dvEsperado = 11 - (suma % 11);
    dv = (dv == 'K') ? 10 : dv;
    dv = (dv == 0) ? 11 : dv;
    if (dvEsperado != dv) { error = 1; }
    if (error == 1) {
        $('#txtRutSecretarioCPHS').val('');
        rutREFOK = document.getElementById('txtRutSecretarioCPHSOk');
        rutREFOK.innerText = "RUT no válido";
        $('#txtRutSecretarioCPHS').focus();

        return false;
    } else {
        if (dv == 10) {
            $('#txtRutSecretarioCPHS').css("border", "")
            $('#txtRutSecretarioCPHS').css("border-radius", "")
            $('#txtRutSecretarioCPHSOk').html("")
            $('#txtRutSecretarioCPHS').val((cuerpo + '-' + 'K').trim())

        } else if (dv == 11) {
            $('#txtRutSecretarioCPHS').css("border", "")
            $('#txtRutSecretarioCPHS').css("border-radius", "")
            $('#txtRutSecretarioCPHSOk').html("")
            $('#txtRutSecretarioCPHS').val((cuerpo + '-' + '0').trim())
        }
        else {
            $('#txtRutSecretarioCPHS').css("border", "")
            $('#txtRutSecretarioCPHS').css("border-radius", "")
            $('#txtRutSecretarioCPHSOk').html("")
            $('#txtRutSecretarioCPHS').val((cuerpo + '-' + dv).trim())
        }
        rutREFOK.innerText = "";
        return true;
    }
});



// SOLO LETRAS

function soloLetras(e) {
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toString();
    letras = "áéíóúabcdefghijklmnñopqrstuvwxyzÁÉÍÓÚABCDEFGHIJKLMNÑOPQRSTUVWXYZ. ";

    if (letras.indexOf(tecla) == -1) {

        return false;
    }



}

// SOLO NUMEROS
function SoloNumeros(evt) {
    if (window.event) {//asignamos el valor de la tecla a keynum
        keynum = evt.keyCode; //IE
    }
    else {
        keynum = evt.which; //FF
    }
    //comprobamos si se encuentra en el rango numérico y que teclas no recibirá.
    if ((keynum > 47 && keynum < 58) || keynum == 8 || keynum == 13 || keynum == 6) {
        return true;
    }
    else {
        return false;
    }
}


$('#file').on('change', function () {
 
  
    var filename = $('#file').val().replace(/C:\\fakepath\\/i, '')

    archivo = document.getElementById('fileActaOK');
    archivo.innerText = filename;



});



// FORMATO CORREO

$('#txtEmailCorporativo').on('change', function () {
    campo = event.target;
    valido = document.getElementById('txtEmailCorporativoOk');

    emailRegex = /^[-\w.]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
    //Se muestra un texto a modo de ejemplo, luego va a ser un icono
    if (emailRegex.test(campo.value)) {

    } else {
        valido.innerText = "El formato del correo es incorrecto.";
        $('#txtEmailCorporativo').focus();
        $('#txtEmailCorporativo').val("");
    }
});

//VALIDAD FONOS

$('#phone').on('change', function () {

    var fono = $('#phone').val().trim().substring(0, 1);

    if (fono == 0) {
        $('#phone').val("")
        $('#phone').attr("placeholder", "El teléfono no puede empezar con 0")
    } else {
        $('#phone').attr("placeholder", "Ejemplo: 948435748")
    }

});

$('#txtFono').on('change', function () {

    var fono = $('#txtFono').val().trim().substring(0, 1);

    if (fono == 0) {
        $('#txtFono').val("")
        $('#txtFono').attr("placeholder", "El teléfono no puede empezar con 0")
    } else {
        $('#txtFono').attr("placeholder", "Ejemplo: 948435748")
    }

});
//GET SECTOR
function getsector() {
    $.ajax({
        type: "GET",
        url: "https://api.achs.lfi.cl/api/comiteparitario/getSector",
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        success: function (response) {
            var arr_from_json = JSON.parse(response.regionlist);

            $("#idSector").append($('<option>', {
                value: "",
                text: "Seleccione sector"
            }));

            for (var x = 0; x < arr_from_json.length; x++) {

                $("#idSector").append($('<option>', {
                    value: arr_from_json[x].sector,
                    text: arr_from_json[x].sector
                }));

            }



        },
        failure: function (response) {
            console.log(response);
        },
        error: function (response) {
            console.log(response);
        }
    });


}


function getComunas() {
    $.ajax({
        type: "GET",
        url: "https://api.achs.lfi.cl/api/comiteparitario/getComunas",
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        success: function (response) {
            var arr_from_json = JSON.parse(response.regionlist);

            $("#idComuna").append($('<option>', {
                value: "",
                text: "Seleccione comuna"
            }));
            $("#idAgencia").append($('<option>', {
                value: "",
                text: "Seleccione agencia"
            }));
            for (var x = 0; x < arr_from_json.length; x++) {

                $("#idComuna").append($('<option>', {
                    value: arr_from_json[x].comuna,
                    text: arr_from_json[x].comuna
                }));

            }



        },
        failure: function (response) {
            console.log(response);
        },
        error: function (response) {
            console.log(response);
        }
    });


}
$("#idComuna").change(function () {
    GetComunaFilter("idAgencia", $(this).val());
});

function GetComunaFilter(id, value) {

    $("#" + id).children('option:not(:first)').remove();
    $.ajax({
        type: "GET",
        url: "https://api.achs.lfi.cl/api/comiteparitario/getAgenciaFilter",
        data: { codcomuna: value },
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        async: true,
        success: function (response) {
            var arr_from_json = JSON.parse(response.comunalist);
            for (var x = 0; x < arr_from_json.length; x++) {

                $("#" + id).append($('<option>', {
                    value: arr_from_json[x].agencia,
                    text: arr_from_json[x].agencia
                }));

            }
        },
        failure: function (response) {
            console.log(response);
        },
        error: function (response) {
            console.log(response);
        }
    });
}


///CONDUCTA CAMPOS

$('#txtNombreEmpresa').on('change', function () {
    $('#txtNombreEmpresa').css("border", "")
    $('#txtNombreEmpresa').css("border-radius", "")
    $('#txtNombreEmpresaOk').html("")
});

$('#idNombreSecretarioCPHS').on('change', function () {
    $('#idNombreSecretarioCPHS').css("border", "")
    $('#idNombreSecretarioCPHS').css("border-radius", "")
    $('#idNombreSecretarioCPHSOk').html("")
});
$('#txtRutEmpresa').on('change', function () {
    $('#txtRutEmpresa').css("border", "")
    $('#txtRutEmpresa').css("border-radius", "")
    $('#txtRutEmpresaOk').html("")
});
$('#txtRutSecretarioCPHS').on('change', function () {
    $('#txtRutSecretarioCPHS').css("border", "")
    $('#txtRutSecretarioCPHS').css("border-radius", "")
    $('#txtRutSecretarioCPHSOk').html("")
});

$('#txtNumTrabajadores').on('change', function () {
    $('#txtNumTrabajadores').css("border", "")
    $('#txtNumTrabajadores').css("border-radius", "")
    $('#txtNumTrabajadoresOk').html("")
});

$('#txtTelefonoSecretario').on('change', function () {
    $('#txtTelefonoSecretario').css("border", "")
    $('#txtTelefonoSecretario').css("border-radius", "")
    $('#txtTelefonoSecretarioOk').html("")
});
$('#txtCalleCasaMatriz').on('change', function () {
    $('#txtCalleCasaMatriz').css("border", "")
    $('#txtCalleCasaMatriz').css("border-radius", "")
    $('#txtCalleCasaMatrizOk').html("")
});
$('#txtNumCasaMatriz').on('change', function () {
    $('#txtNumCasaMatriz').css("border", "")
    $('#txtNumCasaMatriz').css("border-radius", "")
    $('#txtNumCasaMatrizOk').html("")
});
$('#txtComunaCasaMatriz').on('change', function () {
    $('#txtComunaCasaMatriz').css("border", "")
    $('#txtComunaCasaMatriz').css("border-radius", "")
    $('#txtComunaCasaMatrizOk').html("")
});
$('#txtCalleSucursal').on('change', function () {
    $('#txtCalleSucursal').css("border", "")
    $('#txtCalleSucursal').css("border-radius", "")
    $('#txtCalleSucursalOk').html("")
});
$('#txtNombreExpertoACHS').on('change', function () {
    $('#txtNombreExpertoACHS').css("border", "")
    $('#txtNombreExpertoACHS').css("border-radius", "")
    $('#txtNombreExpertoACHSOk').html("")
});

$('#txtNumeroSucursal').on('change', function () {
    $('#txtNumeroSucursal').css("border", "")
    $('#txtNumeroSucursal').css("border-radius", "")
    $('#txtNumeroSucursalOk').html("")
});
$('#idComuna').on('change', function () {
    $('#idComuna').css("border", "")
    $('#idComuna').css("border-radius", "")
    $('#idComunaOk').html("")
});

$('#txtComunaSucursal').on('change', function () {
    $('#txtComunaSucursal').css("border", "")
    $('#txtComunaSucursal').css("border-radius", "")
    $('#txtComunaSucursalOk').html("")
});
$('#idAgencia').on('change', function () {
    $('#idAgencia').css("border", "")
    $('#idAgencia').css("border-radius", "")
    $('#idAgenciaOk').html("")
});

$('#idSector').on('change', function () {
    $('#idSector').css("border", "")
    $('#idSector').css("border-radius", "")
    $('#idSectorOk').html("")
});

$('#txtEmailCorporativo').on('change', function () {
    $('#txtEmailCorporativo').css("border", "")
    $('#txtEmailCorporativo').css("border-radius", "")
    $('#txtEmailCorporativoOk').html("")
});
$('#txtFechaConstitucion').on('change', function () {
    $('#txtFechaConstitucion').css("border", "")
    $('#txtFechaConstitucion').css("border-radius", "")
    $('#txtFechaConstitucionOk').html("")
});
$('#txtFechaUltimaRenovacion').on('change', function () {
    $('#txtFechaUltimaRenovacion').css("border", "")
    $('#txtFechaUltimaRenovacion').css("border-radius", "")
    $('#txtFechaUltimaRenovacionOk').html("")
});
$('#txtNombrePresidente').on('change', function () {
    $('#txtNombrePresidente').css("border", "")
    $('#txtNombrePresidente').css("border-radius", "")
    $('#txtNombrePresidenteOk').html("")
});

$('#txtRutPresidente').on('change', function () {
    $('#txtRutPresidente').css("border", "")
    $('#txtRutPresidente').css("border-radius", "")
    $('#txtRutPresidenteOk').html("")

});
$('#txtTelefonoPresidente').on('change', function () {
    $('#txtTelefonoPresidente').css("border", "")
    $('#txtTelefonoPresidente').css("border-radius", "")
    $('#txtTelefonoPresidenteOk').html("")

});

