﻿//Envio correo


//subida archivo
//var files = $("#file_XLSXData").get(0).files;
//var data = new FormData();
//for (i = 0; i < files.length; i++) {
//    data.append("file" + i, files[i]);
//}
//$.ajax({
//    type: "POST",
//    url: "/api/file",
//    contentType: false,
//    processData: false,
//    data: data,
//    success: function (result) {
//        if (result) {

//            $("#file_XLSXData").val('');
//        }
//    }
//});


//guardar

$('#btnEnviar').on('click', function () {

    var largoNombreEmpresa = $('#txtNombreEmpresa').val().trim().length;
    var largoRutEmpresa = $('#txtRutEmpresa').val().trim().length;
    var largoNroAdherenteACHS = $('#txtNumTrabajadores').val().trim().length;
    var largoTipoCPHS = $('#idTipoCHPS').val().trim().length;
    var largoDireccionCasaMatriz = $('#txtCalleCasaMatriz').val().trim().length;
    var largoDireccionSucursal = $('#txtCalleSucursal').val().trim().length;
    var largoSector = $('#idSector').val().trim().length;
    var largoFechaConstitucion = $('#txtFechaConstitucion').val().trim().length;
    var largoFechaUltimaRenovacion = $('#txtFechaUltimaRenovacion').val().trim().length;
    var largoNombrePresidenteCHPS = $('#txtNombrePresidente').val().trim().length;
    var largoRutPresidenteCPHS = $('#txtRutPresidente').val().trim().length;
    var largoNombreSecretarioCPHS = $('#idNombreSecretarioCPHS').val().trim().length;
    var largoRutSecretaioCPHS = $('#txtRutSecretarioCPHS').val().trim().length;
    var largoTelefonoSecretarioCPHS = $('#txtTelefonoSecretario').val().trim().length;
    var largoNombreExpertoAsesorACHS = $('#txtNombreExpertoACHS').val().trim().length;
    var largoTelefonoPresidenteCPHS = $('#txtTelefonoPresidente').val().trim().length;
    var largoEmailCorporativoComiteParitario = $('#txtEmailCorporativo').val().trim().length;
    var largoAgencia = $('#idAgencia').val().trim().length;
    var largoComuna = $('#idComuna').val().trim().length;
    var largoNumeroCasaMatriz = $('#txtNumCasaMatriz').val().trim().length;
    var largoComunaCasaMatriz = $('#txtComunaCasaMatriz').val().trim().length;
    var largoNumeroSucursal = $('#txtNumeroSucursal').val().trim().length;
    var largoComunaSucursal = $('#txtComunaSucursal').val().trim().length;
    var largofile = $('#file').val().trim().length;


    if (largofile== 0|| largoNombreEmpresa == 0 || largoRutEmpresa == 0 || largoNroAdherenteACHS == 0 || largoTipoCPHS == 0 || largoDireccionCasaMatriz == 0 || largoDireccionSucursal == 0 || largoSector == 0 || largoFechaConstitucion == 0 || largoFechaUltimaRenovacion == 0 || largoNombrePresidenteCHPS == 0 || largoRutPresidenteCPHS == 0 || largoNombreSecretarioCPHS == 0 || largoRutSecretaioCPHS == 0 || largoTelefonoSecretarioCPHS == 0 || largoNombreExpertoAsesorACHS == 0 || largoTelefonoPresidenteCPHS == 0 || largoEmailCorporativoComiteParitario == 0 || largoAgencia == 0 || largoComuna == 0 || largoNumeroCasaMatriz == 0 || largoComunaCasaMatriz == 0 || largoNumeroSucursal == 0 || largoComunaSucursal == 0) {

        if (largofile == 0) {
            $('#file').focus();
            archivo = document.getElementById('fileActaOK');
            archivo.innerText = 'Ingrese un archivo';
        } else {
            archivo = document.getElementById('fileActaOK');
            archivo.innerText = '';
       
        }
        if (largoNombreEmpresa == 0) {
            $('#txtNombreEmpresa').focus();
            $('#txtNombreEmpresa').val("");
            $('#txtNombreEmpresa').css("border", "1px solid red ")
            $('#txtNombreEmpresa').css("border-radius", "4px ")
  
            $('#txtNombreEmpresaOk').html("Debe Ingresar un Nombre de Empresa")
        } else {
            $('#txtNombreEmpresa').css("border", "")
            $('#txtNombreEmpresa').css("border-radius", "")
            $('#txtNombreEmpresaOk').html("")
        }

        if (largoRutEmpresa == 0) {
            $('#txtRutEmpresa').focus();
            $('#txtRutEmpresa').val("");
            $('#txtRutEmpresa').css("border", "1px solid red ")
            $('#txtRutEmpresa').css("border-radius", "4px ")
            $('#txtRutEmpresaOk').html("Debe Ingresar un RUT de Empresa")
        } else {
            $('#txtRutEmpresa').css("border", "")
            $('#txtRutEmpresa').css("border-radius", "")
            $('#txtRutEmpresaOk').html("")
        }
        if (largoNroAdherenteACHS == 0) {
            $('#txtNumTrabajadores').focus();
            $('#txtNumTrabajadores').val("");
            $('#txtNumTrabajadores').css("border", "1px solid red ")
            $('#txtNumTrabajadores').css("border-radius", "4px ")
            $('#txtNumTrabajadoresOk').html("Debe Ingresar un Numero de Trabajadores")
        } else {
            $('#txtNumTrabajadores').css("border", "")
            $('#txtNumTrabajadores').css("border-radius", "")
            $('#txtNumTrabajadoresOk').html("")
        }
        if (largoTipoCPHS == 0) {
            $('#idTipoCHPS').focus();
            $('#idTipoCHPS').val("");
            $('#idTipoCHPS').css("border", "1px solid red ")
            $('#idTipoCHPS').css("border-radius", "4px ")
            $('#idTipoCHPSOk').html("Debe Ingresar un Tipo de CPHS")
        } else {
            $('#idTipoCHPS').css("border", "")
            $('#idTipoCHPS').css("border-radius", "")
            $('#idTipoCHPSOk').html("")
        }
        if (largoDireccionCasaMatriz == 0) {
            $('#txtCalleCasaMatriz').focus();
            $('#txtCalleCasaMatriz').val("");
            $('#txtCalleCasaMatriz').css("border", "1px solid red ")
            $('#txtCalleCasaMatriz').css("border-radius", "4px ")
            $('#txtCalleCasaMatrizOk').html("Debe Ingresar un Tipo de CPHS")
        } else {
            $('#txtCalleCasaMatriz').css("border", "")
            $('#txtCalleCasaMatriz').css("border-radius", "")
            $('#txtCalleCasaMatrizOk').html("")
        }
        if (largoDireccionSucursal == 0) {
            $('#txtCalleSucursal').focus();
            $('#txtCalleSucursal').val("");
            $('#txtCalleSucursal').css("border", "1px solid red ")
            $('#txtCalleSucursal').css("border-radius", "4px ")
            $('#txtCalleSucursalOk').html("Debe Ingresar dirección de la sucursal")
        } else {
            $('#txtCalleSucursal').css("border", "")
            $('#txtCalleSucursal').css("border-radius", "")
            $('#txtCalleSucursalOk').html("")
        }
        if (largoSector == 0) {
            $('#idSector').focus();
            $('#idSector').val("");
            $('#idSector').css("border", "1px solid red ")
            $('#idSector').css("border-radius", "4px ")
            $('#idSectorOk').html("Debe Ingresar un Sector")
        } else {
            $('#idSector').css("border", "")
            $('#idSector').css("border-radius", "")
            $('#idSectorOk').html("")
        }
        if (largoFechaConstitucion == 0) {
            $('#txtFechaConstitucion').focus();
            $('#txtFechaConstitucion').val("");
            $('#txtFechaConstitucion').css("border", "1px solid red ")
            $('#txtFechaConstitucion').css("border-radius", "4px ")
            $('#txtFechaConstitucionOk').html("Debe Ingresar una fecha de Constitución")
        } else {
            $('#txtFechaConstitucion').css("border", "")
            $('#txtFechaConstitucion').css("border-radius", "")
            $('#txtFechaConstitucionOk').html("")
        }

        if (largoFechaUltimaRenovacion == 0) {
            $('#txtFechaUltimaRenovacion').focus();
            $('#txtFechaUltimaRenovacion').val("");
            $('#txtFechaUltimaRenovacion').css("border", "1px solid red ")
            $('#txtFechaUltimaRenovacion').css("border-radius", "4px ")
            $('#txtFechaUltimaRenovacionOk').html("Debe Ingresar una fecha de la última renovación")
        } else {
            $('#txtFechaUltimaRenovacion').css("border", "")
            $('#txtFechaUltimaRenovacion').css("border-radius", "")
            $('#txtFechaUltimaRenovacionOk').html("")
        }
        if (largoNombrePresidenteCHPS == 0) {
            $('#txtNombrePresidente').focus();
            $('#txtNombrePresidente').val("");
            $('#txtNombrePresidente').css("border", "1px solid red ")
            $('#txtNombrePresidente').css("border-radius", "4px ")
            $('#txtNombrePresidenteOk').html("Debe Ingresar el nombre del Presidente CPHS")
        } else {
            $('#txtNombrePresidente').css("border", "")
            $('#txtNombrePresidente').css("border-radius", "")
            $('#txtNombrePresidenteOk').html("")
        }
    
    if (largoRutPresidenteCPHS == 0) {
        $('#txtRutPresidente').focus();
        $('#txtRutPresidente').val("");
        $('#txtRutPresidente').css("border", "1px solid red ")
        $('#txtRutPresidente').css("border-radius", "4px ")
        $('#txtRutPresidenteOk').html("Debe Ingresar el rut del Presidente CPHS")
    } else {
        $('#txtRutPresidente').css("border", "")
        $('#txtRutPresidente').css("border-radius", "")
        $('#txtRutPresidenteOk').html("")
    }
        if (largoNombreSecretarioCPHS == 0) {
            $('#idNombreSecretarioCPHS').focus();
            $('#idNombreSecretarioCPHS').val("");
            $('#idNombreSecretarioCPHS').css("border", "1px solid red ")
            $('#idNombreSecretarioCPHS').css("border-radius", "4px ")
            $('#idNombreSecretarioCPHSOk').html("Debe Ingresar el nombre del secretario CPHS")
        } else {
            $('#idNombreSecretarioCPHS').css("border", "")
            $('#idNombreSecretarioCPHS').css("border-radius", "")
            $('#idNombreSecretarioCPHSOk').html("")
        }
        
        if (largoRutSecretaioCPHS == 0) {
            $('#txtRutSecretarioCPHS').focus();
            $('#txtRutSecretarioCPHS').val("");
            $('#txtRutSecretarioCPHS').css("border", "1px solid red ")
            $('#txtRutSecretarioCPHS').css("border-radius", "4px ")
            $('#txtRutSecretarioCPHSOk').html("Debe Ingresar el rut del secretario CPHS")
        } else {
            $('#txtRutSecretarioCPHS').css("border", "")
            $('#txtRutSecretarioCPHS').css("border-radius", "")
            $('#txtRutSecretarioCPHSOk').html("")
        } 
        if (largoTelefonoSecretarioCPHS == 0) {
            $('#txtTelefonoSecretario').focus();
            $('#txtTelefonoSecretario').val("");
            $('#txtTelefonoSecretario').css("border", "1px solid red ")
            $('#txtTelefonoSecretario').css("border-radius", "4px ")
            $('#txtTelefonoSecretarioOk').html("Debe Ingresar el telefono del secretario CPHS")
        } else {
            $('#txtTelefonoSecretario').css("border", "")
            $('#txtTelefonoSecretario').css("border-radius", "")
            $('#txtTelefonoSecretarioOk').html("")
        }
        if (largoNombreExpertoAsesorACHS == 0) {
            $('#txtNombreExpertoACHS').focus();
            $('#txtNombreExpertoACHS').val("");
            $('#txtNombreExpertoACHS').css("border", "1px solid red ")
            $('#txtNombreExpertoACHS').css("border-radius", "4px ")
            $('#txtNombreExpertoACHSOk').html("Debe Ingresar el nombre del experto ACHS")
        } else {
            $('#txtNombreExpertoACHS').css("border", "")
            $('#txtNombreExpertoACHS').css("border-radius", "")
            $('#txtNombreExpertoACHSOk').html("")
        }
        if (largoTelefonoPresidenteCPHS == 0) {
            $('#txtTelefonoPresidente').focus();
            $('#txtTelefonoPresidente').val("");
            $('#txtTelefonoPresidente').css("border", "1px solid red ")
            $('#txtTelefonoPresidente').css("border-radius", "4px ")
            $('#txtTelefonoPresidenteOk').html("Debe Ingresar el telefono del Presidente CPHS")
        } else {
            $('#txtTelefonoPresidente').css("border", "")
            $('#txtTelefonoPresidente').css("border-radius", "")
            $('#txtTelefonoPresidenteOk').html("")
        }
        if (largoEmailCorporativoComiteParitario == 0) {
            $('#txtEmailCorporativo').focus();
            $('#txtEmailCorporativo').val("");
            $('#txtEmailCorporativo').css("border", "1px solid red ")
            $('#txtEmailCorporativo').css("border-radius", "4px ")
            $('#txtEmailCorporativoOk').html("Debe Ingresar el email corporativo")
        } else {
            $('#txtEmailCorporativo').css("border", "")
            $('#txtEmailCorporativo').css("border-radius", "")
            $('#txtEmailCorporativoOk').html("")
        }
        if (largoAgencia == 0) {
            $('#idAgencia').focus();
            $('#idAgencia').val("");
            $('#idAgencia').css("border", "1px solid red ")
            $('#idAgencia').css("border-radius", "4px ")
            $('#idAgenciaOk').html("Debe seleccionar una agencia")
        } else {
            $('#idAgencia').css("border", "")
            $('#idAgencia').css("border-radius", "")
            $('#idAgenciaOk').html("")
        }
        if (largoComuna == 0) {
            $('#idComuna').focus();
            $('#idComuna').val("");
            $('#idComuna').css("border", "1px solid red ")
            $('#idComuna').css("border-radius", "4px ")
            $('#idComunaOk').html("Debe seleccionar una comuna")
        } else {
            $('#idComuna').css("border", "")
            $('#idComuna').css("border-radius", "")
            $('#idComunaOk').html("")
        }
        if (largoNumeroCasaMatriz == 0) {
            $('#txtNumCasaMatriz').focus();
            $('#txtNumCasaMatriz').val("");
            $('#txtNumCasaMatriz').css("border", "1px solid red ")
            $('#txtNumCasaMatriz').css("border-radius", "4px ")
            $('#txtNumCasaMatrizOk').html("Debe ingresar el numero de la casa matriz")
        } else {
            $('#txtNumCasaMatriz').css("border", "")
            $('#txtNumCasaMatriz').css("border-radius", "")
            $('#txtNumCasaMatrizOk').html("")
        }
        if (largoComunaCasaMatriz == 0) {
            $('#txtComunaCasaMatriz').focus();
            $('#txtComunaCasaMatriz').val("");
            $('#txtComunaCasaMatriz').css("border", "1px solid red ")
            $('#txtComunaCasaMatriz').css("border-radius", "4px ")
            $('#txtComunaCasaMatrizOk').html("Debe ingresar la comuna de la casa matriz")
        } else {
            $('#txtComunaCasaMatriz').css("border", "")
            $('#txtComunaCasaMatriz').css("border-radius", "")
            $('#txtComunaCasaMatrizOk').html("")
        }
        if (largoNumeroSucursal == 0) {
            $('#txtNumeroSucursal').focus();
            $('#txtNumeroSucursal').val("");
            $('#txtNumeroSucursal').css("border", "1px solid red ")
            $('#txtNumeroSucursal').css("border-radius", "4px ")
            $('#txtNumeroSucursalOk').html("Debe ingresar el número de la sucursal")
        } else {
            $('#txtNumeroSucursal').css("border", "")
            $('#txtNumeroSucursal').css("border-radius", "")
            $('#txtNumeroSucursalOk').html("")
        }
        if (largoComunaSucursal == 0) {
            $('#txtComunaSucursal').focus();
            $('#txtComunaSucursal').val("");
            $('#txtComunaSucursal').css("border", "1px solid red ")
            $('#txtComunaSucursal').css("border-radius", "4px ")
            $('#txtComunaSucursalOk').html("Debe ingresar la comuna de la sucursal")
        } else {
            $('#txtComunaSucursal').css("border", "")
            $('#txtComunaSucursal').css("border-radius", "")
            $('#txtComunaSucursalOk').html("")
        }
        
    } else {
        insertar_sf();
        envioCP();

    }




  

})


function envioCP() {
    var frm = {
     nombreEmpresa: $('#txtNombreEmpresa').val(),
        rutEmpresa: $('#txtRutEmpresa').val(),
        NroAdherenteACHS: $('#txtNumTrabajadores').val(),
        tipoCPHS: $('#idTipoCHPS').val(),
        direccionCasaMatriz: $('#txtCalleCasaMatriz').val(),
        direccionSucursal: $('#txtCalleSucursal').val(),
        sector: $('#idSector').val(),
        fechaConstitucion: $('#txtFechaConstitucion').val(),
        fechaUltimaRenovacion: $('#txtFechaUltimaRenovacion').val(),
        nombrePresidenteCHPS: $('#txtNombrePresidente').val(),
        rutPresidenteCPHS: $('#txtRutPresidente').val(),
        nombreSecretarioCPHS: $('#idNombreSecretarioCPHS').val(),
        rutSecretaioCPHS: $('#txtRutSecretarioCPHS').val(),
        telefonoSecretarioCPHS: $('#txtTelefonoSecretario').val(),
        titularEmpresa1: $('#txtIntTitEmpresa1').html(),
        titularEmpresa2: $('#txtIntTitEmpresa2').html(),
        titularEmpresa3: $('#txtIntTitEmpresa3').html(),
        suplenteEmpresa1: $('#txtIntSupEmpresa1').html(),
        suplenteEmpresa2: $('#txtIntSupEmpresa2').html(),
        suplenteEmpresa3: $('#txtIntSupEmpresa3').html(),
        titularTrabajadores1: $('#txtIntTitTrabajadores1').html(),
        titularTrabajadores2: $('#txtIntTitTrabajadores2').html(),
        titularTrabajadores3: $('#txtIntTitTrabajadores3').html(),
        suplenteTrabajadores1: $('#txtIntSupTrabajadores1').html(),
        suplenteTrabajadores2: $('#txtIntSupTrabajadores2').html(),
        suplenteTrabajadores3: $('#txtIntSupTrabajadores3').html(),
        nombreExpertoAsesorACHS: $('#txtNombreExpertoACHS').val(),
        telefonoPresidenteCPHS: $('#txtTelefonoPresidente').val(),
        emailCorporativoComiteParitario: $('#txtEmailCorporativo').val(),
        agencia: $('#idAgencia').val(),
        comuna: $('#idComuna').val(),
        numeroCasaMatriz: $('#txtNumCasaMatriz').val(),
        comunaCasaMatriz: $('#txtComunaCasaMatriz').val(),
        numeroSucursal: $('#txtNumeroSucursal').val(),
        comunaSucursal: $('#txtComunaSucursal').val(),

    };

    $.ajax({
        type: "POST",
        url: "https://api.achs.lfi.cl/api/comiteparitario/envioformulario",

        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        crossDomain: true,
        data: JSON.stringify(frm),
        success: function (response) {
            EnviarCorreo(frm.rutEmpresa, frm.nombreEmpresa)
            location.reload()
           

        },
        failure: function (response) {
            console.log(response);
        },
        error: function (response) {
            console.log(response);
        }
    });

}


function EnviarCorreo(rutEmpresa, nombreEmpresa) {


    var objeto = {
        rutEmpresa: rutEmpresa,
        nombreEmpresa: nombreEmpresa,
    }
    $.ajax({
        url: 'https://api.achs.lfi.cl/api/comiteparitario/send_email',
        method: "post",  // post
        async: true,
        data: JSON.stringify(objeto),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (Result) {
         
        },
        error: function (result) {
     
        }     

    });
   

}



 function insertar_sf() {

    var obj = {
     nombreEmpresa: $('#txtNombreEmpresa').val(),
        rutEmpresa: $('#txtRutEmpresa').val(),
        NroAdherenteACHS: $('#txtNumTrabajadores').val(),
        tipoCPHS: $('#idTipoCHPS').val(),
        direccionCasaMatriz: $('#txtCalleCasaMatriz').val(),
        direccionSucursal: $('#txtCalleSucursal').val(),
        sector: $('#idSector').val(),
        fechaConstitucion: $('#txtFechaConstitucion').val(),
        fechaUltimaRenovacion: $('#txtFechaUltimaRenovacion').val(),
        nombrePresidenteCHPS: $('#txtNombrePresidente').val(),
        rutPresidenteCPHS: $('#txtRutPresidente').val(),
        nombreSecretarioCPHS: $('#idNombreSecretarioCPHS').val(),
        rutSecretaioCPHS: $('#txtRutSecretarioCPHS').val(),
        telefonoSecretarioCPHS: $('#txtTelefonoSecretario').val(),
        titularEmpresa1: $('#txtIntTitEmpresa1').html(),
        titularEmpresa2: $('#txtIntTitEmpresa2').html(),
        titularEmpresa3: $('#txtIntTitEmpresa3').html(),
        suplenteEmpresa1: $('#txtIntSupEmpresa1').html(),
        suplenteEmpresa2: $('#txtIntSupEmpresa2').html(),
        suplenteEmpresa3: $('#txtIntSupEmpresa3').html(),
        titularTrabajadores1: $('#txtIntTitTrabajadores1').html(),
        titularTrabajadores2: $('#txtIntTitTrabajadores2').html(),
        titularTrabajadores3: $('#txtIntTitTrabajadores3').html(),
        suplenteTrabajadores1: $('#txtIntSupTrabajadores1').html(),
        suplenteTrabajadores2: $('#txtIntSupTrabajadores2').html(),
        suplenteTrabajadores3: $('#txtIntSupTrabajadores3').html(),
        nombreExpertoAsesorACHS: $('#txtNombreExpertoACHS').val(),
        telefonoPresidenteCPHS: $('#txtTelefonoPresidente').val(),
        emailCorporativoComiteParitario: $('#txtEmailCorporativo').val(),
        agencia: $('#idAgencia').val(),
        comuna: $('#idComuna').val(),
        numeroCasaMatriz: $('#txtNumCasaMatriz').val(),
        comunaCasaMatriz: $('#txtComunaCasaMatriz').val(),
        numeroSucursal: $('#txtNumeroSucursal').val(),
        comunaSucursal: $('#txtComunaSucursal').val(),

    };

        var form_data = new FormData();

        for ( var key in obj ) {
            form_data.append(key, obj[key]);
        }

        
        var files = $("#file").get(0).files;
        var data = new FormData();
        for (i = 0; i < files.length; i++) {
            form_data.append("file" + i, files[i]);
        }


    
             $.ajax({
                type: "POST",
                 url:"/empresas/comites-paritarios/inscribe-tu-comite-paritario/insertar_sf",
                 contentType : false,
                 processData : false,
                data: form_data,
                success: function (response) {
                   

                },
                failure: function (response) {
                    console.log(response);
                },
                error: function (response) {
                    console.log(response);
                }
            }); 
        }






    function insertar_sf_test() {
                  var obj = {
 nombreEmpresa:"",
rutEmpresa: "",
NroAdherenteACHS: "",
tipoCPHS: "",
direccionCasaMatriz: "",
direccionSucursal: "",
sector: "",
fechaConstitucion: "",
fechaUltimaRenovacion: "",
nombrePresidenteCHPS: "",
rutPresidenteCPHS: "",
nombreSecretarioCPHS: "",
rutSecretaioCPHS: "",
telefonoSecretarioCPHS: "",
titularEmpresa1: "",
titularEmpresa2: "",
titularEmpresa3: "",
suplenteEmpresa1: "",
suplenteEmpresa2: "",
suplenteEmpresa3: "",
titularTrabajadores1: "",
titularTrabajadores2: "",
titularTrabajadores3: "",
suplenteTrabajadores1: "",
suplenteTrabajadores2: "",
suplenteTrabajadores3: "",
nombreExpertoAsesorACHS: "",
telefonoPresidenteCPHS: "",
emailCorporativoComiteParitario: "",
agencia: "",
comuna: "",
numeroCasaMatriz: "",
comunaCasaMatriz: "",
numeroSucursal: "",
comunaSucursal: "",
    };

        var form_data = new FormData();

        for ( var key in obj ) {
            form_data.append(key, obj[key]);
        }

        
        var files = $("#file").get(0).files;
        var data = new FormData();
        for (i = 0; i < files.length; i++) {
            form_data.append("file" + i, files[i]);
        }


    
             $.ajax({
                type: "POST",
                 url:"/empresas/comites-paritarios/inscribe-tu-comite-paritario/insertar_sf",
                 contentType : false,
                 processData : false,
                data: form_data,
                success: function (response) {
                   

                },
                failure: function (response) {
                    console.log(response);
                },
                error: function (response) {
                    console.log(response);
                }
            }); 
        }



