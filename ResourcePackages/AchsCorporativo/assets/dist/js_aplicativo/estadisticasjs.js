

/* acordeon */
$(function() {
    $( "#acordeonEsta" ).accordion({
		active: false,    
		header: "h2",
		heightStyle: "content"
	});
	$( "#acordeonEsta .pestanas" ).tabs();
	
});

var spbx_graficos={
    getParticipacion: function () {

		//var  valores = getDatosGraficosTipos('estadisticas_grafico_01');
        //var valores = getDatosGraficosTipos('grafico_participacion');
        ////////////////////OK
        var valores = "";
        $.ajax({
            url: '/json-estadistica/getParticipacion',
            method: "post",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (response) {
                valores = response;
                var datarows = [];

                datarows.push({ label: "IST", value: $(valores).attr("participacion_ist") });
                datarows.push({ label: "ACHS", value: $(valores).attr("participacion_achs") });
                datarows.push({ label: "MUSEG", value: $(valores).attr("participacion_mutual") });
                datarows.push({ label: "ISL", value: $(valores).attr("participacion_isl") });
                var revenueChart = new FusionCharts({
                    type: 'doughnut2d',
                    renderAt: 'participacionmercado',
                    width: '640',
                    height: '450',
                    dataFormat: 'json',
                    dataSource: {
                        "chart": {
                            "baseFontSize": "11",
                            "numberSuffix": "%",
                            "paletteColors": "#ED6C2A,#1aaf5d,#62398F,#284196",
                            "bgColor": "#ffffff",
                            "showBorder": "0",
                            "use3DLighting": "0",
                            "showShadow": "0",
                            "enableSmartLabels": "0",
                            "startingAngle": "310",
                            "showLabels": "1",
                            "showPercentValues": "1",
                            "showLegend": "1",
                            "legendShadow": "0",
                            "legendBorderAlpha": "0",
                            "centerLabel": "Participación de $label: $value", "centerLabelBold": "1",
                            "showTooltip": "0",
                            "captionFontSize": "14",
                            "subcaptionFontSize": "14",
                            "subcaptionFontBold": "0",
                            "outCnvBaseFontSize": "14"
                        },
                        "data": datarows
                    }
                });
                revenueChart.render();
            },
            error: function (response) {

            }
        });

	 },getNumeroEmpresas:function(){
	 
	     /////////OK
	     //var valores = getDatosGraficosTipos('grafico_empresas');
	      valores = "";
	         $.ajax({
	             url: '/json-estadistica/getNumeroEmpresas',
	             method: "post",
	             dataType: "json",
	             contentType: "application/json; charset=utf-8",
	             success: function (response) {

                     valores = response
                     var datarowsx = [];
                     for (var x = 0; x <= valores.length - 1; x++) {
                         datarowsx.push({ label: $(valores[x]).attr("Fecha"), value: $(valores[x]).attr("nro_empresas").replace(".", "") });
                     }
                     var chartnumeroempresa = new FusionCharts({
                         type: 'column2d',
                         renderAt: 'numeroempresasafiliadas',
                         width: '640',
                         height: '350',
                         dataFormat: 'json',
                         dataSource: {
                             "chart": {
                                 "rotatevalues": "1",
                                 "outCnvBaseFontColor": "#666",
                                 "toolTipBgColor": "666",
                                 "showvalues": "1",
                                 "plotgradientcolor": "",
                                 "formatnumberscale": "0",
                                 "showplotborder": "0",
                                 "palettecolors": "#268040",
                                 "canvaspadding": "0",
                                 "bgcolor": "FFFFFF",
                                 "showalternatehgridcolor": "0",
                                 "divlinecolor": "CCCCCC",
                                 "showcanvasborder": "0",
                                 "interactivelegend": "0",
                                 "showpercentvalues": "0",
                                 "canvasborderalpha": "0",
                                 "showborder": "0",
                                 "palettecolors": "#1d582c",
                                 "yaxismaxvalue": "80000",
                                 "labelDisplay": "rotate",
                                 "thousandSeparator": ".",
                                 "baseFontColor": "#fff",
                                 "placevaluesInside": "1",
                                 "showYAxisValue": "0"
                             },
                             "data": datarowsx

                         }
                     }).render();
	             },
	             error: function (result) {	   }

	         });
	 
	 },getNumeroTrabajadoresAfiliados:function(){

	     //var  valores = getDatosGraficosNew('graficos_hombre_mujer','af_x002d_hombres;af_x002d_mujeres');//getDatosGraficosTipos('estadisticas_grafico_03');

	     valores = "";
	     $.ajax({
	         url: '/json-estadistica/getNumeroTrabajadoresAfiliados',
	         method: "post",
	         dataType: "json",
	         contentType: "application/json; charset=utf-8",
	         success: function (response) {
	             valores = response;
	             var datacategorie = [];
	             var datavalueshombre = [];
	             var datavaluesmujer = [];
	             for (var x = 0; x <= valores.length - 1; x++) {
	                 var hs = $(valores[x]).attr("af_hombres").replace(/\./g, '');
	                 var mjs = $(valores[x]).attr("af_mujeres").replace(/\./g, '');
	                 datacategorie.push({ label: $(valores[x]).attr("fecha") });
	                 datavalueshombre.push({ value: hs });
	                 datavaluesmujer.push({ value: mjs });

	             }
	             var revenueChart = new FusionCharts({
	                 type: 'stackedcolumn2d',
	                 renderAt: 'numerotrabajadoresafiliadas',
	                 width: '640',
	                 height: '350',
	                 dataFormat: 'json',
	                 dataSource:
                             {
                                 "chart": {
                                     "showvalues": "1",
                                     "plotgradientcolor": "",
                                     "formatnumberscale": "0",
                                     "showplotborder": "0",
                                     "palettecolors": "#268040,#7ebc41,#009688,#B0D67A,#2C560A,#DD9D82",
                                     "canvaspadding": "0",
                                     "bgcolor": "FFFFFF",
                                     "showalternatehgridcolor": "0",
                                     "divlinecolor": "CCCCCC",
                                     "showcanvasborder": "0",
                                     "interactivelegend": "0",
                                     "showpercentvalues": "0",
                                     "canvasborderalpha": "0",
                                     "showborder": "0",
                                     "baseFontColor": "#fff",
                                     "outCnvBaseFontColor": "#666",
                                     "toolTipBgColor": "666",
                                     "rotateValues": "1",
                                     "legendBgAlpha": "0",
                                     "legendBorderAlpha": "0",
                                     "legendShadow": "0",
                                     "legendItemFontSize": "10",
                                     "legendItemFontColor": "#666666",
                                     "labelDisplay": "rotate",
                                     "thousandSeparator": ".",
                                     "showYAxisValue": "0"
                                 },
                                 "categories": [
                                     {
                                         "category": datacategorie
                                     }
                                 ],
                                 "dataset": [
                                     {
                                         "seriesname": "Hombres",
                                         "renderas": "Area",
                                         "data": datavalueshombre
                                     },
                                     {
                                         "seriesname": "Mujeres",
                                         "renderas": "Area",
                                         "data": datavaluesmujer
                                     }
                                 ]
                             }

	             }).render();
	         },
	         error: function (result) { }
	     });
			         						 
		},getEvoluacionTasaReposo:function(){
		
		/////////OK
		    valores = "";
		    $.ajax({
		        url: '/json-estadistica/getEvolucionTasaReposo',
		        method: "post",
		        dataType: "json",
		        contentType: "application/json; charset=utf-8",
		        success: function (response) {
                   
		            var valores = response;
		            var datacategorie = [];
		            var datavalor = [];

		            for (var x = 0; x <= valores.length - 1; x++) {
		                var mjs = $(valores[x]).attr("porcentaje").replace(/\,/g, '.');

		                datacategorie.push({ label: $(valores[x]).attr("fecha") });
		                datavalor.push({ value: mjs })
		            }
		            var revenueChartTasa = new FusionCharts({
		                type: 'msline',
		                renderAt: 'evoluciontasa',
		                width: '650',
		                height: '350',
		                dataFormat: 'json',
		                dataSource: {
		                    "chart": {
		                        "rotatevalues": "1",
		                        "outCnvBaseFontColor": "#666",
		                        "toolTipBgColor": "#666",
		                        "showvalues": "1",
		                        "plotgradientcolor": "",
		                        "formatnumberscale": "0",
		                        "showplotborder": "0",
		                        "palettecolors": "#268040,#95c120",
		                        "canvaspadding": "0",
		                        "bgcolor": "FFFFFF",
		                        "showalternatehgridcolor": "0",
		                        "divlinecolor": "CCCCCC",
		                        "showcanvasborder": "0",
		                        "interactivelegend": "0",
		                        "showpercentvalues": "1",
		                        "canvasborderalpha": "0",
		                        "showborder": "1",
		                        "showYAxisValue": "1",
		                        "formatnumberscale": "0",
		                        "showborder": "0",
		                        "baseFontColor": "#fff",
		                        "rotatevalues": "1",
		                        "rotateLabels": "1",
		                        "valueposition": "auto",
		                        "placevaluesInside": "1",
		                        "numberScaleValue": ".01",
		                        "numberScaleUnit": "%"

		                    },
		                    "categories": [
						        {
						            "category": datacategorie
						        }
		                    ],
		                    "dataset": [
						        {

						            "data": datavalor
						        }
		                    ]
		                }
		            }).render();
		        },
		        error: function (result) {

		        }

		    });
		          //var  valores = getDatosGraficosTipos('grafico_evolucion_tasas');
		        

		}, getValoresxIndustria: function () {

            //////////////OK
	        $("#tabla-sectores").empty();
	        $("#tabla-sectores").append("<tr><th>Industria</th><th>Accidentes</th><th>Masa</th><th>Tasa accidentabilidad</th></tr>");

            $.ajax({
                url: '/json-estadistica/getValoresxIndustria',
                method: "post",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (response) {

                    var valor = response;
                    var htmltable
                 
                    for (var x = 0; x <= valor.length - 1; x++) {
                        htmltable += `<tr class="  ${ $(valor[x]).attr('class')  } "><td><span>${$(valor[x]).attr('sector').toLowerCase()   } </span></td><td> ${$(valor[x]).attr("accidentes")} </td><td> ${$(valor[x]).attr("masa")} </td><td> ${$(valor[x]).attr("tasa_accidentibilidad")} </td></tr>`;
                    }
                    $("#tabla-sectores").append(htmltable);
                },
                error: function (result) {

                }

            });

           


     },
     
     
     getnumeroAccidenteLaborales:function(){
     
           //var  valores = getDatosGraficosTipos('estadisticas_grafico_06');
         //  var  valores = getDatosGraficosNew('graficos_hombre_mujer','acc_x002d_hombres;acc_x002d_mujeres');
         $.ajax({
             url: '/json-estadistica/getNumeroTrabajadoresAfiliados',
             method: "post",
             dataType: "json",
             contentType: "application/json; charset=utf-8",
             success: function (response) {

                 var valores = response;
                 var datacategorie = [];
                 var datavalueshombre = [];
                 var datavaluesmujer = [];

                 for (var x = 0; x <= valores.length - 1; x++) {
                     var hs = $(valores[x]).attr("acc_hombres").replace(/\./g, '');
                     var mjs = $(valores[x]).attr("acc_mujeres").replace(/\./g, '');
                     datacategorie.push({ label: $(valores[x]).attr("fecha") });
                     datavalueshombre.push({ value: hs });
                     datavaluesmujer.push({ value: mjs });

                 }


                 var revenueChart = new FusionCharts({
                     type: 'stackedcolumn2d',
                     renderAt: 'numeroaccidentes',
                     width: '640',
                     height: '350',
                     dataFormat: 'json',
                     dataSource:
                             {
                                 "chart": {
                                     "showvalues": "1",
                                     "plotgradientcolor": "",
                                     "formatnumberscale": "0",
                                     "showplotborder": "0",
                                     "palettecolors": "#268040,#7ebc41,#009688,#B0D67A,#2C560A,#DD9D82",
                                     "canvaspadding": "0",
                                     "bgcolor": "FFFFFF",
                                     "showalternatehgridcolor": "0",
                                     "divlinecolor": "CCCCCC",
                                     "showcanvasborder": "0",
                                     "interactivelegend": "0",
                                     "showpercentvalues": "0",
                                     "canvasborderalpha": "0",
                                     "showborder": "0",
                                     "baseFontColor": "#fff",
                                     "outCnvBaseFontColor": "#666",
                                     "toolTipBgColor": "666",
                                     "rotateValues": "1",
                                     "legendBgAlpha": "0",
                                     "legendBorderAlpha": "0",
                                     "legendShadow": "0",
                                     "legendItemFontSize": "10",
                                     "legendItemFontColor": "#666666",
                                     "labelDisplay": "rotate",
                                     "thousandSeparator": ".",
                                     "showYAxisValue": "0"
                                 },
                                 "categories": [
                                     {
                                         "category": datacategorie
                                     }
                                 ],
                                 "dataset": [
                                     {
                                         "seriesname": "Hombres",
                                         "renderas": "Area",
                                         "data": datavalueshombre
                                     },
                                     {
                                         "seriesname": "Mujeres",
                                         "renderas": "Area",
                                         "data": datavaluesmujer
                                     }
                                 ]
                             }

                 }).render();
             },
             error: function (result) {

         }

     });
			        

     
     },getnumeroAccidenteTrayecto:function(){
     
                    //var  valores = getDatosGraficosTipos('estadisticas_grafico_07
		 ////////////////////OK
		 $.ajax({
			 url: '/json-estadistica/getNumeroTrabajadoresAfiliados',
			 method: "post",
			 dataType: "json",
			 contentType: "application/json; charset=utf-8",
			 success: function (response) {
				 var valores = response;

			         var datacategorie=[];
			         var datavalueshombre =[];
			         var datavaluesmujer =[];
                     
			         for(var x=0;x<=valores.length-1;x++){
			         	var hs=$(valores[x]).attr("tr_hombres").replace(/\./g, '');
			         	var mjs=$(valores[x]).attr("tr_mujeres").replace(/\./g, '');
		                 datacategorie.push({label:$(valores[x]).attr("fecha")}); 
		                 datavalueshombre.push({value:hs});
		                 datavaluesmujer.push({value:mjs});

		             }
		      

					    var revenueChart = new FusionCharts({
					        type: 'stackedcolumn2d',
					        renderAt: 'numerotrayecto',
					        width: '640',
					        height: '350',
					        dataFormat: 'json',
					        dataSource:
									{    "chart": {
									        "showvalues": "1",
									        "plotgradientcolor": "",
									        "formatnumberscale": "0",
									        "showplotborder": "0",
									        "palettecolors": "#268040,#7ebc41,#009688,#B0D67A,#2C560A,#DD9D82",
									        "canvaspadding": "0",
									        "bgcolor": "FFFFFF",
									        "showalternatehgridcolor": "0",
									        "divlinecolor": "CCCCCC",
									        "showcanvasborder": "0",
									        "interactivelegend": "0",
									        "showpercentvalues": "0",
									        "canvasborderalpha": "0",
									        "showborder": "0",
											"baseFontColor": "#fff",
											"outCnvBaseFontColor": "#666",
											"toolTipBgColor":"666",
											"rotateValues":"1",
											"legendBgAlpha": "0",
									        "legendBorderAlpha": "0",
									        "legendShadow": "0",
									        "legendItemFontSize": "10",
									        "legendItemFontColor": "#666666",
											"labelDisplay":"rotate",
											"thousandSeparator":".",
											"showYAxisValue":"0"
									    },
									    "categories": [
									        {
									            "category": datacategorie
									        }
									    ],
									    "dataset": [
									        {
									            "seriesname": "Hombres",
									            "renderas": "Area",
									            "data": datavalueshombre
									        },
									        {
									            "seriesname": "Mujeres",
									            "renderas": "Area",
									            "data":datavaluesmujer 
									        }
									    ]
									}
						  
					}).render();
			 },
			 error: function (result) {

			 }

		 });

     
     },getnumeroEnfermedades:function(){
		 $.ajax({
			 url: '/json-estadistica/getNumeroTrabajadoresAfiliados',
			 method: "post",
			 dataType: "json",
			 contentType: "application/json; charset=utf-8",
			 success: function (response) {
				 var valores = response;
			         var datacategorie=[];
			         var datavalueshombre =[];
			         var datavaluesmujer =[];

			         for(var x=0;x<=valores.length-1;x++){
			         	var hs=$(valores[x]).attr("ep_hombres").replace(/\./g, '');
			         	var mjs=$(valores[x]).attr("ep_mujeres").replace(/\./g, '');
		                 datacategorie.push({label:$(valores[x]).attr("fecha")}); 
		                 datavalueshombre.push({value:hs});
		                 datavaluesmujer.push({value:mjs});

		             }
		      

					    var revenueChart = new FusionCharts({
					        type: 'stackedcolumn2d',
					        renderAt: 'numeroenfermedades',
					        width: '640',
					        height: '350',
					        dataFormat: 'json',
					        dataSource:
									{    "chart": {
									        "showvalues": "1",
									        "plotgradientcolor": "",
									        "formatnumberscale": "0",
									        "showplotborder": "0",
									        "palettecolors": "#268040,#7ebc41,#009688,#B0D67A,#2C560A,#DD9D82",
									        "canvaspadding": "0",
									        "bgcolor": "FFFFFF",
									        "showalternatehgridcolor": "0",
									        "divlinecolor": "CCCCCC",
									        "showcanvasborder": "0",
									        "interactivelegend": "0",
									        "showpercentvalues": "0",
									        "canvasborderalpha": "0",
									        "showborder": "0",
											"baseFontColor": "#fff",
											"outCnvBaseFontColor": "#666",
											"toolTipBgColor":"666",
											"rotateValues":"1",
											"legendBgAlpha": "0",
									        "legendBorderAlpha": "0",
									        "legendShadow": "0",
									        "legendItemFontSize": "10",
									        "legendItemFontColor": "#666666",
											"labelDisplay":"rotate",
											"thousandSeparator":".",
											"showYAxisValue":"0"
									    },
									    "categories": [
									        {
									            "category": datacategorie
									        }
									    ],
									    "dataset": [
									        {
									            "seriesname": "Hombres",
									            "renderas": "Area",
									            "data": datavalueshombre
									        },
									        {
									            "seriesname": "Mujeres",
									            "renderas": "Area",
									            "data":datavaluesmujer 
									        }
									    ]
									}
						  
					}).render();
			 },
			 error: function (result) {

			 }

		 });

     
     },getnumeroreposolaboral:function(){
     
		 $.ajax({
			 url: '/json-estadistica/getNumeroTrabajadoresAfiliados',
			 method: "post",
			 dataType: "json",
			 contentType: "application/json; charset=utf-8",
			 success: function (response) {
				 var valores = response;

			         var datacategorie=[];
			         var datavalueshombre =[];
			         var datavaluesmujer =[];

			         for(var x=0;x<=valores.length-1;x++){
			         	var hs=$(valores[x]).attr("dptrab_hombres").replace(/\./g, '');
			         	var mjs=$(valores[x]).attr("dptrab_mujeres").replace(/\./g, '');
		                 datacategorie.push({label:$(valores[x]).attr("fecha")}); 
		                 datavalueshombre.push({value:hs});
		                 datavaluesmujer.push({value:mjs});

		             }
		      

					    var revenueChart = new FusionCharts({
					        type: 'stackedcolumn2d',
					        renderAt: 'diasreposolaboraltrabajo',
					        width: '640',
					        height: '350',
					        dataFormat: 'json',
					        dataSource:
									{    "chart": {
									        "showvalues": "1",
									        "plotgradientcolor": "",
									        "formatnumberscale": "0",
									        "showplotborder": "0",
									        "palettecolors": "#268040,#7ebc41,#009688,#B0D67A,#2C560A,#DD9D82",
									        "canvaspadding": "0",
									        "bgcolor": "FFFFFF",
									        "showalternatehgridcolor": "0",
									        "divlinecolor": "CCCCCC",
									        "showcanvasborder": "0",
									        "interactivelegend": "0",
									        "showpercentvalues": "0",
									        "canvasborderalpha": "0",
									        "showborder": "0",
											"baseFontColor": "#fff",
											"outCnvBaseFontColor": "#666",
											"toolTipBgColor":"666",
											"rotateValues":"1",
											"legendBgAlpha": "0",
									        "legendBorderAlpha": "0",
									        "legendShadow": "0",
									        "legendItemFontSize": "10",
									        "legendItemFontColor": "#666666",
											"labelDisplay":"rotate",
											"thousandSeparator":".",
											"showYAxisValue":"0"
									    },
									    "categories": [
									        {
									            "category": datacategorie
									        }
									    ],
									    "dataset": [
									        {
									            "seriesname": "Hombres",
									            "renderas": "Area",
									            "data": datavalueshombre
									        },
									        {
									            "seriesname": "Mujeres",
									            "renderas": "Area",
									            "data":datavaluesmujer 
									        }
									    ]
									}
						  
					}).render();
			 },
			 error: function (result) {

			 }

		 });
     },getnumeroReposoTrayecto:function(){
     
		 $.ajax({
			 url: '/json-estadistica/getNumeroTrabajadoresAfiliados',
			 method: "post",
			 dataType: "json",
			 contentType: "application/json; charset=utf-8",
			 success: function (response) {
				 var valores = response;

			         var datacategorie=[];
			         var datavalueshombre =[];
			         var datavaluesmujer =[];

			         for(var x=0;x<=valores.length-1;x++){
			         	var hs=$(valores[x]).attr("dptray_hombres").replace(/\./g, '');
			         	var mjs=$(valores[x]).attr("dptray_mujeres").replace(/\./g, '');
		                 datacategorie.push({label:$(valores[x]).attr("fecha")}); 
		                 datavalueshombre.push({value:hs});
		                 datavaluesmujer.push({value:mjs});

		             }
		      

					    var revenueChart = new FusionCharts({
					        type: 'stackedcolumn2d',
					        renderAt: 'diasreposolaboraltrayecto',
					        width: '640',
					        height: '350',
					        dataFormat: 'json',
					        dataSource:
									{    "chart": {
									        "showvalues": "1",
									        "plotgradientcolor": "",
									        "formatnumberscale": "0",
									        "showplotborder": "0",
									        "palettecolors": "#268040,#7ebc41,#009688,#B0D67A,#2C560A,#DD9D82",
									        "canvaspadding": "0",
									        "bgcolor": "FFFFFF",
									        "showalternatehgridcolor": "0",
									        "divlinecolor": "CCCCCC",
									        "showcanvasborder": "0",
									        "interactivelegend": "0",
									        "showpercentvalues": "0",
									        "canvasborderalpha": "0",
									        "showborder": "0",
											"baseFontColor": "#fff",
											"outCnvBaseFontColor": "#666",
											"toolTipBgColor":"666",
											"rotateValues":"1",
											"legendBgAlpha": "0",
									        "legendBorderAlpha": "0",
									        "legendShadow": "0",
									        "legendItemFontSize": "10",
									        "legendItemFontColor": "#666666",
											"labelDisplay":"rotate",
											"thousandSeparator":".",
											"showYAxisValue":"0"
									    },
									    "categories": [
									        {
									            "category": datacategorie
									        }
									    ],
									    "dataset": [
									        {
									            "seriesname": "Hombres",
									            "renderas": "Area",
									            "data": datavalueshombre
									        },
									        {
									            "seriesname": "Mujeres",
									            "renderas": "Area",
									            "data":datavaluesmujer 
									        }
									    ]
									}
						  
					}).render();
			 },
			 error: function (result) {

			 }

		 });
     
     
     },getnumeroReposotEP:function(){
     
		 $.ajax({
			 url: '/json-estadistica/getNumeroTrabajadoresAfiliados',
			 method: "post",
			 dataType: "json",
			 contentType: "application/json; charset=utf-8",
			 success: function (response) {
				 var valores = response;

			         var datacategorie=[];
			         var datavalueshombre =[];
			         var datavaluesmujer =[];

			         for(var x=0;x<=valores.length-1;x++){
			         	var hs=$(valores[x]).attr("dpep_hombres").replace(/\./g, '');
			         	var mjs=$(valores[x]).attr("dpep_mujeres").replace(/\./g, '');
		                 datacategorie.push({label:$(valores[x]).attr("fecha")}); 
		                 datavalueshombre.push({value:hs});
		                 datavaluesmujer.push({value:mjs});

		             }
		      
				 var revenueChart = new FusionCharts({
					 type: 'stackedcolumn2d',
					 renderAt: 'diasreposoenfermedades',
					 width: '640',
					 height: '350',
					 dataFormat: 'json',
					 dataSource:
					 {
						 "chart": {
							 "showvalues": "1",
							 "plotgradientcolor": "",
							 "formatnumberscale": "0",
							 "showplotborder": "0",
							 "palettecolors": "#268040,#7ebc41,#009688,#B0D67A,#2C560A,#DD9D82",
							 "canvaspadding": "0",
							 "bgcolor": "FFFFFF",
							 "showalternatehgridcolor": "0",
							 "divlinecolor": "CCCCCC",
							 "showcanvasborder": "0",
							 "interactivelegend": "0",
							 "showpercentvalues": "0",
							 "canvasborderalpha": "0",
							 "showborder": "0",
							 "baseFontColor": "#fff",
							 "outCnvBaseFontColor": "#666",
							 "toolTipBgColor": "666",
							 "rotateValues": "1",
							 "legendBgAlpha": "0",
							 "legendBorderAlpha": "0",
							 "legendShadow": "0",
							 "legendItemFontSize": "10",
							 "legendItemFontColor": "#666666",
							 "labelDisplay": "rotate",
							 "thousandSeparator": ".",
							 "showYAxisValue": "0"
						 },
						 "categories": [
							 {
								 "category": datacategorie
							 }
						 ],
						 "dataset": [
							 {
								 "seriesname": "Hombres",
								 "renderas": "Area",
								 "data": datavalueshombre
							 },
							 {
								 "seriesname": "Mujeres",
								 "renderas": "Area",
								 "data": datavaluesmujer
							 }
						 ]
					 }

				 }).render();
			 },
			 error: function (result) {

			 }

		 });
     },getsubsidiosgenerales:function(){
     
		 $.ajax({
			 url: '/json-estadistica/getListadoSubsidios',
			 method: "post",
			 dataType: "json",
			 contentType: "application/json; charset=utf-8",
			 success: function (response) {
				 var valores = response;

			         var datacategorie=[];
			         var datavaluesnro =[];
			         var datavaluemontos=[];

			         for(var x=0;x<=valores.length-1;x++){
			         	var hs=$(valores[x]).attr("subsidios").replace(/\./g, '');
			         	var mjs=$(valores[x]).attr("monto_sub").replace(/\./g, '');
		                 datacategorie.push({label:$(valores[x]).attr("fecha"),stepSkipped: false }); 
		                 datavaluesnro.push({value:hs});
		                 datavaluemontos.push({value:mjs});

		             }

     		var chatnrosubsidos = new FusionCharts({
					        type: 'mscombi2d',
					        renderAt: 'subsidiosgeneral',
					        width: '640',
					        height: '300',
					        dataFormat: 'json',
					        dataSource: {
					            "chart": {
					                "showvalues": "1",
									        "plotgradientcolor": "",
									        "formatnumberscale": "0",
									        "showplotborder": "0",
									        "palettecolors": "#268040,#7ebc41,#009688,#B0D67A,#2C560A,#DD9D82",
									        "canvaspadding": "0",
									        "bgcolor": "FFFFFF",
									        "showalternatehgridcolor": "0",
									        "divlinecolor": "CCCCCC",
									        "showcanvasborder": "0",
									        "interactivelegend": "0",
									        "showpercentvalues": "0",
									        "canvasborderalpha": "0",
									        "showborder": "0",
											"baseFontColor": "#fff",
											"outCnvBaseFontColor": "#666",
											"toolTipBgColor":"666",
											"rotateValues":"0",
											"legendBgAlpha": "0",
									        "legendBorderAlpha": "0",
									        "legendShadow": "0",
									        "legendItemFontSize": "10",
									        "legendItemFontColor": "#666666",
											"labelDisplay":"rotate",
											"thousandSeparator":".",
											"showYAxisValue":"0",
											"placevaluesInside":"1" 
					            },
					            "categories": [{
					                "category": datacategorie
					            }],
					            "dataset": [{
					                "seriesName": "N\u00FAmero",


					                "data":  datavaluesnro
					            }, {
					                "seriesName": "Monto($MM)",
					                "renderAs": "line",
					                "showValues": "1",
					                "data": datavaluemontos		
					            }]
					        }
					    }).render();
			 },
			 error: function (result) {

			 }

		 });

     
     
     
     },getsubsidostrayectos:function(){
     
		 $.ajax({
			 url: '/json-estadistica/getListadoSubsidios',
			 method: "post",
			 dataType: "json",
			 contentType: "application/json; charset=utf-8",
			 success: function (response) {
				 var valores = response;

			         var datacategorie=[];
			         var datavaluesnro =[];
			         var datavaluemontos=[];

			         for(var x=0;x<=valores.length-1;x++){
			         	var hs=$(valores[x]).attr("acctray").replace(/\./g, '');
			         	var mjs= $(valores[x]).attr("monto_tray").replace(/\./g, '');
		                 datacategorie.push({label:$(valores[x]).attr("fecha"),stepSkipped: false}); 
		                 datavaluesnro.push({value:hs});
		                 datavaluemontos.push({value:mjs});

		             }

     		var chatnrosubsidostrayecto = new FusionCharts({
					        type: 'mscombi2d',
					        renderAt: 'subsidiostrayecto',
					        width: '640',
					        height: '300',
					        dataFormat: 'json',
					        dataSource: {
					            "chart": {
					                "showvalues": "1",
									        "plotgradientcolor": "",
									        "formatnumberscale": "0",
									        "showplotborder": "0",
									        "palettecolors": "#268040,#7ebc41,#009688,#B0D67A,#2C560A,#DD9D82",
									        "canvaspadding": "0",
									        "bgcolor": "FFFFFF",
									        "showalternatehgridcolor": "0",
									        "divlinecolor": "CCCCCC",
									        "showcanvasborder": "0",
									        "interactivelegend": "0",
									        "showpercentvalues": "0",
									        "canvasborderalpha": "0",
									        "showborder": "0",
											"baseFontColor": "#fff",
											"outCnvBaseFontColor": "#666",
											"toolTipBgColor":"666",
											"rotateValues":"0",
											"legendBgAlpha": "0",
									        "legendBorderAlpha": "0",
									        "legendShadow": "0",
									        "legendItemFontSize": "10",
									        "legendItemFontColor": "#666666",
											"labelDisplay":"rotate",
											"thousandSeparator":".",
											"showYAxisValue":"0",
											"placevaluesInside":"1" 
					            },
					            "categories": [{
					                "category": datacategorie
					            }],
					            "dataset": [{
					                "seriesName": "N\u00FAmero",


					                "data":  datavaluesnro
					            }, {
					                "seriesName": "Monto($MM)",					                "renderAs": "line",
					                "showValues": "1",
					                "data": datavaluemontos		
					            }]
					        }
					    }).render();
			 },
			 error: function (result) {

			 }

		 });


     },getnumerosubsidiosenfermedades:function(){
		 $.ajax({
			 url: '/json-estadistica/getListadoSubsidios',
			 method: "post",
			 dataType: "json",
			 contentType: "application/json; charset=utf-8",
			 success: function (response) {
				 var valores = response;
			         var datacategorie=[];
			         var datavaluesnro =[];
			         var datavaluemontos=[];

			         for(var x=0;x<=valores.length-1;x++){
			         	var hs=$(valores[x]).attr("ep").replace(/\./g, '');
			         	var mjs=$(valores[x]).attr("monto_ep").replace(/\./g, '');
		                 datacategorie.push({label:$(valores[x]).attr("fecha"),stepSkipped: false}); 
		                 datavaluesnro.push({value:hs});
		                 datavaluemontos.push({value:mjs});

		             }

     		var chatnrosubsidosenfermedades = new FusionCharts({
					        type: 'mscombi2d',
					        renderAt: 'subsidiosenfermedades',
					        width: '640',
					        height: '300',
					        dataFormat: 'json',
					        dataSource: {
					            "chart": {
					                "showvalues": "1",
									        "plotgradientcolor": "",
									        "formatnumberscale": "0",
									        "showplotborder": "0",
									        "palettecolors": "#268040,#7ebc41,#009688,#B0D67A,#2C560A,#DD9D82",
									        "canvaspadding": "0",
									        "bgcolor": "FFFFFF",
									        "showalternatehgridcolor": "0",
									        "divlinecolor": "CCCCCC",
									        "showcanvasborder": "0",
									        "interactivelegend": "0",
									        "showpercentvalues": "0",
									        "canvasborderalpha": "0",
									        "showborder": "0",
											"baseFontColor": "#fff",
											"outCnvBaseFontColor": "#666",
											"toolTipBgColor":"666",
											"rotateValues":"0",
											"legendBgAlpha": "0",
									        "legendBorderAlpha": "0",
									        "legendShadow": "0",
									        "legendItemFontSize": "10",
									        "legendItemFontColor": "#666666",
											"labelDisplay":"rotate",
											"thousandSeparator":".",
											"showYAxisValue":"0",
											"placevaluesInside":"1" 
					            },
					            "categories": [{
					                "category": datacategorie
					            }],
					            "dataset": [{
					                "seriesName": "N\u00FAmero",


					                "data":  datavaluesnro
					            }, {
					                "seriesName": "Monto($MM)",					                "renderAs": "line",
					                "showValues": "1",
					                "data": datavaluemontos		
					            }]
					        }
					    }).render();
			 },
			 error: function (result) {

			 }

		 });
     
     },getsubsidioaltrabajo:function(){
		 $.ajax({
			 url: '/json-estadistica/getListadoSubsidios',
			 method: "post",
			 dataType: "json",
			 contentType: "application/json; charset=utf-8",
			 success: function (response) {
				 var valores = response;

			         var datacategorie=[];
			         var datavaluesnro =[];
			         var datavaluemontos=[];

			         for(var x=0;x<=valores.length-1;x++){
			         	var hs=$(valores[x]).attr("acctrabajo").replace(/\./g, '');
			         	var mjs=$(valores[x]).attr("monto_acc").replace(/\./g, '');
		                 datacategorie.push({label:$(valores[x]).attr("fecha"),stepSkipped: false}); 
		                 datavaluesnro.push({value:hs});
		                 datavaluemontos.push({value:mjs});

		             }

     		var chatnrosubsidostrabajo = new FusionCharts({
					        type: 'mscombi2d',
					        renderAt: 'subsidiostrabajo',
					        width: '640',
					        height: '300',
					        dataFormat: 'json',
					        dataSource: {
					            "chart": {
					                		"showvalues": "1",
									        "plotgradientcolor": "",
									        "formatnumberscale": "0",
									        "showplotborder": "0",
									        "palettecolors": "#268040,#7ebc41,#009688,#B0D67A,#2C560A,#DD9D82",
									        "canvaspadding": "0",
									        "bgcolor": "FFFFFF",
									        "showalternatehgridcolor": "0",
									        "divlinecolor": "CCCCCC",
									        "showcanvasborder": "0",
									        "interactivelegend": "0",
									        "showpercentvalues": "0",
									        "canvasborderalpha": "0",
									        "showborder": "0",
											"baseFontColor": "#fff",
											"outCnvBaseFontColor": "#666",
											"toolTipBgColor":"666",
											"rotateValues":"0",
											"legendBgAlpha": "0",
									        "legendBorderAlpha": "0",
									        "legendShadow": "0",
									        "legendItemFontSize": "10",
									        "legendItemFontColor": "#666666",
											"labelDisplay":"rotate",
											"thousandSeparator":".",
											"showYAxisValue":"0",
											"placevaluesInside":"1",
											"valueBgColor": "#268040",
        									"valueBgAlpha": "100",
        									"valueBgHoverAlpha": "5"
											
					            },
					            "categories": [{
					                "category": datacategorie
					            }],
					            "dataset": [{
					                "seriesName": "N\u00FAmero",


					                "data":  datavaluesnro
					            }, {
					                "seriesName": "Monto($MM)",					                "renderAs": "line",
					                "showValues": "1",
					                "data": datavaluemontos		
					            }]
					        }
					    }).render();
				  },
			 error: function (result) {

			 }

		 });
     
     
     },getpensionesgenerales:function(){
     
		 $.ajax({
			 url: '/json-estadistica/getListadoPensiones',
			 method: "post",
			 dataType: "json",
			 contentType: "application/json; charset=utf-8",
			 success: function (response) {
				 var valores = response;

			         var datacategorie=[];
			         var datavaluesnro =[];
			         var datavaluemontos=[];

			         for(var x=0;x<=valores.length-1;x++){
			         	var hs=$(valores[x]).attr("subsidios").replace(/\./g, '');
			         	var mjs=$(valores[x]).attr("monto_sub").replace(/\./g, '');
		                 datacategorie.push({label:$(valores[x]).attr("fecha"),stepSkipped: false}); 
		                 datavaluesnro.push({value:hs});
		                 datavaluemontos.push({value:mjs});

		             }

     		var chatnropensiones = new FusionCharts({
					        type: 'mscombi2d',
					        renderAt: 'pensiongeneral',
					        width: '640',
					        height: '300',
					        dataFormat: 'json',
					        dataSource: {
					            "chart": {
					                "showvalues": "1",
									        "plotgradientcolor": "",
									        "formatnumberscale": "0",
									        "showplotborder": "0",
									        "palettecolors": "#268040,#7ebc41,#009688,#B0D67A,#2C560A,#DD9D82",
									        "canvaspadding": "0",
									        "bgcolor": "FFFFFF",
									        "showalternatehgridcolor": "0",
									        "divlinecolor": "CCCCCC",
									        "showcanvasborder": "0",
									        "interactivelegend": "0",
									        "showpercentvalues": "0",
									        "canvasborderalpha": "0",
									        "showborder": "0",
											"baseFontColor": "#fff",
											"outCnvBaseFontColor": "#666",
											"toolTipBgColor":"666",
											"rotateValues":"0",
											"legendBgAlpha": "0",
									        "legendBorderAlpha": "0",
									        "legendShadow": "0",
									        "legendItemFontSize": "10",
									        "legendItemFontColor": "#666666",
											"labelDisplay":"rotate",
											"thousandSeparator":".",
											"showYAxisValue":"0",
											"placevaluesInside":"1" 
					            },
					            "categories": [{
					                "category": datacategorie
					            }],
					            "dataset": [{
					                "seriesName": "N\u00FAmero",


					                "data":  datavaluesnro
					            }, {
					                "seriesName": "Monto($MM)",					                "renderAs": "line",
					                "showValues": "1",
					                "data": datavaluemontos		
					            }]
					        }
					    }).render();

			 },
			 error: function (result) {

			 }

		 });
     
     
     
     },getpensionadosstrayecto:function(){
     

		 $.ajax({
			 url: '/json-estadistica/getListadoPensiones',
			 method: "post",
			 dataType: "json",
			 contentType: "application/json; charset=utf-8",
			 success: function (response) {
				 var valores = response;

			         var datacategorie=[];
			         var datavaluesnro =[];
			         var datavaluemontos=[];

			         for(var x=0;x<=valores.length-1;x++){
			         	var hs=$(valores[x]).attr("acctray").replace(/\./g, '');
			         	var mjs= $(valores[x]).attr("monto_tray").replace(/\./g, '');
		                 datacategorie.push({label:$(valores[x]).attr("fecha"),stepSkipped: false}); 
		                 datavaluesnro.push({value:hs});
		                 datavaluemontos.push({value:mjs});

		             }

     		var chatnrosubsidostrayecto = new FusionCharts({
					        type: 'mscombi2d',
					        renderAt: 'pensiontrayecto',
					        width: '640',
					        height: '300',
					        dataFormat: 'json',
					        dataSource: {
					            "chart": {
					                "showvalues": "1",
									        "plotgradientcolor": "",
									        "formatnumberscale": "0",
									        "showplotborder": "0",
									        "palettecolors": "#268040,#7ebc41,#009688,#B0D67A,#2C560A,#DD9D82",
									        "canvaspadding": "0",
									        "bgcolor": "FFFFFF",
									        "showalternatehgridcolor": "0",
									        "divlinecolor": "CCCCCC",
									        "showcanvasborder": "0",
									        "interactivelegend": "0",
									        "showpercentvalues": "0",
									        "canvasborderalpha": "0",
									        "showborder": "0",
											"baseFontColor": "#fff",
											"outCnvBaseFontColor": "#666",
											"toolTipBgColor":"666",
											"rotateValues":"0",
											"legendBgAlpha": "0",
									        "legendBorderAlpha": "0",
									        "legendShadow": "0",
									        "legendItemFontSize": "10",
									        "legendItemFontColor": "#666666",
											"labelDisplay":"rotate",
											"thousandSeparator":".",
											"showYAxisValue":"0",
											"placevaluesInside":"1" 
					            },
					            "categories": [{
					                "category": datacategorie
					            }],
					            "dataset": [{
					                "seriesName": "N\u00FAmero",


					                "data":  datavaluesnro
					            }, {
					                "seriesName": "Monto($MM)",					                "renderAs": "line",
					                "showValues": "1",
					                "data": datavaluemontos		
					            }]
					        }
					    }).render();
			 },
			 error: function (result) {

			 }

		 });
     },getnumeropensionadosenfermedades:function(){
     
     
		 $.ajax({
			 url: '/json-estadistica/getListadoPensiones',
			 method: "post",
			 dataType: "json",
			 contentType: "application/json; charset=utf-8",
			 success: function (response) {
				 var valores = response;
			         var datacategorie=[];
			         var datavaluesnro =[];
			         var datavaluemontos=[];

			         for(var x=0;x<=valores.length-1;x++){
			         	var hs=$(valores[x]).attr("ep").replace(/\./g, '');
			         	var mjs=$(valores[x]).attr("monto_ep").replace(/\./g, '');
		                 datacategorie.push({label:$(valores[x]).attr("fecha"),stepSkipped: false}); 
		                 datavaluesnro.push({value:hs});
		                 datavaluemontos.push({value:mjs});

		             }

     		var chatnrosubsidosenfermedades = new FusionCharts({
					        type: 'mscombi2d',
					        renderAt: 'pensionenfermedades',
					        width: '640',
					        height: '300',
					        dataFormat: 'json',
					        dataSource: {
					            "chart": {
					                "showvalues": "1",
									        "plotgradientcolor": "",
									        "formatnumberscale": "0",
									        "showplotborder": "0",
									        "palettecolors": "#268040,#7ebc41,#009688,#B0D67A,#2C560A,#DD9D82",
									        "canvaspadding": "0",
									        "bgcolor": "FFFFFF",
									        "showalternatehgridcolor": "0",
									        "divlinecolor": "CCCCCC",
									        "showcanvasborder": "0",
									        "interactivelegend": "0",
									        "showpercentvalues": "0",
									        "canvasborderalpha": "0",
									        "showborder": "0",
											"baseFontColor": "#fff",
											"outCnvBaseFontColor": "#666",
											"toolTipBgColor":"666",
											"rotateValues":"0",
											"legendBgAlpha": "0",
									        "legendBorderAlpha": "0",
									        "legendShadow": "0",
									        "legendItemFontSize": "10",
									        "legendItemFontColor": "#666666",
											"labelDisplay":"rotate",
											"thousandSeparator":".",
											"showYAxisValue":"0",
											"placevaluesInside":"1" 
					            },
					            "categories": [{
					                "category": datacategorie
					            }],
					            "dataset": [{
					                "seriesName": "N\u00FAmero",


					                "data":  datavaluesnro
					            }, {
					                "seriesName": "Monto($MM)",					                "renderAs": "line",
					                "showValues": "1",
					                "data": datavaluemontos		
					            }]
					        }
					    }).render();
			 },
			 error: function (result) {

			 }

		 });
     
     },getpensionadotrabajo:function(){
     

		 $.ajax({
			 url: '/json-estadistica/getListadoPensiones',
			 method: "post",
			 dataType: "json",
			 contentType: "application/json; charset=utf-8",
			 success: function (response) {
				 var valores = response;

			         var datacategorie=[];
			         var datavaluesnro =[];
			         var datavaluemontos=[];

			         for(var x=0;x<=valores.length-1;x++){
			         	var hs=$(valores[x]).attr("acctrabajo").replace(/\./g, '');
			         	var mjs=$(valores[x]).attr("monto_acc").replace(/\./g, '');
		                 datacategorie.push({label:$(valores[x]).attr("fecha"),stepSkipped: false}); 
		                 datavaluesnro.push({value:hs});
		                 datavaluemontos.push({value:mjs});

		             }

     		var chatnrosubsidostrabajo = new FusionCharts({
					        type: 'mscombi2d',
					        renderAt: 'pensiontrabajo',
					        width: '640',
					        height: '300',
					        dataFormat: 'json',
					        dataSource: {
					            "chart": {
					                "showvalues": "1",
									        "plotgradientcolor": "",
									        "formatnumberscale": "0",
									        "showplotborder": "0",
									        "palettecolors": "#268040,#7ebc41,#009688,#B0D67A,#2C560A,#DD9D82",
									        "canvaspadding": "0",
									        "bgcolor": "FFFFFF",
									        "showalternatehgridcolor": "0",
									        "divlinecolor": "CCCCCC",
									        "showcanvasborder": "0",
									        "interactivelegend": "0",
									        "showpercentvalues": "0",
									        "canvasborderalpha": "0",
									        "showborder": "0",
											"baseFontColor": "#fff",
											"outCnvBaseFontColor": "#666",
											"toolTipBgColor":"666",
											"rotateValues":"0",
											"legendBgAlpha": "0",
									        "legendBorderAlpha": "0",
									        "legendShadow": "0",
									        "legendItemFontSize": "10",
									        "legendItemFontColor": "#666666",
											"labelDisplay":"rotate",
											"thousandSeparator":".",
											"showYAxisValue":"0",
											"placevaluesInside":"1" 
					            },
					            "categories": [{
					                "category": datacategorie
					            }],
					            "dataset": [{
					                "seriesName": "N\u00FAmero",


					                "data":  datavaluesnro
					            }, {
					                "seriesName": "Monto($MM)",					                "renderAs": "line",
					                "showValues": "1",
					                "data": datavaluemontos		
					            }]
					        }
					    }).render();
			 },
			 error: function (result) {

			 }

		 });
     
     
     },getindemnizaciongenerales:function(){
     
		 $.ajax({
			 url: '/json-estadistica/getListadoIndemnizaciones',
			 method: "post",
			 dataType: "json",
			 contentType: "application/json; charset=utf-8",
			 success: function (response) {
				 var valores = response;

			         var datacategorie=[];
			         var datavaluesnro =[];
			         var datavaluemontos=[];

			         for(var x=0;x<=valores.length-1;x++){
			         	var hs=$(valores[x]).attr("indem");
			         	var mjs=$(valores[x]).attr("monto_indem");
		                 datacategorie.push({label:$(valores[x]).attr("fecha"),stepSkipped: false}); 
		                 datavaluesnro.push({value:hs});
		                 datavaluemontos.push({value:mjs});

		             }

     		var chatnrosubsidos = new FusionCharts({
					        type: 'mscombi2d',
					        renderAt: 'indemnizaciongeneral',
					        width: '640',
					        height: '300',
					        dataFormat: 'json',
					        dataSource: {
					            "chart": {
					                "showvalues": "1",
									        "plotgradientcolor": "",
									        "formatnumberscale": "0",
									        "showplotborder": "0",
									        "palettecolors": "#268040,#7ebc41,#009688,#B0D67A,#2C560A,#DD9D82",
									        "canvaspadding": "0",
									        "bgcolor": "FFFFFF",
									        "showalternatehgridcolor": "0",
									        "divlinecolor": "CCCCCC",
									        "showcanvasborder": "0",
									        "interactivelegend": "0",
									        "showpercentvalues": "0",
									        "canvasborderalpha": "0",
									        "showborder": "0",
											"baseFontColor": "#fff",
											"outCnvBaseFontColor": "#666",
											"toolTipBgColor":"666",
											"rotateValues":"0",
											"legendBgAlpha": "0",
									        "legendBorderAlpha": "0",
									        "legendShadow": "0",
									        "legendItemFontSize": "10",
									        "legendItemFontColor": "#666666",
											"labelDisplay":"rotate",
											"thousandSeparator":".",
											"showYAxisValue":"0",
											"placevaluesInside":"1",
											"valueBgColor": "#268040",
        									"valueBgAlpha": "100",
        									"valueBgHoverAlpha": "5"

																            },
					            "categories": [{
					                "category": datacategorie
					            }],
					            "dataset": [{
					                "seriesName": "N\u00FAmero",


					                "data":  datavaluesnro
					            }, {
					                "seriesName": "Monto($MM)",					                "renderAs": "line",
					                "showValues": "1",
					                "data": datavaluemontos		
					            }]
					        }
					    }).render();
			 },
			 error: function (result) {

			 }

		 });


     
     
     
     },getindemizaciontrayectos:function(){
     
		 $.ajax({
			 url: '/json-estadistica/getListadoIndemnizaciones',
			 method: "post",
			 dataType: "json",
			 contentType: "application/json; charset=utf-8",
			 success: function (response) {
				 var valores = response;


			         var datacategorie=[];
			         var datavaluesnro =[];
			         var datavaluemontos=[];

			         for(var x=0;x<=valores.length-1;x++){
			         	var hs=$(valores[x]).attr("acctray");
			         	var mjs= $(valores[x]).attr("monto_tray");
		                 datacategorie.push({label:$(valores[x]).attr("fecha"),stepSkipped: false}); 
		                 datavaluesnro.push({value:hs});
		                 datavaluemontos.push({value:mjs});

		             }

     		var chatnrosubsidostrayecto = new FusionCharts({
					        type: 'mscombi2d',
					        renderAt: 'indemnizaciontrayecto',
					        width: '640',
					        height: '300',
					        dataFormat: 'json',
					        dataSource: {
					            "chart": {
					                "showvalues": "1",
									        "plotgradientcolor": "",
									        "formatnumberscale": "0",
									        "showplotborder": "0",
									        "palettecolors": "#268040,#7ebc41,#009688,#B0D67A,#2C560A,#DD9D82",
									        "canvaspadding": "0",
									        "bgcolor": "FFFFFF",
									        "showalternatehgridcolor": "0",
									        "divlinecolor": "CCCCCC",
									        "showcanvasborder": "0",
									        "interactivelegend": "0",
									        "showpercentvalues": "0",
									        "canvasborderalpha": "0",
									        "showborder": "0",
											"baseFontColor": "#fff",
											"outCnvBaseFontColor": "#666",
											"toolTipBgColor":"666",
											"rotateValues":"0",
											"legendBgAlpha": "0",
									        "legendBorderAlpha": "0",
									        "legendShadow": "0",
									        "legendItemFontSize": "10",
									        "legendItemFontColor": "#666666",
											"labelDisplay":"rotate",
											"thousandSeparator":".",
											"showYAxisValue":"0",
											"placevaluesInside":"1",
											"valueBgColor": "#268040",
        									"valueBgAlpha": "100",
        									"valueBgHoverAlpha": "5"

					            },
					            "categories": [{
					                "category": datacategorie
					            }],
					            "dataset": [{
					                "seriesName": "N\u00FAmero",


					                "data":  datavaluesnro
					            }, {
					                "seriesName": "Monto($MM)",					                "renderAs": "line",
					                "showValues": "1",
					                "data": datavaluemontos		
					            }]
					        }
				}).render();
			 },
			 error: function (result) {

			 }

		 });

     },getnumeroindemnizacionenfermedades:function(){
     

		 $.ajax({
			 url: '/json-estadistica/getListadoIndemnizaciones',
			 method: "post",
			 dataType: "json",
			 contentType: "application/json; charset=utf-8",
			 success: function (response) {
				 var valores = response;


			         var datacategorie=[];
			         var datavaluesnro =[];
			         var datavaluemontos=[];

			         for(var x=0;x<=valores.length-1;x++){
			         	var hs=$(valores[x]).attr("ep");
			         	var mjs=$(valores[x]).attr("monto_ep");
		                 datacategorie.push({label:$(valores[x]).attr("fecha"),stepSkipped: false}); 
		                 datavaluesnro.push({value:hs});
		                 datavaluemontos.push({value:mjs});

		             }

     		var chatnrosubsidosenfermedades = new FusionCharts({
					        type: 'mscombi2d',
					        renderAt: 'indemnizacionenfermedades',
					        width: '640',
					        height: '300',
					        dataFormat: 'json',
					        dataSource: {
					            "chart": {
					                "showvalues": "1",
									        "plotgradientcolor": "",
									        "formatnumberscale": "0",
									        "showplotborder": "0",
									        "palettecolors": "#268040,#7ebc41,#009688,#B0D67A,#2C560A,#DD9D82",
									        "canvaspadding": "0",
									        "bgcolor": "FFFFFF",
									        "showalternatehgridcolor": "0",
									        "divlinecolor": "CCCCCC",
									        "showcanvasborder": "0",
									        "interactivelegend": "0",
									        "showpercentvalues": "0",
									        "canvasborderalpha": "0",
									        "showborder": "0",
											"baseFontColor": "#fff",
											"outCnvBaseFontColor": "#666",
											"toolTipBgColor":"666",
											"rotateValues":"0",
											"legendBgAlpha": "0",
									        "legendBorderAlpha": "0",
									        "legendShadow": "0",
									        "legendItemFontSize": "10",
									        "legendItemFontColor": "#666666",
											"labelDisplay":"rotate",
											"thousandSeparator":".",
											"showYAxisValue":"0",
											"placevaluesInside":"1",
											"valueBgColor": "#268040",
        									"valueBgAlpha": "100",
        									"valueBgHoverAlpha": "5"

					            },
					            "categories": [{
					                "category": datacategorie
					            }],
					            "dataset": [{
					                "seriesName": "N\u00FAmero",


					                "data":  datavaluesnro
					            }, {
					                "seriesName": "Monto($MM)",					                "renderAs": "line",
					                "showValues": "1",
					                "data": datavaluemontos		
					            }]
					        }
					    }).render();
			 },
			 error: function (result) {

			 }

		 });
     
     },getindemnizaciontrabajo:function(){
     
		 $.ajax({
			 url: '/json-estadistica/getListadoIndemnizaciones',
			 method: "post",
			 dataType: "json",
			 contentType: "application/json; charset=utf-8",
			 success: function (response) {
				 var valores = response;
    
			         var datacategorie=[];
			         var datavaluesnro =[];
			         var datavaluemontos=[];

			         for(var x=0;x<=valores.length-1;x++){
			         	var hs=$(valores[x]).attr("acctrabajo");
			         	var mjs=$(valores[x]).attr("monto_acc");
		                 datacategorie.push({label:$(valores[x]).attr("fecha"),stepSkipped: false}); 
		                 datavaluesnro.push({value:hs});
		                 datavaluemontos.push({value:mjs});

		             }

     		var chatnrosubsidostrabajo = new FusionCharts({
					        type: 'mscombi2d',
					        renderAt: 'indemnizaciontrabajo',
					        width: '640',
					        height: '500',
					        dataFormat: 'json',
					        dataSource: {
					            "chart": {
					                        "showValues": "1",
									        "plotgradientcolor": "",
									        "formatnumberscale": "0",
									        "showplotborder": "0",
									        "palettecolors": "#268040,#7ebc41,#009688,#B0D67A,#2C560A,#DD9D82",
									        "canvaspadding": "0",
									        "bgcolor": "FFFFFF",
									        "showalternatehgridcolor": "0",
									        "divlinecolor": "CCCCCC",
									        "showcanvasborder": "0",
									        "interactivelegend": "0",
									        "showpercentvalues": "0",
									        "canvasborderalpha": "0",
									        "showborder": "0",
											"baseFontColor": "#fff",
											"outCnvBaseFontColor": "#666",
											"toolTipBgColor":"666",
											"rotateValues":"0",
											"legendBgAlpha": "0",
									        "legendBorderAlpha": "0",
									        "legendShadow": "0",
									        "legendItemFontSize": "10",
									        "legendItemFontColor": "#666666",
											"labelDisplay":"rotate",
											"thousandSeparator":".",
											"decimalSeparator": ",",
											"showYAxisValue":"0",											
											"placevaluesInside":"1",
											"valueBgColor": "#268040",
        									"valueBgAlpha": "100",
        									"valueBgHoverAlpha": "5"

					            },
					            "categories": [{
					                "category": datacategorie
					            }],
					            "dataset": [{
					                "seriesName": "N\u00FAmero",


					                "data":  datavaluesnro
					            }, {
					                "seriesName": "Monto($MM)",	
					                "renderAs": "line",
					                "showValues": "1",
					                "strokeColor" : "#000",

					                "data": datavaluemontos		
					            }]
					        }
					    }).render();
			 },
			 error: function (result) {

			 }

		 });
     
     
     }


						 

}

function getDatosGraficosTipos(listanameval){
	var returndatos;
	var url = window.location.href.toString().toLowerCase().split('/paginas')[0] + '/';
	if(listanameval==="estadisticas_grafico_05"){
		var query="";

	       query = "<Query><OrderBy>" +
                    "<FieldRef Name='Orden' />" +
                "</OrderBy>" +
            "</Query>";
       }

	     $().SPServices({
		      webURL: url,
		      operation: "GetListItems",
		      async: false,
		      CAMLQuery: query,
		      listName:listanameval,
		      completefunc: function (xData, Status) {
		       		returndatos = $(xData.responseXML).SPFilterNode("z:row")
		       }
		    });    
    return returndatos;
}
function getDatosGraficosNew(listnameval,stringcolumnfield){
    var returndatos;
    var url = window.location.href.toString().toLowerCase().split('/paginas')[0] + '/';
    var arraycolumns = stringcolumnfield.split(";");
    var strQueryfield ="";
    for(var x=0;x<=arraycolumns.length-1;x++){
    
        strQueryfield +="<FieldRef name='"+arraycolumns[x]+"'/>";
    }
    //console.log(strQueryfield);
   
   
     //CAMLViewFields
    $().SPServices({
		      webURL: url,
		      operation: "GetListItems",
		      async: false,
		     // CAMLQuery: query,
		      listName:listnameval,
		    //  CAMLViewFields:"<ViewFields>"+strQueryfield+"</ViewFields>",
		      completefunc: function (xData, Status) {
		       		returndatos = $(xData.responseXML).SPFilterNode("z:row")
		       }
    });    
    return returndatos;


}
$(window).load(function() {

     //spbx_graficos.getTitulosNotas();
     spbx_graficos.getParticipacion();
     spbx_graficos.getNumeroEmpresas();  /////////OK
     spbx_graficos.getNumeroTrabajadoresAfiliados();
     spbx_graficos.getEvoluacionTasaReposo(); /////////OK
     spbx_graficos.getnumeroAccidenteLaborales();
     spbx_graficos.getValoresxIndustria();
     spbx_graficos.getnumeroAccidenteTrayecto();
     spbx_graficos.getnumeroEnfermedades();     
     
     spbx_graficos.getnumeroreposolaboral();
     spbx_graficos.getnumeroReposoTrayecto();
     spbx_graficos.getnumeroReposotEP(); 
     
     spbx_graficos.getsubsidiosgenerales();
     spbx_graficos.getsubsidostrayectos();
     spbx_graficos.getnumerosubsidiosenfermedades();
     spbx_graficos.getsubsidioaltrabajo();
     
     
      spbx_graficos.getpensionesgenerales();
      spbx_graficos.getpensionadosstrayecto();
      spbx_graficos.getnumeropensionadosenfermedades();
      spbx_graficos.getpensionadotrabajo();
      
      
      spbx_graficos.getindemnizaciongenerales();
      spbx_graficos.getindemizaciontrayectos();
      spbx_graficos.getnumeroindemnizacionenfermedades();
      spbx_graficos.getindemnizaciontrabajo();

    //$('.grafico').append('<div class="tapa"></div>');
    
   
 
 
});




FusionCharts.addEventListener('loaded', function (opts) {
   //console.log("grafico con ID " + opts.sender.id + " se ha cargado");
   $('.grafico').append('<div class="tapa"></div>');
});
