﻿$('#rut_empresa').on('change', function () {
    var rut = $('#rut_empresa').val();
    var error = 0;
    var valor = rut.split('.').join('');
    valor = valor.replace('-', '');
    cuerpo = valor.slice(0, -1);
    dv = valor.slice(-1).toUpperCase();
    rut.value = cuerpo + '-' + dv;
    if (cuerpo.length < 7) { error = 1; }
    suma = 0;
    multiplo = 2;
    for (i = 1; i <= cuerpo.length; i++) {
        index = multiplo * valor.charAt(cuerpo.length - i);
        suma = suma + index;
        if (multiplo < 7) { multiplo = multiplo + 1; } else { multiplo = 2; }
    }
    dvEsperado = 11 - (suma % 11);
    dv = (dv == 'K') ? 10 : dv;
    dv = (dv == 0) ? 11 : dv;
    if (dvEsperado != dv) { error = 1; }
    if (error == 1) {
        $('#rut_empresa').val('');
        $('#rut_empresa').focus();

        return false;
    } else {
        if (dv == 10) {
            $('#rut_empresa').val((cuerpo + '-' + 'K').trim())


        } else if (dv == 11) {
            $('#rut_empresa').val((cuerpo + '-' + '0').trim())

        } else {
            $('#rut_empresa').val((cuerpo + '-' + dv).trim())

        }

      

        return true;
    }
});
$('#TxtRutRepresentante').on('change', function () {
    var rut = $('#TxtRutRepresentante').val();
    var error = 0;
    var valor = rut.split('.').join('');
    valor = valor.replace('-', '');
    cuerpo = valor.slice(0, -1);
    dv = valor.slice(-1).toUpperCase();
    rut.value = cuerpo + '-' + dv;
    if (cuerpo.length < 7) { error = 1; }
    suma = 0;
    multiplo = 2;
    for (i = 1; i <= cuerpo.length; i++) {
        index = multiplo * valor.charAt(cuerpo.length - i);
        suma = suma + index;
        if (multiplo < 7) { multiplo = multiplo + 1; } else { multiplo = 2; }
    }
    dvEsperado = 11 - (suma % 11);
    dv = (dv == 'K') ? 10 : dv;
    dv = (dv == 0) ? 11 : dv;
    if (dvEsperado != dv) { error = 1; }
    if (error == 1) {
        $('#TxtRutRepresentante').val('');
        $('#TxtRutRepresentante').focus();

        return false;
    } else {
        if (dv == 10) {
            $('#TxtRutRepresentante').val((cuerpo + '-' + 'K').trim())


        } else if (dv == 11) {
            $('#TxtRutRepresentante').val((cuerpo + '-' + '0').trim())

        } else {
            $('#TxtRutRepresentante').val((cuerpo + '-' + dv).trim())

        }

        return true;
    }
});
$('#TxtRut_Contacto').on('change', function () {
    var rut = $('#TxtRut_Contacto').val();
    var error = 0;
    var valor = rut.split('.').join('');
    valor = valor.replace('-', '');
    cuerpo = valor.slice(0, -1);
    dv = valor.slice(-1).toUpperCase();
    rut.value = cuerpo + '-' + dv;
    if (cuerpo.length < 7) { error = 1; }
    suma = 0;
    multiplo = 2;
    for (i = 1; i <= cuerpo.length; i++) {
        index = multiplo * valor.charAt(cuerpo.length - i);
        suma = suma + index;
        if (multiplo < 7) { multiplo = multiplo + 1; } else { multiplo = 2; }
    }
    dvEsperado = 11 - (suma % 11);
    dv = (dv == 'K') ? 10 : dv;
    dv = (dv == 0) ? 11 : dv;
    if (dvEsperado != dv) { error = 1; }
    if (error == 1) {
        $('#TxtRut_Contacto').val('');
        $('#TxtRut_Contacto').focus();

        return false;
    } else {
        if (dv == 10) {
            $('#TxtRut_Contacto').val((cuerpo + '-' + 'K').trim())


        } else if (dv == 11) {
            $('#TxtRut_Contacto').val((cuerpo + '-' + '0').trim())

        } else {
            $('#TxtRut_Contacto').val((cuerpo + '-' + dv).trim())

        }

        return true;
    }
});
$(document).ready(function () {
    $("#cerrar_alerta").click(function () {
        $(".barra").hide();
    });


});
$('#correo_contacto').on('change', function () {
    campo = event.target;

    emailRegex = /^[-\w.]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
    //Se muestra un texto a modo de ejemplo, luego va a ser un icono
    if (emailRegex.test(campo.value)) {
     
    } else {
        bootbox.alert('El formato del correo es incorrecto')
        $('#correo_contacto').focus();
        $('#correo_contacto').val("");
    }
});
getRegion();
$("#region_casa").change(function () {
    GetComunaFilter("comuna_casa", $(this).val());
});
$("#region_suc").change(function () {
    GetComunaFilter("comuna_suc", $(this).val());
});
function getRegion() {
    $.ajax({
        type: "GET",
        url: "https://api.achs.lfi.cl/api/mandatodigital/GetRegiones",
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        success: function (response) {
            var arr_from_json = JSON.parse(response.regionlist);

            $("#region_casa").append($('<option>', {
                value: "",
                text: "Seleccione región"
            }));
            $("#comuna_casa").append($('<option>', {
                value: "",
                text: "Seleccione comuna"
            }));
            $("#region_suc").append($('<option>', {
                value: "",
                text: "Seleccione región"
            }));
            $("#comuna_suc").append($('<option>', {
                value: "",
                text: "Seleccione comuna"
            }));

            for (var x = 0; x < arr_from_json.length; x++) {

                $("#region_casa").append($('<option>', {
                    value: arr_from_json[x].cod_region,
                    text: arr_from_json[x].nom_region
                }));
                $("#region_suc").append($('<option>', {
                    value: arr_from_json[x].cod_region,
                    text: arr_from_json[x].nom_region
                }));
            }



        },
        failure: function (response) {
            console.log(response);
        },
        error: function (response) {
            console.log(response);
        }
    });


}
function soloNumeros(e) {
    var key = window.Event ? e.which : e.keyCode
    return (key >= 48 && key <= 57)
}

function GetComunaFilter(id, value) {

    $("#" + id).children('option:not(:first)').remove();
    $.ajax({
        type: "GET",
        url: "https://api.achs.lfi.cl/api/mandatodigital/GetComunaFilter",
        data: { codcomuna: value },
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        async: true,
        success: function (response) {
            var arr_from_json = JSON.parse(response.comunalist);
            for (var x = 0; x < arr_from_json.length; x++) {

                $("#" + id).append($('<option>', {
                    value: arr_from_json[x].cod_comuna,
                    text: arr_from_json[x].nom_comuna
                }));

            }
        },
        failure: function (response) {
            console.log(response);
        },
        error: function (response) {
            console.log(response);
        }
    });
}
$('#btnEnviar').on('click', function () {
    guardar();
});

$('#file1').on('change', function () {
    var file = $('#file1').val()
    var filename = $('#file1').val().replace(/C:\\fakepath\\/i, '')
    var extension = file.substr((file.lastIndexOf('.') + 1));
    archivo = document.getElementById('nombreArchivo');
   archivo.innerText = filename;
        $('#nombrefile').val(filename)

});












function guardar() {
    var obj = {
        TxtRut_Empresa: $("#rut_empresa").val(),
        TxtRazonsocialEmpresa: $('#razon_social').val(),
        TxtDireccionEmpresa: $('#direccion_casa').val(),
        TxtNumeroEmpresa: $('#num_casa').val(),
        DrpregionesEmpresa: $('#region_casa').val(),
        ComunasEmpresas: $('#comuna_casa').val(),
        FechaRecibo: $('#TxtFecharecibo').val(),
        NumeroResolucion: $('#num_resolucion').val(),
        TxtRutrepresentante: $('#TxtRutRepresentante').val(),
        TxtNombresrepresentante: $('#nombre_representante').val(),
        TxtRut_ContactoEmpresa: $('#TxtRut_Contacto').val(),
        TxtTelefonoContactoEmpresa: $('#fono_contacto').val(),
        TxtNombresContactoEmpresa: $('#nombre_contacto').val(),
        TxtEmailContantoEmpresa: $('#correo_contacto').val(),
        TxtApellidosContactoEmpresa: $('#apellido_contacto').val(),
        TxtCargoContactoEmpresa: $('#cargo_contacto').val(),
        TxtDireccionContactoEmpresa: $('#direccion_suc').val(),
        TxtNumeroContactoEmpesa: $('#num_suc').val(),
        DrpregionesContactoEmpresa: $('#region_suc').val(),
        ComunasContacto: $('#comuna_suc').val(),
        nombreArchivo: $('#nombrefile').val()

    }
    if (obj.TxtRut_Empresa.trim() == '') {
        bootbox.alert('Debes ingresar el Rut de la empresa');

        return false;
    }
    if (obj.TxtRazonsocialEmpresa.trim() == '') {
        bootbox.alert('Debes ingresar la razón social');
        return false;
    }
    if (obj.TxtDireccionEmpresa.trim() == '') {
        bootbox.alert('Debes ingresar la dirección de la empresa');
        return false;
    }
    if (obj.TxtNumeroEmpresa.trim() == '') {
        bootbox.alert('Debes ingresar el número de la dirección casa matriz');
        return false;
    }
    if (obj.DrpregionesEmpresa == '') {
        bootbox.alert('Debes seleccionar una region');
        return false;
    }
    if (obj.ComunasEmpresas == '') {
        bootbox.alert('Debes seleccionaer una comuna');
        return false;
    }
   
    if (obj.NumeroResolucion.trim() == '') {
        bootbox.alert('Debes ingresar el numero de resolución');
        return false;
    }
    if (obj.TxtRutrepresentante == '') {
        bootbox.alert('Debes ingresar rut del represante');
        return false;
    }
    if (obj.TxtNombresrepresentante == '') {
        bootbox.alert('Debe ingresar nombre del representante');
        return false;
    }

    if (obj.TxtRut_ContactoEmpresa.trim() == '') {
        bootbox.alert('Debes ingresar el rut del contacto empresa');
        return false;
    }
    if (obj.TxtTelefonoContactoEmpresa.trim() == '') {
        bootbox.alert('Debes ingresar el telefono del contacto empresa');
        return false;
    }
    if (obj.TxtNombresContactoEmpresa.trim() == '') {
        bootbox.alert('Debes ingresar el nombre del contacto empresa');
        return false;
    }
    if (obj.TxtEmailContantoEmpresa.trim() == '') {
        bootbox.alert('Debes ingresar el correo del contacto empresa');
        return false;
    }
    if (obj.TxtApellidosContactoEmpresa.trim() == '') {
        bootbox.alert('Debes ingresar el apepllido del contacto empresa');
        return false;
    }
    if (obj.TxtCargoContactoEmpresa.trim() == '') {
        bootbox.alert('Debes ingresar el cargo del contacto empresa');
        return false;
    }
    if (obj.TxtDireccionContactoEmpresa.trim() == '') {
        bootbox.alert('Debes ingresar la direccion de la sucursal');
        return false;
    }
    if (obj.TxtNumeroContactoEmpesa.trim() == '') {
        bootbox.alert('Debes ingresar el número de la sucursal');
        return false;
    }
    if (obj.DrpregionesContactoEmpresa.trim() == '') {
        bootbox.alert('Debes ingresar la region de la sucursal');
        return false;
    }
    if (obj.ComunasContacto.trim() == '') {
        bootbox.alert('Debes ingresar la comuna de la sucursal');
        return false;
    }
    if (obj.nombreArchivo.trim()=='') {
        bootbox.alert('Debes seleccionar un archivo');
        return false;
    }
    var isChecked = document.getElementById('CheckLeido').checked;
    if (isChecked) {
        //alert('checkbox esta seleccionado');
    }
    else {
        bootbox.alert('Debes validar que has leído la Carta de Compromiso');
        return false;

    }

    var files = $("#file1").get(0).files;
    var data = new FormData();
    for (i = 0; i < files.length; i++) {
        data.append("file" + i, files[i]);
    }
    $.ajax({
        type: "POST",
        url: "https://api.achs.lfi.cl/api/pac/post",
        contentType: false,
        processData: false,
        data: data,
        success: function (result) {
            if (result) {
                
                $("#file1").val('');
            }
        }
    });





    insertar_sf();

    $.ajax({
        url: 'https://api.achs.lfi.cl/api/pac/AlmacenarInformacion',
        method: "post",  // post
        async: true,
        dataType: "json",
        data: JSON.stringify(obj),
        contentType: "application/json; charset=utf-8",
        success: function (Result) {
            if (Result) {
                Result = JSON.parse(JSON.stringify(Result));
                if (Result=='1') {

                    bootbox.alert({
                        message: "Los registros han sido almacenados correctamente.",
                        callback: function () {
                            //EnviarMailPac();
                            window.location.href = '/pac';
                        }
                    })

                    
                }
            }
        },
        error: function (result) {
        }

    });
}
//function EnviarMailPac() {


//    $.ajax({
//        type: "POST",
//        url: "/Home/registroseventos/",
//        contentType: "application/json; charset=utf-8",
//        crossDomain: true,
//        dataType: 'json',
//        async: false,
//        data: { Correo: "juan@keyword.cl", Nombres: "Juan Galaz" },
//        success: function (response) {
//            console.table(response);
//            //window.location.href="Felicitaciones.aspx?e="+getUrlParameter('r');


//        },
//        failure: function (response) {
//            console.table(response);
//        },
//        error: function (response) {

//            console.table(response);
//        }
//    });








//}




 function insertar_sf() {
             var obj = {
        TxtRut_Empresa: $("#rut_empresa").val(),
        TxtRazonsocialEmpresa: $('#razon_social').val(),
        TxtDireccionEmpresa: $('#direccion_casa').val(),
        TxtNumeroEmpresa: $('#num_casa').val(),
        DrpregionesEmpresa: $('#region_casa').val(),
        ComunasEmpresas: $('#comuna_casa').val(),
        FechaRecibo: $('#TxtFecharecibo').val(),
        NumeroResolucion: $('#num_resolucion').val(),
        TxtRutrepresentante: $('#TxtRutRepresentante').val(),
        TxtNombresrepresentante: $('#nombre_representante').val(),
        TxtRut_ContactoEmpresa: $('#TxtRut_Contacto').val(),
        TxtTelefonoContactoEmpresa: $('#fono_contacto').val(),
        TxtNombresContactoEmpresa: $('#nombre_contacto').val(),
        TxtEmailContantoEmpresa: $('#correo_contacto').val(),
        TxtApellidosContactoEmpresa: $('#apellido_contacto').val(),
        TxtCargoContactoEmpresa: $('#cargo_contacto').val(),
        TxtDireccionContactoEmpresa: $('#direccion_suc').val(),
        TxtNumeroContactoEmpesa: $('#num_suc').val(),
        DrpregionesContactoEmpresa: $('#region_suc').val(),
        ComunasContacto: $('#comuna_suc').val()

    }

        var form_data = new FormData();

        for ( var key in obj ) {
            form_data.append(key, obj[key]);
        }

        var files = $("#file1").get(0).files;
        var data = new FormData();
        for (i = 0; i < files.length; i++) {
            form_data.append("file" + i, files[i]);
        }


    
             $.ajax({
                type: "POST",
                 url:"/empresas/pac/insertar_sf",
                 contentType : false,
                 processData : false,
                data: form_data,
                success: function (response) {
                   

                },
                failure: function (response) {
                    console.log(response);
                },
                error: function (response) {
                    console.log(response);
                }
            }); 
        }





    function insertar_sf_test() {
               var obj = {
            TxtRut_Empresa:  "TxtRut_Empresa",
            TxtRazonsocialEmpresa:  "TxtRazonsocialEmpresa",
            TxtDireccionEmpresa:  "TxtDireccionEmpresa",
            TxtNumeroEmpresa:  "TxtNumeroEmpresa",
            DrpregionesEmpresa:  "DrpregionesEmpresa",
            ComunasEmpresas:  "ComunasEmpresas",
            FechaRecibo:  "FechaRecibo",
            NumeroResolucion:  "NumeroResolucion",
            TxtRutrepresentante:  "TxtRutrepresentante",
            TxtNombresrepresentante:  "TxtNombresrepresentante",
            TxtRut_ContactoEmpresa:  "TxtRut_ContactoEmpresa",
            TxtTelefonoContactoEmpresa:  "TxtTelefonoContactoEmpresa",
            TxtNombresContactoEmpresa:  "TxtNombresContactoEmpresa",
            TxtEmailContantoEmpresa: "TxtEmailContantoEmpresa",
            TxtApellidosContactoEmpresa: "TxtApellidosContactoEmpresa",
            TxtCargoContactoEmpresa:  "TxtCargoContactoEmpresa",
            TxtDireccionContactoEmpresa:  "TxtDireccionContactoEmpresa",
            TxtNumeroContactoEmpesa:  "TxtNumeroContactoEmpesa",
            DrpregionesContactoEmpresa:  "DrpregionesContactoEmpresa",
            ComunasContacto:  "ComunasContacto",

        }


        var form_data = new FormData();

        for ( var key in obj ) {
            form_data.append(key, obj[key]);
        }

        var files = $("#file1").get(0).files;
        var data = new FormData();
        for (i = 0; i < files.length; i++) {
            form_data.append("file" + i, files[i]);
        }


    
             $.ajax({
                type: "POST",
                 url:"/pac/insertar_sf",
                 contentType : false,
                 processData : false,
                data: form_data,
                success: function (response) {
                   

                },
                failure: function (response) {
                    console.log(response);
                },
                error: function (response) {
                    console.log(response);
                }
            }); 
        }



$('#btnEnviarSf').on('click', function () {
    insertar_sf_test();
});