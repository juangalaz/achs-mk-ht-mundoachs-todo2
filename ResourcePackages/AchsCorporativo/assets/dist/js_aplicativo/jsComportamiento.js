﻿
$(function(){
    $('body').contents().eq(0).each(function(){
        if(this.nodeName.toString()=='#text' && this.data.trim().charCodeAt(0)==8203){
            $(this).remove();
        }
    });
});
function BloqueoInicialDatosContacto() {
    $('.bloqueoInicialDatos input').each(function () {
        $(this).attr('readonly', 'readonly');
    });

    $('.bloqueoInicialDatos select').each(function () {
        $(this).attr('disabled', 'disabled');
    });
}
function CambioTextoFormulario(){
  $(":file").filestyle({buttonBefore: true});
   
  $("#formulario_suseso .datosEmpresa h3").html("Datos empresa").after("<p style='font-size:15px'>Ingresa el rut de la empresa</p>");
  $(".datosRL h3").html("Datos representante legal").after("<p style='font-size:15px'>Ingresa el rut del representante legal. Si eres el representante legal marca la casilla YO SOY el representante legal</p>");
  $(".datosContacto h3").after("<p style='font-size:15px'>Ingresa tus datos de contacto y adjunta el formulario. Si hay un error en el archivo te avisará con un informe de errores para que puedas corregirlo</p>");
  $(".datosFile .form-group").after("<p style='font-size:15px'>Debes subir tu planillla aquí</p>");
  $("#ctl00_SPWebPartManager1_g_0e9c945c_399d_4678_808e_4eb105f282a8_txb_RutEmpresa").after("<p style='font-style: oblique;'>RUT debe ser sin puntos ni guión</p>")
  $("#advertenciaUsuarioNoRL").html("El RUT no pertenece al Representante Legal.");
  $("#ctl00_SPWebPartManager1_g_0e9c945c_399d_4678_808e_4eb105f282a8_txb_FonoFijo").after("<p style='font-style: oblique;'>Telefono fíjo es opcional</p>");

}

function BloqueoPermanenteDatosContacto(bloquear) {
    $('.bloqueoPermanenteDatos input').each(function () {
        if (bloquear)
            $(this).attr('readonly', 'readonly');
        else
            $(this).prop('readonly', false);
    });
}

//INICIO EVENTOS VALIDACION RUT EMPRESA

function HideMensajesAnexosEmpresa() {
    var element = $('.contRUTEmpresa input');

    $(element).removeClass(loadingClass);
    $(element).removeClass(successClass);
    $(element).removeClass(failClass);

    $(msjERREmpresaNoAfiliada).hide();
    $(msjEmpresaAfiliada).hide();
}

//PROBANDO EL MANEJO POSTERIOR DE LA DATA... VER COMPORTAMIENTO DE LOS DEMÁS CONTROLES TAMBIEN.
function ManejarDataPosteriorConsultaRUTEmpresa(colocarData) {
    var inputRazonSocial = $('.rznSocial input');
    var inputRutRL = $('.rutRLContenedor input');

    if (colocarData) {
        $(inputRazonSocial).val(arrayDataConsultada[0][7]);
        $(inputRutRL).removeAttr('readonly');
        $(msjERREmpresaNoAfiliada).hide();
        $(msjEmpresaAfiliada).show();
    }
    else {
        $(inputRutRL).val('');
        $(inputRutRL).attr('readonly', 'readonly');
        $(inputRazonSocial).val('');
        $(msjERREmpresaNoAfiliada).show();
    }
}

function ManejarDataPosteriorConsultaRUTRL(rutIsRL, dataUser) {
    var nmbRepre = $('.nmbRepre input');
    var appRepre = $('.appRepre input');
    var element = $('.rutRLContenedor input');

    //Siempre ocurre el desbloqueo...
    //$(nmbRepre).removeAttr('readonly');
    //$(appRepre).removeAttr('readonly');

    $(element).removeClass(loadingClass);

    if (rutIsRL) {
        var nombre = dataUser[4];
        var app1 = dataUser[5];
        var app2 = dataUser[6];
        var apps = "";
        if (app1 != "") apps = app1;

        if (apps != "" && app2 != "") apps += " " + app2;

        $(nmbRepre).val(nombre);
        $(appRepre).val(apps);
        $(msjUsuarioEsRL).show();
        $(element).addClass(successClass);

        //Acá hacer la magia de los controles de abajo.
        ValidarCamposContacto(true);
        EstablecerCamposObligatoriosContacto();
        AbrirCerrarNombresContacto(true);

    } else {
        $(msjADVUsuarioNoRL).show();
        $(nmbRepre).val('');
        $(appRepre).val('');
        $(element).addClass(failClass);
    }
}

function HideMensajesAnexosRL() {
    var element = $('.rutRLContenedor input');

    $(element).removeClass(loadingClass);
    $(element).removeClass(successClass);
    $(element).removeClass(failClass);

    $(msjADVUsuarioNoRL).hide();
    $(msjUsuarioEsRL).hide();
    
}

//FIN EVENTOS VALIDACION RUT EMPRESA

function AbrirCerrarNombresContacto(abrirLlenado) {
    $('.campoContactoDespliegueOpcional').each(function () {
        var inputData = $(this).find('input');
        $(inputData).prop('readonly', false);
    });
}

function ValidarCamposContacto(requerir) {
    $('.datosContacto input').each(function () {                    
        $(this).attr('required', requerir);
    });
    $('.datosContacto select').each(function () {
        $(this).attr('required', requerir);
    });
}

function EstablecerCamposObligatoriosContacto() {
    $('.campoContactoObligatorio').each(function () {
        var inputData = $(this).find('input');
        var selectData = $(this).find('select');
        var attrMinLargo = $(this).attr('minlength');
        var attrMaxLargo = $(this).attr('maxlength');
        var tipoDato = $(this).attr('datatype');

        if (attrMinLargo != undefined && attrMinLargo != null) {
            $(inputData).attr('minlength', attrMinLargo);
        }

        if (attrMaxLargo != undefined && attrMaxLargo != null) {
            $(inputData).attr('maxlength', attrMaxLargo);
        }

        if (tipoDato != undefined && tipoDato != null) {
            if (tipoDato == 'email')
                $(inputData).attr('type', 'email');
            if (tipoDato == 'fono') {
                $(inputData).keydown(function (e) {
                    onlyNumberKeyDown(e);
                });
            }
        }

        $(inputData).attr('required', 'true');                
        $(inputData).prop('readonly', false);

        if ($(selectData) != undefined) {
            $(selectData).attr('required', 'true');
            $(selectData).prop('disabled', false);
        }
    });
}


function hideShowContactoData(mostrar) {
    if (mostrar) {
        $('.form-group.campoContactoDespliegueOpcional').show();
        $('.campoContactoDespliegueOpcional input').each(function () {
            $(this).addClass('requerido');
            $(this).attr('required', 'true');
        });
    } else {
        $('.form-group.campoContactoDespliegueOpcional').hide();
        $('.campoContactoDespliegueOpcional input').each(function () {
            $(this).removeClass('requerido');
            $(this).removeAttr('required');
        });
    }
}

function mostrarIconoCargando(){
	$('.datosConsultarData input').fadeOut();
	$('#loadingGif').fadeIn();
}


/*
 * bootstrap-filestyle
 * doc: http://markusslima.github.io/bootstrap-filestyle/
 * github: https://github.com/markusslima/bootstrap-filestyle
 *
 * Copyright (c) 2017 Markus Vinicius da Silva Lima
 * Version 1.2.3
 * Licensed under the MIT license.
 */
(function($) {
	"use strict";

    var nextId = 0;

	var Filestyle = function(element, options) {
		this.options = options;
		this.$elementFilestyle = [];
		this.$element = $(element);
	};

	Filestyle.prototype = {
		clear : function() {
			this.$element.val('');
			this.$elementFilestyle.find(':text').val('');
			this.$elementFilestyle.find('.badge').remove();
		},

		destroy : function() {
			this.$element.removeAttr('style').removeData('filestyle');
			this.$elementFilestyle.remove();
		},

		disabled : function(value) {
			if (value === true) {
				if (!this.options.disabled) {
					this.$element.attr('disabled', 'true');
					this.$elementFilestyle.find('label').attr('disabled', 'true');
					this.options.disabled = true;
				}
			} else if (value === false) {
				if (this.options.disabled) {
					this.$element.removeAttr('disabled');
					this.$elementFilestyle.find('label').removeAttr('disabled');
					this.options.disabled = false;
				}
			} else {
				return this.options.disabled;
			}
		},

		buttonBefore : function(value) {
			if (value === true) {
				if (!this.options.buttonBefore) {
					this.options.buttonBefore = true;
					if (this.options.input) {
						this.$elementFilestyle.remove();
						this.constructor();
						this.pushNameFiles();
					}
				}
			} else if (value === false) {
				if (this.options.buttonBefore) {
					this.options.buttonBefore = false;
					if (this.options.input) {
						this.$elementFilestyle.remove();
						this.constructor();
						this.pushNameFiles();
					}
				}
			} else {
				return this.options.buttonBefore;
			}
		},

		icon : function(value) {
			if (value === true) {
				if (!this.options.icon) {
					this.options.icon = true;
					this.$elementFilestyle.find('label').prepend(this.htmlIcon());
				}
			} else if (value === false) {
				if (this.options.icon) {
					this.options.icon = false;
					this.$elementFilestyle.find('.icon-span-filestyle').remove();
				}
			} else {
				return this.options.icon;
			}
		},
		
		input : function(value) {
			if (value === true) {
				if (!this.options.input) {
					this.options.input = true;

					if (this.options.buttonBefore) {
						this.$elementFilestyle.append(this.htmlInput());
					} else {
						this.$elementFilestyle.prepend(this.htmlInput());
					}

					this.$elementFilestyle.find('.badge').remove();

					this.pushNameFiles();

					this.$elementFilestyle.find('.group-span-filestyle').addClass('input-group-btn');
				}
			} else if (value === false) {
				if (this.options.input) {
					this.options.input = false;
					this.$elementFilestyle.find(':text').remove();
					var files = this.pushNameFiles();
					if (files.length > 0 && this.options.badge) {
						this.$elementFilestyle.find('label').append(' <span class="badge">' + files.length + '</span>');
					}
					this.$elementFilestyle.find('.group-span-filestyle').removeClass('input-group-btn');
				}
			} else {
				return this.options.input;
			}
		},

		size : function(value) {
			if (value !== undefined) {
				var btn = this.$elementFilestyle.find('label'), input = this.$elementFilestyle.find('input');

				btn.removeClass('btn-lg btn-sm');
				input.removeClass('input-lg input-sm');
				if (value != 'nr') {
					btn.addClass('btn-' + value);
					input.addClass('input-' + value);
				}
			} else {
				return this.options.size;
			}
		},
		
		placeholder : function(value) {
			if (value !== undefined) {
				this.options.placeholder = value;
				this.$elementFilestyle.find('input').attr('placeholder', value);
			} else {
				return this.options.placeholder;
			}
		},		

		buttonText : function(value) {
			if (value !== undefined) {
				this.options.buttonText = value;
				this.$elementFilestyle.find('label .buttonText').html(this.options.buttonText);
			} else {
				return this.options.buttonText;
			}
		},
		
		buttonName : function(value) {
			if (value !== undefined) {
				this.options.buttonName = value;
				this.$elementFilestyle.find('label').attr({
					'class' : 'btn2 ' + this.options.buttonName
				});
			} else {
				return this.options.buttonName;
			}
		},

		iconName : function(value) {
			if (value !== undefined) {
				this.$elementFilestyle.find('.icon-span-filestyle').attr({
					'class' : 'icon-span-filestyle ' + this.options.iconName
				});
			} else {
				return this.options.iconName;
			}
		},

		htmlIcon : function() {
			if (this.options.icon) {
				return '<span style="margin-right: 3px;" class="icon-span-filestyle ' + this.options.iconName + '"></span> ';
			} else {
				return '';
			}
		},

		htmlInput : function() {
			if (this.options.input) {
				return '<input type="text" class="form-control ' + (this.options.size == 'nr' ? '' : 'input-' + this.options.size) + '" placeholder="'+ this.options.placeholder +'" disabled> ';
			} else {
				return '';
			}
		},

		// puts the name of the input files
		// return files
		pushNameFiles : function() {
			var content = '', files = [];
			if (this.$element[0].files === undefined) {
				files[0] = {
					'name' : this.$element[0] && this.$element[0].value
				};
			} else {
				files = this.$element[0].files;
			}

			for (var i = 0; i < files.length; i++) {
				content += files[i].name.split("\\").pop() + ', ';
			}

			if (content !== '') {
				this.$elementFilestyle.find(':text').val(content.replace(/\, $/g, ''));
			} else {
				this.$elementFilestyle.find(':text').val('');
			}
			
			return files;
		},

		constructor : function() {
			var _self = this, 
				html = '', 
				id = _self.$element.attr('id'), 
				files = [], 
				btn = '', 
				$label;

			if (id === '' || !id) {
				id = 'filestyle-' + nextId;
				_self.$element.attr({
					'id' : id
				});
                nextId++;
			}

			btn = '<span class="group-span-filestyle ' + (_self.options.input ? 'input-group-btn' : '') + '">' + 
			  '<label for="' + id + '" class="btn2 ' + _self.options.buttonName + ' ' + 
			(_self.options.size == 'nr' ? '' : 'btn-' + _self.options.size) + '" ' + 
			(_self.options.disabled || _self.$element.attr('disabled') ? 'disabled="true"' : '') + '>' + 
			_self.htmlIcon() + '<span class="buttonText">' + _self.options.buttonText + '</span>' + 
			  '</label>' + 
			  '</span>';
			
			html = _self.options.buttonBefore ? btn + _self.htmlInput() : _self.htmlInput() + btn;
			
			_self.$elementFilestyle = $('<div class="bootstrap-filestyle input-group">' + html + '</div>');
			_self.$elementFilestyle.find('.group-span-filestyle').attr('tabindex', "0").keypress(function(e) {
			if (e.keyCode === 13 || e.charCode === 32) {
				_self.$elementFilestyle.find('label').click();
					return false;
				}
			});

			// hidding input file and add filestyle
			_self.$element.css({
				'position' : 'absolute',
				'clip' : 'rect(0px 0px 0px 0px)' // using 0px for work in IE8
			}).attr('tabindex', "-1").after(_self.$elementFilestyle);

			if (_self.options.disabled || _self.$element.attr('disabled')) {
				_self.$element.attr('disabled', 'true');
			}

			// Getting input file value
			_self.$element.change(function() {
				var files = _self.pushNameFiles();

				if (_self.options.input == false && _self.options.badge) {
					if (_self.$elementFilestyle.find('.badge').length == 0) {
						_self.$elementFilestyle.find('label').append(' <span class="badge">' + files.length + '</span>');
					} else if (files.length == 0) {
						_self.$elementFilestyle.find('.badge').remove();
					} else {
						_self.$elementFilestyle.find('.badge').html(files.length);
					}
				} else {
					_self.$elementFilestyle.find('.badge').remove();
				}
			});

			// Check if browser is Firefox
			if (window.navigator.userAgent.search(/firefox/i) > -1) {
				// Simulating choose file for firefox
				_self.$elementFilestyle.find('label').click(function() {
					_self.$element.click();
					return false;
				});
			}
		}
	};

	var old = $.fn.filestyle;

	$.fn.filestyle = function(option, value) {
		var get = '', element = this.each(function() {
			if ($(this).attr('type') === 'file') {
				var $this = $(this), data = $this.data('filestyle'), options = $.extend({}, $.fn.filestyle.defaults, option, typeof option === 'object' && option);

				if (!data) {
					$this.data('filestyle', ( data = new Filestyle(this, options)));
					data.constructor();
				}

				if ( typeof option === 'string') {
					get = data[option](value);
				}
			}
		});

		if ( typeof get !== undefined) {
			return get;
		} else {
			return element;
		}
	};

	$.fn.filestyle.defaults = {
		'buttonText' : 'Seleccionar archivo',
		'iconName' : 'glyphicon glyphicon-folder-open',
		'buttonName' : 'btn-default',
		'size' : 'nr',
		'input' : true,
		'badge' : true,
		'icon' : true,
		'buttonBefore' : false,
		'disabled' : false,
		'placeholder': ''
	};

	$.fn.filestyle.noConflict = function() {
		$.fn.filestyle = old;
		return this;
	};

	$(function() {
		$('.filestyle').each(function() {
			var $this = $(this), options = {

				'input' : $this.attr('data-input') !== 'false',
				'icon' : $this.attr('data-icon') !== 'false',
				'buttonBefore' : $this.attr('data-buttonBefore') === 'true',
				'disabled' : $this.attr('data-disabled') === 'true',
				'size' : $this.attr('data-size'),
				'buttonText' : $this.attr('data-buttonText'),
				'buttonName' : $this.attr('data-buttonName'),
				'iconName' : $this.attr('data-iconName'),
				'badge' : $this.attr('data-badge') !== 'false',
				'placeholder': $this.attr('data-placeholder')
			};

			$this.filestyle(options);
		});
	});
})(window.jQuery);
document.getElementById('chk_IsRepresentanteLegal').addEventListener('input', function () {


	var checkeado = document.getElementById("chk_IsRepresentanteLegal").checked;

	if (checkeado == true) {
		$('#divRutContacto').hide();
		$('#divNombrecontacto').hide();
		$('#divAppContacto').hide();
	} else {
		$('#divRutContacto').show();
		$('#divNombrecontacto').show();
		$('#divAppContacto').show();
    }
});