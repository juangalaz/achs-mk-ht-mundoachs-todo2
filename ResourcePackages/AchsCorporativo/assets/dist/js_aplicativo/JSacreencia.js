﻿
$('.g-recaptcha').empty();


$("#btnConsultar").on('click', function () {

 
    var recaptcha = $("#g-recaptcha-response").val();
    if (recaptcha === "" || $("#txt_rut").val() === "") {
        event.preventDefault();
        $("#divRutNoExiste").removeClass("hidden");

    } else {
        GetList($("#txt_rut").val());
    }

 
})



function GetList(rut) {

    $.ajax({
        type: "GET",
        url: "/trabajadores/estado-de-pagos-y-acreencias/acreencias",
        data: { rut: rut },
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        async: true,
        success: function (response) {

            var json = response;
            if (json.length > 0) {
                $("#divTabla").removeClass("hidden");
                $("#btnConsultar").attr("disabled", true);
                $("#divNoExiste").addClass("hidden");

            } else {
                $("#divNoExiste").removeClass("hidden");
                $("#divTabla").addClass("hidden");
            }
            for (var x = 0; x < json.length; x++) {
               // var largo = arr_from_json[x].Monto.length;
               //var monto = arr_from_json[x].Monto.substring(0, largo - 3);
                $("#tabla_resultado > tbody").append("<tr><td>$" + formatNumber(json[x].Monto) + "</td><td>" + json[x].Banco + "</td><td>" + json[x].Medio + "</td></tr>");
 
            }
        },
        failure: function (response) {
            console.log(response);
        },
        error: function (response) {
            console.log(response);
        }
    });
}

$('#txt_rut').on('change', function () {
    var rut = $('#txt_rut').val();
    var error = 0;
    var valor = rut.split('.').join('');
    valor = valor.replace('-', '');
    cuerpo = valor.slice(0, -1);
    dv = valor.slice(-1).toUpperCase();
    rut.value = cuerpo + '-' + dv;
    if (cuerpo.length < 7) { error = 1; }
    suma = 0;
    multiplo = 2;
    for (i = 1; i <= cuerpo.length; i++) {
        index = multiplo * valor.charAt(cuerpo.length - i);
        suma = suma + index;
        if (multiplo < 7) { multiplo = multiplo + 1; } else { multiplo = 2; }
    }
    dvEsperado = 11 - (suma % 11);
    dv = (dv == 'K') ? 10 : dv;
    dv = (dv == 0) ? 11 : dv;
    if (dvEsperado != dv) { error = 1; }
    if (error == 1) {
        $('#txt_rut').val('');
        $('#txt_rut').focus();
        
        return false;
    } else {
        if (dv == 10) {
            $('#txt_rut').val((cuerpo + '-' + 'K').trim())


        } else if (dv == 11) {
            $('#txt_rut').val((cuerpo + '-' + '0').trim())
        }
        else {
            $('#txt_rut').val((cuerpo + '-' + dv).trim())
        }
        $("#btnConsultar").attr("disabled", false);
        return true;
    }
});

function formatNumber(num) {
    if (!num || num == 'NaN') return '-';
    if (num == 'Infinity') return '&#x221e;';
    num = num.toString().replace(/\$|\,/g, '');
    if (isNaN(num))
        num = "0";
    sign = (num == (num = Math.abs(num)));
    num = Math.floor(num * 100 + 0.50000000001);
    cents = num % 100;
    num = Math.floor(num / 100).toString();
    if (cents < 10)
        cents = "0" + cents;
    for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
        num = num.substring(0, num.length - (4 * i + 3)) + '.' + num.substring(num.length - (4 * i + 3));
    return (((sign) ? '' : '-') + num );
}


