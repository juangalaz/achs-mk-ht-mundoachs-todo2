﻿




//ERRORES EXCEL


//INICIO VALIDACION DE POSTBACK
function ValidarDataAfterPostback(checkIsRLEstaCheckeado) {            
    var inputRutEm = $('.contRUTEmpresa input');
    var inputRutRL = $('.rutRLContenedor input');

    if (inputRutEm.val() != "") {
        console.log('consultando data de rut empresa postback');
        consultarRutEmpresaData();
        
        if (inputRutRL.val() != "") {
            setTimeout(function () {
                console.log('consultando data de rut RL postback');
                consultarRutRLData();
            }, 1000);
        }

        //Region seleccionada
 
        //Manejar despliegue datos contacto
        hideShowContactoData(!checkIsRLEstaCheckeado);
    }
}




//Validación de rut para consulta con SPServices, se ejecuta desde el JS en en sitio con Designer.        
function GoBackServiceControlRUTSuccessOrFail(controlObj, isSuccess, arrayData) {
    $(controlObj).removeClass(loadingClass);
    $(controlObj).removeClass(successClass);
    $(controlObj).removeClass(failClass);
          
    arrayDataConsultada = arrayData;    

    if (isSuccess) {
        $(controlObj).addClass(successClass);
        ManejarDataPosteriorConsultaRUTEmpresa(true);
    }
    else {
        $(controlObj).addClass(failClass);
        ManejarDataPosteriorConsultaRUTEmpresa(false);                
    }        
}

function consultarRutEmpresaData() {
    var element = $('.contRUTEmpresa input');

    //$(element).removeClass(loadingClass);
    $(element).removeClass(successClass);
    $(element).removeClass(failClass);
    
        $(element).addClass(loadingClass);
  

    
    var rut = GetRutLimpio($(element).val().split('-')[0]);
    consultas.GetDataEmpresaRut(rut+"-"+$(element).val().split('-')[1] , $(element));
}

//Apartado para RL
function consultarRutRLData() {
    var element = $('.rutRLContenedor input');

    $(element).removeClass(successClass);
    $(element).removeClass(failClass);
    $(element).addClass(loadingClass);
    
    setTimeout(function () {  }, 1000);

    var arrUsuario = [];
    var rutLimpio = GetRutLimpio($(element).val().split('-')[0]);
    var rutIsRL = false;
    console.table(arrayDataConsultada);
    for (var i = 0; i < arrayDataConsultada.length; i++) {
        var rutRL = arrayDataConsultada[i][1];
        console.log(rutRL);
        var rolPersonaConsultada = arrayDataConsultada[i][2];

        rutRL = GetRutLimpio(rutRL.split('-')[0]);

        if (rutRL.indexOf(rutLimpio) > -1 && rolPersonaConsultada.toLowerCase().indexOf('representan') > -1) {
            rutIsRL = true;
            arrUsuario = arrayDataConsultada[i];
            break;
        }
    }

    ManejarDataPosteriorConsultaRUTRL(rutIsRL, arrUsuario);
}       



var raiz= window.location.href.toString().toLowerCase().split('/paginas')[0] + '/';
//console.log(raiz);
//raiz = "";
//console.log(raiz);
var consultas={
	GetDataEmpresaRut:function(rutEmpresa, textBoxObj){	
		//var raiz= window.location.href.toString().toLowerCase().split('/trabajaconnosotros')[0] + '/';
			
		try
		{ 
		   if(rutEmpresa!=''){
		   var arrayData = [];
		       $.ajax({  
                    type: "GET",  
                    url: " https://wa-prod-teletrabajo.azurewebsites.net/api/empresa/"+rutEmpresa,  
                    contentType: "application/json; charset=utf-8",  
                    crossDomain: true,
   					dataType: 'json', 
   				    success: function (response) {  
  						for(var x=0;x<response.length;x++){
	  						 	var arr = [];
				        					        		
				        		var rutEmpresa = response[x].RUT_EMPRESA;
				        		var rutContacto = response[x].RUT_CONTACTO
				        		var tipoContacto = response[x].TIPO_CONTACTO
				        		var vigenciaContacto = response[x].VIGENCIA_CONTACTO
				        		var nombresContacto = response[x].NOMBRES_CONTACTO
				        		var apPatContacto = response[x].APELLIDO_PATERNO_CONTACTO
				        		var apMatContacto = response[x].APELLIDO_MATERNO_CONTACTO
				        		var razonSocial =  response[x].RAZON_SOCIAL
				        		var nombreFantasia =  response[x].NOMBRE_FANTASIA	 
				        		
				        		arr[0] = rutEmpresa;
								arr[1] = rutContacto;       					        					        		
								arr[2] = tipoContacto;
								arr[3] = vigenciaContacto;
								arr[4] = nombresContacto;
								arr[5] = apPatContacto;																																		
								arr[6] = apMatContacto;
								arr[7] = razonSocial;
								arr[8] = nombreFantasia;		
								
								arrayData.push(arr);
							}
							  	GoBackServiceControlRUTSuccessOrFail(textBoxObj, true, arrayData);		
								//Objeto, true si la validación fue correcta, ver opción de pasar los datos igual, en array más que sea.
			        				
                    },  
                    failure: function (response) {  
                       console.log(response);	
                    },  
                    error: function (response) {  
                      console.log(response);	
                    }  
                }); 
              	 
		   
		   
		   
		      
		      
		    
	       }else{
	          GoBackServiceControlRUTSuccessOrFail(textBoxObj, false, arrayData);
	       }
		}catch(err){
	    	console.log(err);
	    }
	},
}

function MostrarModalErroresExcel(jsonErrores) {

jsonErrores = JSON.parse(jsonErrores);
console.log(jsonErrores);

    var htmlCompleto = '<div class="contenedorError">';
    for (var i = 0; i < jsonErrores.length; i++) {
        var errorData = '<div class="errorData">';
        errorData += '<div class="errorNroLinea">Error en la fila ' + jsonErrores[i].ReferenciaFila + '</div>';
        errorData += '<div class="errorDetalleListado">';

        for (var j = 0; j < jsonErrores[i].Errores.length; j++) {
        	var errorMsj = jsonErrores[i].Errores[j];
        	var errorX = errorMsj.split('|');
        	
        	if(errorX.length > 0){
        		errorMsj = "";
        		var huboApertura = false;
	        	for(var k = 0; k < errorX.length; k++){	        		
	        		if(k % 2 == 0)
	        		{
	        			if(!huboApertura){
	        				huboApertura = true;
	        				errorMsj += errorX[k] + '"';
        				}else{
	        				huboApertura = false;
	        				errorMsj += '"' + errorX[k];
        				}
        			}else
        				errorMsj += errorX[k];
	        	}
        	}
        	
            errorData += '<div class="errorDetalleItem">' + errorMsj + '</div>';
        }

        errorData += '</div></div>'; //Cierre de "errorDetalleListado" y contenedor "errorData"        
        htmlCompleto += errorData;
    }

    htmlCompleto += '</div>'

    $('#contenedor-errores-modal').html(htmlCompleto);    
    $('#modalErroresCarga').modal('show');
    $(".header-image-data img").attr('src','/PublishingImages/ico_alerta_tele.svg').after("<br/><br/><p>Se han detectado algunos errores en los datos de su plantilla.&nbsp; Para continuar, corregir los errores del archivo y volver a cargar.  .</p>");
    $(".boton-cerrar-modal").addClass("pull-left").css('margin-top',"-25px"); 
    
    
}


/*
";
*/
	
	/*
	
			var raiz= window.location.href.toString().toLowerCase().split('/trabajaconnosotros')[0] + '/';
		$().SPServices({
			operation: "GetListItems",
			webURL: raiz,
			async: false,
			listName: "spbx_region_sedes",
	        CAMLQuery: "<Query><Where><Eq><FieldRef Name='ID'></FieldRef><Value Type='Text'>"+id+"</Value></Eq></Where></Query>",
	        CAMLViewFields: "<ViewFields><FieldRef Name='Title' /></ViewFields>",
	        completefunc: function(xData, Status) {
	        	if(Status=='success'){
		        	$(xData.responseXML).SPFilterNode("z:row").each(function() {
		        		bRegion=$(this).attr("ows_Title");
					});
				}
	        }
	    });

	
	getDataFromServerObject:function(objeto,valorBlanco){
		var data=valorBlanco;
		if(objeto!=undefined){
			data=objeto.toString();			
			if(data.indexOf(';#') > -1){
				data = helper.getStringDataForLookupValues(data);
			}
		}
		return data;
	},
	
	*/
