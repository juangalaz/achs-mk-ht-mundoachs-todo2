﻿function eventosdeControlesIniciales(){
        CambioTextoFormulario();
        
        
    /*INICIO VALIDACIONES PARA CAMPO SÓLO TEXTO SIN NUMEROS */

//NUEVA VALIDACION
			
    $("input[name*='txb_NombreContacto']").keydown(function (e){
    	onlyLettersKeyDown(e);
    });

    $("input[name*='txb_ApellidosContacto']").keydown(function (e){
    	onlyLettersKeyDown(e);
    });
    
    $("input[name*='txb_Cargo']").keydown(function (e){
    	onlyLettersKeyDown(e);
    });

    $("input[name*='txb_Direccion']").keydown(function (e){
    	onlyLettersAndNumbersKeyDown(e);
    });       
    
/* FIN VALIDACIONES SOLO TEXTO */     
        
    $('input[required="true"]').each(function () {
        $(this).addClass('requerido');
    });

    $('input[onlynumbers="true"]').each(function () {
        $(this).keydown(function (e) {
            onlyNumberKeyDown(e);
        });
    });

    $('.rutDataContenedor input').each(function () {
        $(this).keydown(function (e) {
            onlyRutDataKeyDown(e);
        });
    });

 
}

function realizarValidacionesDatos(){

setTimeout(function(){ 
	realizarValidacionesVerdaderas();
}, 1000);



}

function realizarValidacionesVerdaderas(){
            $("form").validate({
                submitHandler: function (form) {
                    // call your function                           
					BloqueoPermanenteDatosContacto(false);
					mostrarIconoCargando();
                    console.log("el submit");
                    $("form")[0].submit();
                    //return false;
                    //$("form").submit();
                }
            });


            $.validator.addMethod(
                "validRUTGenerico",
                function (value, element) {
                    //console.log(value);
                    //console.log(element);

                    //Validar si es la caja correcta...
                    var esCajaRutEmpresa = false;
                    if ($(element).hasClass('fieldRUTEmpresa')) { esCajaRutEmpresa = true; HideMensajesAnexosEmpresa(); }

                    var checkRutIsValid = checkRutGenerico($(element).attr('id'), 0, 2);
                    if (checkRutIsValid && esCajaRutEmpresa) consultarRutEmpresaData();
                    return checkRutIsValid;
                },
                "RUT incorrecto."
            );

            $.validator.addMethod(
                "validRUTPersona",
                function (value, element) {
                    //console.log(value);
                    //console.log(element);
                    //HideMensajesAnexosRL

                    var esCajaRutRL = false;
                    if ($(element).hasClass('fieldRUTRL')) { esCajaRutRL = true; HideMensajesAnexosRL(); }

                    var checkRutIsValid = checkRutGenerico($(element).attr('id'), 1, 2);
                    //en esta parte consultar el famoso array...
                    if (checkRutIsValid && esCajaRutRL) consultarRutRLData();
                    return checkRutIsValid;


                },
                "RUT incorrecto o no es RUT de Persona Natural."
            );

            $.validator.addMethod(
                "validRUTEmpresa",
                function (value, element) {
                    //console.log(value);
                    //console.log(element);
                    var checkRutIsValid = checkRutGenerico($(element).attr('id'), 2, 2);
                    return checkRutIsValid;
                },
                "RUT incorrecto o no es RUT de Empresa."
            );
            
            //NUEVA VALIDACION
            $.validator.addMethod(
                "direccionData",
                function (value, element) {
                    var checkDireccionValid = validarDireccionData($(element).val());
                    return checkDireccionValid;
                },
                "La dirección debe contener letras y números."
            );





            //Agregar o quitar reglas dinámicamente, o simplemente inhabilitarlas según condición.
            /*
                $(".selector").validate({
                  rules: {
                    contact: {
                      required: true,
                      email: {
                        depends: function(element) {
                          return $("#contactform_email:checked")
                        }
                      }
                    }
                  }
                });
            */

            /*
            https://stackoverflow.com/questions/8137844/jquery-validation-two-fields-only-required-to-fill-in-one
            //TELEFONO UNO DE LOS 2
            rules: {
                telephone: {
                    required: function(element) {
                        return $("#mobile").is(':empty');
                    }
                },
                mobile: {
                    required: function(element) {
                        return $("#telephone").is(':empty');
                    }
                }
            }
            */                   





            $(".fieldRUTEmpresa").rules('add', { 
                required: function (element) {
                    var isVacio = !$(element).val();
                    if (isVacio) HideMensajesAnexosEmpresa();                            
                    return isVacio;
                },
                minlength: 0,
                maxlength: 12,                        
                validRUTGenerico: true
            });
             
            $(".fieldRUTRL").rules('add', { 
                required: function (element) {
                    var isVacio = !$(element).val();
                    if (isVacio) HideMensajesAnexosRL();
                    return isVacio;
                },
                minlength: 0,
                maxlength: 12,
                validRUTPersona: true
            });

            $(".rutContactoData").rules('add', { 
                required: checkedOrNotOut,
                minlength: 8,
                maxlength: 12,
                validRUTPersona: true
            });
            $("input[name*='txb_Direccion']").rules('add', { 
                required: true,
                minlength: 10,
                direccionData: true
            });


            $(".fieldFileData").rules('add', { 
                required: true,
                extension: "xlsx",
                messages: {
                    required: "Por favor incluya la planilla con la información de las personas con trabajo remoto.",
                    extension: "El archivo debe ser en formato 'XLSX'."
                }

            });

            $(".fieldFonoCelu").rules('add', {
                required: function (element) {                            
                    var isVacio = !$(".fonoFijo input").val();
                    //console.log(isVacio);
                    return isVacio;
                }
            });

            $(".fieldFonoFijo").rules('add', {
                required: function (element) {
                    var isVacio = !$(".fonoCelu input").val();
                    //console.log(isVacio);
                    return isVacio;
                }
            });




            /*
            $("form").bind("invalid-form", function () {
                console.log('invalid!');
                //setInterval(function () { resizeIframeInParent(); }, 1500);
            });            
            */

            //Acá el fin del document.ready

}

//REVISARRRRR ESTE ES EL CODIGO NUEVO

function onlyNumberKeyDown(e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
        // Allow: Ctrl+A, Command+A
        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
        // let it happen, don't do anything
        return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
}

//NUEVA VALIDACION
function onlyLettersKeyDown(e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [32, 46, 8, 9, 27, 13, 110]) !== -1 ||
        // Allow: Ctrl+A, Command+A
        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
        // let it happen, don't do anything
        return;
    }
    // Ensure that it is a letter and stop the keypress
    if (e.keyCode < 65 || e.keyCode > 90) {
        e.preventDefault();
    }
}

//NUEVA VALIDACION
function onlyLettersAndNumbersKeyDown(e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [32, 46, 8, 9, 27, 13, 110]) !== -1 ||
        // Allow: Ctrl+A, Command+A
        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
        // let it happen, don't do anything
        return;
    }
    // Ensure that it is a number and letter stop the keypress
    if ((e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 65 || e.keyCode > 90)) {
        e.preventDefault();
    }
}


function onlyRutDataKeyDown(e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
        // Allow: Ctrl+A, Command+A
        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40) ||
        // es la K
        (e.keyCode == 75)) {
        // let it happen, don't do anything
        return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
}

//NUEVA VALIDACION
function validarDireccionData(dato){
	if (/\d/.test(dato) && /[a-zA-Z]/.test(dato)) {
		return true;
	}
	return false;
}
