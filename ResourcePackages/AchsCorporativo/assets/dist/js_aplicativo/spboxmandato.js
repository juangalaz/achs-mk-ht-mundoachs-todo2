﻿/* jQuery.rut.js */
!function (a) { function c(a) { return a.replace(/[\.\-]/g, "")} function d(a, b) { var c = j(a), d = c[0], e = c[1]; if (!d || !e) return d || a; for (var f = "", g = b ? "." : ""; d.length > 3;) f = g + d.substr(d.length - 3) + f, d = d.substring(0, d.length - 3); return d + f + "-" + e } function e(a) { return a.type && a.type.match(/^key(up|down|press)/) && (8 === a.keyCode || 16 === a.keyCode || 17 === a.keyCode || 18 === a.keyCode || 20 === a.keyCode || 27 === a.keyCode || 37 === a.keyCode || 38 === a.keyCode || 39 === a.keyCode || 40 === a.keyCode || 91 === a.keyCode) } function f(a, d) { if ("string" !== typeof a) return !1; var e = c(a); if ("boolean" === typeof d.minimumLength) { if (d.minimumLength && e.length < b.minimumLength) return !1 } else { var f = parseInt(d.minimumLength, 10); if (e.length < f) return !1 } var h = e.charAt(e.length - 1).toUpperCase(), i = parseInt(e.substr(0, e.length - 1)); return !isNaN(i) && g(i).toString().toUpperCase() === h } function g(a) { var b = 0, c = 2; if ("number" === typeof a) { a = a.toString(); for (var d = a.length - 1; d >= 0; d--) b += a.charAt(d) * c, c = (c + 1) % 8 || 2; switch (b % 11) { case 1: return "k"; case 0: return 0; default: return 11 - b % 11 } } } function h(a, b) { a.val(d(a.val(), b)) } function i(a) { f(a.val(), a.opts) ? a.trigger("rutValido", j(a.val())) : a.trigger("rutInvalido") } function j(a) { var b = c(a); if (0 === b.length) return [null, null]; if (1 === b.length) return [b, null]; var d = b.charAt(b.length - 1), e = b.substring(0, b.length - 1); return [e, d] } var b = { validateOn: "blur", formatOn: "blur", ignoreControlKeys: !0, useThousandsSeparator: !0, minimumLength: 2 }, k = { init: function (c) { if (this.length > 1) for (var d = 0; d < this.length; d++) console.log(this[d]), a(this[d]).rut(c); else { var f = this; f.opts = a.extend({}, b, c), f.opts.formatOn && f.on(f.opts.formatOn, function (a) { f.opts.ignoreControlKeys && e(a) || h(f, f.opts.useThousandsSeparator) }), f.opts.validateOn && f.on(f.opts.validateOn, function () { i(f) }) } return this } }; a.fn.rut = function (b) { return k[b] ? k[b].apply(this, Array.prototype.slice.call(arguments, 1)) : "object" !== typeof b && b ? void a.error("El método " + b + " no existe en jQuery.rut") : k.init.apply(this, arguments) }, a.formatRut = function (a, b) { return void 0 === b && (b = !0), d(a, b) }, a.computeDv = function (a) { var b = c(a); return g(parseInt(b, 10)) }, a.validateRut = function (b, c, d) { if (d = d || {}, f(b, d)) { var e = j(b); return a.isFunction(c) && c(e[0], e[1]), !0 } return !1 } }(jQuery);
var rutempresavalido = false;
var arrListadoRepresentantes = [];
var rutrepresentanteselct = "";
var heleido;
var listadocargadorepresentante = false;
$(document).ready(function () {
    $('a[data-toggle="tooltip"]').tooltip({
        animated: 'fade',
        placement: 'right',
        html: true
    });

    $('input[type=text]').on('keydown', function (e) {
        if (e.which === 13) {
            e.preventDefault();
        }
    });
    $("#nextrutempresa").click(function () {
        $("#divbienvenida").hide();//.addClass('hidden').fadeIn("slow");
        $("#empresa_ing").removeClass('hidden');
    });

    $("#rut_empresa").rut({ minimumLength: 7, useThousandsSeparator: false }).on('rutInvalido', function (e) {
        $("#rut_empresa").addClass("error");
        $(".mensajeusuario").html("Rut inválido / Esta empresa no se encuentra en nuestra base de datos").addClass("mensajeerror");
        setTimeout(function () { $(".mensajeusuario").html('Ingrese el RUT de su Empresa, debe ser sin puntos y con guión.').removeClass("mensajeerror"); }, 2500);
        rutempresavalido = false;
    }).on('rutValido', function () {
        rutempresavalido = true;
        $("#rut_empresa").removeClass("error");
        $("#rut_empresa").addClass("loading");
  
    });

    $("#rut_rol").rut({ minimumLength: 7, useThousandsSeparator: false }).on('rutInvalido', function (e) {
        $("#rut_rol").addClass("error");
    }).on('rutValido', function () {
        $("#rut_rol").removeClass("error");
    });
    $("#rut_representante").rut({ minimumLength: 7, useThousandsSeparator: false }).on('rutInvalido', function (e) {
        $(this).addClass("error");
    }).on('rutValido', function () {
        $(this).removeClass("error");
    });
    $(".validacioniden").click(function () {
        
        $.when(spbxmandato.EnvioRespuestas()).done(function (d) {
     
        }).fail(function () {

        });


    });

    $("#runequifax").rut({ minimumLength: 7, useThousandsSeparator: false }).on('rutInvalido', function (e) {
        $(this).addClass("error");
        $("#mensajerun").html("Rut inválido");
    }).on('rutValido', function () {
        setTimeout(function () { $(this).removeClass("error"); $("#mensajerun").addClass("hidden")}, 2500);
    });


    $(".next-step").click(function (e) {

        if ($(this).attr("id") !== "nextrutempresa") {
            var $active = $('.nav-tabs li.active');
            if (ValidacionDatos($active.find('a').attr("aria-controls")) === true) {
                $active.next().removeClass('disabled');
                nextTab($active);
                $active.find("a").addClass("check");
            }

        }

    });
    $(".prev-step").click(function (e) {

        var $active = $('.nav-tabs li.active');
        prevTab($active);

    });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
     
        var target = $(e.target).attr("href"); // activated tab
        ValidacionDatos(target.replace(/#/g, ""));
    });
   

    $(".validardoc").click(function () {
        if ($("#runequifax").val() !== "" && $("#serieequifax").val() !== "") {
            $("#serieequifax").addClass("loading");
            $("#runequifax").prop('disabled', true);
            $("#serieequifax").prop('disabled', true);
            $.when(spbxmandato.ValidacionEquifax()).done(function (d) {
                $("#serieequifax").removeClass("loading");
            }).fail(function () {

            });

        }
       
    });

    $(".btnenvioinformacion").click(function () {
   
        if (heleido === true) {
            var validator = $('#formulariototalenvio').data('bootstrapValidator');
            validator.validate();
         
            if (validator.isValid()) {
                $.when(spbxmandato.EnviarFormularioMandato()).done(function (d) {
                    $("#finalizado").removeClass("hidden");
                    $("#new_admin").addClass("hidden");
                    $(".titulomandado").addClass("hidden");
                }).fail(function () {

                });
            }
        }
    });


 
    $("#heleido").click(function () {
      
        if ($(this).prop("checked") === true) {
            //$("#formulariototalenvio").removeClass('hidden');
            heleido = true;
            $("#formulariototalenvio :input").removeAttr("readonly").removeClass("readOnly");
            $("#nombre_representante").prop("readonly", true);
            $("#apellido_p_representante").prop("readonly", true);
            $("#apellido_m_representante").prop("readonly", true);
            $("#rut_representante").prop("readonly", true);
        
    
        } else {
            //$("#formulariototalenvio").addClass('hidden');
            $("#formulariototalenvio :input").attr("readonly", "readonly").addClass("readOnly");
            $("#nombre_representante").prop("readonly", true);
            $("#apellido_p_representante").prop("readonly", true);
            $("#apellido_m_representante").prop("readonly", true);
            $("#rut_representante").prop("readonly", true);
            heleido = false;
        }
    });
    $(".finalizarmandato").click(function () {
        location.reload();
    });
    spbxmandato.ValidateForm();
    $("#region_rol").change(function () {
        spbxmandato.GetComunaFilter("comuna_rol", $(this).val());
    });
    $("#region_representante").change(function () {
        spbxmandato.GetComunaFilter("comuna_representante", $(this).val());
    });

    $.when(spbxmandato.GetListaCargos(), spbxmandato.GetRegion()).done(function (d) {


    }).fail(function () {

    });
  

});

function ValidacionDatos(idvalidar) {
    var pasoalsiguiente = false;
    
    switch (idvalidar) {
        case "bienvenida":
            if ($("#rut_empresa").val() === "" || rutempresavalido === false) {
                pasoalsiguiente = false;
                $("#rut_empresa").addClass("error");
                $(".mensajeusuario").html("Rut inválido / Esta empresa no se encuentra en nuestra base de datos").addClass("mensajeerror");
                setTimeout(function () { $("#rut_empresa").val('').removeClass("error"); $(".mensajeusuario").html('Ingrese el RUT de su Empresa, debe ser sin puntos y con guión.').removeClass("mensajeerror"); }, 3000);

            } else {
                $.when(spbxmandato.ValidacionEmpresaACHS($("#rut_empresa").val()))
                    .done(function (d) {
                        $("#rut_empresa").removeClass("loading");
                        pasoalsiguiente = d;

                    })
                    .fail(function () {
                        $("#rut_empresa").removeClass("loading");
                    });

            }
            break;

        case "datos_contacto":
            
            if (listadocargadorepresentante === true) {
                $.when(spbxmandato.pasoRutSeleccionado()).done(function (d) {
                    $("#rut_empresa").removeClass("loading");
                    pasoalsiguiente = d;

                }).fail(function () {

                });
            } else {
                if ($("#rut_empresa").val() === "" || rutempresavalido === false) {
                    var $active = $('.nav-tabs li.active');
                    
                    prevTab($active);
                    pasoalsiguiente = false;
                    $("#rut_empresa").addClass("error");
                    $(".mensajeusuario").html("Rut inválido / Esta empresa no se encuentra en nuestra base de datos").addClass("mensajeerror");
                    setTimeout(function () { $("#rut_empresa").val('').removeClass("error"); $(".mensajeusuario").html('Ingrese el RUT de su Empresa, debe ser sin puntos y con guión.').removeClass("mensajeerror"); }, 3000);
                    
                }
            }
            break;

        case "equifax":

            if ($("#rut_empresa").val() === "" || rutempresavalido === false) {
                var $active = $('.nav-tabs li.active');

                prevTab($active);
                pasoalsiguiente = false;
                $("#rut_empresa").addClass("error");
                $(".mensajeusuario").html("Rut inválido / Esta empresa no se encuentra en nuestra base de datos").addClass("mensajeerror");
                setTimeout(function () { $("#rut_empresa").val('').removeClass("error"); $(".mensajeusuario").html('Ingrese el RUT de su Empresa, debe ser sin puntos y con guión.').removeClass("mensajeerror"); }, 3000);

            }
            for (var x = 0; x < arrListadoRepresentantes.length; x++) {

                if ((arrListadoRepresentantes[x].TIPO_CONTACTO === "REPRESENTANTE_LEGAL" || arrListadoRepresentantes[x].TIPO_CONTACTO === "COMERCIAL" || arrListadoRepresentantes[x].TIPO_CONTACTO==="PREVENTIVO") && rutrepresentanteselct === arrListadoRepresentantes[x].RUT_CONTACTO) {

                    $("#nombre_representante").val(arrListadoRepresentantes[x].NOMBRES_CONTACTO).prop("readonly", true);
                    $("#apellido_p_representante").val(arrListadoRepresentantes[x].APELLIDO_PATERNO_CONTACTO).prop("readonly", true);
                    $("#apellido_m_representante").val(arrListadoRepresentantes[x].APELLIDO_MATERNO_CONTACTO).prop("readonly", true);
                    $("#rut_representante").val(arrListadoRepresentantes[x].RUT_CONTACTO).prop("readonly", true);

                }
            }
            pasoalsiguiente = true;
            break;
        case "nombramiento":
            if ($("#rut_empresa").val() === "" || rutempresavalido === false) {
                var $active = $('.nav-tabs li.active');

                prevTab($active);
                pasoalsiguiente = false;
                $("#rut_empresa").addClass("error");
                $(".mensajeusuario").html("Rut inválido / Esta empresa no se encuentra en nuestra base de datos").addClass("mensajeerror");
                setTimeout(function () { $("#rut_empresa").val('').removeClass("error"); $(".mensajeusuario").html('Ingrese el RUT de su Empresa, debe ser sin puntos y con guión.').removeClass("mensajeerror"); }, 3000);

            }
            break;

    }


    return pasoalsiguiente;
}

var spbxmandato = {
    ValidacionEquifax: function () {
     var run = $("#runequifax").val();
        var serie = $("#serieequifax").val();
        $("#listadopregunasequifax").html('');
        $.ajax
            ({
                type: 'POST',
                url: '/Home/EsValidoDocumento',
                dataType: 'json',
                data: { run: run.replace(/\./g, ""), serie: serie },
                beforeSend: function () {},
                complete: function () {},
                success: function (data) {
                    resultado =  data;
                    if (resultado.autoriza === 1) {
                        var arrpreguntas = JSON.parse(resultado.estructura);
                        var htmlpreguntas = "";
                        $("#tk").val(arrpreguntas.transactionKey);
                        for (f = 0; f < arrpreguntas.interactiveQuery.question.length; f++) {
                            htmlpreguntas += "<div class='mod_preg'>";

                        
                            htmlpreguntas += "<h3>" + arrpreguntas.interactiveQuery.question[f].questionText + "</h3>";
                            htmlpreguntas += " <div class='row'>";
                            for (d = 0; d < arrpreguntas.interactiveQuery.question[f].answerChoice.length; d++) {
                                if (d < 4) {
                                    htmlpreguntas += " <div class='col-md-2'>";
                                } else {
                                    htmlpreguntas += " <div class='col-md-3' style='text-align:center;'>";
                                }
                                htmlpreguntas += "<input type='radio' name='P" + f + "' value='" + arrpreguntas.interactiveQuery.question[f].answerChoice[d].answerId + "'>" + arrpreguntas.interactiveQuery.question[f].answerChoice[d].value + " ";
                                htmlpreguntas += " </div>";
                          
                               
                            }
                            htmlpreguntas += "<div class='clearfix'></div>";
                            htmlpreguntas += "</div>";
                            htmlpreguntas += "</div>";
                          
                        }
                        $("#listadopregunasequifax").html(htmlpreguntas);
                        $(".equifaxpaso1").addClass('hidden');
                        //$("#validacion").removeClass('hidden');
                        $("#equifax2").removeClass('hidden');
                        $("#validacion2").addClass('hidden');
                    } else {
                      
                        $(".equifaxpaso1").addClass('hidden');
                        $("#validacion").addClass('hidden');
                        $("#equifax2").addClass('hidden');
                        $(".menjaseequifaxvalidacion").html(resultado.msj);
                        $("#validacion2").removeClass('hidden');
                    }
                },
                error: function (ex) {
                    tr = jQuery.parseJSON(response.responseText);

                }
            });

   

    }, ValidacionEmpresaACHS: function (run) {
        var datoscompletado = false;
        $("#rut_empresa").addClass("loading");
        $.ajax({
            type: "GET",
            url: " /Home/ValidateEmpresaACHS",
            data: { run: run },
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            async: false,
            success: function (response) {
                var arr_from_json = JSON.parse(response);
                //console.log(arr_from_json);
                if (arr_from_json.length > 0) {
                    $("#nombreempresa").html(arr_from_json[0].RAZON_SOCIAL);
                    $("#razonsocial").val(arr_from_json[0].RAZON_SOCIAL);
                    $("#rutempresa").html(": " + arr_from_json[0].RUT_EMPRESA);
                    $("#bpmatrizfinanzas").val(arr_from_json[0].BP_MATRIZ_FINANZAS);
                 
                    var resultrepresentante = arr_from_json.filter(function (v, i) {
                        return ((v["TIPO_CONTACTO"] === "COMERCIAL" || v["TIPO_CONTACTO"] === "REPRESENTANTE_LEGAL" || v["TIPO_CONTACTO"] === "PREVENTIVO" ) && v.RUT_CONTACTO !== "");
                    });
                    if (resultrepresentante.length > 0) {
                        $('#listadocontacto > tr').remove();
                        for (var x = 0; x < arr_from_json.length; x++) {


                            //$("#Hidden_SSegmento").value = arr_from_json[x].SUBSEGMENTO.toString();

                            document.getElementById('SSegmento').value = arr_from_json[x].SUBSEGMENTO;


                            if (arr_from_json[x].SUBSEGMENTO === "GCC" || arr_from_json[x].SUBSEGMENTO === "GC" || arr_from_json[x].SUBSEGMENTO === "GCR" || arr_from_json[x].SUBSEGMENTO === "GCN") {


                                if (arr_from_json[x].RUT_CONTACTO !== null) {
                                    if (arr_from_json[x].TIPO_CONTACTO === "REPRESENTANTE_LEGAL" || arr_from_json[x].TIPO_CONTACTO === "COMERCIAL" || arr_from_json[x].TIPO_CONTACTO === "PREVENTIVO") {
                                        $('#listadocontacto > tbody:last').after('<tr><td><input type="radio" name="radio" id="' + arr_from_json[x].RUT_CONTACTO + '" value="radio"><label for="radio">' + arr_from_json[x].NOMBRE_COMPLETO_CONTACTO.replace(/Señor/g, '').replace(/Señora/g, '') + '</label></td><td>' + arr_from_json[x].RUT_CONTACTO + '</td></tr>');
                                        arrListadoRepresentantes.push(arr_from_json[x]);
                                    }
                                }
                            } else {
                                if (arr_from_json[x].RUT_CONTACTO !== null) {
                                    if (arr_from_json[x].TIPO_CONTACTO === "REPRESENTANTE_LEGAL" ) {
                                        $('#listadocontacto > tbody:last').after('<tr><td><input type="radio" name="radio" id="' + arr_from_json[x].RUT_CONTACTO + '" value="radio"><label for="radio">' + arr_from_json[x].NOMBRE_COMPLETO_CONTACTO.replace(/Señor/g, '').replace(/Señora/g, '') + '</label></td><td>' + arr_from_json[x].RUT_CONTACTO + '</td></tr>');
                                        arrListadoRepresentantes.push(arr_from_json[x]);
                                    }
                                }

                            }

                        }
                    } else {
                        $("#validacion3").removeClass('hidden');
                        $("#divempresa").addClass('hidden');
                    }

                    listadocargadorepresentante = true;
                } else {
                    $("#validacion3").removeClass('hidden');
                    $("#divempresa").addClass('hidden');
                }
              
                datoscompletado = true; 
            },
            failure: function (response) {
                console.log(response);
            },
            error: function (response) {
                console.log(response);
            }
        });
        
        return datoscompletado;
       

    }, pasoRutSeleccionado: function () {
        var returnpaso = false;
        var manageradiorel = $("input:radio[name ='radio']:checked").attr("id");
    
        if (manageradiorel !== "" && manageradiorel !== undefined) {
            rutrepresentanteselct = manageradiorel;
            $("#runequifax").val(manageradiorel);
            returnpaso = true;
        } else {
            $(".mensajeerrorseleccionrepresentante").removeClass("hidden");
            setTimeout(function () { $(".mensajeerrorseleccionrepresentante").addClass("hidden");}, 3000);
        }
        return returnpaso;
    }, EnvioRespuestas: function () {
        var respuestas = {
            p1: $("input[name='P0']:checked").val(),
            p2: $("input[name='P1']:checked").val(),
            p3: $("input[name='P2']:checked").val(),
            p4: $("input[name='P3']:checked").val(),
            tk: $("#tk").val()
        };
       
        $.ajax
            ({
                type: 'POST',
                url: '/Home/EnvioRespuesta',
                dataType: 'json',
                data: { objrespuesta: respuestas},
                beforeSend: function () { },
                complete: function () { },
                success: function (data) {
                    if (data.esvalido === 1) {
                        $(".equifaxpaso1").addClass('hidden');
                        $("#validacion").removeClass('hidden');
                        $("#equifax2").addClass('hidden');
                    } else {
                        $(".equifaxpaso1").addClass('hidden');
                        $("#validacion").addClass('hidden');
                        $("#validacion2").removeClass('hidden');

                    }
                },
                error: function (ex) {
                    tr = jQuery.parseJSON(response.responseText);
                    $(".equifaxpaso1").addClass('hidden');
                    $("#validacion").addClass('hidden');
                    $("#validacion2").removeClass('hidden');
                }
            });

    }, GetListaCargos: function () {
        $.ajax({
            type: "GET",
            url: " /Home/GetListCargos",
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            success: function (response) {
                var arr_from_json = JSON.parse(response);
             
                $("#cargo_rol").append($('<option>', {
                    value: "",
                    text: "Seleccione cargo"
                }));
                $("#cargo_representante").append($('<option>', {
                    value: "",
                    text: "Seleccione cargo"
                }));
                for (var x = 0; x < arr_from_json.length; x++) {
                    
                  $("#cargo_rol").append($('<option>', {
                      value: arr_from_json[x].codigocargo,
                      text: arr_from_json[x].cargo
                  }));
                  $("#cargo_representante").append($('<option>', {
                      value: arr_from_json[x].codigocargo,
                      text: arr_from_json[x].cargo
                  }));
                }

              

            },
            failure: function (response) {
                console.log(response);
            },
            error: function (response) {
                console.log(response);
            }
        }); 

    }, GetRegion: function () {
        $.ajax({
            type: "GET",
            url: " /Home/GetRegiones",
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            success: function (response) {
                var arr_from_json = JSON.parse(response.regionlist);

                $("#region_rol").append($('<option>', {
                    value: "",
                    text: "Seleccione región"
                }));
                $("#comuna_rol").append($('<option>', {
                    value: "",
                    text: "Seleccione comuna"
                }));
                $("#region_representante").append($('<option>', {
                    value: "",
                    text: "Seleccione región"
                }));
                $("#comuna_representante").append($('<option>', {
                    value: "",
                    text: "Seleccione comuna"
                }));
                
                for (var x = 0; x < arr_from_json.length; x++) {

                    $("#region_rol").append($('<option>', {
                        value: arr_from_json[x].cod_region,
                        text: arr_from_json[x].nom_region
                    }));
                    $("#region_representante").append($('<option>', {
                        value: arr_from_json[x].cod_region,
                        text: arr_from_json[x].nom_region
                    }));
                }



            },
            failure: function (response) {
                console.log(response);
            },
            error: function (response) {
                console.log(response);
            }
        }); 


    }, ValidateForm: function () {

        $('[id*=formulariototalenvio]').bootstrapValidator({
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                nombre_rol: {
                    validators: {
                        notEmpty: {
                            message: 'Debes completar la información'
                        }
                    }
                },
                apellido_p_rol: {
                    validators: {
                        notEmpty: {
                            message: 'Debes completar la información'
                        }
                    }
                },
                apellido_m_rol: {
                    validators: {
                        notEmpty: {
                            message: 'Debes completar la información'
                        }
                    }
                },
                 rut_rol: {
                    validators: {
                        notEmpty: {
                            message: 'Debes completar la información'
                        }
                    }
                },
                movil_rol: {
                    validators: {
                        notEmpty: {
                            message: 'Debes completar la información'
                        }
                    }
                }, direccion_rol: {
                    validators: {
                        notEmpty: {
                            message: 'Debes completar la información'
                        }
                    }
                }, numeracion_rol: {
                    validators: {
                        notEmpty: {
                            message: 'Debes completar la información'
                        }
                    }
                }, comuna_rol: {
                    validators: {
                        notEmpty: {
                            message: 'Debes completar la información'
                        }
                    }
                }, region_rol: {
                    validators: {
                        notEmpty: {
                            message: 'Debes completar la información'
                        }
                    }
                }, email_rol: {
                    validators: {
                        notEmpty: {
                            message: 'Debes completar la información'
                        }, regexp: {
                            regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                            message: 'El correo no es valido'
                        }
                    }
                }, email_r_rol: {
                    validators: {
                        notEmpty: {
                            message: 'Debes completar la información'
                        }, regexp: {
                            regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                            message: 'El correo no es valido'
                        },identical: {
                            field: 'email_rol',
                            message: 'Los correos ingresado no son iguales'
                        }
                    }
                }, cargo_rol: {
                    validators: {
                        notEmpty: {
                            message: 'Debes completar la información'
                        }
                    }
                }, email_representante: {
                    validators: {
                        notEmpty: {
                            message: 'Debes completar la información'
                        }, regexp: {
                            regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                            message: 'El correo no es valido'
                        }
                    }
                }, email_r_representante: {
                    validators: {
                        notEmpty: {
                            message: 'Debes completar la información'
                        }, regexp: {
                            regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                            message: 'El correo no es valido'
                        }, identical: {
                            field: 'email_representante',
                            message: 'Los correos ingresado no son iguales'
                        }
                    }
                }, movil_representante: {
                    validators: {
                        notEmpty: {
                            message: 'Debes completar la información'
                        }
                    }
                }, direccion_representante: {
                    validators: {
                        notEmpty: {
                            message: 'Debes completar la información'
                        }
                    }
                }, numeracion_representante: {
                    validators: {
                        notEmpty: {
                            message: 'Debes completar la información'
                        }
                    }
                }, comuna_representante: {
                    validators: {
                        notEmpty: {
                            message: 'Debes completar la información'
                        }
                    }
                }, region_representante: {
                    validators: {
                        notEmpty: {
                            message: 'Debes completar la información'
                        }
                    }
                }

            }
        });


    }, EnviarFormularioMandato: function () {

        var frmrepresentante = {
            nombre_rol: $("#nombre_rol").val(),
            apellido_p_rol: $("#apellido_p_rol").val(),
            apellido_m_rol: $("#apellido_m_rol").val(),
            rut_rol: $("#rut_rol").val(),
            cargo_rol: $("#cargo_rol").val(),
            email_rol: $("#email_rol").val(),
            email_r_rol: $("#email_r_rol").val(),
            movil_rol: $("#movil_rol").val(),
            fijo_rol: $("#fijo_rol").val(),
            direccion_rol: $("#direccion_rol").val(),
            numeracion_rol: $("#numeracion_rol").val(),
            comuna_rol: $("#comuna_rol").val(),
            region_rol: $("#region_rol").val(),
            nombre_representante: $("#nombre_representante").val(),
            apellido_p_representante: $("#apellido_p_representante").val(),
            apellido_m_representante: $("#apellido_m_representante").val(),
            rut_representante: $("#rut_representante").val(),
            cargo_representante: $("#cargo_representante").val(),
            email_representante: $("#email_representante").val(),
            email_r_representante: $("#email_r_representante").val(),
            movil_representante: $("#movil_representante").val(),
            fijo_representante: $("#fijo_representante").val(),
            direccion_representante: $("#direccion_representante").val(),
            numeracion_representante: $("#numeracion_representante").val(),
            comuna_representante: $("#comuna_representante").val(),
            region_representante: $("#region_representante").val(),
            bpmatrizfinanzas: $("#bpmatrizfinanzas").val(),
            rutempresa: $("#rut_empresa").val(),
            razonempresa: $("#razonsocial").val(),
            subsegmento: document.getElementById('SSegmento').value

            //aqui se debe pasar el segmento MAFD



        };
        $.ajax({
            type: "POST",
            url: " /Home/AddFormularioRepresentante",
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            data: JSON.stringify(frmrepresentante),
            success: function (response) {},
            failure: function (response) {
                console.log(response);
            },
            error: function (response) {
                console.log(response);
            }
        }); 

    }, GetComunaFilter(id,value) {

        $("#" + id).children('option:not(:first)').remove();
        $.ajax({
            type: "GET",
            url: " /Home/GetComunaFilter",
            data: { codcomuna: value },
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            async: true,
            success: function (response) {
                var arr_from_json = JSON.parse(response.comunalist);
                for (var x = 0; x < arr_from_json.length; x++) {

                    $("#" + id).append($('<option>', {
                        value: arr_from_json[x].cod_comuna,
                        text: arr_from_json[x].nom_comuna
                    }));
                    
                }
            },
            failure: function (response) {
                console.log(response);
            },
            error: function (response) {
                console.log(response);
            }
        });
    }

};

function nextTab(elem) {
    $(elem).next().find('a[data-toggle="tab"]').click();
}
function prevTab(elem) {
    $(elem).prev().find('a[data-toggle="tab"]').click();
}