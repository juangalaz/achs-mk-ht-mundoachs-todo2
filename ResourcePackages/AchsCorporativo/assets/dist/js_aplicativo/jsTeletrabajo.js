﻿      
            $('#file_XLSXData').on('change', function () {
                var file = $('#file_XLSXData').val()
                var filename = $('#file_XLSXData').val().replace(/C:\\fakepath\\/i, '')
                var extension = file.substr((file.lastIndexOf('.') + 1));

                if (extension == 'xls' || extension == 'xlsx') {

                    $('#nombrefile').val(filename)
                } else {
                    alert('Debe seleccionar un archivo excel');
                    $('#file_XLSXData').val("")
                    $('#nombrefile').val("")
                }

            });
           
            function guardarformulario() {
                if ($("#txtRutEmpresa").val().length > 0 && $('#txt_RutRepresentante').val().length > 0 && $('#nombrefile').val().length > 0 && $('#txt_Cargo').val().length > 0) {
                var objeto =
                {
                    strRutEmpresa: $("#txtRutEmpresa").val(),
                    strNombreEmpresa: $('#txt_RazonSocial').val(),
                    rutRepresentante: $('#txt_RutRepresentante').val(),
                    nombreRepresentante: $('#txt_NombreRepresentante').val(),
                    apellidoRepresentante: $('#txt_ApellidoRepresentante').val(),
                    strRutEmpleado: $('#txt_RUTContacto').val(),
                    strNombreEmpleado: $('#txt_NombreContacto').val(),
                    strApellidEmpleado: $('#txt_ApellidosContacto').val(),
                    strCargoEmpleado: $('#txt_Cargo').val(),
                    strRegion: $('#ddl_Region').val(),
                    strComuna: $('#ddl_ComunasChile').val(),
                    strCelular: $('#txt_Celular').val(),
                    strTelefono: $('#txt_FonoFijo').val(),
                    strDireccion: $('#txt_Direccion').val(),
                    strRutaArchivo: $('#nombrefile').val(),
                    strEmail: $('#txt_EmailContacto').val()

                }
                $.ajax({
                    type: "POST",
                    url: "https://api.achs.lfi.cl/api/circularteletrabajo/envioformulario",
                    data: JSON.stringify(objeto),
                    contentType: "application/json; charset=utf-8",
                    crossDomain: true,
                    dataType: 'json',
                    success: function (response) {
                        alert("Se ingresó con exito");
                    },
                    failure: function (response) {
                        console.log(response);
                    },
                    error: function (response) {
                        console.log(response);
                    }
                }); 

                    var files = $("#file_XLSXData").get(0).files;
                    var data = new FormData();
                    for (i = 0; i < files.length; i++) {
                        data.append("file" + i, files[i]);
                    }
                    $.ajax({
                       
                        url: "https://api.achs.lfi.cl/api/circularteletrabajo/post",
                        type: "POST",
                        contentType: false,
                        processData: false,
                        data: data,
                        success: function (result) {
                            if (result) {

                                $("#file_XLSXData").val('');
                                location.reload();
                            }
                        }, failure: function (response) {
                            console.log(response);
                        },
                        error: function (response) {
                            console.log(response);
                        }
                    });


                } else {
                    alert('Debe llenar los campos obligatorios')
                    if ($("#txtRutEmpresa").val().length == 0) {
                        $("#txtRutEmpresa").focus();

                    }
                    if ($("#txt_RutRepresentante").val().length == 0 && $("#txtRutEmpresa").val().length > 0) {
                        $("#txt_RutRepresentante").focus();

                    }
                    if ($("#nombrefile").val().length == 0) {
                        $("#nombrefile").focus();

                    }
                }


            }
               

            
     
            var loadingClass = "loadingGifTextBox";
            var successClass = "successIconTextBox";
            var failClass = "failIconTextBox";

            var msjERREmpresaNoAfiliada = "#errorEmpresaNoAfiliada";
            var msjEmpresaAfiliada = "#empresaAfiliada";
            var msjADVUsuarioNoRL = "#advertenciaUsuarioNoRL";
            var msjUsuarioEsRL = "#usuarioEsRL";

            var arrayDataConsultada = [];

            var checkedOrNotOut;

            $(document).ready(function () {



                //Para ver si volvió del postback, y esconder la sección
                var pageIsPostback = 'False';
                pageIsPostback = pageIsPostback.toLowerCase() == 'true' ? true : false;


            });

