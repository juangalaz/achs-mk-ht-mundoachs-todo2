﻿var vlambito;
var otrocontacto=false;
$(document).ready(function(){
$(".radio").each(function() { var $this = $(this); $this.html($this.html().replace(/&nbsp;/g, '')); });
$('input[type=text]').on('keydown', function(e) {
    if (e.which == 13) {
        e.preventDefault();
    }
});

$("#phone").inputmask({"mask": "+569 99999999"});	
$("#phoneotro").inputmask({"mask": "+569 99999999"});

$("#rut_empresa").rut(		

			{
				minimumLength: 7,useThousandsSeparator : false,
				formatOn: 'keyup'
				}
				)
		.on('rutInvalido', function(e) {		
	   	     $(this).parent().addClass("has-feedback has-error");
	   	     $(this).val('');
	   	     $("#mensajeerrorrut").html('<span style="color:#A94442;font-size:85%;font:status-bar">El Rut ingresado no es válido</span>');	   	    
			})	
		.on('rutValido', function(){ 
	        $.when(spbxmodulorequerimiento.getInformacionEmpresa($("#rut_empresa").val())).done(function (d) {
		         $(this).parent().removeClass("has-feedback has-error");
		         $(this).parent().addClass("has-feedback has-success");
		         $("#mensajeerrorrut").html('');
		    }).fail(function () {
		          $(this).parent().addClass("has-feedback has-error");

		    });
	    
});


$("#inlineCheckbox1").click(function(){
   
   if($(this).prop("checked")===true){
      $("#otrocontacto").removeClass('hidden');
      $("#pasodetalle").html("5");
      otrocontacto=true;
   
   }else{
         $("#otrocontacto").addClass('hidden');
         $("#pasodetalle").html("4");
         otrocontacto=false;

   }
});
 $.when(spbxmodulorequerimiento.getCargos()).done(function (d) {
     

 }).fail(function () {

 });
 
 
$("#enviarequerimientomovil").click(function(e){

	var datos = getinputsform();
    //ajaxfx(true,"https://ges.achs.cl",datos,true,null);
    //ajaxfx(true,"https://req.achs.cl",datos,true,null);



	

   if(otrocontacto==false){
   
   

       var validator = $('.formulario').data('bootstrapValidator');
       validator.validate();
       var detallerequerimiento=$("#detallerequerimiento").val();
       
        if(validator.isValid() && detallerequerimiento!=="" && detallerequerimiento.length>=10){
             if($("input[name='ambitoRadios01']").is(":checked")==true || $("input[name='ambitoRadios02']").is(":checked")==true || $("input[name='ambitoRadios03']").is(":checked")==true || $("input[name='ambitoRadios04']").is(":checked")==true){
                 $.when(spbxmodulorequerimiento.enviorequerimiento()).done(function (d) {
			          $(".gracias").removeClass('hidden');
				      $(".contenido").addClass('hidden');
		
				    }).fail(function () {
				          $("#validacion3").removeClass('hidden');
					      $(".contenido").addClass('hidden');
		
				
				    });
             
             }
             
            
        }else{
           
        }
     }else{
     
         var validator = $('#formularioprincipal').data('bootstrapValidator');
         var validator2 = $('#formulariootrocontacto').data('bootstrapValidator');
         validator.validate();
         validator2.validate();
         var detallerequerimiento=$("#detallerequerimiento").val();

        if(validator.isValid() && validator2.isValid() && detallerequerimiento!==""  && detallerequerimiento.length>=10){
             $.when(spbxmodulorequerimiento.enviorequerimiento()).done(function (d) {
		          $(".gracias").removeClass('hidden');
			      $(".contenido").addClass('hidden');

		    }).fail(function () {
		          $("#validacion3").removeClass('hidden');
			      $(".contenido").addClass('hidden');

		
		    });
        }

     
     
     
     }

}) 
	
	var getinputsform = function(contextid) {
        if (!contextid) { contextid = ""; }
        var inputs = "",
            select = "",
            todo = "";
			
		$((contextid == "" ? "" : "#" + contextid + " ") + "input").each(function(idx, elm) {
            if ($(this).attr("type") != "radio" && $(this).attr("id") != "file") {
                inputs += (inputs != "" ? ',' : '') + '"' + $(this).attr("id") + '":"' + ($(this).attr("xtype") == "number" ? $(this).val() : $(this).val()) + '"';
            }
        });
		inputs += ',"requerimiento":"'+$('input[name="requerimiento"]:checked').parent().text().trim()+'",';
		inputs += '"ambito":"'+$('input[name="ambitoRadio"]:checked').parent().find('span.tipo').text().trim()+'",';
		inputs += '"item_requerimiento":"'+	$('input[name="ambitoRadios'+$('input[name="ambitoRadio"]:checked').val()+'"]:checked').parent().text().trim()+'",';
		inputs += $('input[id="inlineCheckbox1"]').is(':checked')?'"chk_deseo":"Si"':'"chk_deseo":"No"';
		
        $((contextid == "" ? "" : "#" + contextid + " ") + "select").each(function(idx, elm) {
            select += (select != "" ? ',' : '') + '"' + $(this).attr("id") + '":"' + $(this).find('option:selected').val() + '"';
        });

        $((contextid == "" ? "" : "#" + contextid + " ") + "textarea").each(function(idx, elm) {
            inputs += (inputs != "" ? ',' : '') + '"' + $(this).attr("id") + '":"' + (htmlEntities($(this).val().replace(/\n/g,' '))) + '"';
        });
        //todo = '{' + '"acc":"' + $("#acc").val() + '",' + inputs + select + "}";
        todo = '{'  + inputs + ','+select +"}";
        
        console.log(todo);

        var rs = JSON.parse(todo);

        return rs;
    }
    
    var ajaxfx = function(ispost, urlpage, data , async, fn) {
        var result = null;
        $.ajax({
            type: (ispost ? "POST" : "GET"),
            url: urlpage,
            data: data,
            dataType: "json",
            async: async,
            success: function(response) {
                if (fn != null) {
                    fn(response);
                } else {

                    result = response;
                }
            },
            error: function(msg) {
                console.log("ERROR:", msg.responseText, JSON.stringify(msg), JSON.stringify(data));
            }
        });

        return result;
    };
$("#enviarequerimiento").click(function(e){

  
    //try
    //{
	   // var datos = getinputsform();
	   // //ajaxfx(true,"https://ges.achs.cl",datos,true,null);
	   // ajaxfx(true,"https://req.achs.cl",datos,true,null);
    //}
    //catch(e)
    //{
    //	console.log(e);
    //}




	if(otrocontacto==false){
	
	
	  
	
	

       var validator = $('.formulario').data('bootstrapValidator');
       validator.validate();
       var detallerequerimiento=$("#detallerequerimiento").val();
       	
        if(validator.isValid() && detallerequerimiento!=="" && detallerequerimiento.length>=10){
             if($("input[name='ambitoRadios01']").is(":checked")==true || $("input[name='ambitoRadios02']").is(":checked")==true || $("input[name='ambitoRadios03']").is(":checked")==true || $("input[name='ambitoRadios04']").is(":checked")==true){
                 $.when(spbxmodulorequerimiento.enviorequerimiento()).done(function (d) {
			          $(".gracias").removeClass('hidden');
				      $(".contenido").addClass('hidden');
		
				    }).fail(function () {
				          $("#validacion3").removeClass('hidden');
					      $(".contenido").addClass('hidden');
		
				
				    });
             
             }
             else
             {
             	alert('Debe seleccionar las opciones que se muestran en el formulario');
             }
             
            
        }
        else
        {
        	alert('No ha sido posible almacenar la información. Verifique el detalle del requerimiento o alguna otra información faltante.');
        }
     }else{
     
         var validator = $('#formularioprincipal').data('bootstrapValidator');
         var validator2 = $('#formulariootrocontacto').data('bootstrapValidator');
         validator.validate();
         validator2.validate();
         var detallerequerimiento=$("#detallerequerimiento").val();

        if(validator.isValid() && validator2.isValid() && detallerequerimiento!==""  && detallerequerimiento.length>=10){
             $.when(spbxmodulorequerimiento.enviorequerimiento()).done(function (d) {
		          $(".gracias").removeClass('hidden');
			      $(".contenido").addClass('hidden');

		    }).fail(function () {
		          $("#validacion3").removeClass('hidden');
			      $(".contenido").addClass('hidden');

		
		    });
        }

     
     
     
     }
    e.preventDefault();
});
  
     
     $("input[name='ambitoRadios01']").click(function(){ 
          $("input[type=radio]").attr('disabled', false);
	      $(".formulario :input[id='rut_empresa']").removeAttr("readonly");
	      $(".formulario").removeClass('bloqueo');
	      $(".opcion1").removeClass("disabled");
	      $('input[name="ambitoRadio"]')[0].checked = true;
	       vlambito="01";


     });
       $("input[name='ambitoRadios02']").click(function(){
          $("input[type=radio]").attr('disabled', false);
	      $(".formulario :input[id='rut_empresa']").removeAttr("readonly");
	      $(".formulario").removeClass('bloqueo');
	      $(".opcion2").removeClass("disabled");
	      $('input[name="ambitoRadio"]')[1].checked = true;
          vlambito="02";


     });
     $("input[name='ambitoRadios03']").click(function(){
          $("input[type=radio]").attr('disabled', false);
	      $(".formulario :input[id='rut_empresa']").removeAttr("readonly");

	      $(".formulario").removeClass('bloqueo');
	       $(".opcion3").removeClass("disabled");
	        $('input[name="ambitoRadio"]')[2].checked = true;
           vlambito="03";


     });
     $("input[name='ambitoRadios04']").click(function(){
          $("input[type=radio]").attr('disabled', false);
	      $(".formulario :input[id='rut_empresa']").removeAttr("readonly");

	      $(".formulario").removeClass('bloqueo');
	      $(".opcion4").removeClass("disabled");
	      $('input[name="ambitoRadio"]')[3].checked = true;
          vlambito="04";


     });


      $("input[name='ambitoRadios01']").change(function(){
	       $("input[name='ambitoRadios02']").prop("checked",false);
	       $("input[name='ambitoRadios03']").prop("checked",false);
	       $("input[name='ambitoRadios04']").prop("checked",false);


	       console.log('ambitoRadios01');
	       $("#prevencion").addClass("activo");
	       $(".opcion1").removeClass("disabled");
	       $("#salud").removeClass("activo");
	       $(".opcion2").addClass("disabled");
	       $("#economica").removeClass("activo");
           $(".opcion3").addClass("disabled");
           $("#procesods67").removeClass("activo");
           $(".opcion4").addClass("disabled");

            $(".formulario :input[id='rut_empresa']").removeAttr("readonly");

	       $(".formulario").removeClass('bloqueo');
            vlambito="01";


     });
       $("input[name='ambitoRadios02']").change(function(){
           $("input[name='ambitoRadios01']").prop("checked",false);

	       $("input[name='ambitoRadios03']").prop("checked",false);
	       $("#prevencion").removeClass("activo");
           $(".opcion1").addClass("disabled");
	       $("#salud").addClass("activo");
	       $(".opcion2").removeClass("disabled");
	       $("#economica").removeClass("activo");
           $(".opcion3").addClass("disabled");
           $("#procesods67").removeClass("activo");
           $(".opcion4").addClass("disabled");

            $(".formulario :input[id='rut_empresa']").removeAttr("readonly");

	       $(".formulario").removeClass('bloqueo');
           vlambito="02";


     });
       $("input[name='ambitoRadios03']").change(function(){
           $("input[name='ambitoRadios01']").prop("checked",false);
	       $("input[name='ambitoRadios02']").prop("checked",false);
           $("input[name='ambitoRadios04']").prop("checked",false);

	       
	        $("#prevencion").removeClass("activo");
	        $(".opcion1").addClass("disabled");
	        $("#salud").removeClass("activo");
	        $(".opcion2").addClass("disabled");
	        $("#economica").addClass("activo");
	        $(".opcion3").removeClass("disabled");
	        $("#procesods67").removeClass("activo");
            $(".opcion4").addClass("disabled");

	        $(".formulario :input[id='rut_empresa']").removeAttr("readonly");

	       $(".formulario").removeClass('bloqueo');
           vlambito="03";


     });
     $("input[name='ambitoRadios04']").change(function(){
           $("input[name='ambitoRadios01']").prop("checked",false);
	       $("input[name='ambitoRadios02']").prop("checked",false);
           $("input[name='ambitoRadios03']").prop("checked",false);
	       
	        $("#prevencion").removeClass("activo");
	        $(".opcion1").addClass("disabled");
	        $("#salud").removeClass("activo");
	        $(".opcion2").addClass("disabled");
	        $("#economica").removeClass("activo");
	        $(".opcion3").addClass("disabled");
	        $("#procesods67").addClass("activo");
            $(".opcion4").removeClass("disabled");

	        $(".formulario :input[id='rut_empresa']").removeAttr("readonly");

	       $(".formulario").removeClass('bloqueo');
           vlambito="04";


     });




	 $("input[name='ambitoRadio']").change(function(){
	      $("input[type=radio]").attr('disabled', false);
	    
	  
	       $("input[name='ambitoRadios01']").prop("checked",false);
	       $("input[name='ambitoRadios02']").prop("checked",false);
	       $("input[name='ambitoRadios03']").prop("checked",false);
	       $("input[name='ambitoRadios04']").prop("checked",false);

	     if($(this).val()==="01"){
	       $("#prevencion").addClass("activo");
	       $(".opcion1").removeClass("disabled");
	       $("#salud").removeClass("activo");
	       $(".opcion2").addClass("disabled");
	       $("#economica").removeClass("activo");
           $(".opcion3").addClass("disabled");
           $("#procesods67").removeClass("activo");
           $(".opcion4").addClass("disabled");

           $(".formulario :input[id='rut_empresa']").removeAttr("readonly");
            vlambito="01";

	     }else if($(this).val()==="02"){
           $("#prevencion").removeClass("activo");
           $(".opcion1").addClass("disabled");
	       $("#salud").addClass("activo");
	       $(".opcion2").removeClass("disabled");
	       $("#economica").removeClass("activo");
           $(".opcion3").addClass("disabled");
            $("#procesods67").removeClass("activo");
           $(".opcion4").addClass("disabled");


           $(".formulario :input[id='rut_empresa']").removeAttr("readonly");
	        ambito="02";

	     }else if($(this).val()==="03"){
	       $("#prevencion").removeClass("activo");
	        $(".opcion1").addClass("disabled");
	       $("#salud").removeClass("activo");
	        $(".opcion2").addClass("disabled");
	       $("#economica").addClass("activo");
	       $(".opcion3").removeClass("disabled");
	        $("#procesods67").removeClass("activo");
           $(".opcion4").addClass("disabled");


           $(".formulario :input[id='rut_empresa']").removeAttr("readonly");
            vlambito="03";

	     }else if($(this).val()==="04"){
	        $("#prevencion").removeClass("activo");
	        $(".opcion1").addClass("disabled");
	        $("#salud").removeClass("activo");
	        $(".opcion2").addClass("disabled");
	        $("#economica").removeClass("activo");
	        $(".opcion3").addClass("disabled");
	        $("#procesods67").addClass("activo");
            $(".opcion4").removeClass("disabled");


            $(".formulario :input[id='rut_empresa']").removeAttr("readonly");
            vlambito="04";

	     }
	     
	 });
	 ValidateForm();
	 ValidateForm2();
     $("#detallerequerimiento").on("keyup",function(event){
	  checkTextAreaMaxLength(this,event);
	});
});
/*
Checks the MaxLength of the Textarea
-----------------------------------------------------
@prerequisite:	textBox = textarea dom element
				e = textarea event
                length = Max length of characters
*/
function checkTextAreaMaxLength(textBox, e) { 
    
    var maxLength = parseInt($(textBox).data("length"));
    
  
    if (!checkSpecialKeys(e)) { 
        if (textBox.value.length > maxLength - 1) textBox.value = textBox.value.substring(0, maxLength); 
   } 
  $(".char-count").html(maxLength - textBox.value.length);
    
    return true; 
} 

function htmlEntities(str) {
      return String(str).replace('&ntilde;', 'ñ')
                        .replace('&Ntilde;', 'Ñ')
                        .replace('&amp;', '&')
                        .replace('&Ntilde;', 'Ñ')
                        .replace('&ntilde;', 'ñ')
                        .replace('&Ntilde;', 'Ñ')
                        .replace('&Agrave;', 'À')
                        .replace('&Aacute;', 'Á')  
                        .replace('&Acirc;','Â')
                        .replace('&Atilde;','Ã')   
                        .replace('&Auml;','Ä')
                        .replace('&Aring;','Å')
                        .replace('&AElig;','Æ')
                        .replace('&Ccedil;','Ç')
                        .replace('&Egrave;','È')
                        .replace('&Eacute;','É')
                        .replace('&Ecirc;', 'Ê')
                        .replace('&Euml;','Ë')
                        .replace(   '&Igrave;', 'Ì')
                        .replace('&Iacute;', 'Í'    )
                        .replace('&Icirc;', 'Î' )
                        .replace(   '&Iuml;', 'Ï')
                        .replace(   '&ETH;', 'Ð')
                        .replace(   '&Ntilde;', 'Ñ')
                        .replace(   '&Ograve;', 'Ò')
                        .replace(   '&Oacute;', 'Ó')
                        .replace('&Ocirc;', 'Ô' )
                        .replace(   '&Otilde;', 'Õ')
                        .replace('&Ouml;', 'Ö'  )
                        .replace('&Oslash;', 'Ø'    )
                        .replace(   '&Ugrave;' ,'Ù')
                        .replace(   '&Uacute;', 'Ú')
                        .replace(   '&Ucirc;', 'Û')
                        .replace(   '&Uuml;', 'Ü')
                        .replace(   '&Yacute;', 'Ý')
                        .replace('&THORN;', 'Þ' )
                        .replace(   '&szlig;', 'ß')
                        .replace(   '&agrave;', 'à')
                        .replace(   '&aacute;', 'á')
                        .replace(   '&acirc;', 'â')
                        .replace(   '&atilde;', 'ã')
                        .replace('&auml;', 'ä'  )
                        .replace(   '&aring;', 'å')
                        .replace(   '&aelig;', 'æ')
                        .replace(   '&ccedil;', 'ç')
                        .replace('&egrave;', 'è'    )
                        .replace('&eacute;', 'é'    )
                        .replace('&ecirc;', 'ê' )
                        .replace('&euml;', 'ë'  )
                        .replace(   '&igrave;', 'ì')
                        .replace('&iacute;', 'í'    )
                        .replace('&icirc;', 'î' )
                        .replace('&iuml;', 'ï'  )
                        .replace('&eth;', 'ð'   )
                        .replace(   '&ntilde;', 'ñ')
                        .replace('&ograve;','ò')
                        .replace('&oacute;','ó')
                        .replace('&ocirc;','ô')
                        .replace('&otilde;','õ')
                        .replace('&ouml;','ö')
                        .replace('&oslash;','ø')
                        .replace('&ugrave;','ù')
                        .replace('&uacute;','ú')
                        .replace('&ucirc;','û')
                        .replace('&uuml;' , 'ü')   
                        .replace('&yacute;', 'ý')  
                        .replace('&thorn;', 'þ')
                        .replace('&yuml;', 'ÿ');
    }



/*
Checks if the keyCode pressed is inside special chars
-------------------------------------------------------
@prerequisite:	e = e.keyCode object for the key pressed
*/
function checkSpecialKeys(e) { 
    if (e.keyCode != 8 && e.keyCode != 46 && e.keyCode != 37 && e.keyCode != 38 && e.keyCode != 39 && e.keyCode != 40) 
        return false; 
    else 
        return true; 
}

function ValidateForm2() {
    $('[id*=formulariootrocontacto]').bootstrapValidator({
                feedbackIcons: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    nombre_complotro: {
                        validators: {
                            notEmpty: {
                                message: 'Debes completar la información'
                            }
                        }
                    },emailotro: {
                        validators: {
                            notEmpty: {
                                message: 'Debes completar la información'
                            }, 
                            regexp: {
					           regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
					          message: 'El correo no es valido'
					        }
                        }
                    },phoneotro: {
                        validators: {
                            notEmpty: {
                                message: 'Debes completar la información'
                            },stringLength: {
		                      
		                        message: 'Debe completar la información'
		                    }
                        }
                    },cargootro: {
                        validators: {
                            notEmpty: {
                                message: 'Debes completar la información'
                            }
                        }
                    }

                    
                 }   
            });

}


function ValidateForm() {
            $('[id*=formularioprincipal]').bootstrapValidator({
                feedbackIcons: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                     rut_empresa:{
                          validators: {
                            notEmpty: {
                                message:'' //'Debes completar la información de RUT EMPRESA'
                            },
                           /* regexp:{
                              regexp: '[0-9]{7,8}-[0-9Kk]{1}',
					          message: 'RUT ingresado no es válido' //Mensaje "RUN no tiene formato correcto" cambiado por "RUT ingresado no es válido" - Tarea Jira 141

                            }*/
                        }

                     },nombre_compl: {
                        validators: {
                            notEmpty: {
                                message: 'Debes completar la información'
                            }
                        }
                    },email: {
                        validators: {
                            notEmpty: {
                                message: 'Debes completar la información'
                            }, 
                            regexp: {
					           regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
					          message: 'El correo electrónico ingresado no es válido' //Mensaje "El correo no es válido" cambiado por El correo electrónico ingresado no es válido" - Tarea Jira 141
					        }
                        }
                    },phone: {
                        validators: {
                            notEmpty: {
                                message: 'Debes completar la información'
                            }

                        }
                    },cargo: {
                        validators: {
                            notEmpty: {
                                message: 'Debes completar la información'
                            }
                        }
                    }
                 }   
            });
 }           

var spbxmodulorequerimiento={
   getInformacionEmpresa:function(rutEmpresa){
             $("#rut_empresa").addClass("loading");

 		    $.ajax({  
		                    type: "GET",  
                    url: "/empresas/modulo-de-requerimiento/ValidateEmpresaACHS?run=" + $("#rut_empresa").val().replace(/\./g, "").toUpperCase(),  
		                    contentType: "application/json; charset=utf-8",  
		                    crossDomain: true,
		   					dataType: 'json',
		   					async:true,
                    success: function (response) { 
                        var arr_from_json = JSON.parse(response);
	   				           if(response.length>0){
                                           $("#razonsocial").val(arr_from_json[0].RAZON_SOCIAL);
                                           $("#segmento").val(arr_from_json[0].SUBSEGMENTO);
                                           $("#comuna").val(arr_from_json[0].COMUNA_DESC)
			   				        $("#rut_empresa").removeClass("loading");
                                    $("#rut_empresa").parent().addClass("has-feedback has-success");
                                    $(".formulario :input").removeAttr("readonly");

			   				        
							    }else{
							     $("#validacion3").removeClass('hidden');
							     $(".contenido").addClass('hidden');
							    }		
								
					        				
		                    },  
		                    failure: function (response) {  
		                       console.log(response);	
		                    },  
		                    error: function (response) {  
		                      console.log(response);	
		                    }  
		                }); 
    },getCargos:function(){
               
                $("#cargo").append($('<option>', {
                    value: "",
                    text: "Seleccione cargo"
                }));
                $("#cargootro").append($('<option>', {
                    value: "",
                    text: "Seleccione cargo"
                }));

                
        	    $.ajax({  
		                    type: "GET",  
                            url: "https://api.achs.lfi.cl/api/modulorequerimiento/getcargos",  
		                    contentType: "application/json; charset=utf-8",  
		                    crossDomain: true,
		   					dataType: 'json', 
		   				    success: function (response) { 
		   				       if(response.length>0){
		   				       var arr_from_json = JSON.parse(response)
			   				        for(var x=0;x<arr_from_json.length;x++){
			   				             $("#cargo").append($('<option>', {
						                      value: arr_from_json[x].codigocargo,
						                      text: arr_from_json[x].cargo
						                  }));
						                  $("#cargootro").append($('<option>', {
						                      value: arr_from_json[x].codigocargo,
						                      text: arr_from_json[x].cargo
						                  }));

			   				        }

							    }else{
							     
							    }		
								
					        				
		                    },  
		                    failure: function (response) {  
		                       console.log(response);	
		                    },  
		                    error: function (response) {  
		                      console.log(response);	
		                    }  
		                }); 
 
    
    },enviorequerimiento:function(){
        var frmrequerimiento = {
           ambito:vlambito,
           tiporequerimiento:$("input[name='ambitoRadios"+vlambito+"']:checked").val(),
           rutempresa:$("#rut_empresa").val(),
           RazonSocial:$("#razonsocial").val(),
           segmento:$("#segmento").val(),
           comuna:$("#comuna").val(),
           NombreCompletoSolicitante:$("#nombre_compl").val(),
           CargoSolicitante:$("#cargo :selected").text(),
           Cargo:$("#cargootro :selected").text(),
           EmailSolicitante:$("#email").val(),
           TelefonoSolicitante:$("#phone").val(),
           NombreCompleto:$("#nombre_complotro").val(),
           Telefono:$("#phoneotro").val(),
           Email:$("#emailotro").val(),
           DetalleRequerimiento:$("#detallerequerimiento").val(),
           tipogestion:$("input[name='requerimiento']:checked").val(),
        };
        
        insertar_sf();
         $.ajax({
            type: "POST",
            //url: "https://wa-qa-mtdigital-achsvirtual.azurewebsites.net/api/modulorequerimiento/EnvioRequerimiento",
           //  url:"https://api.achs.lfi.cl/api/modulorequerimiento/EnvioRequerimiento",
            	url:"https://api.achs.lfi.cl/api/modulorequerimiento/EnvioRequerimientoSengrid",			
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            crossDomain: true,
            data: JSON.stringify(frmrequerimiento),
            success: function (response) {
                console.log(response);
                $(".gracias").removeClass('hidden');
				$(".contenido").addClass('hidden');

            },
            failure: function (response) {
                console.log(response);
            },
            error: function (response) {
                console.log(response);
            }
        }); 
    }
}


 $("#otrocontacto").addClass('hidden');
 
  function mayuscula(e){
    e.value = e.value.toUpperCase();
    }
       

 /* jQuery.rut.js */
!function(a){function c(a){return a.replace(/[\.\-]/g,"")}function d(a,b){var c=j(a),d=c[0],e=c[1];if(!d||!e)return d||a;for(var f="",g=b?".":"";d.length>3;)f=g+d.substr(d.length-3)+f,d=d.substring(0,d.length-3);return d+f+"-"+e}function e(a){return a.type&&a.type.match(/^key(up|down|press)/)&&(8===a.keyCode||16===a.keyCode||17===a.keyCode||18===a.keyCode||20===a.keyCode||27===a.keyCode||37===a.keyCode||38===a.keyCode||39===a.keyCode||40===a.keyCode||91===a.keyCode)}function f(a,d){if("string"!=typeof a)return!1;var e=c(a);if("boolean"==typeof d.minimumLength){if(d.minimumLength&&e.length<b.minimumLength)return!1}else{var f=parseInt(d.minimumLength,10);if(e.length<f)return!1}var h=e.charAt(e.length-1).toUpperCase(),i=parseInt(e.substr(0,e.length-1));return!isNaN(i)&&g(i).toString().toUpperCase()===h}function g(a){var b=0,c=2;if("number"==typeof a){a=a.toString();for(var d=a.length-1;d>=0;d--)b+=a.charAt(d)*c,c=(c+1)%8||2;switch(b%11){case 1:return"k";case 0:return 0;default:return 11-b%11}}}function h(a,b){a.val(d(a.val(),b))}function i(a){f(a.val(),a.opts)?a.trigger("rutValido",j(a.val())):a.trigger("rutInvalido")}function j(a){var b=c(a);if(0===b.length)return[null,null];if(1===b.length)return[b,null];var d=b.charAt(b.length-1),e=b.substring(0,b.length-1);return[e,d]}var b={validateOn:"blur",formatOn:"blur",ignoreControlKeys:!0,useThousandsSeparator:!0,minimumLength:2},k={init:function(c){if(this.length>1)for(var d=0;d<this.length;d++)console.log(this[d]),a(this[d]).rut(c);else{var f=this;f.opts=a.extend({},b,c),f.opts.formatOn&&f.on(f.opts.formatOn,function(a){f.opts.ignoreControlKeys&&e(a)||h(f,f.opts.useThousandsSeparator)}),f.opts.validateOn&&f.on(f.opts.validateOn,function(){i(f)})}return this}};a.fn.rut=function(b){return k[b]?k[b].apply(this,Array.prototype.slice.call(arguments,1)):"object"!=typeof b&&b?void a.error("El método "+b+" no existe en jQuery.rut"):k.init.apply(this,arguments)},a.formatRut=function(a,b){return void 0===b&&(b=!0),d(a,b)},a.computeDv=function(a){var b=c(a);return g(parseInt(b,10))},a.validateRut=function(b,c,d){if(d=d||{},f(b,d)){var e=j(b);return a.isFunction(c)&&c(e[0],e[1]),!0}return!1}}(jQuery);




function insertar_sf() {
	let cargo = '';
	
	if($("#cargootro").val()!='') {
		cargo = $("#cargootro :selected").text();
	}

     var frmrequerimiento = {
           ambito:vlambito,
           tiporequerimiento:$("input[name='ambitoRadios"+vlambito+"']:checked").val(),
           rutempresa:$("#rut_empresa").val(),
           RazonSocial:$("#razonsocial").val(),
           segmento:$("#segmento").val(),
           comuna:$("#comuna").val(),
           NombreCompletoSolicitante:$("#nombre_compl").val(),
           CargoSolicitante:$("#cargo :selected").text(),
           Cargo:cargo,
           EmailSolicitante:$("#email").val(),
           TelefonoSolicitante:$("#phone").val(),
           NombreCompleto:$("#nombre_complotro").val(),
           Telefono:$("#phoneotro").val(),
           Email:$("#emailotro").val(),
           DetalleRequerimiento:$("#detallerequerimiento").val(),
           tipogestion:$("input[name='requerimiento']:checked").val(),
        };
        
         $.ajax({
            type: "POST",
             url:"/empresas/modulo-de-requerimiento/insertar_sf",
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            crossDomain: true,
            data: JSON.stringify(frmrequerimiento),
            success: function (response) {
               

            },
            failure: function (response) {
                console.log(response);
            },
            error: function (response) {
                console.log(response);
            }
        }); 
    }



function insertar_sf_test() {
        var frmrequerimiento = {
           ambito:"vlambito",
           tiporequerimiento:"tiporequerimiento",
           rutempresa:"rutempresa",
           RazonSocial:"RazonSocial",
           segmento:"segmento",
           comuna:"comuna",
           NombreCompletoSolicitante:"NombreCompletoSolicitante",
           CargoSolicitante:"CargoSolicitante",
           Cargo:"Cargo",
           EmailSolicitante:"EmailSolicitante",
           TelefonoSolicitante:"TelefonoSolicitante",
           NombreCompleto:"NombreCompleto",
           Telefono:"Telefono",
           Email:"Email",
           DetalleRequerimiento:"DetalleRequerimiento",
           tipogestion:"tipogestion",
        };
        
         $.ajax({
            type: "POST",
             url:"insertar_sf",
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            crossDomain: true,
            data: JSON.stringify(frmrequerimiento),
            success: function (response) {
               

            },
            failure: function (response) {
                console.log(response);
            },
            error: function (response) {
                console.log(response);
            }
        }); 
    }

// A $( document ).ready() block.
$( document ).ready(function() {
    console.log( "ready!" );
    console.log($("#phoneotro").val());
    //insertar_sf_test();
});









