﻿$('#txtRutEmpresa').on('change', function () {
    var rut = $('#txtRutEmpresa').val();
    var error = 0;
    var valor = rut.split('.').join('');
    valor = valor.replace('-', '');
    cuerpo = valor.slice(0, -1);
    dv = valor.slice(-1).toUpperCase();
    rut.value = cuerpo + '-' + dv;
    if (cuerpo.length < 7) { error = 1; }
    suma = 0;
    multiplo = 2;
    for (i = 1; i <= cuerpo.length; i++) {
        index = multiplo * valor.charAt(cuerpo.length - i);
        suma = suma + index;
        if (multiplo < 7) { multiplo = multiplo + 1; } else { multiplo = 2; }
    }
    dvEsperado = 11 - (suma % 11);
    dv = (dv == 'K') ? 10 : dv;
    dv = (dv == 0) ? 11 : dv;
    if (dvEsperado != dv) { error = 1; }
    if (error == 1) {
        $('#txtRutEmpresa').val('');
        $('#txtRutEmpresa').focus();

        return false;
    } else {
        if (dv == 10) {
            $('#txtRutEmpresa').val((cuerpo + '-' + 'K').trim())
      
    
        } else if (dv == 11) {
            $('#txtRutEmpresa').val((cuerpo + '-' + '0').trim())
        
        } else {
            $('#txtRutEmpresa').val((cuerpo + '-' + dv).trim())
            
        }

        ValidoR() 
        $('#imagenLoading').css("display", "block")
        return true;
    }
});
$('#txt_RutRepresentante').on('change', function () {
    var rut = $('#txt_RutRepresentante').val();
    var error = 0;
    var valor = rut.split('.').join('');
    valor = valor.replace('-', '');
    cuerpo = valor.slice(0, -1);
    dv = valor.slice(-1).toUpperCase();
    rut.value = cuerpo + '-' + dv;
    if (cuerpo.length < 7) { error = 1; }
    suma = 0;
    multiplo = 2;
    for (i = 1; i <= cuerpo.length; i++) {
        index = multiplo * valor.charAt(cuerpo.length - i);
        suma = suma + index;
        if (multiplo < 7) { multiplo = multiplo + 1; } else { multiplo = 2; }
    }
    dvEsperado = 11 - (suma % 11);
    dv = (dv == 'K') ? 10 : dv;
    dv = (dv == 0) ? 11 : dv;
    if (dvEsperado != dv) { error = 1; }
    if (error == 1) {
        $('#txt_RutRepresentante').val('');
        $('#txt_RutRepresentante').focus();

        return false;
    } else {
        if (dv == 10) {
            $('#txt_RutRepresentante').val((cuerpo + '-' + 'K').trim())


        } else if (dv == 11) {
            $('#txt_RutRepresentante').val((cuerpo + '-' + '0').trim())

        } else {
            $('#txt_RutRepresentante').val((cuerpo + '-' + dv).trim())

        } $('#imagenLoading1').css("display", "block")
        RepresentanteLegal($("#txtRutEmpresa").val())
  
        return true;
    }
});

$('#txt_RUTContacto').on('change', function () {
    var rut = $('#txt_RUTContacto').val();
    var error = 0;
    var valor = rut.split('.').join('');
    valor = valor.replace('-', '');
    cuerpo = valor.slice(0, -1);
    dv = valor.slice(-1).toUpperCase();
    rut.value = cuerpo + '-' + dv;
    if (cuerpo.length < 7) { error = 1; }
    suma = 0;
    multiplo = 2;
    for (i = 1; i <= cuerpo.length; i++) {
        index = multiplo * valor.charAt(cuerpo.length - i);
        suma = suma + index;
        if (multiplo < 7) { multiplo = multiplo + 1; } else { multiplo = 2; }
    }
    dvEsperado = 11 - (suma % 11);
    dv = (dv == 'K') ? 10 : dv;
    dv = (dv == 0) ? 11 : dv;
    if (dvEsperado != dv) { error = 1; }
    if (error == 1) {
        $('#txt_RUTContacto').val('');
        $('#txt_RUTContacto').focus();

        return false;
    } else {
        if (dv == 10) {
            $('#txt_RUTContacto').val((cuerpo + '-' + 'K').trim())


        } else if (dv == 11) {
            $('#txt_RUTContacto').val((cuerpo + '-' + '0').trim())

        } else {
            $('#txt_RUTContacto').val((cuerpo + '-' + dv).trim())

        }


        $('#imagenLoading3').css("display", "block")
        RepresentanteLegal3($("#txtRutEmpresa").val())
        return true;
    }
});
function ValidoR() {
    validoR = document.getElementById('rutOk');
    document.getElementById('rutOk').style.color = "black";
    validoR.innerText = "Verificando";

    $.ajax({
        type: "GET",
        url: "/teletrabajo/validacion_Empresa_Achs?run=" + $("#txtRutEmpresa").val().replace(/\./g, "").toUpperCase(),
        contentType: "application/json",
        crossDomain: true,
        dataType: 'json',
        async: true,
        success: function (response) {
            
            if (response != '0') {
                $('#imagenLoading').css("display", "none")
                $('#empresaAfiliada').css("display", "block")
                $('#errorEmpresaNoAfiliada').css("display", "none");
                var nombre = response.split('|');
                var nombreEmpresa = nombre[0];
                $('#txt_RazonSocial').val(nombreEmpresa);
                validoR.innerText = "";
                $('#txt_RutRepresentante').removeAttr("readonly", "readonly");
                $('#txt_RUTContacto').removeAttr("readonly", "readonly");
                
              
            } else {           
                $('#imagenLoading').css("display", "none")
                $('#errorEmpresaNoAfiliada').css("display", "block");
                $('#empresaAfiliada').css("display", "none")
                validoR.innerText = "";
          
            }
        },
        failure: function (response) { console.log(response); },
        error: function (response) { console.log(response); }
    });

}

function RepresentanteLegal(run) {
    validoR = document.getElementById('rutOk1');
    document.getElementById('rutOk1').style.color = "black";
    validoR.innerText = "Verificando";
    var datoscompletado = false;
 
    $.ajax({
        type: "GET",
        url: " /teletrabajo/ValidateEmpresaACHS",
        data: { run: run },
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        async: true,
        success: function (response) {
            var arr_from_json = JSON.parse(response);
            for (var i = 0; i < arr_from_json.length; i++) {
                validoR.innerText = "";
                $('#imagenLoading1').css("display", "none")
                if (arr_from_json[i].RUT_CONTACTO == $('#txt_RutRepresentante').val()) {
                    if (arr_from_json[i].TIPO_CONTACTO == 'REPRESENTANTE_LEGAL') {
                        //$('#divRutContacto').hide();
                        //$('#divNombrecontacto').hide();
                        //$('#divAppContacto').hide();
                        $('#usuarioEsRL').css("display", "block")
                        $('#advertenciaUsuarioNoRL').css("display", "none");
                        $('#txt_NombreRepresentante').val(arr_from_json[i].NOMBRES_CONTACTO)
                        $('#txt_ApellidoRepresentante').val(arr_from_json[i].APELLIDO_PATERNO_CONTACTO + ' ' + arr_from_json[i].APELLIDO_MATERNO_CONTACTO);
                        $('#checkInd').removeClass('hidden');
                    } else {
                        $('#usuarioEsRL').css("display", "none")
                        $('#advertenciaUsuarioNoRL').css("display", "block");
                    }
          
                    //$('#txt_Cargo').val(arr_from_json[i].TIPO_CONTACTO);
                    //$('#txt_Celular').val(arr_from_json[i].TELEFONO_CONTACTO);
                    //$('#txt_FonoFijo').val(arr_from_json[i].TELEFONO_EMPRESA);
                    //$('#txt_EmailContacto').val(arr_from_json[i].EMAIL_CONTACTO);
                    //$('#ddl_Region').val(arr_from_json[i].REGION);
                    //var rest = arr_from_json[i].COMUNA;
                    //var comuna = String(rest.substring(7));
                 
                    //$('#ddl_ComunasChile').val(comuna);
                    //$('#txt_Direccion').val(arr_from_json[i].NOMBRE_FANTASIA);
                  
                }
            }

        },
        failure: function (response) { console.log(response); },
        error: function (response) { console.log(response); }
    });

}
function RepresentanteLegal2(run) {
    validoR = document.getElementById('obtenerDatos');
    document.getElementById('obtenerDatos').style.color = "black";
    validoR.innerText = "Obteniendo datos...";
    var datoscompletado = false;

    $.ajax({
        type: "GET",
        url: " /teletrabajo/ValidateEmpresaACHS",
        data: { run: run },
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        async: true,
        success: function (response) {
            var arr_from_json = JSON.parse(response);
            for (var i = 0; i < arr_from_json.length; i++) {
                validoR.innerText = "";
                $('#imagenLoading1').css("display", "none")
                if (arr_from_json[i].RUT_CONTACTO == $('#txt_RutRepresentante').val()) {
                    if (arr_from_json[i].TIPO_CONTACTO == 'REPRESENTANTE_LEGAL') {
                        $('#divRutContacto').hide();
                        $('#divNombrecontacto').hide();
                        $('#divAppContacto').hide();

                        $('#usuarioEsRL').css("display", "block")
                        $('#advertenciaUsuarioNoRL').css("display", "none");
                        $('#txt_NombreRepresentante').val(arr_from_json[i].NOMBRES_CONTACTO)
                        $('#txt_ApellidoRepresentante').val(arr_from_json[i].APELLIDO_PATERNO_CONTACTO + ' ' + arr_from_json[i].APELLIDO_MATERNO_CONTACTO);
                        $('#txt_Cargo').val(arr_from_json[i].TIPO_CONTACTO);
                        $('#txt_Celular').val(arr_from_json[i].TELEFONO_CONTACTO);
                        $('#txt_FonoFijo').val(arr_from_json[i].TELEFONO_EMPRESA);
                        $('#txt_EmailContacto').val(arr_from_json[i].EMAIL_CONTACTO);
                        $('#ddl_Region').val(arr_from_json[i].REGION);
                        var rest = arr_from_json[i].COMUNA;
                        var comuna = String(rest.substring(7));

                        $('#ddl_ComunasChile').val(comuna);
                        $('#txt_Direccion').val(arr_from_json[i].NOMBRE_FANTASIA);
                        validoR.innerText = "";
                        $('#imagenLoading2').css("display", "none")
                    } else {
                        $('#usuarioEsRL').css("display", "none")
                        $('#advertenciaUsuarioNoRL').css("display", "block");
                    }
                  

                }
            }

        },
        failure: function (response) { console.log(response); },
        error: function (response) { console.log(response); }
    });

}
function RepresentanteLegal3(run) {
    validoR = document.getElementById('RutOk3');
    document.getElementById('RutOk3').style.color = "black";
    validoR.innerText = "Obteniendo datos...";
    var datoscompletado = false;

    $.ajax({
        type: "GET",
        url: " /teletrabajo/ValidateEmpresaACHS",
        data: { run: run },
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        async: true,
        success: function (response) {
            var arr_from_json = JSON.parse(response);
            for (var i = 0; i < arr_from_json.length; i++) {
                validoR.innerText = "";
                $('#imagenLoading3').css("display", "none")
                if (arr_from_json[i].RUT_CONTACTO == $('#txt_RUTContacto').val()) {
                    if (arr_from_json[i].TIPO_CONTACTO != 'REPRESENTANTE_LEGAL') {
                 
                        $('#txt_NombreContacto').val(arr_from_json[i].NOMBRES_CONTACTO)
                        $('#txt_ApellidosContacto').val(arr_from_json[i].APELLIDO_PATERNO_CONTACTO + ' ' + arr_from_json[i].APELLIDO_MATERNO_CONTACTO);
                        $('#txt_Cargo').val(arr_from_json[i].TIPO_CONTACTO);
                        $('#txt_Celular').val(arr_from_json[i].TELEFONO_CONTACTO);
                        $('#txt_FonoFijo').val(arr_from_json[i].TELEFONO_EMPRESA);
                        $('#txt_EmailContacto').val(arr_from_json[i].EMAIL_CONTACTO);
                        $('#ddl_Region').val(arr_from_json[i].REGION);
                        var rest = arr_from_json[i].COMUNA;
                        var comuna = String(rest.substring(7));

                        $('#ddl_ComunasChile').val(comuna);
                        $('#txt_Direccion').val(arr_from_json[i].NOMBRE_FANTASIA);

                        validoR.innerText = "";
                        $('#imagenLoading3').css("display", "none")
                    } else {
                     
                    }
                 

                }
            }

        },
        failure: function (response) { console.log(response); },
        error: function (response) { console.log(response); }
    });

}
document.getElementById('chk_IsRepresentanteLegal').addEventListener('input', function () {


    var checkeado = document.getElementById("chk_IsRepresentanteLegal").checked;
 
        if (checkeado == true) {
            $('#divRutContacto').hide();
            $('#divNombrecontacto').hide();
            $('#divAppContacto').hide();
            $('#imagenLoading2').css("display", "block")
            RepresentanteLegal2($("#txtRutEmpresa").val())

        } else {

            $('#divRutContacto').show();
            $('#divNombrecontacto').show();
            $('#divAppContacto').show();
        }
    }
    
  
);