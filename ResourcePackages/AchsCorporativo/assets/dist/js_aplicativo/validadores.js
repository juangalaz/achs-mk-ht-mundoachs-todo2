﻿var rutempresavalido = false;
var arrListadoRepresentantes = [];
var rutrepresentanteselct = "";
var heleido;
var listadocargadorepresentante = false;

$('#rut_empresa').on('change', function () {
    var rut = $('#rut_empresa').val();
    var error = 0;
    var valor = rut.split('.').join('');
    valor = valor.replace('-', '');
    cuerpo = valor.slice(0, -1);
    dv = valor.slice(-1).toUpperCase();
    rut.value = cuerpo + '-' + dv;
    if (cuerpo.length < 7) { error = 1; }
    suma = 0;
    multiplo = 2;
    for (i = 1; i <= cuerpo.length; i++) {
        index = multiplo * valor.charAt(cuerpo.length - i);
        suma = suma + index;
        if (multiplo < 7) { multiplo = multiplo + 1; } else { multiplo = 2; }
    }
    dvEsperado = 11 - (suma % 11);
    dv = (dv == 'K') ? 10 : dv;
    dv = (dv == 0) ? 11 : dv;
    if (dvEsperado != dv) { error = 1; }
    if (error == 1) {
        $('#rut_empresa').val('');
        $('#rut_empresa').focus();

        return false;
    } else {
        if (dv == 10) {
            $('#rut_empresa').val((cuerpo + '-' + 'K').trim())


        } else if (dv == 11) {
            $('#rut_empresa').val((cuerpo + '-' + '0').trim())

        } else {
            $('#rut_empresa').val((cuerpo + '-' + dv).trim())

        }

        RepresentanteLegal2($('#rut_empresa').val())
        return true;
    }
});
$(".next-step").click(function (e) {
    var codigo = $(this).attr("id");
    if ($(this).attr("id") !== "nextrutempresa") {
        var $active = $('.nav-tabs li.active');
        if (ValidacionDatos(codigo)) {
            $active.next().removeClass('disabled');
            nextTab($active);
            $active.find("a").addClass("check");
        }

    }

});
function ValidacionDatos(idvalidar) {
  

    switch (idvalidar) {
        case "bienvenida":
            if ($("#rut_empresa").val() === "" || rutempresavalido === false) {
           
                $("#rut_empresa").addClass("error");
                $(".mensajeusuario").html("Rut inválido / Esta empresa no se encuentra en nuestra base de datos").addClass("mensajeerror");
                setTimeout(function () { $("#rut_empresa").val('').removeClass("error"); $(".mensajeusuario").html('Ingrese el RUT de su Empresa, debe ser sin puntos y con guión.').removeClass("mensajeerror"); }, 3000);

            } else {
               
        
            }
            break;

        case "datos_contacto":

            if (listadocargadorepresentante === true) {
                $.when(getRutSeleccionado()).done(function (d) {
                    $("#rut_empresa").removeClass("loading");
           

                }).fail(function () {

                });
            } else {
                if ($("#rut_empresa").val() === "" || rutempresavalido === false) {
             
              
                    $("#rut_empresa").addClass("error");
                    $(".mensajeusuario").html("Rut inválido / Esta empresa no se encuentra en nuestra base de datos").addClass("mensajeerror");
                    setTimeout(function () { $("#rut_empresa").val('').removeClass("error"); $(".mensajeusuario").html('Ingrese el RUT de su Empresa, debe ser sin puntos y con guión.').removeClass("mensajeerror"); }, 3000);

                }
            }
            break;

        case "equifax":

            if ($("#rut_empresa").val() === "" || rutempresavalido === false) {

                $("#rut_empresa").addClass("error");
                $(".mensajeusuario").html("Rut inválido / Esta empresa no se encuentra en nuestra base de datos").addClass("mensajeerror");
                setTimeout(function () { $("#rut_empresa").val('').removeClass("error"); $(".mensajeusuario").html('Ingrese el RUT de su Empresa, debe ser sin puntos y con guión.').removeClass("mensajeerror"); }, 3000);

            }
            for (var x = 0; x < arrListadoRepresentantes.length; x++) {

                if ((arrListadoRepresentantes[x].TIPO_CONTACTO === "REPRESENTANTE_LEGAL" || arrListadoRepresentantes[x].TIPO_CONTACTO === "COMERCIAL" || arrListadoRepresentantes[x].TIPO_CONTACTO === "PREVENTIVO") && rutrepresentanteselct === arrListadoRepresentantes[x].RUT_CONTACTO) {

                    $("#nombre_representante").val(arrListadoRepresentantes[x].NOMBRES_CONTACTO).prop("readonly", true);
                    $("#apellido_p_representante").val(arrListadoRepresentantes[x].APELLIDO_PATERNO_CONTACTO).prop("readonly", true);
                    $("#apellido_m_representante").val(arrListadoRepresentantes[x].APELLIDO_MATERNO_CONTACTO).prop("readonly", true);
                    $("#rut_representante").val(arrListadoRepresentantes[x].RUT_CONTACTO).prop("readonly", true);

                }
            }
  
            break;
        case "nombramiento":
            if ($("#rut_empresa").val() === "" || rutempresavalido === false) {
                $("#rut_empresa").addClass("error");
                $(".mensajeusuario").html("Rut inválido / Esta empresa no se encuentra en nuestra base de datos").addClass("mensajeerror");
                setTimeout(function () { $("#rut_empresa").val('').removeClass("error"); $(".mensajeusuario").html('Ingrese el RUT de su Empresa, debe ser sin puntos y con guión.').removeClass("mensajeerror"); }, 3000);

            }
            break;

    }


 
}
$('#nextrutempresa').on('click', function () {
    $("#divbienvenida1").addClass('hidden d-none');
    $("#divbienvenida").removeClass('hidden d-none');
});

//TRABAJO CON LOS TABS
$("#navValidarUser").addClass("disabled");
$("#navMandatoForm").addClass("disabled");
$("#navDatoUsuario").addClass("disabled");

$('#bienvenida').on('click', function () {
    if (arrListadoRepresentantes.length>0) {
         $("#navDatoEmpresa").addClass("disabled");
        $("#divbienvenida").addClass('hidden d-none');
        $("#divempresa").removeClass('hidden d-none');
            //nav tab
        $("#navDatoEmpresa").removeClass("active");
        $("#navDatoUsuario").addClass("active");
    } else {
        alert('No se encontraron empleados asociados a la empresa.')
    }

});
$('a[data-toggle="tooltip"]').tooltip({
    animated: 'fade',
    placement: 'right',
    html: true
});

$('#datos_contacto').on('click', function () {
    var rutSeleccionado = getRutSeleccionado();
    if (rutSeleccionado != undefined) {
        $("#divempresa").addClass('hidden d-none');
        $("#myModal").removeClass('hidden d-none');
        $("#divValidarDatos").removeClass('hidden d-none');
        $("#elegirOtro").addClass('hidden d-none');

        //nav tab
        $("#navDatoUsuario").removeClass("active");
        $("#navValidarUser").addClass("active");

        getRegion();
        GetListaCargos();
    }else {
        alert('Debe seleccionar un representante.')
    }

});

$('#btnSalirValidacion2').on('click', function () {
    $("#validacion2").addClass('hidden d-none');
    $("#elegirOtro").removeClass('hidden d-none');
    $("#serieequifax").val('');
});
$('#elegirOtro').on('click', function () {
    $("#divempresa").removeClass('hidden d-none');
    $("#divValidarDatos").addClass('hidden d-none');
    $('#serieequifax').val('');

});

$('#llenarMandato').on('click', function () {
    $("#divValidarDatos").addClass('hidden d-none');
    $('#mandatoform').removeClass('hidden d-none');
    $('#validacion').addClass('hidden d-none')
    //tab
    $("#navValidarUser").removeClass("active");
    $("#navMandatoForm").addClass("active");
})

function RepresentanteLegal2(run) {
    $('#divValidando').css("display", "block");
    $.ajax({
        type: "GET",
        url: " /empresas/mandato-digital/ValidateEmpresaACHS",
        data: { run: run },
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        async: true,
        success: function (response) {
            var arr_from_json = JSON.parse(response);

            $("#nombreempresa").html(arr_from_json[0].RAZON_SOCIAL);
            $("#razonsocial").val(arr_from_json[0].RAZON_SOCIAL);
            $("#rutempresa").html(": " + arr_from_json[0].RUT_EMPRESA);
            $("#bpmatrizfinanzas").val(arr_from_json[0].BP_MATRIZ_FINANZAS);
            $('#listadocontacto > tr').remove();
            for (var x = 0; x < arr_from_json.length; x++) {
                if (arr_from_json[x].SUBSEGMENTO === "GCC" || arr_from_json[x].SUBSEGMENTO === "GC" || arr_from_json[x].SUBSEGMENTO === "GCR" || arr_from_json[x].SUBSEGMENTO === "GCN") {
                if (arr_from_json[x].RUT_CONTACTO !== null) {
                    if (arr_from_json[x].TIPO_CONTACTO === "REPRESENTANTE_LEGAL" || arr_from_json[x].TIPO_CONTACTO === "COMERCIAL" || arr_from_json[x].TIPO_CONTACTO === "PREVENTIVO") {
                        $('#listadocontacto > tbody:last').after('<tr><td><input type="radio" name="radio" id="' + arr_from_json[x].RUT_CONTACTO + '" value="radio" ><label for="radio">' + arr_from_json[x].NOMBRE_COMPLETO_CONTACTO.replace(/Señor/g, '').replace(/Señora/g, '') + '</label></td><td>' + arr_from_json[x].RUT_CONTACTO + '</td></tr>');
                        arrListadoRepresentantes.push(arr_from_json[x]);
                        rutempresavalido = true;
                        $('#divValidando').css("display", "none");
                       $("#bienvenida").removeClass('hidden d-none');
                    }
                    }
                }
            }  


            
        },
        failure: function (response) { console.log(response); },
        error: function (response) { console.log(response); }
    });

}

$('#btnEnviaRespuestas').on('click', function () {
    $('#divValidando3').removeClass('hidden d-none');
    EnvioRespuestas();
});

function getRutSeleccionado() {
    var manageradiorel = $("input:radio[name ='radio']:checked").attr("id");
    $('#rut_empleado').val(manageradiorel);
    rutrepresentanteselct = manageradiorel;
    return manageradiorel;
}
$('#equifax').on('click', function () {
    if ($('#serieequifax').val().trim() != "") {
        $('#divValidando2').removeClass('hidden d-none')
        ValidacionEquifax();
    } else {
        alert('Debe ingresar el número de documento o número de seríe.')
    }


});

function GetAlgo(run) {

    $.ajax({
        type: "GET",
        url: " /empresas/mandato-digital/validarUsuario",
        data: { run: run },
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        async: true,
        success: function (response) {
           
          
        },
        failure: function (response) { console.log(response); },
        error: function (response) { console.log(response); }
    });

}

function ValidacionEquifax() {
    var run = $("#rut_empleado").val().trim();
    var serie = $("#serieequifax").val().trim();
    $("#listadopregunasequifax").html('');

    $.ajax
        ({
            type: 'POST',
            url: '/empresas/mandato-digital/EsValidoDocumento',
            dataType: 'json',
            data: { run: run.replace(/\./g, ""), serie: serie },
            beforeSend: function () { },
            complete: function () { },
            success: function (data) {
                resultado = data;
                if (resultado.autoriza === 1) {
                    var arrpreguntas = JSON.parse(resultado.estructura);
                    var htmlpreguntas = "";
                    $("#tk").val(arrpreguntas.transactionKey);
                    for (f = 0; f < arrpreguntas.interactiveQuery.question.length; f++) {
                        htmlpreguntas += "<div class='mod_preg'>";


                        htmlpreguntas += "<h3>" + arrpreguntas.interactiveQuery.question[f].questionText + "</h3>";
                        htmlpreguntas += " <div class='row'>";
                        for (d = 0; d < arrpreguntas.interactiveQuery.question[f].answerChoice.length; d++) {
                            if (d < 4) {
                                htmlpreguntas += " <div class='col-md-2'>";
                            } else {
                                htmlpreguntas += " <div class='col-md-3' style='text-align:center;'>";
                            }
                            htmlpreguntas += "<input type='radio' name='P" + f + "' value='" + arrpreguntas.interactiveQuery.question[f].answerChoice[d].answerId + "'>" + arrpreguntas.interactiveQuery.question[f].answerChoice[d].value + " ";
                            htmlpreguntas += " </div>";


                        }
                        htmlpreguntas += "<div class='clearfix'></div>";
                        htmlpreguntas += "</div>";
                        htmlpreguntas += "</div>";

                    }
                    $("#listadopregunasequifax").html(htmlpreguntas);
                    $(".equifaxpaso1").addClass('hidden d-none');
                    //$("#validacion").removeClass('hidden d-none');
                    $("#equifax2").removeClass('hidden d-none');
                    $("#validacion2").addClass('hidden d-none');
                    $("#divValidando2").addClass('hidden d-none');
                    $("#divValidarDatos").addClass('hidden d-none');
                 
                } else {
                    $("#divValidando2").addClass('hidden d-none');
                    $(".equifaxpaso1").addClass('hidden d-none');
                    $("#validacion").addClass('hidden d-none');
                    $("#equifax2").addClass('hidden d-none');
                    $(".menjaseequifaxvalidacion").html(resultado.msj);
                    $("#validacion2").removeClass('hidden d-none');
                }
            },
            error: function (ex) {
                tr = jQuery.parseJSON(response.responseText);

            }
        })
}
$("#heleido").click(function () {

    if ($(this).prop("checked") === true) {
        //$("#formulariototalenvio").removeClass('hidden d-none');
        heleido = true;
        $("#formulariototalenvio :input").removeAttr("readonly").removeClass("readOnly");
        $("#nombre_representante").prop("readonly", true);
        $("#apellido_p_representante").prop("readonly", true);
        $("#apellido_m_representante").prop("readonly", true);
        $("#rut_representante").prop("readonly", true);


    } else {
        //$("#formulariototalenvio").addClass('hidden d-none');
        $("#formulariototalenvio :input").attr("readonly", "readonly").addClass("readOnly");
        $("#nombre_representante").prop("readonly", true);
        $("#apellido_p_representante").prop("readonly", true);
        $("#apellido_m_representante").prop("readonly", true);
        $("#rut_representante").prop("readonly", true);
        heleido = false;
    }
});
$("#region_rol").change(function () {
    GetComunaFilter("comuna_rol", $(this).val());
});
$("#region_representante").change(function () {
    GetComunaFilter("comuna_representante", $(this).val());
});
function getRegion() {
    $.ajax({
        type: "GET",
        url: "https://api.achs.lfi.cl/api/mandatodigital/GetRegiones",
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        success: function (response) {
            var arr_from_json = JSON.parse(response.regionlist);

            $("#region_rol").append($('<option>', {
                value: "",
                text: "Seleccione región"
            }));
            $("#comuna_rol").append($('<option>', {
                value: "",
                text: "Seleccione comuna"
            }));
            $("#region_representante").append($('<option>', {
                value: "",
                text: "Seleccione región"
            }));
            $("#comuna_representante").append($('<option>', {
                value: "",
                text: "Seleccione comuna"
            }));

            for (var x = 0; x < arr_from_json.length; x++) {

                $("#region_rol").append($('<option>', {
                    value: arr_from_json[x].cod_region,
                    text: arr_from_json[x].nom_region
                }));
                $("#region_representante").append($('<option>', {
                    value: arr_from_json[x].cod_region,
                    text: arr_from_json[x].nom_region
                }));
            }



        },
        failure: function (response) {
            console.log(response);
        },
        error: function (response) {
            console.log(response);
        }
    });


}
function GetComunaFilter(id, value) {

    $("#" + id).children('option:not(:first)').remove();
    $.ajax({
        type: "GET",
        url: " https://api.achs.lfi.cl/api/mandatodigital/GetComunaFilter",
        data: { codcomuna: value },
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        async: true,
        success: function (response) {
            var arr_from_json = JSON.parse(response.comunalist);
            for (var x = 0; x < arr_from_json.length; x++) {

                $("#" + id).append($('<option>', {
                    value: arr_from_json[x].cod_comuna,
                    text: arr_from_json[x].nom_comuna
                }));

            }
        },
        failure: function (response) {
            console.log(response);
        },
        error: function (response) {
            console.log(response);
        }
    });
}
function  GetListaCargos() {
    $.ajax({
        type: "GET",
        url: "https://api.achs.lfi.cl/api/mandatodigital/GetCargos",
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        success: function (response) {
            var arr_from_json = JSON.parse(response);

            $("#cargo_rol").append($('<option>', {
                value: "",
                text: "Seleccione cargo"
            }));
            $("#cargo_representante").append($('<option>', {
                value: "",
                text: "Seleccione cargo"
            }));
            for (var x = 0; x < arr_from_json.length; x++) {

                $("#cargo_rol").append($('<option>', {
                    value: arr_from_json[x].codigocargo,
                    text: arr_from_json[x].cargo
                }));
                $("#cargo_representante").append($('<option>', {
                    value: arr_from_json[x].codigocargo,
                    text: arr_from_json[x].cargo
                }));
            }



        },
        failure: function (response) {
            console.log(response);
        },
        error: function (response) {
            console.log(response);
        }
    });

}
function EnvioRespuestas() {

    var respuestas = {
        p1: $("input[name='P0']:checked").val(),
        p2: $("input[name='P1']:checked").val(),
        p3: $("input[name='P2']:checked").val(),
        p4: $("input[name='P3']:checked").val(),
        tk: $("#tk").val()
    };

    $.ajax
        ({
            type: 'POST',
            url: '/empresas/mandato-digital/EnvioRespuesta',
            dataType: 'json',
            data: { objrespuesta: respuestas },
            beforeSend: function () { },
            complete: function () { },
            success: function (data) {
                if (data.esvalido === 1) {
                    $(".equifaxpaso1").addClass('hidden d-none');
                    $("#validacion").removeClass('hidden d-none');
                    $("#equifax2").addClass('hidden d-none');
                    $('#divValidando3').addClass('hidden d-none');
                } else {
                    $('#divValidando3').addClass('hidden d-none');
                    $(".equifaxpaso1").addClass('hidden d-none');
                    $("#validacion").addClass('hidden d-none');
                    $("#validacion2").removeClass('hidden d-none');

                }
            },
            error: function (ex) {
                tr = jQuery.parseJSON(response.responseText);
                $('#divValidando3').addClass('hidden d-none');
                $(".equifaxpaso1").addClass('hidden d-none');
                $("#validacion").addClass('hidden d-none');
                $("#validacion2").removeClass('hidden d-none');
            }
        });

}

///////////////////INGRESO DEL FORMULARIO A LA BASE DE DATOS
function EnviarFormularioMandato()  {

    var frmrepresentante = {
        nombre_rol: $("#nombre_rol").val(),
        apellido_p_rol: $("#apellido_p_rol").val(),
        apellido_m_rol: $("#apellido_m_rol").val(),
        rut_rol: $("#rut_rol").val(),
        cargo_rol: $("#cargo_rol").val(),
        email_rol: $("#email_rol").val(),
        email_r_rol: $("#email_r_rol").val(),
        movil_rol: $("#movil_rol").val(),
        fijo_rol: $("#fijo_rol").val(),
        direccion_rol: $("#direccion_rol").val(),
        numeracion_rol: $("#numeracion_rol").val(),
        comuna_rol: $("#comuna_rol").val(),
        region_rol: $("#region_rol").val(),
        nombre_representante: $("#nombre_representante").val(),
        apellido_p_representante: $("#apellido_p_representante").val(),
        apellido_m_representante: $("#apellido_m_representante").val(),
        rut_representante: $("#rut_representante").val(),
        cargo_representante: $("#cargo_representante").val(),
        email_representante: $("#email_representante").val(),
        email_r_representante: $("#email_r_representante").val(),
        movil_representante: $("#movil_representante").val(),
        fijo_representante: $("#fijo_representante").val(),
        direccion_representante: $("#direccion_representante").val(),
        numeracion_representante: $("#numeracion_representante").val(),
        comuna_representante: $("#comuna_representante").val(),
        region_representante: $("#region_representante").val(),
        bpmatrizfinanzas: $("#bpmatrizfinanzas").val(),
        rutempresa: $("#rut_empresa").val(),
        razonempresa: $("#razonsocial").val(),
        subsegmento: document.getElementById('SSegmento').value

        //aqui se debe pasar el segmento MAFD



    };

    insertar_sf();
    $.ajax({
        type: "POST",
        url: "https://api.achs.lfi.cl/api/mandatodigital/AddFormularioRepresentante",
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        data: JSON.stringify(frmrepresentante),
        success: function (response) { },
        failure: function (response) {
            console.log(response);
        },
        error: function (response) {
            console.log(response);
        }
    });

}
ValidateForm();
function ValidateForm() {

    $('[id*=formulariototalenvio]').bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            nombre_rol: {
                validators: {
                    notEmpty: {
                        message: 'Debes completar la información'
                    }
                }
            },
            apellido_p_rol: {
                validators: {
                    notEmpty: {
                        message: 'Debes completar la información'
                    }
                }
            },
            apellido_m_rol: {
                validators: {
                    notEmpty: {
                        message: 'Debes completar la información'
                    }
                }
            },
            rut_rol: {
                validators: {
                    notEmpty: {
                        message: 'Debes completar la información'
                    }
                }
            },
            movil_rol: {
                validators: {
                    notEmpty: {
                        message: 'Debes completar la información'
                    }
                }
            }, direccion_rol: {
                validators: {
                    notEmpty: {
                        message: 'Debes completar la información'
                    }
                }
            }, numeracion_rol: {
                validators: {
                    notEmpty: {
                        message: 'Debes completar la información'
                    }
                }
            }, comuna_rol: {
                validators: {
                    notEmpty: {
                        message: 'Debes completar la información'
                    }
                }
            }, region_rol: {
                validators: {
                    notEmpty: {
                        message: 'Debes completar la información'
                    }
                }
            }, email_rol: {
                validators: {
                    notEmpty: {
                        message: 'Debes completar la información'
                    }, regexp: {
                        regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                        message: 'El correo no es valido'
                    }
                }
            }, email_r_rol: {
                validators: {
                    notEmpty: {
                        message: 'Debes completar la información'
                    }, regexp: {
                        regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                        message: 'El correo no es valido'
                    }, identical: {
                        field: 'email_rol',
                        message: 'Los correos ingresado no son iguales'
                    }
                }
            }, cargo_rol: {
                validators: {
                    notEmpty: {
                        message: 'Debes completar la información'
                    }
                }
            }, email_representante: {
                validators: {
                    notEmpty: {
                        message: 'Debes completar la información'
                    }, regexp: {
                        regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                        message: 'El correo no es valido'
                    }
                }
            }, email_r_representante: {
                validators: {
                    notEmpty: {
                        message: 'Debes completar la información'
                    }, regexp: {
                        regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                        message: 'El correo no es valido'
                    }, identical: {
                        field: 'email_representante',
                        message: 'Los correos ingresado no son iguales'
                    }
                }
            }, movil_representante: {
                validators: {
                    notEmpty: {
                        message: 'Debes completar la información'
                    }
                }
            }, direccion_representante: {
                validators: {
                    notEmpty: {
                        message: 'Debes completar la información'
                    }
                }
            }, numeracion_representante: {
                validators: {
                    notEmpty: {
                        message: 'Debes completar la información'
                    }
                }
            }, comuna_representante: {
                validators: {
                    notEmpty: {
                        message: 'Debes completar la información'
                    }
                }
            }, region_representante: {
                validators: {
                    notEmpty: {
                        message: 'Debes completar la información'
                    }
                }
            }

        }
    })
}


$(".btnenvioinformacion").click(function () {

    if (heleido === true) {
        var validator = $('#formulariototalenvio').data('bootstrapValidator');
        validator.validate();

        if (validator.isValid()) {
            $.when(EnviarFormularioMandato()).done(function (d) {
                $("#finalizado").removeClass("hidden d-none");
                $("#new_admin").addClass("hidden d-none");
                $(".titulomandado").addClass("hidden d-none");
            }).fail(function () {

            });
        }
    }
});

$(".finalizarmandato").click(function () {
    location.reload();
});













function insertar_sf_test()  {

    var frmrepresentante = {
        nombre_rol: 'nombre_rol',
        apellido_p_rol: 'apellido_p_rol',
        apellido_m_rol: 'apellido_m_rol',
        rut_rol: 'rut_rol',
        cargo_rol: 'cargo_rol',
        email_rol: 'email_rol',
        email_r_rol: 'email_r_rol',
        movil_rol: 'movil_rol',
        fijo_rol: 'fijo_rol',
        direccion_rol: 'direccion_rol',
        numeracion_rol: 'numeracion_rol',
        comuna_rol: 'comuna_rol',
        region_rol: 'region_rol',
        nombre_representante: 'nombre_representante',
        apellido_p_representante: 'apellido_p_representante',
        apellido_m_representante: 'apellido_m_representante',
        rut_representante: 'rut_representante',
        cargo_representante: 'cargo_representante',
        email_representante: 'email_representante',
        email_r_representante: 'email_r_representante',
        movil_representante: 'movil_representante',
        fijo_representante: 'fijo_representante',
        direccion_representante: 'direccion_representante',
        numeracion_representante: 'numeracion_representante',
        comuna_representante: 'comuna_representante',
        region_representante: 'region_representante',
        bpmatrizfinanzas: 'bpmatrizfinanzas',
        rutempresa: 'rut_empresa',
        razonempresa: 'razonsocial',
        subsegmento: 'SSegmento'
    };
        let json = JSON.stringify(frmrepresentante);
        console.log(json);

    $.ajax({
        type: "POST",
        url: "/empresas/mandato-digital/insertar_sf",
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        data: JSON.stringify(frmrepresentante),
        success: function (response) { },
        failure: function (response) {
            console.log(response);
        },
        error: function (response) {
            console.log(response);
        }
    });

}



function insertar_sf()  {

    var frmrepresentante = {
 nombre_rol: $("#nombre_rol").val(),
        apellido_p_rol: $("#apellido_p_rol").val(),
        apellido_m_rol: $("#apellido_m_rol").val(),
        rut_rol: $("#rut_rol").val(),
        cargo_rol: $("#cargo_rol").val(),
        email_rol: $("#email_rol").val(),
        email_r_rol: $("#email_r_rol").val(),
        movil_rol: $("#movil_rol").val(),
        fijo_rol: $("#fijo_rol").val(),
        direccion_rol: $("#direccion_rol").val(),
        numeracion_rol: $("#numeracion_rol").val(),
        comuna_rol: $("#comuna_rol").val(),
        region_rol: $("#region_rol").val(),
        nombre_representante: $("#nombre_representante").val(),
        apellido_p_representante: $("#apellido_p_representante").val(),
        apellido_m_representante: $("#apellido_m_representante").val(),
        rut_representante: $("#rut_representante").val(),
        cargo_representante: $("#cargo_representante").val(),
        email_representante: $("#email_representante").val(),
        email_r_representante: $("#email_r_representante").val(),
        movil_representante: $("#movil_representante").val(),
        fijo_representante: $("#fijo_representante").val(),
        direccion_representante: $("#direccion_representante").val(),
        numeracion_representante: $("#numeracion_representante").val(),
        comuna_representante: $("#comuna_representante").val(),
        region_representante: $("#region_representante").val(),
        bpmatrizfinanzas: $("#bpmatrizfinanzas").val(),
        rutempresa: $("#rut_empresa").val(),
        razonempresa: $("#razonsocial").val(),
        subsegmento: document.getElementById('SSegmento').value
    };
        

    $.ajax({
        type: "POST",
        url: "/empresas/mandato-digital/insertar_sf",
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        data: JSON.stringify(frmrepresentante),
        success: function (response) { },
        failure: function (response) {
            console.log(response);
        },
        error: function (response) {
            console.log(response);
        }
    });

}


// A $( document ).ready() block.
$( document ).ready(function() {
   
    //insertar_sf_test();
});
