//Función de botones Siguente/Volver/Hablemos



//identificador de botones Volver
let btnVolverIndependienteUno = document.getElementById('btn-volver-independiente-uno');
let btnVolverIndependienteDos = document.getElementById('btn-volver-independiente-dos');

let pasoMovil = document.getElementById('PasoMovil');
let IndependientePasoUno = document.getElementById('formulario-independiente-uno');
let IndependientePasoDos = document.getElementById('formulario-independiente-dos');

$(document).ready(function () {

    if ($(window).width() < 426) {

        pasoMovil.innerText = "";
        IndependientePasoUno.classList.add('d-none')
        IndependientePasoDos.classList.remove('d-none')
        $("#divMovil").removeClass("d-none")
        $("#btn-volver-independiente-uno").attr("value", "<");
        $("#btn-volver-independiente-dos").attr("value", "<");
    } else {

        pasoMovil.innerText = "Paso 2 de 2";
        IndependientePasoUno.classList.remove('d-none')
        IndependientePasoDos.classList.add('d-none')
        $("#divMovil").addClass("d-none")
        $("#btn-volver-independiente-uno").attr("value", "< Volver");
        $("#btn-volver-independiente-dos").attr("value", "< Volver");
    }

    $(window).resize(function () {
        if ($(window).width() < 426) {

            pasoMovil.innerText = "";
            IndependientePasoUno.classList.add('d-none')
            IndependientePasoDos.classList.remove('d-none')
            $("#divMovil").removeClass("d-none")
            $("#btn-volver-independiente-uno").attr("value", "<");
            $("#btn-volver-independiente-dos").attr("value", "<");
        } else {
          
            pasoMovil.innerText = "Paso 2 de 2";
            IndependientePasoUno.classList.remove('d-none')
            IndependientePasoDos.classList.add('d-none')
            $("#divMovil").addClass("d-none")
            $("#btn-volver-independiente-uno").attr("value", "< Volver");
            $("#btn-volver-independiente-dos").attr("value", "< Volver");
        }
    });

});

//Volver
btnVolverIndependienteUno.addEventListener('click', function(){
    //IndependientePasoUno.classList.add('d-none')
    window.parent.location.href = "https://www.achs.cl/portal/ACHS-Corporativo/Paginas/afiliate-aqui.aspx";
});

btnVolverIndependienteDos.addEventListener('click', function () {
    if ($(window).width() < 426) {
        window.parent.location.href = "https://www.achs.cl/portal/ACHS-Corporativo/Paginas/afiliate-aqui.aspx";
    } else {
        IndependientePasoDos.classList.add('d-none')
        IndependientePasoUno.classList.remove('d-none')
    }

});

