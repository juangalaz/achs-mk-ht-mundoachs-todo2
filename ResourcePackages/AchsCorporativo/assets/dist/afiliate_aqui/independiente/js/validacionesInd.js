﻿
document.getElementById('cboxFuncion').value = '0001';

$(document).ready(function () { $('input.disablecopypaste').bind('copy paste', function (e) { e.preventDefault(); }); });

$('#btn-siguiente-independiente').on('click', function () {
    var primerNombre = $('#fist_name').val().trim().length;
    var paterno = $('#last_name').val().trim().length;
    var telefono = $('#phone').val().trim().length;
    var rutcontactoafiInd = $('#rut_contacto_afiliate').val().trim().length;
    if (primerNombre < 3 || paterno < 3 || telefono < 3 || rutcontactoafiInd == 0) {
        if (rutcontactoafiInd == "") {
            $('#rut_contacto_afiliate').focus();
            $('#rut_contacto_afiliate').val("");
            $('#rut_contacto_afiliate').css("border", "1px solid red")
            $('#rut_contacto_afiliate').css("border-radius", "4px")
            $('#rut_contacto_afiliate').attr("placeholder", "Debe ingresar un RUT")
        } else {
            $('#rut_contacto_afiliate').css("border", "")
            $('#rut_contacto_afiliate').css("border-radius", "")
            $('#rut_contacto_afiliate').attr("placeholder", "")
        }
        if (primerNombre < 3) {

            $('#fist_name').focus();
            $('#fist_name').val("");
            $('#fist_name').css("border", "1px solid red")
            $('#fist_name').css("border-radius", "4px")
            $('#fist_name').attr("placeholder", "El nombre debe ser mayor o igual a 3 caracteres")

        } else {
            $('#fist_name').css("border", "")
            $('#fist_name').css("border-radius", "")
            $('#fist_name').attr("placeholder", "")
        }

        if (paterno < 3) {

            $('#last_name').focus();
            $('#last_name').val("");
            $('#last_name').css("border", "1px solid red")
            $('#last_name').css("border-radius", "4px")
            $('#last_name').attr("placeholder", "El apellido debe ser mayor o igual a 3 caracteres")

        } else {
            $('#last_name').css("border", "")
            $('#last_name').css("border-radius", "")
            $('#last_name').attr("placeholder", "")

        } 
        if (telefono !=9 || $('#phone').val().trim() == '000000000' || $('#phone').val().trim() == '111111111' || $('#phone').val().trim() == '222222222' || $('#phone').val().trim() == '333333333' || $('#phone').val().trim() == '444444444' || $('#phone').val().trim() == '555555555' || $('#phone').val().trim() == '666666666' || $('#phone').val().trim() == '777777777' || $('#phone').val().trim() == '888888888' || $('#phone').val().trim() == '999999999') {
            $('#phone').focus();
            $('#phone').val("");
            $('#phone').css("border", "1px solid red")
            $('#phone').css("border-radius", "4px")
            $('#phone').attr("placeholder", "El teléfono debe contener 9 números.")
        } else {
            $('#phone').css("border", "")
            $('#phone').css("border-radius", "")
            $('#phone').attr("placeholder", "")
        }
    } else {
        let IndependientePasoUno = document.getElementById('formulario-independiente-uno');
        let IndependientePasoDos = document.getElementById('formulario-independiente-dos');
        IndependientePasoUno.classList.add('d-none')
        IndependientePasoDos.classList.remove('d-none')
    }
});

$('#btnEnviar').on('click', function () {


    var email = $('#emailOK').html();
    var valorFuncionContacto = $('#cboxFuncion').val();
    var region = $('#region').val();
    var comuna = $('#comuna').val();

    var organismo = $('#organismo').val();
    var correo1 = $('#txtemail').val().trim().length;
    var correo2 = $('#txtConfirmarCorreo').val().trim().length;


    var RutMovil = $('#rut_contacto_afiliateMovil').val().trim().length;
    var NombreMovil = $('#fist_nameMovil').val().trim().length;
    var ApellidoMovil = $('#last_nameMovil').val().trim().length;
    var PhoneMovil = $('#phoneMovil').val().trim().length;


  

        if ($(window).width() < 426) {

            var nombre = $('#fist_nameMovil').val().trim() +' '+$('#last_nameMovil').val().trim();
            $('#company').val(nombre);
           

            if (RutMovil == 0 || NombreMovil == 0 || ApellidoMovil == 0 || PhoneMovil == 0 || correo2 == 0 || correo1 == 0 || organismo == "" || valorFuncionContacto == "" || comuna == "" || region == "" || email != 'Los correos coinciden') {
                if (RutMovil == 0) {
                    $('#rut_contacto_afiliateMovil').focus();
                    $('#rut_contacto_afiliateMovil').val("");
                    $('#rut_contacto_afiliateMovil').css("border", "1px solid red")
                    $('#rut_contacto_afiliateMovil').css("border-radius", "4px")
                    $('#rut_contacto_afiliateMovil').attr("placeholder", "Debe ingresar un RUT")
                } else {
                    $('#rut_contacto_afiliateMovil').css("border", "")
                    $('#rut_contacto_afiliateMovil').css("border-radius", "")
                    $('#rut_contacto_afiliateMovil').attr("placeholder", "")
                }
                if (NombreMovil < 3) {
                    $('#fist_nameMovil').focus();
                    $('#fist_nameMovil').val("");
                    $('#fist_nameMovil').css("border", "1px solid red")
                    $('#fist_nameMovil').css("border-radius", "4px")
                    $('#fist_nameMovil').attr("placeholder", "El nombre debe ser mayor o igual a 3 caracteres")

                } else {
                    $('#fist_nameMovil').css("border", "")
                    $('#fist_nameMovil').css("border-radius", "")
                    $('#fist_nameMovil').attr("placeholder", "")
                }

                if (ApellidoMovil < 3) {

                    $('#last_nameMovil').focus();
                    $('#last_nameMovil').val("");
                    $('#last_nameMovil').css("border", "1px solid red")
                    $('#last_nameMovil').css("border-radius", "4px")
                    $('#last_nameMovil').attr("placeholder", "El apellido debe ser mayor o igual a 3 caracteres")

                } else {
                    $('#last_nameMovil').css("border", "")
                    $('#last_nameMovil').css("border-radius", "")
                    $('#last_nameMovil').attr("placeholder", "")

                }
                if (PhoneMovil < 3) {
                    $('#phoneMovil').focus();
                    $('#phoneMovil').val("");
                    $('#phoneMovil').css("border", "1px solid red")
                    $('#phoneMovil').css("border-radius", "4px")
                    $('#phoneMovil').attr("placeholder", "El teléfono debe contener 9 números.")
                } else {
                    $('#phoneMovil').css("border", "")
                    $('#phoneMovil').css("border-radius", "")
                    $('#phoneMovil').attr("placeholder", "")
                }
                if (email != 'Los correos coinciden') {
                    $('#txtemail').focus();
                    $('#txtemail').val("");
                    $('#txtemail').css("border", "1px solid red ")
                    $('#txtemail').css("border-radius", "4px ")
                    $('#emailOK').html("Los correos no coinciden")
                } else {
                    $('#txtemail').css("border", "")
                    $('#txtemail').css("border-radius", "")
                    $('#emailOK').html("Los correos coinciden")
                }
                if (correo1 == "") {
                    $('#txtemail').focus();
                    $('#txtemail').val("");
                    $('#txtemail').css("border", "1px solid red ")
                    $('#txtemail').css("border-radius", "4px ")
                    $('#txtemail').attr("placeholder", "Debe ingresar un correo")
                } else {
                    $('#txtemail').css("border", "")
                    $('#txtemail').css("border-radius", "")
                    $('#txtemail').attr("placeholder", "")
                }
                if (correo2 == "") {
                    $('#txtConfirmarCorreo').focus();
                    $('#txtConfirmarCorreo').val("");
                    $('#txtConfirmarCorreo').css("border", "1px solid red")
                    $('#txtConfirmarCorreo').css("border-radius", "4px")
                    $('#txtConfirmarCorreo').attr("placeholder", "Debe ingresar un correo")
                } else {
                    $('#txtConfirmarCorreo').css("border", "")
                    $('#txtConfirmarCorreo').css("border-radius", "")
                    $('#txtConfirmarCorreo').attr("placeholder", "")
                }




                if (organismo == "") {
                    $('#organismo').focus();
                    $('#organismo').val("");
                    $('#organismo').css("border", "1px solid red")
                    $('#organismo').css("border-radius", "4px")
                    $('#Errororganismo').css("display", "block")
                } else {
                    $('#organismo').css("border", "")
                    $('#organismo').css("border-radius", "")
                    $('#Errororganismo').css("display", "none")
                }

                if (region == "") {
                    $('#region').focus();
                    $('#region').val("");
                    $('#region').css("border", "1px solid red")
                    $('#region').css("border-radius", "4px")
                    $('#ErrorRegion').css("display", "block")
                } else {
                    $('#region').css("border", "")
                    $('#region').css("border-radius", "")
                    $('#ErrorRegion').css("display", "none")
                }
                if (comuna == "") {
                    $('#comuna').focus();
                    $('#comuna').val("");
                    $('#comuna').css("border", "1px solid red")
                    $('#comuna').css("border-radius", "4px")
                    $('#ErrorComuna').css("display", "block")
                } else {
                    $('#comuna').css("border", "")
                    $('#comuna').css("border-radius", "")
                    $('#ErrorComuna').css("display", "none")
                }
                event.preventDefault()
            }
        } else {
            if (correo2 == 0 || correo1 == 0 || organismo == "" || valorFuncionContacto == "" || comuna == "" || region == "" || email != 'Los correos coinciden') {

                if (email != 'Los correos coinciden') {
                    $('#txtemail').focus();
                    $('#txtemail').val("");
                    $('#txtemail').css("border", "1px solid red ")
                    $('#txtemail').css("border-radius", "4px ")
                    $('#emailOK').html("Los correos no coinciden")
                } else {
                    $('#txtemail').css("border", "")
                    $('#txtemail').css("border-radius", "")
                    $('#emailOK').html("Los correos coinciden")
                }
                if (correo1 == "") {
                    $('#txtemail').focus();
                    $('#txtemail').val("");
                    $('#txtemail').css("border", "1px solid red ")
                    $('#txtemail').css("border-radius", "4px ")
                    $('#txtemail').attr("placeholder", "Debe ingresar un correo")
                } else {
                    $('#txtemail').css("border", "")
                    $('#txtemail').css("border-radius", "")
                    $('#txtemail').attr("placeholder", "")
                }
                if (correo2 == "") {
                    $('#txtConfirmarCorreo').focus();
                    $('#txtConfirmarCorreo').val("");
                    $('#txtConfirmarCorreo').css("border", "1px solid red")
                    $('#txtConfirmarCorreo').css("border-radius", "4px")
                    $('#txtConfirmarCorreo').attr("placeholder", "Debe ingresar un correo")
                } else {
                    $('#txtConfirmarCorreo').css("border", "")
                    $('#txtConfirmarCorreo').css("border-radius", "")
                    $('#txtConfirmarCorreo').attr("placeholder", "")
                }




                if (organismo == "") {
                    $('#organismo').focus();
                    $('#organismo').val("");
                    $('#organismo').css("border", "1px solid red")
                    $('#organismo').css("border-radius", "4px")
                    $('#Errororganismo').css("display", "block")
                } else {
                    $('#organismo').css("border", "")
                    $('#organismo').css("border-radius", "")
                    $('#Errororganismo').css("display", "none")
                }

                if (region == "") {
                    $('#region').focus();
                    $('#region').val("");
                    $('#region').css("border", "1px solid red")
                    $('#region').css("border-radius", "4px")
                    $('#ErrorRegion').css("display", "block")
                } else {
                    $('#region').css("border", "")
                    $('#region').css("border-radius", "")
                    $('#ErrorRegion').css("display", "none")
                }
                if (comuna == "") {
                    $('#comuna').focus();
                    $('#comuna').val("");
                    $('#comuna').css("border", "1px solid red")
                    $('#comuna').css("border-radius", "4px")
                    $('#ErrorComuna').css("display", "block")
                } else {
                    $('#comuna').css("border", "")
                    $('#comuna').css("border-radius", "")
                    $('#ErrorComuna').css("display", "none")
                }




                //mostrarModalError()
                event.preventDefault()

            }
        }
      })


// FUNCIONES MOVIL

$(document).ready(function () {

    if ($(window).width() < 426) {

        $('#fist_name').val($('#fist_nameMovil').val())
        $('#rut_contacto_afiliate').val($('#rut_contacto_afiliateMovil').val())
        $('#last_name').val($('#last_nameMovil').val())
        $('#phone').val($('#phoneMovil').val())

        $("#fist_nameMovil").on('change', function () {
            var nombre = $('#fist_nameMovil').val().trim();
            $('#fist_name').val(nombre);
        })

        $("#last_nameMovil").on('change', function () {
            var apellido = $('#last_nameMovil').val().trim();
            $('#last_name').val(apellido);
        })
        $("#phoneMovil").on('change', function () {
            var nombre = $('#phoneMovil').val().trim();
            $('#phone').val(nombre);
        })


    } else {
        $('#fist_name').val('')
        $('#rut_contacto_afiliate').val('')
        $('#last_name').val('')
        $('#phone').val('')
        $('#company').val('');
    }

})
$('#rut_contacto_afiliateMovil').on('change', function () {
    var rut = $('#rut_contacto_afiliateMovil').val();
    var error = 0;
    var valor = rut.split('.').join('');
    valor = valor.replace('-', '');
    cuerpo = valor.slice(0, -1);
    dv = valor.slice(-1).toUpperCase();
    rut.value = cuerpo + '-' + dv;
    if (cuerpo.length < 7) { error = 1; }
    suma = 0;
    multiplo = 2;
    for (i = 1; i <= cuerpo.length; i++) {
        index = multiplo * valor.charAt(cuerpo.length - i);
        suma = suma + index;
        if (multiplo < 7) { multiplo = multiplo + 1; } else { multiplo = 2; }
    }
    dvEsperado = 11 - (suma % 11);
    dv = (dv == 'K') ? 10 : dv;
    dv = (dv == 0) ? 11 : dv;
    if (dvEsperado != dv) { error = 1; }
    if (error == 1) {
        $('#rut_contacto_afiliateMovil').val('');
        rutREFOK1 = document.getElementById('rutConOKAfiMovil');
        rutREFOK1.style.color = "red";
        rutREFOK1.innerText = "RUT no válido";
        $('#rut_contacto_afiliateMovil').focus();

        return false;
    } else {
        if (dv == 10) {
            $('#rut_contacto_afiliateMovil').val((cuerpo + '-' + 'K').trim())
            $('#rut_empresa2').val((cuerpo + '-' + 'K').trim())
            $('#rut_contacto_afiliate').val((cuerpo + '-' + 'K').trim())

        } else if (dv == 11) {
            $('#rut_contacto_afiliateMovil').val((cuerpo + '-' + '0').trim())
            $('#rut_empresa2').val((cuerpo + '-' + '0').trim())
            $('#rut_contacto_afiliate').val((cuerpo + '-' + '0').trim())
        }
        else {
            $('#rut_contacto_afiliateMovil').val((cuerpo + '-' + dv).trim())
            $('#rut_empresa2').val((cuerpo + '-' + dv).trim())
            $('#rut_contacto_afiliate').val((cuerpo + '-' + dv).trim())
        }
        rutREFOK1.innerText = "";
        return true;
    }
});

// FIN FUNCIONES MOVILES
function mostrarModalError() {

    var modal = document.getElementById("myModal");
    var span = document.getElementsByClassName("close")[0];
    modal.style.display = "block";
    span.onclick = function () {
        modal.style.display = "none";
    }
    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
}


$('#txtemail').on('change', function () {
    campo = event.target;
    valido = document.getElementById('emailOK');

    var correo1 = $('#txtemail').val();

    var correo2 = $('#txtConfirmarCorreo').val();

    emailRegex = /^[-\w.]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
    //Se muestra un texto a modo de ejemplo, luego va a ser un icono
    if (emailRegex.test(campo.value)) {
        valido.innerText = "Correo con formato correcto";
        if (correo2 != "") {

            if (correo1 === correo2) {
                document.getElementById('emailOK').style.color = "black";
                valido.innerText = "Los correos coinciden";

            } else {
                document.getElementById('emailOK').style.color = "red";
                valido.innerText = "Los correos no coinciden";




            }
        }
    } else {
        valido.innerText = "El formato del correo es incorrecto.";
        $('#txtemail').focus();
        $('#txtemail').val("");
    }
});

//validad Correos iguales
$('#txtConfirmarCorreo').on('change', function () {

    campo = event.target;
    valido = document.getElementById('emailOK');
    var correo1 = $('#txtemail').val();
    var correo2 = $('#txtConfirmarCorreo').val();
    //emailRegex = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    emailRegex = /^[-\w.]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
    //Se muestra un texto a modo de ejemplo, luego va a ser un icono

    if (correo1 === correo2 && correo1.length > 0 && correo2.length > 0) {

        document.getElementById('emailOK').style.color = "black";
        valido.innerText = "Los correos coinciden";

    } else {
        document.getElementById('emailOK').style.color = "red";
        valido.innerText = "Los correos no coinciden";



    }

});



//Se utiliza para que el campo de texto solo acepte numeros
function SoloNumeros(evt) {
    if (window.event) {//asignamos el valor de la tecla a keynum
        keynum = evt.keyCode; //IE
    }
    else {
        keynum = evt.which; //FF
    }
    //comprobamos si se encuentra en el rango numérico y que teclas no recibirá.
    if ((keynum > 47 && keynum < 58) || keynum == 8 || keynum == 13 || keynum == 6) {
        return true;
    }
    else {
        return false;
    }
}
$('#phone').on('change', function () {

    var fono = $('#phone').val().trim().substring(0, 1);

    if (fono == 0) {
        $('#phone').val("")
        $('#phone').attr("placeholder", "El teléfono no puede empezar con 0")
    } else {
        $('#phone').attr("placeholder", "Ejemplo: 948435748")
    }

});


$('#company').on('change', function () {

    var compania = $('#company').val().trim().split('.').length - 1;
    if (compania >= 1) {


        if ((compania - 1) >= 2) {
            $('#company').val("")
            $('#company').attr("placeholder", "Formato incorrecto")

        }
    }
});

//Se utiliza para que el campo de texto solo acepte letras
function soloLetras(e) {
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toString();
    letras = "áéíóúabcdefghijklmnñopqrstuvwxyzÁÉÍÓÚABCDEFGHIJKLMNÑOPQRSTUVWXYZ. ";

    if (letras.indexOf(tecla) == -1) {

        return false;
    }



}

$('#rut_contacto_afiliate').on('change', function () {
    var rut = $('#rut_contacto_afiliate').val();
    var error = 0;
    var valor = rut.split('.').join('');
    valor = valor.replace('-', '');
    cuerpo = valor.slice(0, -1);
    dv = valor.slice(-1).toUpperCase();
    rut.value = cuerpo + '-' + dv;
    if (cuerpo.length < 7) { error = 1; }
    suma = 0;
    multiplo = 2;
    for (i = 1; i <= cuerpo.length; i++) {
        index = multiplo * valor.charAt(cuerpo.length - i);
        suma = suma + index;
        if (multiplo < 7) { multiplo = multiplo + 1; } else { multiplo = 2; }
    }
    dvEsperado = 11 - (suma % 11);
    dv = (dv == 'K') ? 10 : dv;
    dv = (dv == 0) ? 11 : dv;
    if (dvEsperado != dv) { error = 1; }
    if (error == 1) {
        $('#rut_contacto_afiliate').val('');
        rutREFOK = document.getElementById('rutConOKAfi');
        rutREFOK.style.color = "red";
        rutREFOK.innerText = "RUT no válido";
        $('#rut_contacto_afiliate').focus();

        return false;
    } else {
        if (dv == 10) {
            $('#rut_contacto_afiliate').val((cuerpo + '-' + 'K').trim())
            $('#rut_empresa2').val((cuerpo + '-' + 'K').trim())

        } else if (dv == 11) {
            $('#rut_contacto_afiliate').val((cuerpo + '-' + '0').trim())
            $('#rut_empresa2').val((cuerpo + '-' + '0').trim())
        }
        else {
            $('#rut_contacto_afiliate').val((cuerpo + '-' + dv).trim())
            $('#rut_empresa2').val((cuerpo + '-' + dv).trim())
        }
        rutREFOK.innerText = "";
        return true;
    }
});

$("#last_name").on('change', function () {
    var nombre = $('#fist_name').val().trim() + ' ' + $('#last_name').val().trim();
    $('#company').val(nombre);
})

$("#last_name").on('change', function () {
    var nombre = $('#fist_name').val().trim() + ' ' + $('#last_name').val().trim();
    $('#company').val(nombre);
})


//document.getElementById('checkTrabajadorInd').addEventListener('input', function () {


//    var checkeado = document.getElementById("checkTrabajadorInd").checked;

//    if (checkeado == true) {
//        var segundoNombre = $('#middle_name').val().trim().length;
//        var segundoApellido = $('#suffix').val().trim().length;
//        var nombre = "";
//        if (segundoNombre == 0 && segundoApellido == 0) {
//            var nombre = $('#fist_name').val().trim() + ' ' + $('#last_name').val().trim();
//        }
//        if (segundoNombre == 0 && segundoApellido > 0) {
//            var nombre = $('#fist_name').val().trim() + ' ' + $('#last_name').val().trim() + ' ' + $('#suffix').val().trim();
//        }
//        if (segundoNombre > 0 && segundoApellido == 0) {
//            var nombre = $('#fist_name').val().trim() + ' ' + $('#middle_name').val().trim() + ' ' + $('#last_name').val().trim();
//        }
//        if (segundoNombre > 0 && segundoApellido > 0) {
//            var nombre = $('#fist_name').val().trim() + ' ' + $('#middle_name').val().trim() + ' ' + $('#last_name').val().trim() + ' ' + $('#suffix').val().trim();
//        }

//        var rutContactoPlan = $('#rut_contacto_plan').val().trim();
//        var rutContactoAfi = $('#rut_contacto_afiliate').val().trim();
//        if (rutContactoAfi.length != 0) {
//            var rutEmpresaAfi = $('#rut_empresa2').val(rutContactoAfi);
//            //ValidoR()
//        }
//        if (rutContactoPlan.length != 0) {
//            var rutEmpresaPlan = $('#rut_empresa1').val(rutContactoPlan);
//            //ValidoR1()
//        }




//        $('#rut_contacto_plan').attr("readonly", "readonly");
//        $('#rut_contacto_afiliate').attr("readonly", "readonly");

//        $('#rut_empresa1').attr("readonly", "readonly");

//        $('#rut_empresa2').attr("readonly", "readonly");

//        //document.getElementById('employees').disabled = true;
//        document.getElementById('company').value = nombre;
//        $('#company').attr("readonly", "readonly");
//        document.getElementById('employees').value = 1;
//        $('#employees').attr("readonly", "readonly");
//        //document.getElementById('company').disabled = true;
//        document.getElementById('cboxFuncion').value = '0001';

//        $('#cboxFuncion').css('pointer-events', 'none');
//        //document.getElementById('cboxFuncion').disabled = true;
//    } else {
//        document.getElementById('employees').disabled = false;
//        document.getElementById('company').value = "";
//        $('#company').removeAttr("readonly", "readonly");
//        $('#employees').removeAttr("readonly", "readonly");
//        $('#cboxFuncion').css('pointer-events', '');
//        document.getElementById('company').disabled = false;
//        document.getElementById('employees').value = "";
//        document.getElementById('cboxFuncion').value = '';
//        document.getElementById('cboxFuncion').disabled = false;
//        if ($('#rut_empresa1').val() != "test") {
//            $('#rut_empresa1').val('');
//        }
//        if ($('#rut_empresa2').val() != "test") {
//            $('#rut_empresa2').val('');
//        }

//        $('#rut_contacto_plan').removeAttr("readonly", "readonly");
//        $('#rut_contacto_afiliate').removeAttr("readonly", "readonly");
//        $('#rut_empresa1').removeAttr("readonly", "readonly");
//        $('#rut_empresa2').removeAttr("readonly", "readonly");
//        $('#rutOk1').html('');
//        $('#rutOk').html('');
//    }

//});



document.getElementById('phone').addEventListener('change', function () {
    campo = event.target;
    valido = document.getElementById('fonoOk');
    var cantidad = $('#phone').val().length;


    //Se muestra un texto a modo de ejemplo, luego va a ser un icono

    if (cantidad == 9) {
        document.getElementById('fonoOk').style.color = "black";
        valido.innerText = "";
        document.getElementById("btnEnviar").disabled = false;
    }

    else {
        document.getElementById('fonoOk').style.color = "red";
        valido.innerText = "El número de teléfono debe ser de un largo de 9.";


        document.getElementById("btnEnviar").disabled = true;
    }
});
