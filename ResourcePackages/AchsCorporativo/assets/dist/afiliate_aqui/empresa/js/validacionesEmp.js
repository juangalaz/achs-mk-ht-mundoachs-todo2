﻿



//-------------VALIDA TROOD LOS CAMPOS DE PLAN NUEVOS----------------------
document.getElementById('cboxFuncion').value = '0001';

$(document).ready(function () { $('input.disablecopypaste').bind('copy paste', function (e) { e.preventDefault(); }); });

let EmpresaPasoUno1 = document.getElementById('formulario-empresa-uno');
let EmpresaPasoDos2 = document.getElementById('formulario-empresa-dos');


$('#btn-siguiente-empresa').on('click', function () {

    var primerNombre = $('#fist_name').val().trim().length;
    var paterno = $('#last_name').val().trim().length;
    var telefono = $('#phone').val().trim().length;
    var correo1 = $('#txtemail').val().trim().length;
    var correo2 = $('#txtConfirmarCorreo').val().trim().length;
    var email = $('#emailOK').html();
    if (correo2 == 0 || correo1 == 0 || primerNombre < 3 || paterno < 3 || telefono < 3 || email != 'Los correos coinciden' ) {

        if (email != 'Los correos coinciden') {
            $('#txtemail').focus();
            $('#txtemail').val("");
            $('#txtemail').css("border", "1px solid red ")
            $('#txtemail').css("border-radius", "4px ")
            $('#emailOK').html("Los correos no coinciden")
        } else {
            $('#txtemail').css("border", "")
            $('#txtemail').css("border-radius", "")
            $('#emailOK').html("Los correos coinciden")
        }

        if (correo1 == "") {
            $('#txtemail').focus();
            $('#txtemail').val("");
            $('#txtemail').css("border", "1px solid red ")
            $('#txtemail').css("border-radius", "4px ")
            $('#txtemail').attr("placeholder", "Debe ingresar un correo")
        } else {
            $('#txtemail').css("border", "")
            $('#txtemail').css("border-radius", "")
            $('#txtemail').attr("placeholder", "")
        }
        if (correo2 == "") {
            $('#txtConfirmarCorreo').focus();
            $('#txtConfirmarCorreo').val("");
            $('#txtConfirmarCorreo').css("border", "1px solid red")
            $('#txtConfirmarCorreo').css("border-radius", "4px")
            $('#txtConfirmarCorreo').attr("placeholder", "Debe ingresar un correo")
        } else {
            $('#txtConfirmarCorreo').css("border", "")
            $('#txtConfirmarCorreo').css("border-radius", "")
            $('#txtConfirmarCorreo').attr("placeholder", "")
        }
        if (primerNombre < 3) {

            $('#fist_name').focus();
            $('#fist_name').val("");
            $('#fist_name').css("border", "1px solid red")
            $('#fist_name').css("border-radius", "4px")
            $('#fist_name').attr("placeholder", "El nombre debe ser mayor o igual a 3 caracteres")

        } else {
            $('#fist_name').css("border", "")
            $('#fist_name').css("border-radius", "")
            $('#fist_name').attr("placeholder", "")
        }

        if (paterno < 3) {

            $('#last_name').focus();
            $('#last_name').val("");
            $('#last_name').css("border", "1px solid red")
            $('#last_name').css("border-radius", "4px")
            $('#last_name').attr("placeholder", "El apellido debe ser mayor o igual a 3 caracteres")

        } else {
            $('#last_name').css("border", "")
            $('#last_name').css("border-radius", "")
            $('#last_name').attr("placeholder", "")
        }


        if (telefono <= 3 || $('#phone').val().trim() == '000000000' || $('#phone').val().trim() == '111111111' || $('#phone').val().trim() == '222222222' || $('#phone').val().trim() == '333333333' || $('#phone').val().trim() == '444444444' || $('#phone').val().trim() == '555555555' || $('#phone').val().trim() == '666666666' || $('#phone').val().trim() == '777777777' || $('#phone').val().trim() == '888888888' || $('#phone').val().trim() == '999999999') {
            $('#phone').focus();
            $('#phone').val("");
            $('#phone').css("border", "1px solid red")
            $('#phone').css("border-radius", "4px")
            $('#phone').attr("placeholder", "El teléfono debe contener 9 números.")
        } else {
            $('#phone').css("border", "")
            $('#phone').css("border-radius", "")
            $('#phone').attr("placeholder", "")
        }
    } else {

        EmpresaPasoUno1.classList.add('d-none')
        EmpresaPasoDos2.classList.remove('d-none')
    }
})

$('#btnEnviar').on('click', function () {

    var company = $('#company').val().trim().length;
    var employees = $('#employees').val().trim().length;


    var region = $('#region').val();
    var comuna = $('#comuna').val();
    var organismo = $('#organismo').val();
    var rutEmpresaAfiEmp = $('#rut_empresa2').val().trim().length;

    var primerNombreMovil = $('#fist_nameMovil').val().trim().length;
    var paternoMovil = $('#last_nameMovil').val().trim().length;
    var telefonoMovil = $('#phoneMovil').val().trim().length;
    var correo1Movil = $('#txtemailMovil').val().trim().length;
    var correo2Movil = $('#txtConfirmarCorreoMovil').val().trim().length;
    var emailMovil = $('#emailOKMovil').html();

    if ($(window).width() < 426) {
        //&& paterno >= 3 && company >= 3 && telefono == 9 && email == 'Los correos coinciden'
        if (rutEmpresaAfiEmp == 0 || organismo == "" || comuna == "" || region == "" || company < 3 || employees == 0 || correo2Movil == 0 || correo1Movil == 0 || primerNombreMovil < 3 || paternoMovil < 3 || telefonoMovil < 3 || emailMovil != 'Los correos coinciden') {
            if (emailMovil != 'Los correos coinciden') {
                $('#txtemailMovil').focus();
                $('#txtemailMovil').val("");
                $('#txtemailMovil').css("border", "1px solid red ")
                $('#txtemailMovil').css("border-radius", "4px ")
                $('#emailOKMovil').html("Los correos no coinciden")
            } else {
                $('#txtemailMovil').css("border", "")
                $('#txtemailMovil').css("border-radius", "")
                $('#emailOKMovil').html("Los correos coinciden")
            }

            if (correo1Movil == "") {
                $('#txtemailMovil').focus();
                $('#txtemailMovil').val("");
                $('#txtemailMovil').css("border", "1px solid red ")
                $('#txtemailMovil').css("border-radius", "4px ")
                $('#txtemailMovil').attr("placeholder", "Debe ingresar un correo")
            } else {
                $('#txtemailMovil').css("border", "")
                $('#txtemailMovil').css("border-radius", "")
                $('#txtemailMovil').attr("placeholder", "")
            }
            if (correo2Movil == "") {
                $('#txtConfirmarCorreoMovil').focus();
                $('#txtConfirmarCorreoMovil').val("");
                $('#txtConfirmarCorreoMovil').css("border", "1px solid red")
                $('#txtConfirmarCorreoMovil').css("border-radius", "4px")
                $('#txtConfirmarCorreoMovil').attr("placeholder", "Debe ingresar un correo")
            } else {
                $('#txtConfirmarCorreoMovil').css("border", "")
                $('#txtConfirmarCorreoMovil').css("border-radius", "")
                $('#txtConfirmarCorreoMovil').attr("placeholder", "")
            }
            if (primerNombreMovil < 3) {

                $('#fist_nameMovil').focus();
                $('#fist_nameMovil').val("");
                $('#fist_nameMovil').css("border", "1px solid red")
                $('#fist_nameMovil').css("border-radius", "4px")
                $('#fist_nameMovil').attr("placeholder", "El nombre debe ser mayor o igual a 3 caracteres")

            } else {
                $('#fist_nameMovil').css("border", "")
                $('#fist_nameMovil').css("border-radius", "")
                $('#fist_nameMovil').attr("placeholder", "")
            }

            if (paternoMovil < 3) {

                $('#last_nameMovil').focus();
                $('#last_nameMovil').val("");
                $('#last_nameMovil').css("border", "1px solid red")
                $('#last_nameMovil').css("border-radius", "4px")
                $('#last_nameMovil').attr("placeholder", "El apellido debe ser mayor o igual a 3 caracteres")

            } else {
                $('#last_nameMovil').css("border", "")
                $('#last_nameMovil').css("border-radius", "")
                $('#last_nameMovil').attr("placeholder", "")
            }


            if (telefonoMovil <= 3 || $('#phone').val().trim() == '000000000' || $('#phone').val().trim() == '111111111' || $('#phone').val().trim() == '222222222' || $('#phone').val().trim() == '333333333' || $('#phone').val().trim() == '444444444' || $('#phone').val().trim() == '555555555' || $('#phone').val().trim() == '666666666' || $('#phone').val().trim() == '777777777' || $('#phone').val().trim() == '888888888' || $('#phone').val().trim() == '999999999') {
                $('#phoneMovil').focus();
                $('#phoneMovil').val("");
                $('#phoneMovil').css("border", "1px solid red")
                $('#phoneMovil').css("border-radius", "4px")
                $('#phoneMovil').attr("placeholder", "El teléfono debe contener 9 números.")
            } else {
                $('#phoneMovil').css("border", "")
                $('#phoneMovil').css("border-radius", "")
                $('#phoneMovil').attr("placeholder", "")
            }



            if (rutEmpresaAfiEmp == 0) {
                $('#rut_empresa2').focus();
                $('#rut_empresa2').val("");
                $('#rut_empresa2').css("border", "1px solid red ")
                $('#rut_empresa2').css("border-radius", "4px")
                $('#rut_empresa2').attr("placeholder", "Debe ingresar un rut");
            } else {
                $('#rut_empresa2').css("border", "")
                $('#rut_empresa2').css("border-radius", "")
            }

            if (organismo == "") {
                $('#organismo').focus();
                $('#organismo').val("");
                $('#organismo').css("border", "1px solid red")
                $('#organismo').css("border-radius", "4px")
                $('#Errororganismo').css("display", "block")
            } else {
                $('#organismo').css("border", "")
                $('#organismo').css("border-radius", "")
                $('#Errororganismo').css("display", "none")
            }

            if (region == "") {
                $('#region').focus();
                $('#region').val("");
                $('#region').css("border", "1px solid red")
                $('#region').css("border-radius", "4px")
                $('#ErrorRegion').css("display", "block")
            } else {
                $('#region').css("border", "")
                $('#region').css("border-radius", "")
                $('#ErrorRegion').css("display", "none")
            }
            if (comuna == "") {
                $('#comuna').focus();
                $('#comuna').val("");
                $('#comuna').css("border", "1px solid red")
                $('#comuna').css("border-radius", "4px")
                $('#ErrorComuna').css("display", "block")
            } else {
                $('#comuna').css("border", "")
                $('#comuna').css("border-radius", "")
                $('#ErrorComuna').css("display", "none")
            }

            if (company <= 3) {
                $('#company').focus();
                $('#company').val("");
                $('#company').css("border", "1px solid red")
                $('#company').css("border-radius", "4px")
                $('#company').attr("placeholder", "El nombre de la compañía debe ser mayor o igual a 3 caracteres")
            } else {
                $('#company').css("border", "")
                $('#company').css("border-radius", "")
                $('#company').attr("placeholder", "")
            }

            if (employees == 0) {
                $('#employees').focus();
                $('#employees').val("");
                $('#employees').css("border", "1px solid red")
                $('#employees').css("border-radius", "4px")
                $('#employees').attr("placeholder", "El número de empleados debe ser mayor a 0")
            } else {
                $('#employees').css("border", "")
                $('#employees').css("border-radius", "")
                $('#employees').attr("placeholder", "")
            }

            //mostrarModalError()
            event.preventDefault()

        }

    }
    else {
        if (rutEmpresaAfiEmp == 0 || organismo == "" || comuna == "" || region == "" || company < 3 || employees == 0) {

            if (rutEmpresaAfiEmp == 0) {
                $('#rut_empresa2').focus();
                $('#rut_empresa2').val("");
                $('#rut_empresa2').css("border", "1px solid red ")
                $('#rut_empresa2').css("border-radius", "4px")
                $('#rut_empresa2').attr("placeholder", "Debe ingresar un rut");
            } else {
                $('#rut_empresa2').css("border", "")
                $('#rut_empresa2').css("border-radius", "")
            }





            if (organismo == "") {
                $('#organismo').focus();
                $('#organismo').val("");
                $('#organismo').css("border", "1px solid red")
                $('#organismo').css("border-radius", "4px")
                $('#Errororganismo').css("display", "block")
            } else {
                $('#organismo').css("border", "")
                $('#organismo').css("border-radius", "")
                $('#Errororganismo').css("display", "none")
            }

            if (region == "") {
                $('#region').focus();
                $('#region').val("");
                $('#region').css("border", "1px solid red")
                $('#region').css("border-radius", "4px")
                $('#ErrorRegion').css("display", "block")
            } else {
                $('#region').css("border", "")
                $('#region').css("border-radius", "")
                $('#ErrorRegion').css("display", "none")
            }
            if (comuna == "") {
                $('#comuna').focus();
                $('#comuna').val("");
                $('#comuna').css("border", "1px solid red")
                $('#comuna').css("border-radius", "4px")
                $('#ErrorComuna').css("display", "block")
            } else {
                $('#comuna').css("border", "")
                $('#comuna').css("border-radius", "")
                $('#ErrorComuna').css("display", "none")
            }

            if (company <= 3) {
                $('#company').focus();
                $('#company').val("");
                $('#company').css("border", "1px solid red")
                $('#company').css("border-radius", "4px")
                $('#company').attr("placeholder", "El nombre de la compañía debe ser mayor o igual a 3 caracteres")
            } else {
                $('#company').css("border", "")
                $('#company').css("border-radius", "")
                $('#company').attr("placeholder", "")
            }

            if (employees == 0) {
                $('#employees').focus();
                $('#employees').val("");
                $('#employees').css("border", "1px solid red")
                $('#employees').css("border-radius", "4px")
                $('#employees').attr("placeholder", "El número de empleados debe ser mayor a 0")
            } else {
                $('#employees').css("border", "")
                $('#employees').css("border-radius", "")
                $('#employees').attr("placeholder", "")
            }

            //mostrarModalError()
            event.preventDefault()
        }
    }

});
//$('#fist_name').on('change', function () {

//    var letras = "áéíóúabcdefghijklmnñopqrstuvwxyzÁÉÍÓÚABCDEFGHIJKLMNÑOPQRSTUVWXYZ ";

//    var texto = $('#fist_name').val().trim();
//    for (i = 0; i < texto.length; i++) {
//        if (letras.indexOf(texto.charAt(i), 0) != -1) {
//            $('#fist_name').val($('#fist_name').val().trim())  
//        } else {
//            $('#fist_name').val('')
//        }
//    }
//    return 0;

//});

// FUCNIONES MOVILES

$(document).ready(function () {

    if ($(window).width() < 426) {

        $('#fist_name').val($('#fist_nameMovil').val())
        $('#last_name').val($('#last_nameMovil').val())
        $('#phone').val($('#phoneMovil').val())
        $('#txtemail').val($('#txtemailMovil').val())

        $("#fist_nameMovil").on('change', function () {
            var nombre = $('#fist_nameMovil').val().trim();
            $('#fist_name').val(nombre);
        })

        $("#last_nameMovil").on('change', function () {
            var apellido = $('#last_nameMovil').val().trim();
            $('#last_name').val(apellido);
        })

        $("#phoneMovil").on('change', function () {
            var nombre = $('#phoneMovil').val().trim();
            $('#phone').val(nombre);
        })
        $("#txtemailMovil").on('change', function () {
            var correo = $('#txtemailMovil').val().trim();
            $('#txtemail').val(correo);
        })

        $("#txtConfirmarCorreoMovil").on('change', function () {
            var correo2 = $('#txtConfirmarCorreoMovil').val().trim();
            $('#txtConfirmarCorreo').val(correo2);
        })

    } else {
        $('#fist_name').val('')
        $('#fist_name').val('')
        $('#txtemailMovil').val('')
        $('#txtConfirmarCorreoMovil').val('')
        $('#phone').val('')

    }

})

$('#txtemailMovil').on('change', function () {
    campo = event.target;
    valido = document.getElementById('emailOKMovil');

    var correo1 = $('#txtemailMovil').val();

    var correo2 = $('#txtConfirmarCorreoMovil').val();

    emailRegex = /^[-\w.]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
    //Se muestra un texto a modo de ejemplo, luego va a ser un icono
    if (emailRegex.test(campo.value)) {
        valido.innerText = "Correo con formato correcto";
        if (correo2 != "") {

            if (correo1 === correo2) {
                document.getElementById('emailOKMovil').style.color = "black";
                valido.innerText = "Los correos coinciden";

            } else {
                document.getElementById('emailOKMovil').style.color = "red";
                valido.innerText = "Los correos no coinciden";




            }
        }
    } else {
        valido.innerText = "El formato del correo es incorrecto.";
        $('#txtemailMovil').focus();
        $('#txtemailMovil').val("");
    }
});

//validad Correos iguales
$('#txtConfirmarCorreoMovil').on('change', function () {

    campo = event.target;
    valido = document.getElementById('emailOKMovil');
    var correo1 = $('#txtemailMovil').val();
    var correo2 = $('#txtConfirmarCorreoMovil').val();
    //emailRegex = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    emailRegex = /^[-\w.]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
    //Se muestra un texto a modo de ejemplo, luego va a ser un icono

    if (correo1 === correo2 && correo1.length > 0 && correo2.length > 0) {

        document.getElementById('emailOKMovil').style.color = "black";
        valido.innerText = "Los correos coinciden";

    } else {
        document.getElementById('emailOKMovil').style.color = "red";
        valido.innerText = "Los correos no coinciden";



    }

});




// FIN FUNCIONES MOVILES

function mostrarModalError() {

    var modal = document.getElementById("myModal");
    var span = document.getElementsByClassName("close")[0];
    modal.style.display = "block";
    span.onclick = function () {
        modal.style.display = "none";
    }
    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
}



$('#txtemail').on('change', function () {
    campo = event.target;
    valido = document.getElementById('emailOK');

    var correo1 = $('#txtemail').val();

    var correo2 = $('#txtConfirmarCorreo').val();

    emailRegex = /^[-\w.]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
    //Se muestra un texto a modo de ejemplo, luego va a ser un icono
    if (emailRegex.test(campo.value)) {
        valido.innerText = "Correo con formato correcto";
        if (correo2 != "") {

            if (correo1 === correo2) {
                document.getElementById('emailOK').style.color = "black";
                valido.innerText = "Los correos coinciden";

            } else {
                document.getElementById('emailOK').style.color = "red";
                valido.innerText = "Los correos no coinciden";




            }
        }
    } else {
        valido.innerText = "El formato del correo es incorrecto.";
        $('#txtemail').focus();
        $('#txtemail').val("");
    }
});

//validad Correos iguales
$('#txtConfirmarCorreo').on('change', function () {

    campo = event.target;
    valido = document.getElementById('emailOK');
    var correo1 = $('#txtemail').val();
    var correo2 = $('#txtConfirmarCorreo').val();
    //emailRegex = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    emailRegex = /^[-\w.]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
    //Se muestra un texto a modo de ejemplo, luego va a ser un icono

    if (correo1 === correo2 && correo1.length > 0 && correo2.length > 0) {

        document.getElementById('emailOK').style.color = "black";
        valido.innerText = "Los correos coinciden";

    } else {
        document.getElementById('emailOK').style.color = "red";
        valido.innerText = "Los correos no coinciden";



    }

});



//Se utiliza para que el campo de texto solo acepte numeros
function SoloNumeros(evt) {
    if (window.event) {//asignamos el valor de la tecla a keynum
        keynum = evt.keyCode; //IE
    }
    else {
        keynum = evt.which; //FF
    }
    //comprobamos si se encuentra en el rango numérico y que teclas no recibirá.
    if ((keynum > 47 && keynum < 58) || keynum == 8 || keynum == 13 || keynum == 6) {
        return true;
    }
    else {
        return false;
    }
}
$('#phone').on('change', function () {

    var fono = $('#phone').val().trim().substring(0, 1);

    if (fono == 0) {
        $('#phone').val("")
        $('#phone').attr("placeholder", "El teléfono no puede empezar con 0")
    } else {
        $('#phone').attr("placeholder", "Ejemplo: 948435748")
    }

});
$('#company').on('change', function () {

    var compania = $('#company').val().trim().split('.').length - 1;
    if (compania >= 1) {


        if ((compania - 1) >= 2) {
            $('#company').val("")
            $('#company').attr("placeholder", "Formato incorrecto")


        }
    }
});

//Se utiliza para que el campo de texto solo acepte letras
function soloLetras(e) {
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toString();
    letras = "áéíóúabcdefghijklmnñopqrstuvwxyzÁÉÍÓÚABCDEFGHIJKLMNÑOPQRSTUVWXYZ. ";

    if (letras.indexOf(tecla) == -1) {

        return false;
    }



}

$('#rut_empresa2').on('change', function () {
    var rut = $('#rut_empresa2').val();
    var error = 0;
    var valor = rut.split('.').join('');
    valor = valor.replace('-', '');
    cuerpo = valor.slice(0, -1);
    dv = valor.slice(-1).toUpperCase();
    rut.value = cuerpo + '-' + dv;
    if (cuerpo.length < 7) { error = 1; }
    suma = 0;
    multiplo = 2;
    for (i = 1; i <= cuerpo.length; i++) {
        index = multiplo * valor.charAt(cuerpo.length - i);
        suma = suma + index;
        if (multiplo < 7) { multiplo = multiplo + 1; } else { multiplo = 2; }
    }
    dvEsperado = 11 - (suma % 11);
    dv = (dv == 'K') ? 10 : dv;
    dv = (dv == 0) ? 11 : dv;
    if (dvEsperado != dv) { error = 1; }
    if (error == 1) {
        $('#rut_empresa2').val('');
        rutOK = document.getElementById('rutOk');
        rutOK.style.color = "red";
        rutOK.innerText = "RUT no válido";
        $('#rut_empresa2').focus();

        return false;
    } else {
        if (dv == 10) {
            $('#rut_empresa2').val((cuerpo + '-' + 'K').trim())

        } else if (dv == 11) {
            $('#rut_empresa2').val((cuerpo + '-' + '0').trim())
        }
        else {
            $('#rut_empresa2').val((cuerpo + '-' + dv).trim())
        }
        rutOK.innerText = "";
        return true;
    }
});

document.getElementById('employees').addEventListener('input', function () {
    campo = event.target;
    valido = document.getElementById('NumOk');
    var cantidad = $('#employees').val();


    //Se muestra un texto a modo de ejemplo, luego va a ser un icono

    if (cantidad > 0) {
        document.getElementById('NumOk').style.color = "black";
        valido.innerText = "";


    }
    else {
        document.getElementById('emailOK').style.color = "red";
        valido.innerText = "El número de empleados debe ser mayor a 0";



    }
});




document.getElementById('phone').addEventListener('change', function () {
    campo = event.target;
    valido = document.getElementById('fonoOk');
    var cantidad = $('#phone').val().length;


    //Se muestra un texto a modo de ejemplo, luego va a ser un icono

    if (cantidad == 9) {
        document.getElementById('fonoOk').style.color = "black";
        valido.innerText = "";
        document.getElementById("btnEnviar").disabled = false;
    }

    else {
        document.getElementById('fonoOk').style.color = "red";
        valido.innerText = "El número de teléfono debe ser de un largo de 9.";


        document.getElementById("btnEnviar").disabled = true;
    }
});

