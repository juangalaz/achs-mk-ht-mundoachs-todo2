//Función de botones Siguente/Volver/Hablemos



//identificador de botones Volver
let btnVolverEmpresaUno = document.getElementById('btn-volver-empresa-uno');
let btnVolverEmpresaDos = document.getElementById('btn-volver-empresa-dos');
let pasoDivDatos = document.getElementById('divDatos');
//identificador de botones Hablemos
let pasoMovil = document.getElementById('PasoMovil');
let btnHablemosEmpresa = document.getElementById('btn-hablemos-empresa');
let EmpresaPasoUno = document.getElementById('formulario-empresa-uno');
let EmpresaPasoDos = document.getElementById('formulario-empresa-dos');

$(document).ready(function () {

    if ($(window).width() < 426) {
        pasoDivDatos.innerHTML = "Datos de Contacto/Empresa";
        pasoMovil.innerText = "";
        EmpresaPasoUno.classList.add('d-none')
        EmpresaPasoDos.classList.remove('d-none')
        $("#divMovil").removeClass("d-none")
        $("#btn-volver-empresa-uno").attr("value", "<");
        $("#btn-volver-empresa-dos").attr("value", "<");
    } else {
        pasoDivDatos.innerHTML = "Datos de la Empresa";
        pasoMovil.innerText = "Paso 2 de 2";
        EmpresaPasoUno.classList.remove('d-none')
        EmpresaPasoDos.classList.add('d-none')
        $("#divMovil").addClass("d-none")
        $("#btn-volver-empresa-uno").attr("value", "< Volver");
        $("#btn-volver-empresa-dos").attr("value", "< Volver");
    }

    $(window).resize(function () {
        if ($(window).width() < 426) {
            pasoDivDatos.innerHTML = "Datos de Contacto/Empresa";
            pasoMovil.innerText = "";
            EmpresaPasoUno.classList.add('d-none')
            EmpresaPasoDos.classList.remove('d-none')
            $("#divMovil").removeClass("d-none")
            $("#btn-volver-empresa-uno").attr("value", "<");
            $("#btn-volver-empresa-dos").attr("value", "<");
        } else {
            pasoDivDatos.innerHTML = "Datos de la Empresa";
            pasoMovil.innerText = "Paso 2 de 2";
            EmpresaPasoUno.classList.remove('d-none')
            EmpresaPasoDos.classList.add('d-none')
            $("#divMovil").addClass("d-none")
            $("#btn-volver-empresa-uno").attr("value", "< Volver");
            $("#btn-volver-empresa-dos").attr("value", "< Volver");
        }
    });

});

//Volver
btnVolverEmpresaUno.addEventListener('click', function(){
    //EmpresaPasoUno.classList.add('d-none')
    window.parent.location.href = "https://www.achs.cl/portal/ACHS-Corporativo/Paginas/afiliate-aqui.aspx";

});

btnVolverEmpresaDos.addEventListener('click', function(){
    if ($(window).width() < 426) {
        window.parent.location.href = "https://www.achs.cl/portal/ACHS-Corporativo/Paginas/afiliate-aqui.aspx";
    } else {
        EmpresaPasoDos.classList.add('d-none')
        EmpresaPasoUno.classList.remove('d-none')
    }
});

