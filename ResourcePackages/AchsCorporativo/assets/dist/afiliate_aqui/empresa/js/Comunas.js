﻿$('#region').on('change', function () {
    //alert(this.value);
    if (this.value == '') {
        $('option', '#comuna').remove();
        $('#comuna').append($("<option></option>").attr("value", '').text('Comuna'));
    }
    if (this.value == '01') {
        $('option', '#comuna').remove();
        $('#comuna').append($("<option></option>").attr("value", '').text('Comuna'));
        $('#comuna').append($("<option></option>").attr("value", "1107").text("Alto Hospicio"));
        $('#comuna').append($("<option></option>").attr("value", "1402").text("Camiña"));
        $('#comuna').append($("<option></option>").attr("value", "1403").text("Colchane"));
        $('#comuna').append($("<option></option>").attr("value", "1404").text("Huara"));
        $('#comuna').append($("<option></option>").attr("value", "1101").text("Iquique"));
        $('#comuna').append($("<option></option>").attr("value", "1405").text("Pica"));
        $('#comuna').append($("<option></option>").attr("value", "1401").text("Pozo Almonte"));
    }
    if (this.value == '02') {
        $('option', '#comuna').remove();
        $('#comuna').append($("<option></option>").attr("value", '').text('Comuna'));
        $('#comuna').append($("<option></option>").attr("value", "2101").text("Antofagasta"));
        $('#comuna').append($("<option></option>").attr("value", "2201").text("Calama"));
        $('#comuna').append($("<option></option>").attr("value", "2302").text("María Elena"));
        $('#comuna').append($("<option></option>").attr("value", "2102").text("Mejillones"));
        $('#comuna').append($("<option></option>").attr("value", "2202").text("Ollague"));
        $('#comuna').append($("<option></option>").attr("value", "2203").text("San Pedro de Atacama"));
        $('#comuna').append($("<option></option>").attr("value", "2103").text("Sierra Gorda"));
        $('#comuna').append($("<option></option>").attr("value", "2104").text("Taltal"));
        $('#comuna').append($("<option></option>").attr("value", "2301").text("Tocopilla"));
    }
    if (this.value == '03') {
        $('option', '#comuna').remove();
        $('#comuna').append($("<option></option>").attr("value", '').text('Comuna'));
        $('#comuna').append($("<option></option>").attr("value", "3302").text("Alto del Carmen"));
        $('#comuna').append($("<option></option>").attr("value", "3102").text("Caldera"));
        $('#comuna').append($("<option></option>").attr("value", "3201").text("Chañaral"));
        $('#comuna').append($("<option></option>").attr("value", "3101").text("Copiapó"));
        $('#comuna').append($("<option></option>").attr("value", "3202").text("Diego de Almagro"));
        $('#comuna').append($("<option></option>").attr("value", "3303").text("Freirina"));
        $('#comuna').append($("<option></option>").attr("value", "3304").text("Huasco"));
        $('#comuna').append($("<option></option>").attr("value", "3103").text("Tierra Amarilla"));
        $('#comuna').append($("<option></option>").attr("value", "3301").text("Vallenar"));
    }
    if (this.value == '04') {
        $('option', '#comuna').remove();
        $('#comuna').append($("<option></option>").attr("value", '').text('Comuna'));
        $('#comuna').append($("<option></option>").attr("value", "4103").text("Andacollo"));
        $('#comuna').append($("<option></option>").attr("value", "4202").text("Canela"));
        $('#comuna').append($("<option></option>").attr("value", "4302").text("Combarbalá"));
        $('#comuna').append($("<option></option>").attr("value", "4102").text("Coquimbo"));
        $('#comuna').append($("<option></option>").attr("value", "4201").text("Illapel"));
        $('#comuna').append($("<option></option>").attr("value", "4104").text("La Higuera"));
        $('#comuna').append($("<option></option>").attr("value", "4101").text("La Serena"));
        $('#comuna').append($("<option></option>").attr("value", "4203").text("Los Vilos"));
        $('#comuna').append($("<option></option>").attr("value", "4303").text("Monte Patria"));
        $('#comuna').append($("<option></option>").attr("value", "4301").text("Ovalle"));
        $('#comuna').append($("<option></option>").attr("value", "4105").text("Paihuano"));
        $('#comuna').append($("<option></option>").attr("value", "4304").text("Punitaqui"));
        $('#comuna').append($("<option></option>").attr("value", "4305").text("Río Hurtado"));
        $('#comuna').append($("<option></option>").attr("value", "4204").text("Salamanca"));
        $('#comuna').append($("<option></option>").attr("value", "4106").text("Vicuña"));
    }
    if (this.value == '05') {
        $('option', '#comuna').remove();
        $('#comuna').append($("<option></option>").attr("value", '').text('Comuna'));
        $('#comuna').append($("<option></option>").attr("value", "5602").text("Algarrobo"));
        $('#comuna').append($("<option></option>").attr("value", "5402").text("Cabildo"));
        $('#comuna').append($("<option></option>").attr("value", "5502").text("Calera"));
        $('#comuna').append($("<option></option>").attr("value", "5302").text("Calle Larga"));
        $('#comuna').append($("<option></option>").attr("value", "5603").text("Cartagena"));
        $('#comuna').append($("<option></option>").attr("value", "5102").text("Casablanca"));
        $('#comuna').append($("<option></option>").attr("value", "5702").text("Catemu"));
        $('#comuna').append($("<option></option>").attr("value", "5103").text("Concón"));
        $('#comuna').append($("<option></option>").attr("value", "5604").text("El Quisco"));
        $('#comuna').append($("<option></option>").attr("value", "5605").text("El Tabo"));
        $('#comuna').append($("<option></option>").attr("value", "5503").text("Hijuelas"));
        $('#comuna').append($("<option></option>").attr("value", "5201").text("Isla de Pascua"));
        $('#comuna').append($("<option></option>").attr("value", "5104").text("Juan Fernández"));
        $('#comuna').append($("<option></option>").attr("value", "5504").text("La Cruz"));
        $('#comuna').append($("<option></option>").attr("value", "5401").text("La Ligua"));
        $('#comuna').append($("<option></option>").attr("value", "5802").text("Limache"));
        //$('#comuna').append($("<option></option>").attr("value", "5703").text("Llaillay"));
        $('#comuna').append($("<option></option>").attr("value", "5704").text("Llay Llay"));
        $('#comuna').append($("<option></option>").attr("value", "5301").text("Los Andes"));
        $('#comuna').append($("<option></option>").attr("value", "5506").text("Nogales"));
        $('#comuna').append($("<option></option>").attr("value", "5803").text("Olmué"));
        //$('#comuna').append($("<option></option>").attr("value", "PANQUEHUE").text("Panquehue"));
        $('#comuna').append($("<option></option>").attr("value", "5403").text("Papudo"));
        $('#comuna').append($("<option></option>").attr("value", "5404").text("Petorca"));
        $('#comuna').append($("<option></option>").attr("value", "5105").text("Puchuncaví"));
        $('#comuna').append($("<option></option>").attr("value", "5705").text("Putaendo"));
        $('#comuna').append($("<option></option>").attr("value", "5501").text("Quillota"));
        $('#comuna').append($("<option></option>").attr("value", "5801").text("Quilpué"));
        $('#comuna').append($("<option></option>").attr("value", "5107").text("Quintero"));
        $('#comuna').append($("<option></option>").attr("value", "5303").text("Rinconada"));
        $('#comuna').append($("<option></option>").attr("value", "5601").text("San Antonio"));
        $('#comuna').append($("<option></option>").attr("value", "5304").text("San Esteban"));
        $('#comuna').append($("<option></option>").attr("value", "5701").text("San Felipe"));
        $('#comuna').append($("<option></option>").attr("value", "5706").text("Santa María"));
        $('#comuna').append($("<option></option>").attr("value", "5606").text("Santo Domingo"));
        $('#comuna').append($("<option></option>").attr("value", "5101").text("Valparaíso"));
        $('#comuna').append($("<option></option>").attr("value", "5804").text("Villa Alemana"));
        $('#comuna').append($("<option></option>").attr("value", "5109").text("Viña del Mar"));
        $('#comuna').append($("<option></option>").attr("value", "5405").text("Zapallar"));
        //$('#comuna').append($("<option></option>").attr("value", "LAS CRUCES").text("Las Cruces"));
        //$('#comuna').append($("<option></option>").attr("value", "LLO LLEO").text("Llo Lleo"));
        //$('#comuna').append($("<option></option>").attr("value", "ROCAS DE SANTO DOMINGO").text("Rocas de Santo Domingo"));
    }
    if (this.value == '06') {
        $('option', '#comuna').remove();
        $('#comuna').append($("<option></option>").attr("value", '').text('Comuna'));
        $('#comuna').append($("<option></option>").attr("value", "6302").text("Chépica"));
        $('#comuna').append($("<option></option>").attr("value", "6303").text("Chimbarongo"));
        $('#comuna').append($("<option></option>").attr("value", "6102").text("Codegua"));
        $('#comuna').append($("<option></option>").attr("value", "6103").text("Coinco"));
        $('#comuna').append($("<option></option>").attr("value", "6104").text("Coltauco"));
        $('#comuna').append($("<option></option>").attr("value", "6105").text("Doñihue"));
        $('#comuna').append($("<option></option>").attr("value", "6106").text("Graneros"));
        $('#comuna').append($("<option></option>").attr("value", "6202").text("La Estrella"));
        $('#comuna').append($("<option></option>").attr("value", "6107").text("Las Cabras"));
        $('#comuna').append($("<option></option>").attr("value", "6203").text("Litueche"));
        $('#comuna').append($("<option></option>").attr("value", "6304").text("Lolol"));
        $('#comuna').append($("<option></option>").attr("value", "6108").text("Machalí"));
        $('#comuna').append($("<option></option>").attr("value", "6109").text("Malloa"));
        $('#comuna').append($("<option></option>").attr("value", "6204").text("Marchihue"));
        $('#comuna').append($("<option></option>").attr("value", "6110").text("Mostazal"));
        $('#comuna').append($("<option></option>").attr("value", "6305").text("Nancagua"));
        $('#comuna').append($("<option></option>").attr("value", "6205").text("Navidad"));
        $('#comuna').append($("<option></option>").attr("value", "6111").text("Olivar"));
        $('#comuna').append($("<option></option>").attr("value", "6306").text("Palmilla"));
        $('#comuna').append($("<option></option>").attr("value", "6206").text("Paredones"));
        $('#comuna').append($("<option></option>").attr("value", "6307").text("Peralillo"));
        $('#comuna').append($("<option></option>").attr("value", "6112").text("Peumo"));
        $('#comuna').append($("<option></option>").attr("value", "6113").text("Pichidegua"));
        $('#comuna').append($("<option></option>").attr("value", "6201").text("Pichilemu"));
        $('#comuna').append($("<option></option>").attr("value", "6308").text("Placilla"));
        $('#comuna').append($("<option></option>").attr("value", "6309").text("Pumanque"));
        $('#comuna').append($("<option></option>").attr("value", "6114").text("Quinta de Tilcoco"));
        $('#comuna').append($("<option></option>").attr("value", "6101").text("Rancagua"));
        $('#comuna').append($("<option></option>").attr("value", "6115").text("Rengo"));
        $('#comuna').append($("<option></option>").attr("value", "6116").text("Requínoa"));
        $('#comuna').append($("<option></option>").attr("value", "6301").text("San Fernando"));
        $('#comuna').append($("<option></option>").attr("value", "6117").text("San Vicente"));
        $('#comuna').append($("<option></option>").attr("value", "6310").text("Santa Cruz"));
    }
    if (this.value == '07') {
        $('option', '#comuna').remove();
        $('#comuna').append($("<option></option>").attr("value", '').text('Comuna'));
        $('#comuna').append($("<option></option>").attr("value", "7201").text("Cauquenes"));
        $('#comuna').append($("<option></option>").attr("value", "7202").text("Chanco"));
        $('#comuna').append($("<option></option>").attr("value", "7402").text("Colbún"));
        $('#comuna').append($("<option></option>").attr("value", "7102").text("Constitución"));
        $('#comuna').append($("<option></option>").attr("value", "7103").text("Curepto"));
        $('#comuna').append($("<option></option>").attr("value", "7301").text("Curicó"));
        $('#comuna').append($("<option></option>").attr("value", "7104").text("Empedrado"));
        $('#comuna').append($("<option></option>").attr("value", "7302").text("Hualañé"));
        $('#comuna').append($("<option></option>").attr("value", "7303").text("Licantén"));
        $('#comuna').append($("<option></option>").attr("value", "7401").text("Linares"));
        $('#comuna').append($("<option></option>").attr("value", "7403").text("Longaví"));
        $('#comuna').append($("<option></option>").attr("value", "7105").text("Maule"));
        $('#comuna').append($("<option></option>").attr("value", "7304").text("Molina"));
        $('#comuna').append($("<option></option>").attr("value", "7404").text("Parral"));
        $('#comuna').append($("<option></option>").attr("value", "7106").text("Pelarco"));
        $('#comuna').append($("<option></option>").attr("value", "7203").text("Pelluhue"));
        $('#comuna').append($("<option></option>").attr("value", "7107").text("Pencahue"));
        $('#comuna').append($("<option></option>").attr("value", "7305").text("Rauco"));
        $('#comuna').append($("<option></option>").attr("value", "7405").text("Retiro"));
        $('#comuna').append($("<option></option>").attr("value", "7108").text("Río Claro"));
        $('#comuna').append($("<option></option>").attr("value", "7306").text("Romeral"));
        $('#comuna').append($("<option></option>").attr("value", "7307").text("Sagrada Familia"));
        $('#comuna').append($("<option></option>").attr("value", "7109").text("San Clemente"));
        $('#comuna').append($("<option></option>").attr("value", "7406").text("San Javier"));
        $('#comuna').append($("<option></option>").attr("value", "7110").text("San Rafael"));
        $('#comuna').append($("<option></option>").attr("value", "7101").text("Talca"));
        $('#comuna').append($("<option></option>").attr("value", "7308").text("Teno"));
        $('#comuna').append($("<option></option>").attr("value", "7309").text("Vichuquén"));
        $('#comuna').append($("<option></option>").attr("value", "7407").text("Villa Alegre"));
        $('#comuna').append($("<option></option>").attr("value", "7408").text("Yerbas Buenas"));
    }
    if (this.value == '08') {
        $('option', '#comuna').remove();
        $('#comuna').append($("<option></option>").attr("value", '').text('Comuna'));
        $('#comuna').append($("<option></option>").attr("value", "8314").text("Alto Bío-Bío"));
        $('#comuna').append($("<option></option>").attr("value", "8302").text("Antuco"));
        $('#comuna').append($("<option></option>").attr("value", "8202").text("Arauco"));

        $('#comuna').append($("<option></option>").attr("value", "8303").text("Cabrero"));
        $('#comuna').append($("<option></option>").attr("value", "8203").text("Cañete"));
        $('#comuna').append($("<option></option>").attr("value", "8103").text("Chiguayante"));

        $('#comuna').append($("<option></option>").attr("value", "8101").text("Concepción"));
        $('#comuna').append($("<option></option>").attr("value", "8204").text("Contulmo"));
        $('#comuna').append($("<option></option>").attr("value", "8102").text("Coronel"));
        $('#comuna').append($("<option></option>").attr("value", "8205").text("Curanilahue"));

        $('#comuna').append($("<option></option>").attr("value", "8104").text("Florida"));
        $('#comuna').append($("<option></option>").attr("value", "8112").text("Hualpén"));
        $('#comuna').append($("<option></option>").attr("value", "8105").text("Hualqui"));
        $('#comuna').append($("<option></option>").attr("value", "8304").text("Laja"));
        $('#comuna').append($("<option></option>").attr("value", "8201").text("Lebu"));
        $('#comuna').append($("<option></option>").attr("value", "8206").text("Los Álamos"));
        $('#comuna').append($("<option></option>").attr("value", "8301").text("Los Ángeles"));
        $('#comuna').append($("<option></option>").attr("value", "8106").text("Lota"));
        $('#comuna').append($("<option></option>").attr("value", "8305").text("Mulchén"));
        $('#comuna').append($("<option></option>").attr("value", "8306").text("Nacimiento"));
        $('#comuna').append($("<option></option>").attr("value", "8307").text("Negrete"));
        $('#comuna').append($("<option></option>").attr("value", "8107").text("Penco"));

        $('#comuna').append($("<option></option>").attr("value", "8308").text("Quilaco"));
        $('#comuna').append($("<option></option>").attr("value", "8309").text("Quilleco"));

        $('#comuna').append($("<option></option>").attr("value", "8108").text("San Pedro de la Paz"));
        $('#comuna').append($("<option></option>").attr("value", "8310").text("San Rosendo"));
        $('#comuna').append($("<option></option>").attr("value", "8311").text("Santa Bárbara"));
        $('#comuna').append($("<option></option>").attr("value", "8109").text("Santa Juana"));
        $('#comuna').append($("<option></option>").attr("value", "8110").text("Talcahuano"));
        $('#comuna').append($("<option></option>").attr("value", "8207").text("Tirúa"));
        $('#comuna').append($("<option></option>").attr("value", "8111").text("Tomé"));

        $('#comuna').append($("<option></option>").attr("value", "8312").text("Tucapel"));
        $('#comuna').append($("<option></option>").attr("value", "8313").text("Yumbel"));

    }
    if (this.value == '09') {
        $('option', '#comuna').remove();
        $('#comuna').append($("<option></option>").attr("value", '').text('Comuna'));
        $('#comuna').append($("<option></option>").attr("value", "9201").text("Angol"));
        $('#comuna').append($("<option></option>").attr("value", "9102").text("Carahue"));
        $('#comuna').append($("<option></option>").attr("value", "9121").text("Cholchol"));
        $('#comuna').append($("<option></option>").attr("value", "9202").text("Collipulli"));
        $('#comuna').append($("<option></option>").attr("value", "9103").text("Cunco"));
        $('#comuna').append($("<option></option>").attr("value", "9203").text("Curacautín"));
        $('#comuna').append($("<option></option>").attr("value", "9104").text("Curarrehue"));
        $('#comuna').append($("<option></option>").attr("value", "9204").text("Ercilla"));
        $('#comuna').append($("<option></option>").attr("value", "9105").text("Freire"));
        $('#comuna').append($("<option></option>").attr("value", "9106").text("Galvarino"));
        $('#comuna').append($("<option></option>").attr("value", "9107").text("Gorbea"));
        $('#comuna').append($("<option></option>").attr("value", "9108").text("Lautaro"));
        $('#comuna').append($("<option></option>").attr("value", "9109").text("Loncoche"));
        $('#comuna').append($("<option></option>").attr("value", "9205").text("Lonquimay"));
        $('#comuna').append($("<option></option>").attr("value", "9206").text("Los Sauces"));
        $('#comuna').append($("<option></option>").attr("value", "9207").text("Lumaco"));
        $('#comuna').append($("<option></option>").attr("value", "9110").text("Melipeuco"));
        $('#comuna').append($("<option></option>").attr("value", "9111").text("Nueva Imperial"));
        $('#comuna').append($("<option></option>").attr("value", "9112").text("Padre las Casas"));
        $('#comuna').append($("<option></option>").attr("value", "9113").text("Perquenco"));
        $('#comuna').append($("<option></option>").attr("value", "9114").text("Pitrufquén"));
        $('#comuna').append($("<option></option>").attr("value", "9115").text("Pucón"));
        $('#comuna').append($("<option></option>").attr("value", "9208").text("Purén"));
        $('#comuna').append($("<option></option>").attr("value", "9209").text("Renaico"));
        $('#comuna').append($("<option></option>").attr("value", "9116").text("Saavedra"));
        $('#comuna').append($("<option></option>").attr("value", "9101").text("Temuco"));
        $('#comuna').append($("<option></option>").attr("value", "9117").text("Teodoro Schmidt"));
        $('#comuna').append($("<option></option>").attr("value", "9118").text("Toltén"));
        $('#comuna').append($("<option></option>").attr("value", "9210").text("Traiguén"));
        $('#comuna').append($("<option></option>").attr("value", "9211").text("Victoria"));
        $('#comuna').append($("<option></option>").attr("value", "9119").text("Vilcún"));
        $('#comuna').append($("<option></option>").attr("value", "9120").text("Villarrica"));
    }
    if (this.value == '10') {
        $('option', '#comuna').remove();
        $('#comuna').append($("<option></option>").attr("value", '').text('Comuna'));
        $('#comuna').append($("<option></option>").attr("value", "10202").text("Ancud"));
        $('#comuna').append($("<option></option>").attr("value", "10102").text("Calbuco"));
        $('#comuna').append($("<option></option>").attr("value", "10201").text("Castro"));
        $('#comuna').append($("<option></option>").attr("value", "10401").text("Chaitén"));
        $('#comuna').append($("<option></option>").attr("value", "10203").text("Chonchi"));
        $('#comuna').append($("<option></option>").attr("value", "10103").text("Cochamó"));
        $('#comuna').append($("<option></option>").attr("value", "10204").text("Curaco de Vélez"));
        $('#comuna').append($("<option></option>").attr("value", "10205").text("Dalcahue"));
        $('#comuna').append($("<option></option>").attr("value", "10104").text("Fresia"));
        $('#comuna').append($("<option></option>").attr("value", "10105").text("Frutillar"));
        $('#comuna').append($("<option></option>").attr("value", "10402").text("Futaleufú"));
        $('#comuna').append($("<option></option>").attr("value", "10403").text("Hualaihué"));
        $('#comuna').append($("<option></option>").attr("value", "10107").text("Llanquihue"));
        $('#comuna').append($("<option></option>").attr("value", "10106").text("Los Muermos"));
        $('#comuna').append($("<option></option>").attr("value", "10108").text("Maullín"));
        $('#comuna').append($("<option></option>").attr("value", "10301").text("Osorno"));
        $('#comuna').append($("<option></option>").attr("value", "10404").text("Palena"));
        $('#comuna').append($("<option></option>").attr("value", "10101").text("Puerto Montt"));
        $('#comuna').append($("<option></option>").attr("value", "10302").text("Puerto Octay"));
        $('#comuna').append($("<option></option>").attr("value", "10109").text("Puerto Varas"));
        $('#comuna').append($("<option></option>").attr("value", "10206").text("Puqueldón"));
        $('#comuna').append($("<option></option>").attr("value", "10303").text("Purranque"));
        $('#comuna').append($("<option></option>").attr("value", "10304").text("Puyehue"));
        $('#comuna').append($("<option></option>").attr("value", "10207").text("Queilen"));
        $('#comuna').append($("<option></option>").attr("value", "10208").text("Quellón"));
        $('#comuna').append($("<option></option>").attr("value", "10209").text("Quemchi"));
        $('#comuna').append($("<option></option>").attr("value", "10210").text("Quinchao"));
        $('#comuna').append($("<option></option>").attr("value", "10305").text("Río Negro"));
        $('#comuna').append($("<option></option>").attr("value", "10306").text("San Juan de La Costa"));
        $('#comuna').append($("<option></option>").attr("value", "10307").text("San Pablo"));
        //$('#comuna').append($("<option></option>").attr("value", "CHILOE").text("Chiloé"));
    }
    if (this.value == '11') {
        $('option', '#comuna').remove();
        $('#comuna').append($("<option></option>").attr("value", '').text('Comuna'));
        $('#comuna').append($("<option></option>").attr("value", "11201").text("Aysen"));
        $('#comuna').append($("<option></option>").attr("value", "11401").text("Chile Chico"));
        $('#comuna').append($("<option></option>").attr("value", "11202").text("Cisnes"));
        $('#comuna').append($("<option></option>").attr("value", "11301").text("Cochrane"));
        $('#comuna').append($("<option></option>").attr("value", "11101").text("Coihaique"));
        $('#comuna').append($("<option></option>").attr("value", "11203").text("Guaitecas"));
        $('#comuna').append($("<option></option>").attr("value", "11102").text("Lago Verde"));
        $('#comuna').append($("<option></option>").attr("value", "11302").text("O'Higgins"));
        $('#comuna').append($("<option></option>").attr("value", "11402").text("Río Ibáñez"));
        $('#comuna').append($("<option></option>").attr("value", "11303").text("Tortel"));
    }
    if (this.value == '12') {
        $('option', '#comuna').remove();
        $('#comuna').append($("<option></option>").attr("value", '').text('Comuna'));
        $('#comuna').append($("<option></option>").attr("value", "12202").text("Antártica"));
        $('#comuna').append($("<option></option>").attr("value", "12201").text("Cabo de Hornos"));
        $('#comuna').append($("<option></option>").attr("value", "12102").text("Laguna Blanca"));
        $('#comuna').append($("<option></option>").attr("value", "12401").text("Puerto Natales"));
        $('#comuna').append($("<option></option>").attr("value", "12301").text("Porvenir"));
        $('#comuna').append($("<option></option>").attr("value", "12302").text("Primavera"));
        $('#comuna').append($("<option></option>").attr("value", "12101").text("Punta Arenas"));
        $('#comuna').append($("<option></option>").attr("value", "12103").text("Río Verde"));
        $('#comuna').append($("<option></option>").attr("value", "12104").text("San Gregorio"));
        $('#comuna').append($("<option></option>").attr("value", "12303").text("Timaukel"));
        $('#comuna').append($("<option></option>").attr("value", "12402").text("Torres del Paine"));
    }
    if (this.value == '13') {
        $('option', '#comuna').remove();
        $('#comuna').append($("<option></option>").attr("value", '').text('Comuna'));
        $('#comuna').append($("<option></option>").attr("value", "13502").text("Alhué"));
        $('#comuna').append($("<option></option>").attr("value", "13402").text("Buin"));
        $('#comuna').append($("<option></option>").attr("value", "13403").text("Calera de Tango"));
        $('#comuna').append($("<option></option>").attr("value", "13102").text("Cerrillos"));
        $('#comuna').append($("<option></option>").attr("value", "13103").text("Cerro Navia"));
        $('#comuna').append($("<option></option>").attr("value", "13301").text("Colina"));
        $('#comuna').append($("<option></option>").attr("value", "13104").text("Conchalí"));
        $('#comuna').append($("<option></option>").attr("value", "13503").text("Curacaví"));
        $('#comuna').append($("<option></option>").attr("value", "13105").text("El Bosque"));
        $('#comuna').append($("<option></option>").attr("value", "13602").text("El Monte"));
        $('#comuna').append($("<option></option>").attr("value", "13106").text("Estación Central "));
        $('#comuna').append($("<option></option>").attr("value", "13107").text("Huechuraba"));
        $('#comuna').append($("<option></option>").attr("value", "13108").text("Independencia"));
        $('#comuna').append($("<option></option>").attr("value", "13603").text("Isla de Maipo"));
        $('#comuna').append($("<option></option>").attr("value", "13109").text("La Cisterna"));
        $('#comuna').append($("<option></option>").attr("value", "13110").text("La Florida"));
        $('#comuna').append($("<option></option>").attr("value", "13111").text("La Granja"));
        $('#comuna').append($("<option></option>").attr("value", "13112").text("La Pintana"));
        $('#comuna').append($("<option></option>").attr("value", "13113").text("La Reina"));
        $('#comuna').append($("<option></option>").attr("value", "13302").text("Lampa"));
        $('#comuna').append($("<option></option>").attr("value", "13114").text("Las Condes"));
        $('#comuna').append($("<option></option>").attr("value", "13115").text("Lo Barnechea"));
        $('#comuna').append($("<option></option>").attr("value", "13116").text("Lo Espejo"));
        $('#comuna').append($("<option></option>").attr("value", "13117").text("Lo Prado"));
        $('#comuna').append($("<option></option>").attr("value", "13118").text("Macul"));
        $('#comuna').append($("<option></option>").attr("value", "13119").text("Maipú"));
        $('#comuna').append($("<option></option>").attr("value", "13504").text("María Pinto"));
        $('#comuna').append($("<option></option>").attr("value", "13501").text("Melipilla"));
        $('#comuna').append($("<option></option>").attr("value", "13120").text("Ñuñoa"));
        $('#comuna').append($("<option></option>").attr("value", "13604").text("Padre Hurtado"));
        $('#comuna').append($("<option></option>").attr("value", "13404").text("Paine"));
        $('#comuna').append($("<option></option>").attr("value", "13121").text("Pedro Aguirre Cerda"));
        $('#comuna').append($("<option></option>").attr("value", "13605").text("Peñaflor"));
        $('#comuna').append($("<option></option>").attr("value", "13122").text("Peñalolén"));
        $('#comuna').append($("<option></option>").attr("value", "13202").text("Pirque"));
        $('#comuna').append($("<option></option>").attr("value", "13123").text("Providencia"));
        $('#comuna').append($("<option></option>").attr("value", "13124").text("Pudahuel"));
        $('#comuna').append($("<option></option>").attr("value", "13201").text("Puente Alto"));
        $('#comuna').append($("<option></option>").attr("value", "13125").text("Quilicura"));
        $('#comuna').append($("<option></option>").attr("value", "13126").text("Quinta Normal"));
        $('#comuna').append($("<option></option>").attr("value", "13128").text("Recoleta"));
        $('#comuna').append($("<option></option>").attr("value", "13128").text("Renca"));
        $('#comuna').append($("<option></option>").attr("value", "13401").text("San Bernardo"));
        $('#comuna').append($("<option></option>").attr("value", "13129").text("San Joaquín"));
        $('#comuna').append($("<option></option>").attr("value", "13203").text("San José de Maipo"));
        $('#comuna').append($("<option></option>").attr("value", "13130").text("San Miguel"));
        $('#comuna').append($("<option></option>").attr("value", "13505").text("San Pedro"));
        $('#comuna').append($("<option></option>").attr("value", "13131").text("San Ramón"));
        $('#comuna').append($("<option></option>").attr("value", "13101").text("Santiago"));
        $('#comuna').append($("<option></option>").attr("value", "13601").text("Talagante"));
        $('#comuna').append($("<option></option>").attr("value", "13303").text("Tiltil"));
        $('#comuna').append($("<option></option>").attr("value", "13132").text("Vitacura"));
    }
    if (this.value == '14') {
        $('option', '#comuna').remove();
        $('#comuna').append($("<option></option>").attr("value", '').text('Comuna'));
        $('#comuna').append($("<option></option>").attr("value", "14102").text("Corral"));
        $('#comuna').append($("<option></option>").attr("value", "14202").text("Futrono"));
        $('#comuna').append($("<option></option>").attr("value", "14201").text("La Unión"));
        $('#comuna').append($("<option></option>").attr("value", "14203").text("Lago Ranco"));
        $('#comuna').append($("<option></option>").attr("value", "14103").text("Lanco"));
        $('#comuna').append($("<option></option>").attr("value", "14104").text("Los Lagos"));
        $('#comuna').append($("<option></option>").attr("value", "14105").text("Máfil"));
        $('#comuna').append($("<option></option>").attr("value", "14106").text("Mariquina"));
        $('#comuna').append($("<option></option>").attr("value", "14107").text("Paillaco"));
        $('#comuna').append($("<option></option>").attr("value", "14108").text("Panguipulli"));
        $('#comuna').append($("<option></option>").attr("value", "14204").text("Río Bueno"));
        $('#comuna').append($("<option></option>").attr("value", "14101").text("Valdivia"));
        //$('#comuna').append($("<option></option>").attr("value", "CENTINELA").text("Centinela"));
    }
    if (this.value == '15') {
        $('option', '#comuna').remove();
        $('#comuna').append($("<option></option>").attr("value", '').text('Comuna'));
        $('#comuna').append($("<option></option>").attr("value", "15101").text("Arica"));
        $('#comuna').append($("<option></option>").attr("value", "15102").text("Camarones"));
        $('#comuna').append($("<option></option>").attr("value", "15202").text("General Lagos"));
        $('#comuna').append($("<option></option>").attr("value", "15201").text("Putre"));
    }

    if (this.value == '16') {
        $('option', '#comuna').remove();
        $('#comuna').append($("<option></option>").attr("value", '').text('Comuna'));
        $('#comuna').append($("<option></option>").attr("value", "16101").text("Chillán"));
        $('#comuna').append($("<option></option>").attr("value", "16103").text("Chillán Viejo"));
        $('#comuna').append($("<option></option>").attr("value", "16202").text("Cobquecura"));
        $('#comuna').append($("<option></option>").attr("value", "16203").text("Coelemu"));
        $('#comuna').append($("<option></option>").attr("value", "16302").text("Coihueco"));
        $('#comuna').append($("<option></option>").attr("value", "16107").text("Quillón"));
        $('#comuna').append($("<option></option>").attr("value", "16201").text("Quirihue"));
        $('#comuna').append($("<option></option>").attr("value", "16206").text("Ránquil"));
        $('#comuna').append($("<option></option>").attr("value", "16301").text("San Carlos"));
        $('#comuna').append($("<option></option>").attr("value", "16304").text("San Fabián"));
        $('#comuna').append($("<option></option>").attr("value", "16108").text("San Ignacio"));
        $('#comuna').append($("<option></option>").attr("value", "16305").text("San Nicolás"));
        $('#comuna').append($("<option></option>").attr("value", "16204").text("Ninhue"));
        $('#comuna').append($("<option></option>").attr("value", "16104").text("El Carmen"));
        $('#comuna').append($("<option></option>").attr("value", "16109").text("Yungay"));
        $('#comuna').append($("<option></option>").attr("value", "16207").text("Trehuaco"));
        $('#comuna').append($("<option></option>").attr("value", "16102").text("Bulnes"));
        $('#comuna').append($("<option></option>").attr("value", "16105").text("Pemuco"));
        $('#comuna').append($("<option></option>").attr("value", "16106").text("Pinto"));
        $('#comuna').append($("<option></option>").attr("value", "16303").text("Ñiquén"));
        $('#comuna').append($("<option></option>").attr("value", "16205").text("Portezuelo"));
    }
});