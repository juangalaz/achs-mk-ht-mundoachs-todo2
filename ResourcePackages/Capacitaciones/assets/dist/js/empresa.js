$(function () {
    $("#slider-range").slider({
        range: true,
        min: 1,
        max: 50,
        values: [1,50],
        slide: function (event, ui) {
            $("#amount").val(ui.values[0] + " hrs." + " - " + ui.values[1] + " hrs.");
        }
    });
    $("#amount").val($("#slider-range").slider("values", 0) + " hrs." +
        " - " + $("#slider-range").slider("values", 1) + " hrs.");

    
});
