var url_modalidad = '/api/capacitaciones/flat-taxa?$filter=TaxonomyId eq 743eebbe-af10-40f9-ae02-b852680fa678&$select=Title,Id';
var url_sectores = '/api/capacitaciones/flat-taxa?$filter=TaxonomyId eq 1f8afb6a-fc66-459b-a629-05140bbfe1d8&$select=Title,Id';
var url_segmento = '/api/capacitaciones/flat-taxa?$filter=TaxonomyId eq 86fedd0f-3a8d-4748-b0da-b9bcc8c58470&$select=Title,Id';
var url_areas = '/api/capacitaciones/flat-taxa?$filter=TaxonomyId eq 9cf398be-2ed5-4a7f-a5d0-7b709ee06191&$select=Title,Id';
var url_capacitaciones = '/api/capacitaciones/capacitacions?$expand=Imagen($select=Url)&$select=Nombre,UrlName,Bajada,DictadoPor,Cupos,Formatos,TipodeCapacitaciones,DuracionConFacilitador,DuracionAutoAprendizaje,Segmentos,Sectores,tiposdemodalidades,Areas';
var url_abierto_cerrado = '/api/capacitaciones/flat-taxa?$filter=TaxonomyId%20eq%200c14d388-d184-43e7-8d07-6bef40270894&$select=Title,Id';

var inicio_duracion = 0;
var fin_duracion = 100;
var array_capacitaciones = [];
var array_modalidad = [];
var array_modalidad_nombre_aux = [];
var array_sector = [];
var array_segmento = [];
var array_area = [];
var array_tipo_modalidad = [];
array_tipo_modalidad.push('51f3f154-b0cc-4196-b228-193d35a55071"');
array_tipo_modalidad.push('2422a487-acd3-40d4-99f6-7e59ee2229d7');

//array_segmento.push('4cd72e3e-0279-49fe-a9b9-6327e516a5cf');
//array_segmentos.push('7746defd-d307-467c-b3f0-f54acd5cac45');


function getCheckedBoxes(chkboxName) {
  var checkboxes = document.getElementsByName(chkboxName);
  var checkboxesChecked = [];
  // loop over them all
  for (var i = 0; i < checkboxes.length; i++) {
    // And stick the checked ones onto an array...
    if (checkboxes[i].checked) {
      checkboxesChecked.push(checkboxes[i].value);
    }
  }
  // Return the array if it is non-empty, or null
  return checkboxesChecked.length > 0 ? checkboxesChecked : [];
}


function getCheckedBoxesName(chkboxName) {
  var checkboxes = document.getElementsByName(chkboxName);
  var checkboxesChecked = [];
  // loop over them all
  for (var i = 0; i < checkboxes.length; i++) {
    // And stick the checked ones onto an array...
    if (checkboxes[i].checked) {
      var o = { nombre: checkboxes[i].dataset.nombre, id: checkboxes[i].id };

      checkboxesChecked.push(o);
    }

  }
  // Return the array if it is non-empty, or null
  return checkboxesChecked.length > 0 ? checkboxesChecked : [];
}



function arma_checkbox_area(url, id_html, name) {
  fetch(url).then((response) => {
    if (response.ok) {
      return response.json();
    } else {
      throw new Error('Something went wrong');
    }
  })
    .then((responseJson) => {
      data = responseJson.value;
      data = data.sort(compareValues('Title'));
      let html_check = '';
      data.forEach(function (item, i) {
        let id = item.Id;
        let title = item.Title;
        let html_check_aux = `<div><input id='id${id}' data-nombre='${title}' input type="checkbox" name="${name}" value="${id}"><label for="${name}">${title}</label></div>`;
        html_check = html_check + html_check_aux;

      });
      let htmlObject = document.getElementById(id_html);
      htmlObject.innerHTML = html_check;

      var checkbox = document.querySelectorAll(`input[type=checkbox][name="${name}"]`);
      checkbox.forEach(checkbox => checkbox.addEventListener('change', () => buscar_capacitaciones()));


    })
    .catch((error) => {
      console.log(error)
    });


}

function arma_checkbox_abierto_cerrado(url, id_html, name) {
  fetch(url).then((response) => {
    if (response.ok) {
      return response.json();
    } else {
      throw new Error('Something went wrong');
    }
  })
    .then((responseJson) => {
      data = responseJson.value;
      data = data.sort(compareValues('Title'));
      let html_check = '';
      data.forEach(function (item, i) {
        let id = item.Id;
        let title = item.Title;
        let html_check_aux = `<div><input id='id${id}' data-nombre='${title}' input type="checkbox" name="${name}" value="${id}"><label for="${name}">${title}</label></div>`;
        html_check = html_check + html_check_aux;

      });
      let htmlObject = document.getElementById(id_html);
      htmlObject.innerHTML = html_check;

      var checkbox = document.querySelectorAll(`input[type=checkbox][name="${name}"]`);
      checkbox.forEach(checkbox => checkbox.addEventListener('change', () => buscar_capacitaciones()));


    })
    .catch((error) => {
      console.log(error)
    });


}


function arma_checkbox_segmento(url, id_html, name) {
  fetch(url).then((response) => {
    if (response.ok) {
      return response.json();
    } else {
      throw new Error('Something went wrong');
    }
  })
    .then((responseJson) => {
      data = responseJson.value;
      data = data.sort(compareValues('Title'));
      let html_check = '';
      data.forEach(function (item, i) {
        let id = item.Id;
        let title = item.Title;
        let html_check_aux = `<div><input id='id${id}' data-nombre='${title}' input type="checkbox" name="${name}" value="${id}"><label for="${name}">${title}</label></div>`;
        html_check = html_check + html_check_aux;

      });
      let htmlObject = document.getElementById(id_html);
      htmlObject.innerHTML = html_check;

      var checkbox = document.querySelectorAll(`input[type=checkbox][name="${name}"]`);
      checkbox.forEach(checkbox => checkbox.addEventListener('change', () => buscar_capacitaciones()));


    })
    .catch((error) => {
      console.log(error)
    });


}


function arma_checkbox_sector(url, id_html, name) {
  fetch(url).then((response) => {
    if (response.ok) {
      return response.json();
    } else {
      throw new Error('Something went wrong');
    }
  })
    .then((responseJson) => {
      data = responseJson.value;
      data = data.sort(compareValues('Title'));
      let html_check = '';
      data.forEach(function (item, i) {
        let id = item.Id;
        let title = item.Title;
        let html_check_aux = `<div><input id='id${id}' data-nombre='${title}' type="checkbox" name="${name}" value="${id}"><label for="${name}">${title}</label></div>`;
        html_check = html_check + html_check_aux;

      });
      let htmlObject = document.getElementById(id_html);
      htmlObject.innerHTML = html_check;

      var checkbox = document.querySelectorAll(`input[type=checkbox][name="${name}"]`);
      checkbox.forEach(checkbox => checkbox.addEventListener('change', () => buscar_capacitaciones()));


    })
    .catch((error) => {
      console.log(error)
    });


}



function arma_checkbox_modalidad(url, id_html, name) {
  fetch(url).then((response) => {
    if (response.ok) {
      return response.json();
    } else {
      throw new Error('Something went wrong');
    }
  })
    .then((responseJson) => {
      data = responseJson.value;
      data = data.sort(compareValues('Title'));
      array_modalidad_nombre_aux = data;
      let html_check = '';
      data.forEach(function (item, i) {
        let id = item.Id;
        let title = item.Title;
        let color_punto = '';


        if (id == '7746defd-d307-467c-b3f0-f54acd5cac45') {
          color_punto = 'img_punto_morado.svg';

        } else if (id == '125f5dc1-14ef-4c85-98a9-e676f245ff1d') {
          color_punto = 'img_punto_rojo.svg';

        } else if (id == '1e6b727c-258d-4aa2-b7c5-b4fd034e6f68') {
          color_punto = 'img_punto_lila.svg';

        } else if (id == '201733fb-cf8e-4a81-8fe3-cce32ff7271a') {
          color_punto = 'img_punto_amarillo.svg';

        } else {
          color_punto = 'img_punto_rojo.svg';
        }






        let html_check_aux = `<div class="d-flex justify-content-between">
                                            <input type="checkbox"  id='id${id}' data-nombre='${title}' name="${name}" value="${id}">
                                            <label for="presencial">${title}</label>
                                            <img src="/ResourcePackages/Capacitaciones/assets/dist/images/${color_punto}" alt="">
                                        </div>`;
        html_check = html_check + html_check_aux;

      });
      let htmlObject = document.getElementById(id_html);
      htmlObject.innerHTML = html_check;

      var checkbox = document.querySelectorAll(`input[type=checkbox][name="${name}"]`);
      checkbox.forEach(checkbox => checkbox.addEventListener('change', () => buscar_capacitaciones()));
      //checkbox.forEach(checkbox => checkbox.addEventListener('click', () => click_checkbox(this)));

    })
    .catch((error) => {
      console.log(error)
    });


}


function click_checkbox() {
  console.log(this);
}

function compareValues(key, order = 'asc') {
  return function innerSort(a, b) {
    if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
      // property doesn't exist on either object
      return 0;
    }

    const varA = (typeof a[key] === 'string')
      ? a[key].toUpperCase() : a[key];
    const varB = (typeof b[key] === 'string')
      ? b[key].toUpperCase() : b[key];

    let comparison = 0;
    if (varA > varB) {
      comparison = 1;
    } else if (varA < varB) {
      comparison = -1;
    }
    return (
      (order === 'desc') ? (comparison * -1) : comparison
    );
  };
}


function filterArray(array, filters) {
  const filterKeys = Object.keys(filters);
  return array.filter(item => {
    // validates all filter criteria
    return filterKeys.every(key => {
      // ignores non-function predicates
      if (typeof filters[key] !== 'function') return true;
      return filters[key](item[key]);
    });
  });
}



function getFilteredByKey(array, key, value) {
  return array.filter(function (e) {
    return e[key] == value;
  });
}




function arma_html_capacitaciones(item) {



  //console.log(array_capacitaciones.length);

  let nombre = '';
  let color = '';
  let id = item.formatos[0];

  let modalidad = getFilteredByKey(array_modalidad_nombre_aux, 'Id', id);

  let html_cupos = '';
  if (item.Cupos != null) {

    html_cupos = `<div class="cupones-cursos-destacados d-flex">
                                        <img src="/ResourcePackages/Capacitaciones/assets/dist/images/img_cupos.svg" alt="">
                                        <p>${item.Cupos} cupos</p>
                                    </div>`;
  }


  let s = item.Nombre.toLowerCase();
  if (typeof s !== 'string') s = '';
  s = s.charAt(0).toUpperCase() + s.slice(1);
  if (s.length > 55) s = s.substring(0, 55) + '...';

  let d = item.Bajada;
  if (typeof d !== 'string') d = '';
  if (d.length > 120) d = d.substring(0, 120) + '...';
s = item.Nombre;
  let html_capacitaciones = `             <div class="col-sm-12 col-md-12 col-lg-6 col-xl-4 card-curso media">
           <a target="_blank" href="/capacitaciones/${item.UrlName}">
                            <div class="ficha-cursos-destacados">

                                <div class="img-curso-destacado">
                                    <div class="etiqueta-curso-presencial c-${id}">
                                       ${modalidad[0].Title}
                                    </div>
                                    <!--<img src="${item.Imagen[0].Url}" alt="${item.Nombre}">-->
                                </div>

                                <div class="info-curso-destacado">
                                    <div class="curso">
                                        <h3>${s}</h3>
                                    </div>
                                    
                                    <div class="descripcion">
                                        <p>${d}
                                        </p>
                                        <p> <i class="far fa-clock mr-2"></i><strong>Duración: </strong>${item.DuracionAutoAprendizaje} horas</p>
                                    </div>
                                    ${html_cupos}
                                </div>

                                <div class="link-cursos-destacados">
                                    <a target="_blank" href="/capacitaciones/${item.UrlName}">Conoce más</a>
                                </div>
                            </div>
                            </a>
                        </div>
                        `;
  return html_capacitaciones;
}

function buscar_capacitaciones() {

  //var a = getFilteredByKey(array_capacitaciones,'formatos','1e6b727c-258d-4aa2-b7c5-b4fd034e6f68' );
  //let filterResult = array_capacitaciones.filter(u => u.formatos == '1e6b727c-258d-4aa2-b7c5-b4fd034e6f68' || u.formatos == '7746defd-d307-467c-b3f0-f54acd5cac45');
  /*const filters = {
    formatos: formatos => formatos === '1e6b727c-258d-4aa2-b7c5-b4fd034e6f68' || formatos === '7746defd-d307-467c-b3f0-f54acd5cac45'
  };*/

  let array_modalidad_nombre1 = getCheckedBoxesName('segmento');

  let html_tag1 = '';
  for (let i = 0; i < array_modalidad_nombre1.length; i++) {

    let tag1 = `<div class="alert alert-secondary alert-dismissible fade show" role="alert">
                                            ${array_modalidad_nombre1[i].nombre}
                                            <button onclick="eliminar_tag('${array_modalidad_nombre1[i].id}')" type="button" data-guid='${array_modalidad_nombre1[i].id}' id="id${array_modalidad_nombre1[i].id}"  class="close" data-dismiss="alert" aria-label="Close">
                                                <img src="/ResourcePackages/Capacitaciones/assets/dist/images/img_cerrar_filtro.svg" alt="">
                                            </button>
                                        </div>`;

    html_tag1 = html_tag1 + tag1;
    //document.getElementById('id'+array_modalidad_nombre[0].id).addEventListener("click", eliminar_tag(array_modalidad_nombre[0].id));

  }


  let array_modalidad_nombre2 = getCheckedBoxesName('modalidad');
  let html_tag2 = '';
  for (let i = 0; i < array_modalidad_nombre2.length; i++) {
    let tag2 = `<div class="alert alert-secondary alert-dismissible fade show" role="alert">
                                            ${array_modalidad_nombre2[i].nombre}
                                            <button onclick="eliminar_tag('${array_modalidad_nombre2[i].id}')" type="button" data-guid='${array_modalidad_nombre2[i].id}' id="id${array_modalidad_nombre2[i].id}"  class="close" data-dismiss="alert" aria-label="Close">
                                                <img src="/ResourcePackages/Capacitaciones/assets/dist/images/img_cerrar_filtro.svg" alt="">
                                            </button>
                                        </div>`;

    html_tag2 = html_tag2 + tag2;
    //document.getElementById('id'+array_modalidad_nombre[0].id).addEventListener("click", eliminar_tag(array_modalidad_nombre[0].id));
  }



  let array_modalidad_nombre3 = getCheckedBoxesName('sectores');
  let html_tag3 = '';
  for (let i = 0; i < array_modalidad_nombre3.length; i++) {
    let tag3 = `<div class="alert alert-secondary alert-dismissible fade show" role="alert">
                                            ${array_modalidad_nombre3[i].nombre}
                                            <button onclick="eliminar_tag('${array_modalidad_nombre3[i].id}')" type="button" data-guid='${array_modalidad_nombre3[i].id}' id="id${array_modalidad_nombre3[i].id}"  class="close" data-dismiss="alert" aria-label="Close">
                                                <img src="/ResourcePackages/Capacitaciones/assets/dist/images/img_cerrar_filtro.svg" alt="">
                                            </button>
                                        </div>`;

    html_tag3 = html_tag3 + tag3;
    //document.getElementById('id'+array_modalidad_nombre[0].id).addEventListener("click", eliminar_tag(array_modalidad_nombre[0].id));
  }



  let array_modalidad_nombre4 = getCheckedBoxesName('areas');
  let html_tag4 = '';
  for (let i = 0; i < array_modalidad_nombre4.length; i++) {
    let tag4 = `<div class="alert alert-secondary alert-dismissible fade show" role="alert">
                                            ${array_modalidad_nombre4[i].nombre}
                                            <button onclick="eliminar_tag('${array_modalidad_nombre4[i].id}')" type="button" data-guid='${array_modalidad_nombre4[i].id}' id="id${array_modalidad_nombre4[i].id}"  class="close" data-dismiss="alert" aria-label="Close">
                                                <img src="/ResourcePackages/Capacitaciones/assets/dist/images/img_cerrar_filtro.svg" alt="">
                                            </button>
                                        </div>`;

    html_tag4 = html_tag4 + tag4;
    //document.getElementById('id'+array_modalidad_nombre[0].id).addEventListener("click", eliminar_tag(array_modalidad_nombre[0].id));
  }





  let filters1 = {};
  array_segmento = getCheckedBoxes('segmento');
  if (array_segmento.length > 0) {
    filters1 = { segmentos: segmentos => segmentos.find(x => array_segmento.includes(x)) };
  }


  let filters0 = { tiposdemodalidades: tiposdemodalidades => tiposdemodalidades.find(x => array_tipo_modalidad.includes(x)) };
  //console.log(filters0);

  let filters2 = {};
  array_modalidad = getCheckedBoxes('modalidad');
  if (array_modalidad.length > 0) {
    filters2 = {
      formatos: formatos => formatos.find(x => array_modalidad.includes(x))
    };
  }


  let filters3 = {};
  array_sector = getCheckedBoxes('sectores');
  if (array_sector.length > 0) {
    filters3 = {
      sectores: sectores => sectores.find(x => array_sector.includes(x))
    };
  }



  let filters6 = {};
  array_area = getCheckedBoxes('areas');
  if (array_area.length > 0) {
    filters6 = {
      areas: areas => areas.find(x => array_area.includes(x))
    };
  }


  let filters7 = {};
  array_abierto_cerrado = getCheckedBoxes('abierto_cerrado');
  if (array_abierto_cerrado.length > 0) {
    filters7 = {
      tiposdemodalidades: tiposdemodalidades => tiposdemodalidades.find(x => array_abierto_cerrado.includes(x))
    };
  }


  let filters4 = { DuracionAutoAprendizaje: DuracionAutoAprendizaje => DuracionAutoAprendizaje >= inicio_duracion && DuracionAutoAprendizaje <= fin_duracion };


  const filters = { ...filters1, ...filters2, ...filters3, ...filters4, ...filters6, ...filters7 }

  const filterResult = filterArray(array_capacitaciones, filters);
  let html_capacitaciones = '';
  filterResult.forEach(function (item, i) {
    let html_capacitaciones_aux = arma_html_capacitaciones(item);
    html_capacitaciones = html_capacitaciones + html_capacitaciones_aux;

  });

  let htmlObjectTag = document.getElementById('id_tag');
  htmlObjectTag.innerHTML = html_tag1 + html_tag2 + html_tag3 + html_tag4;



  let htmlObject = document.getElementById('id_capacitaciones');
  htmlObject.innerHTML = html_capacitaciones;



  //loadmore();

  var elmnt = document.getElementById("id_filtros");
  elmnt.scrollIntoView();


}



function uncheckAll(divid) {
  var checks = document.querySelectorAll('#' + divid + ' input[type="checkbox"]');
  for (var i = 0; i < checks.length; i++) {
    var check = checks[i];
    if (!check.disabled) {
      check.checked = false;
    }
  }


  var url_string = window.location.href;
  var url = new URL(url_string);
  var b = url.searchParams.get("b");


  if (b != null) {
    window.location.href = " /buscar-capacitaciones";


  } else {

    buscar_capacitaciones();



  }


}

function eliminar_tag(guid) {
  // console.log(guid);
  document.getElementById(guid).checked = false;
  buscar_capacitaciones();
}

function cargar_capacitaciones() {


  arma_checkbox_modalidad(url_modalidad, 'id_modalidad', 'modalidad');
  arma_checkbox_segmento(url_segmento, 'id_segmento', 'segmento');
  arma_checkbox_sector(url_sectores, 'id_sectores', 'sectores');

  arma_checkbox_area(url_areas, 'id_areas', 'areas');
  arma_checkbox_abierto_cerrado (url_abierto_cerrado, 'id_abierto_cerrado', 'abierto_cerrado');

  var url_string = window.location.href;
  var url = new URL(url_string);
  var b = url.searchParams.get("b");
  //console.log(url_string);
  if (b != null) {
    let url_like = `&$filter=contains(Nombre,'${b}')`;
    url_capacitaciones = url_capacitaciones + url_like;
    // console.log(url_capacitaciones);
    document.getElementById('txt_search').value = b;


  }


  fetch(url_capacitaciones).then((response) => {
    if (response.ok) {
      return response.json();
    } else {
      throw new Error('Something went wrong');
    }
  })
    .then((responseJson) => {
      array_capacitaciones = responseJson.value;

      array_capacitaciones = array_capacitaciones.sort(compareValues('Nombre'));

      for (var i in array_capacitaciones) {
        array_capacitaciones[i].buscar=array_capacitaciones[i].Nombre;
        if (array_capacitaciones[i].DuracionAutoAprendizaje == 0) {
          array_capacitaciones[i].DuracionAutoAprendizaje = array_capacitaciones[i].DuracionConFacilitador;
        }

      }


      console.log(array_capacitaciones);
      if (b == null) {
        let html_capacitaciones = '';
        array_capacitaciones.forEach(function (item, i) {

          let html_capacitaciones_aux = arma_html_capacitaciones(item);
          html_capacitaciones = html_capacitaciones + html_capacitaciones_aux;

        });
        let htmlObject = document.getElementById('id_capacitaciones');
        htmlObject.innerHTML = html_capacitaciones;

      } else {
        //console.log(b);
        //document.getElementById("id125f5dc1-14ef-4c85-98a9-e676f245ff1d").checked = true;
        buscar_capacitaciones();
      }

    })
    .catch((error) => {
      console.log(error)
    });
}



function getFilteredByKey(array, key, value) {
  return array.filter(function (e) {
    return e[key] == value;
  });
}


//captura de valores al seleccionar el rango
$("#slider-range").on("slidechange", function (event, ui) {
  //console.log(ui.values[0])
  //console.log(ui.values[1])
  inicio_duracion = ui.values[0];
  fin_duracion = ui.values[1];
  buscar_capacitaciones();

});

////https://gist.github.com/jherax/f11d669ba286f21b7a2dcff69621eb72
///https://knowledgebase.progress.com/articles/Article/Remove-OData-limit-for-50-records-per-page

// window.addEventListener('load', function () {})
cargar_capacitaciones();

var elementList = document.getElementById('list');
var elementListContent = document.getElementById('listcontent');
var elementButtonMore = document.getElementById('loadmore');
var heightMedia = document.getElementsByClassName('card-curso');


var height = window.innerHeight;

function loadmore() {
  if (elementList) {
    var heightMediaOff = 511 + 50;
    //console.log(heightMediaOff);
    if (height <= 823) {
      //Celular
      elementList.style.height = ((heightMediaOff * 2) + 50) + 'px';
      console.log("1");
      // 7 cards
    } else if (height <= 1366) {
      //Tablet
      elementList.style.height = ((heightMediaOff * 15) + 50) + 'px';
      console.log("2");
      // 14 cards
    } else if (height <= 1920) {
      //Desk
      elementList.style.height = ((heightMediaOff * 22) + 50) + 'px';
      console.log("3");
      // 21 cards
    } else {
      //Mayor
      elementList.style.height = 'auto';
      console.log("4");
      // todas
    }

  }

  setTimeout(function () {


    if (elementList) {
      elementList.classList.remove('d-none');
    }

    if (document.getElementById('loadmore')) {
      elementButtonMore.classList.remove('d-none');
      elementButtonMore.classList.add('d-flex');
    } else { }

  }, 1000);
}

window.onload = function () {

  //loadmore();

}

/*
window.onscroll = function () {
  var tester = document.getElementById('loadmore');
  var heightMedia = document.getElementsByClassName('media');

  if (elementList) {

    var heightMediaOff = 511;

    var heightList = elementList.clientHeight;
    var heightOne = elementList.children[0].clientHeight;

    console.log(heightList);
    var maxheight = (heightOne);

    if (heightList < maxheight) {
      setTimeout(function () {
        checkVisible(tester) ? elementList.style.height = (heightList + ((heightMediaOff + 16) * 3)) + 'px' : '';
      }, 1000);
    } else {
      elementButtonMore.classList.remove('d-flex');
      elementButtonMore.classList.add('d-none');
    }

  }
};

*/

function checkVisible(elm) {
  var rect = elm.getBoundingClientRect();
  var viewHeight = Math.max(document.documentElement.clientHeight, window.innerHeight);
  return !(rect.bottom < 0 || rect.top - viewHeight >= 0);
}







