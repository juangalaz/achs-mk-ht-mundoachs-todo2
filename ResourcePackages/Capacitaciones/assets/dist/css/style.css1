@font-face {
    font-family: 'Catamaran-Bold';
    src: url('../css/font/Catamaran-Bold.ttf');
}

@font-face {
    font-family: 'Catamaran-Regular';
    src: url('../css/font/Catamaran-Regular.ttf');
}

@font-face {
    font-family: 'Barlow-Bold';
    src: url('../css/font/Barlow-Bold.ttf');
}

@font-face {
    font-family: 'Barlow-Regular';
    src: url('../css/font/Barlow-Regular.ttf');
}

body {
    font-family: 'Catamaran-Regular';
}

a {
    display: inline-block;
    text-decoration: none !important;
}

a:hover, button:hover {
    filter: opacity(0.7);
}

p {
    margin-bottom: 0;
}

button {
    background-color: transparent;
    border: none;
}

:focus {
    outline: transparent;
}

.col {
    flex-grow: 0;
}

.gris-menu {
    background-color: #f2f2f2;
}

.azul {
    background-color: #00B2A9;
}

.verde {
    background-color: #007A33;
}

html {
    scroll-behavior: smooth;
}

.seccion-cabecera .logo-achs a {
    cursor: pointer;
}

.seccion-cabecera .logo-achs img {
    padding: 17px 0px;
}

.seccion-cabecera .col-md-6 .informacion-achs {
    display: flex;
    justify-content: space-between;
    align-items: center;
}

.seccion-cabecera .col-md-6 .informacion-achs a {
    color: #787878 !important;
}

.seccion-cabecera .col-md-6 .informacion-achs .buscador {
    border-left: 1px solid #DEDEDE;
    border-right: 1px solid #DEDEDE;
}

.seccion-cabecera .col-md-6 .informacion-achs .buscador button {
    padding: 41px 17px;
}

nav .menu-capacitacion {
    justify-content: space-between;
}

nav.nav-capacitacion.navbar {
    padding: 0;
}

nav.nav-capacitacion .navbar-nav .nav-link {
    padding: .82rem 1rem;
}

nav.nav-capacitacion .nav-botonera-izquierda .nav-link {
    color: #373737;
    font-size: 16px;
}

nav.nav-capacitacion .nav-botonera-derecha .nav-link {
    color: #FFFFFF !important;
    font-size: 16px;
    font-family: 'Barlow-Bold';
}

.seccion-banner-buscador {
    background-image: url(../images/img_banner_capacitacion.png);
    background-repeat: no-repeat;
    background-size: cover;
    padding: 50px 0px;
}

.seccion-banner-buscador h1 {
    font-family: 'Catamaran-Bold';
    font-size: 40px;
    color: #FFFFFF;
}

.seccion-banner-buscador .div-buscador {
    display: flex;
    height: 50px;
    width: 361px;
    background-color: #FFFFFF;
    border-radius: 32px;
    justify-content: space-between;
    margin: auto;
    align-items: center;
    padding: 0px 20px;
}

.seccion-banner-buscador .div-buscador input {
    width: 275px;
    color: #373737;
    font-size: 16px;
    border: none;
}

.list-group.sticky-top {
    background-color: #F2F2F2;
}

.menu-pegagoso {
    display: flex;
    justify-content: center;
}

.menu-pegagoso .list-group-item.active {
    color: #007A33;
    background-color: transparent;
    border-bottom: 4px solid;
    font-family: 'Catamaran-Bold';
}

.menu-pegagoso .list-group-item {
    border-radius: 0;
    padding: .875rem 0rem;
    margin: 0px 22.5px;
    color: #373737;
    font-size: 16px;
    text-transform: uppercase;
    background-color: transparent;
    border: 1px solid transparent;
}

.menu-pegagoso .list-group-item-action {
    width: auto;
}

.seccion-nuestros-cursos {
    padding: 44px 0px;
}

.seccion-nuestros-cursos h2, .seccion-cursos-lograras h2, .seccion-cursos-destacados h2, .seccion-noticias-capacitaciones h2, .seccion-preguntas-frecuentes h2 {
    font-size: 25px;
    color: #000000;
    font-family: 'Catamaran-Bold';
    text-align: center;
    margin-bottom: 44px;
}

.seccion-nuestros-cursos .ficha-conoce-nuestros-cursos {
    background-color: #FFFFFF;
    box-shadow: 4px 4px 30px rgba(0, 43, 41, 0.15);
    border-radius: 10px;
    padding: 30px;
    margin-bottom: 20px;
    display: flex;
    flex-direction: column;
    align-items: center;
    width: 210px;
    height: 195px;
}

.seccion-nuestros-cursos .ficha-conoce-nuestros-cursos img {
    margin-bottom: 20px;
}

.seccion-nuestros-cursos .ficha-conoce-nuestros-cursos h3 {
    font-size: 20px;
    text-align: center;
    color: #373737;
    font-family: 'Catamaran-Bold';
}

.seccion-cursos-lograras {
    background-color: #F4F4F4;
    padding: 44px 0px;
}

.seccion-cursos-lograras .ficha-lograras {
    display: flex;
    align-items: center;
    margin-bottom: 50px;
}

.seccion-cursos-lograras .ficha-lograras img {
    margin-right: 16px;
}

.seccion-cursos-destacados {
    padding: 67px 0px;
}

.ficha-cursos-destacados {
    background: #FFFFFF;
    box-shadow: 4px 4px 30px rgba(0, 43, 41, 0.15);
    border-radius: 10px;
    height: 491px;
    margin-bottom: 20px;
    max-width: 349px;
}

.seccion-cursos-disponibles .ficha-cursos-destacados{
    width: 349px;
}

.seccion-cursos-destacados h4 {
    font-size: 20px;
    color: #000000;
    text-align: center;
    margin-bottom: 47px;
}

.ficha-cursos-destacados .img-curso-destacado {
    position: relative;
    height: 162px;
    overflow: hidden;
}

.ficha-cursos-destacados .img-curso-destacado img {
    width: 100%;
}

.ficha-cursos-destacados .img-curso-destacado .etiqueta-curso-presencial,
.seccion-cursos-detalle .etiqueta-curso-presencial {
    position: absolute;
    background-color: #DE172E;
    border-radius: 4px;
    color: #fff;
    width: 121px;
    height: 22px;
    font-size: 12px;
    text-transform: uppercase;
    letter-spacing: 0.3em;
    top: 16px;
    right: 8px;
    align-items: center;
    display: flex;
    justify-content: center;
}

.ficha-cursos-destacados .img-curso-destacado .etiqueta-curso-elearnig, .seccion-cursos-detalle .img-curso-detalle .etiqueta-curso-elearnig {
    position: absolute;
    background-color: #722573;
    border-radius: 4px;
    color: #fff;
    width: 121px;
    height: 22px;
    font-size: 12px;
    text-transform: uppercase;
    letter-spacing: 0.3em;
    top: 16px;
    right: 8px;
    align-items: center;
    display: flex;
    justify-content: center;
}

.ficha-cursos-destacados .img-curso-destacado .etiqueta-curso-presencial-dos {
    position: absolute;
    background-color: #8F98FF;
    border-radius: 4px;
    color: #fff;
    width: 121px;
    height: 22px;
    font-size: 12px;
    text-transform: uppercase;
    letter-spacing: 0.3em;
    top: 16px;
    right: 8px;
    align-items: center;
    display: flex;
    justify-content: center;
}

.ficha-cursos-destacados .img-curso-destacado .etiqueta-curso-trailer {
    position: absolute;
    background-color: #F2A100;
    border-radius: 4px;
    color: #fff;
    width: 121px;
    height: 22px;
    font-size: 12px;
    text-transform: uppercase;
    letter-spacing: 0.3em;
    top: 16px;
    right: 8px;
    align-items: center;
    display: flex;
    justify-content: center;
}

.ficha-cursos-destacados .info-curso-destacado {
    padding: 18px 23px;
    border-bottom: 1px solid #C4C4C4;
    height: 269px;
}

.ficha-cursos-destacados .info-curso-destacado .curso h3 {
    font-size: 20px;
    color: #373737;
    font-family: 'Catamaran-Bold';
    margin-bottom: 10px;
}

.ficha-cursos-destacados .info-curso-destacado .dictado p {
    font-size: 16px;
    line-height: 18px;
    color: #373737;
    margin-bottom: 21px;
}

.ficha-cursos-destacados .info-curso-destacado .descripcion p {
    font-size: 14px;
    line-height: 23px;
    color: #787878;
    margin-bottom: 19px;
}

.ficha-cursos-destacados .cupones-cursos-destacados img {
    margin-right: 6px;
}

.ficha-cursos-destacados .cupones-cursos-destacados p {
    color: #373737;
}

.ficha-cursos-destacados .link-cursos-destacados a {
    font-family: 'Catamaran-Bold';
    font-size: 16px;
    color: #007A33;
    text-decoration-line: underline !important;
    padding: 14px 23px;
}

.seccion-cursos-destacados .btn-ver-cursos a, .seccion-preguntas-frecuentes .btn-comunicate button {
    background-color: #007934;
    border-radius: 4px;
    font-size: 16px;
    color: #FFFFFF !important;
    font-family: 'Catamaran-Bold';
    padding: 13px 29px;
    margin-top: 56px;
}

.seccion-unete-achs {
    background-image: url(../images/img_unete_achs.png);
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center;
    padding: 70px 0px;
}

.seccion-unete-achs p {
    color: #FFFFFF;
    text-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
    font-size: 32px;
    font-family: 'Catamaran-Bold';
    margin: 5px 25px;
}

.seccion-unete-achs a {
    background: #2DB300;
    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
    border-radius: 4px;
    color: #FFFFFF !important;
    font-size: 16px;
    font-family: 'Catamaran-Bold';
    margin: 5px 25px;
    padding: 10px 56px;
}

.seccion-noticias-capacitaciones {
    padding: 56px 0px;
    background-color: #F4F4F4;
}

.seccion-noticias-capacitaciones .fichas-noticias-capacitacion {
    background-color: #FFFFFF;
    box-shadow: 4px 4px 30px rgba(0, 43, 41, 0.15);
    border-radius: 10px;
    display: flex;
    justify-content: center;
    padding: 44px;
    margin-bottom: 35px;
}

.seccion-noticias-capacitaciones .fichas-noticias-capacitacion img {
    width: 200px;
    height: 160px;
    margin-right: 32px;
}

.seccion-noticias-capacitaciones .fichas-noticias-capacitacion h3 {
    font-size: 20px;
    color: #373737;
    line-height: 1.3;
}

.seccion-noticias-capacitaciones .fichas-noticias-capacitacion a {
    text-decoration-line: underline !important;
    color: #007A33 !important;
    font-size: 16px;
    font-family: 'Catamaran-Bold';
}

.seccion-preguntas-frecuentes {
    padding: 68px 0px;
}

.seccion-modalidad-cursos .modalidades {
    display: flex;
    align-items: center;
    border-bottom: 1px solid #DEDEDE;
    justify-content: space-between;
    padding: 80px 0px 27px;
}

.seccion-modalidad-cursos h1, .seccion-cursos-detalle .titular h1 {
    font-family: 'Catamaran-Bold';
    font-size: 32px;
    color: #373737;
}

.seccion-modalidad-cursos h4, .seccion-cursos-detalle .titular h4 {
    font-size: 20px;
    line-height: 1.5;
    color: #787878;
}

.seccion-modalidad-cursos .ancla-volver-inicio, .seccion-cursos-disponibles .ancla-volver-inicio {
    display: flex;
    justify-content: right;
}

.seccion-modalidad-cursos .ancla-volver-inicio a, .seccion-cursos-disponibles .ancla-volver-inicio a, .seccion-cursos-detalle .ancla-volver-inicio a {
    text-decoration-line: underline !important;
    color: #007A33;
    font-size: 18px;
    font-family: 'Catamaran-Bold';
}

.seccion-modalidad-cursos .fichas-cursos {
    padding: 36px 0px;
    border-bottom: 1px solid #DEDEDE;
}

.seccion-modalidad-cursos .fichas-cursos p {
    font-size: 20px;
    color: #373737;
    font-family: 'Catamaran-Bold';
    padding-top: 36px;
}

.seccion-modalidad-cursos .fichas-cursos .texto-rojo h2,
.seccion-cursos-detalle .info-curso-detalle .texto-rojo h2{
    font-size: 25px;
    color: #DE172E;
    font-family: 'Catamaran-Bold';
    margin-bottom: 0px;
}

.seccion-modalidad-cursos .fichas-cursos .texto-lila h2 {
    font-size: 25px;
    color: #8F98FF;
    font-family: 'Catamaran-Bold';
    margin-bottom: 0px;
}

.seccion-modalidad-cursos .fichas-cursos .texto-amarillo h2 {
    font-size: 25px;
    color: #F2A100;
    font-family: 'Catamaran-Bold';
    margin-bottom: 0px;
}

.seccion-modalidad-cursos .fichas-cursos img {
    margin-right: 13px;
}

.seccion-modalidad-cursos .fichas-cursos h4 {
    padding-top: 16px;
}

.seccion-modalidad-cursos .fichas-cursos .btn-ir-a-curso {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: flex-end;
}

.seccion-modalidad-cursos .fichas-cursos .btn-ir-a-curso a,
.seccion-cursos-detalle .ficha-solicitar-curso .btn-ir-a-curso a,
.seccion-cursos-detalle .ficha-solicitar-curso .btn-ir-a-curso button {
    width: 199px;
    height: 50px;
    border-radius: 4px;
    color: #FFFFFF;
    font-size: 16px;
    font-family: 'Catamaran-Bold';
    text-align: center;
    padding: 12px 0px;
}

.seccion-modalidad-cursos .fichas-cursos .btn-ir-a-curso.btn-rojo button {
    background-color: #DE172E;
}

.seccion-modalidad-cursos .fichas-cursos .btn-ir-a-curso.btn-lila a {
    background-color: #8F98FF;
}

.seccion-modalidad-cursos .fichas-cursos .btn-ir-a-curso.btn-amarillo a {
    background-color: #F2A100;
}

.seccion-cursos-disponibles h1 {
    font-size: 32px;
    color: #000000;
    font-family: 'Catamaran-Bold';
    margin-top: 0;
}

.seccion-cursos-disponibles h4 {
    color: #787878;
    font-size: 20px;
}

.seccion-cursos-disponibles .ficha-cursos-destacados {
    margin-left: auto;
    margin-right: auto;
}

.seccion-cursos-disponibles .acordeon-menu button.collapsed img {
    transform: rotatex(180deg);
}

.seccion-cursos-disponibles .acordeon-menu {
    max-width: 376px;
    height: 100%;
}

.seccion-cursos-disponibles .acordeon-menu, .seccion-cursos-disponibles .acordeon-menu .card, .seccion-cursos-disponibles .acordeon-menu .card .card-header {
    background-color: #F4F4F4;
}

.seccion-cursos-disponibles .acordeon-menu .card {
    margin: 0px 43px;
    padding-top: 44px;
}

.seccion-cursos-disponibles .acordeon-menu .card-header {
    border-bottom: 1px solid transparent;
    padding: 0;
}

.seccion-cursos-disponibles .acordeon-menu .card-body {
    padding: 0px 0px 26px 0px;
}

.seccion-cursos-disponibles .acordeon-menu .card {
    border: 1px solid transparent;
    border-radius: 0;
    border-bottom: 1px solid #787878;
}

.seccion-cursos-disponibles .acordeon-menu .card .btn {
    padding: 0px 0px 19px 0px;
}

.seccion-cursos-disponibles .acordeon-menu .card .btn-link {
    color: #373737;
    font-size: 20px;
    font-family: 'Catamaran-Bold';
    letter-spacing: 0.03em;
    text-transform: capitalize;
    text-decoration: none !important;
    width: 100%;
    display: flex;
    justify-content: space-between;
    align-items: baseline;
}

.seccion-cursos-disponibles .acordeon-menu .card:nth-child(1) .btn-link {
    justify-content: flex-start;
    text-transform: uppercase;
}

.seccion-cursos-disponibles .acordeon-menu .card:nth-child(1) .btn-link img {
    margin-right: 13px;
}

.seccion-cursos-disponibles .acordeon-menu .card input[type="checkbox"] {
    display: none;
}

.seccion-cursos-disponibles .acordeon-menu .card label:before {
    font-family: "Font Awesome 5 Free";
    content: "\f0c8";
    margin-right: 9px;
    font-size: 25px;
    color: #787878;
    background-color: #fff;
}

.seccion-cursos-disponibles .acordeon-menu .card input[type="checkbox"]:checked+label:before {
    color: #007a33;
    font-size: 25px;
    background: url('../images/img_input_check.svg') no-repeat top left;
}

.seccion-cursos-disponibles .acordeon-menu input[type=range] {
    margin: auto;
    outline: none;
    padding: 0;
    width: 100%;
    height: 10px;
    background-color: #dedede;
    background-image: -webkit-gradient(linear, 50% 0%, 50% 100%, color-stop(0%, #DEDEDE), color-stop(100%, #DEDEDE));
    background-size: 50% 100%;
    background-repeat: no-repeat;
    border-radius: 10px;
    cursor: pointer;
    -webkit-appearance: none;
}

.seccion-cursos-disponibles .acordeon-menu input[type=range]::-webkit-slider-runnable-track {
    box-shadow: none;
    border: none;
    background: transparent;
    -webkit-appearance: none;
}

.seccion-cursos-disponibles .acordeon-menu input[type=range]::-webkit-slider-thumb {
    height: 18px;
    width: 18px;
    border: 0;
    background: #007A33;
    border: 1px solid #007A33;
    border-radius: 20px;
    box-shadow: 0 0 1px 0px rgba(0, 0, 0, 0.1);
    -webkit-appearance: none;
}

.seccion-cursos-disponibles .acordeon-menu .alert-secondary {
    letter-spacing: 0.03em;
    text-transform: uppercase;
    color: #787878;
    background-color: #fff;
    border: 1px solid #787878;
}

.seccion-cursos-disponibles .acordeon-menu .alert {
    padding: 6px 12px;
    font-size: 12px;
    border-radius: 20px;
    display: inline-block;
}

.seccion-cursos-disponibles .acordeon-menu .alert-dismissible .close {
    padding: 3px 10px;
}

.seccion-cursos-disponibles .acordeon-menu .alert-dismissible {
    padding-right: 2rem;
}

.seccion-cursos-disponibles .acordeon-menu h5 {
    font-size: 20px;
    font-family: 'Catamaran-Bold';
    color: #373737;
}

.seccion-cursos-disponibles .btn-menu-limpiar-filtro {
    color: #007A33;
    font-family: 'Catamaran-Bold';
    text-decoration-line: underline;
}

.seccion-cursos-detalle {
    padding: 24px 0px 47px 0px;
}

.seccion-cursos-detalle .franja-detalle {
    background-color: #f4f4f4;
    justify-content: space-between;
    padding: 13px 23px;
    color: #373737;
    margin: 29px 0px;
    font-size: 13px;
}

.seccion-cursos-detalle .img-curso-detalle {
    position: relative;
}

.seccion-cursos-detalle .info-curso-detalle {
    padding-top: 47px;
}

.seccion-cursos-detalle .info-curso-detalle h1 {
    margin-top: 25px;
}

.seccion-cursos-detalle .info-curso-detalle h2 {
    color: #373737;
    font-size: 25px;
    font-family: 'Catamaran-Bold';
}

.seccion-cursos-detalle .info-curso-detalle p {
    font-size: 20px;
    color: #787878;
    margin-top: 26px;
}

.seccion-cursos-detalle .ficha-solicitar-curso {
    background-color: #F4F4F4;
    padding: 31px 17px;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    text-align: center;
    font-size: 14px;
    color: #787878;
    margin-top: 25px;
}

.seccion-cursos-detalle .ficha-solicitar-curso h2 {
    color: #373737;
    font-size: 25px;
    font-family: 'Catamaran-Bold';
    margin-bottom: 6px;
    text-align: center;
}

.seccion-cursos-detalle .ficha-solicitar-curso h6 {
    font-size: 16px;
    color: #373737;
    margin-bottom: 17px;
}

.seccion-cursos-detalle .ficha-solicitar-curso .linea {
    width: 18px;
    margin: auto;
}

.seccion-cursos-detalle .ficha-solicitar-curso .linea.morada {
    border-bottom: 2px solid #722573;
}

.seccion-cursos-detalle .ficha-solicitar-curso .linea.roja {
    border-bottom: 2px solid #DE172E;
}

.seccion-cursos-detalle .ficha-solicitar-curso p {
    padding: 20px 0px;
}

.seccion-cursos-detalle .ficha-solicitar-curso .btn-morado {
    background-color: #722573;
    border-radius: 4px;
}

.seccion-cursos-detalle .ficha-solicitar-curso .btn-rojo {
    background-color: #DE172E;
    border-radius: 4px;
}

.seccion-cursos-detalle .linea-borde-top {
    border-top: 1px solid #DEDEDE;
}
/* CAMBIOS CURSOS */

.seccion-modalidad-cursos .box-curso .texto-rojo h2,
.seccion-cursos-detalle .info-curso-detalle .texto-rojo h2,
.box-curso.texto-rojo h2
{
    font-size: 25px;
    color: #DE172E;
    font-family: 'Catamaran-Bold';
    margin-bottom: 0px;
}

.box-curso {
    padding-top: 47px;
}

.box-curso img {
    margin-right: 13px;
}
.fichas-cursos p,
.box-curso + p,
.box-curso + h6 + p {
    padding-top: 16px;
    line-height: 1.5;
    color: #787878 !important;
    font-family: 'Catamaran-regular' !important;
}

#seccion-cursos-detalle .box-curso + h6 + p,
#seccion-cursos-detalle p {
    font-size: 20px;
    padding-top: 16px;
    line-height: 1.5;
    color: #787878 !important;
    font-family: 'Catamaran-regular' !important;
}
.box-curso + h6,
.fichas-cursos h6,
#seccion-cursos-detalle h6 {
    font-size: 20px;
    color: #373737;
    font-family: 'Catamaran-Bold';
    padding-top: 36px;
}

#seccion-cursos-detalle .box-curso + p {
    font-size: 20px;
}

.texto-lila h2 {
    font-size: 25px;
    color: #8F98FF;
    font-family: 'Catamaran-Bold';
    margin-bottom: 0px;
}

.texto-amarillo h2 {
    font-size: 25px;
    color: #F2A100;
    font-family: 'Catamaran-Bold';
    margin-bottom: 0px;
}

#seccion-cursos-detalle .box-btn {
    position: relative;
    display: flex;
    align-items: center;
    justify-content: flex-end;
}

.seccion-modalidad-cursos .fichas-cursos .btn-ir-a-curso.nuevo {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
}
#seccion-cursos-detalle .btn-ir-a-curso.nuevo,
#seccion-modalidad-cursos .btn-ir-a-curso.nuevo {
    width: 199px;
    height: 50px;
    border-radius: 4px;
    color: #fff;
    font-size: 16px;
    font-family: 'Catamaran-Bold';
    text-align: center;
    padding: 12px 0px;
    position: absolute;
    top: 50%;
    text-align: center;
}

#seccion-cursos-detalle a.btn-lila,
#seccion-modalidad-cursos a.btn-lila {
    background-color: #8F98FF;;
}
#seccion-cursos-detalle a.btn-amarillo,
#seccion-modalidad-cursos a.btn-amarillo {
    background-color: #F2A100;;
}

/* CAMBIOS NUMERO 2 */



/* FIN CAMBIOS CURSOS */
/*Media Query*/

@media (min-width: 1500px) {
    .seccion-cursos-disponibles .container {
        max-width: 100%;
        padding-left: 15px;
        margin-left: auto;
    }

    .seccion-cursos-disponibles .acordeon-menu {
        margin-left: -15px;
        max-width: 100%;
    }
}

@media (min-width: 1200px) and (max-width: 1499px) {
    .seccion-cursos-disponibles .container {
        max-width: 1245px;
    }
    .ficha-cursos-destacados .img-curso-destacado {
    height: 120px;
    }
    .ficha-cursos-destacados {
    height: 490px;
    }
    .ficha-cursos-destacados .info-curso-destacado {
    height: 310px;
    }
}

@media (min-width: 768px) and (max-width: 991px) {
    .seccion-cursos-disponibles .container {
        max-width: 100%;
    }
}

@media (min-width: 992px) {
    .navbar-expand-lg .navbar-nav .dropdown-menu {
        position: absolute;
        z-index: 1021;
    }

    #id_capacitaciones .col.card-curso {
        flex: 0 1 auto;
        width: 50%;
    }

    .seccion-cursos-disponibles .ficha-cursos-destacados {
    width: auto !important;
    }
}
@media (min-width: 1200px) {
    #id_capacitaciones .col.card-curso {
        flex: 0 1 auto;
        width: 33%;
    }
}

@media (min-width: 992px) and (max-width: 1199px) {
    .ficha-cursos-destacados {
        height: auto;
    }
    .ficha-cursos-destacados .info-curso-destacado {
        height: 324px;
    }
}

@media (max-width: 991px) {
    nav.nav-capacitacion .container {
        max-width: 100%;
    }
    nav.nav-capacitacion.navbar {
        padding: 15px;
    }
    nav .menu-movil {
        display: flex;
        justify-content: space-between;
        align-items: center;
    }
    .ficha-cursos-destacados {
        margin-left: auto;
        margin-right: auto;
    }
    .menu-pegagoso {
        display: flex;
        justify-content: center;
        flex-direction: column;
        align-items: center;
    }
    .menu-capacitacion .dropdown-menu {
        background-color: #f2f2f2;
        border: 1px solid #f2f2f2;
        border-radius: 0;
    }
    .seccion-modalidad-cursos .fichas-cursos .btn-ir-a-curso {
        align-items: center;
        margin-top: 20px;
    }
    .seccion-modalidad-cursos .modalidades {
        align-items: flex-start;
        justify-content: space-between;
        flex-direction: column-reverse;
    }
}

@media (min-width: 768px) and (max-width: 1499px) {
    .seccion-cursos-disponibles .container {
        padding-left: 0;
        margin-left: 0;
    }
}

@media (max-width: 767px) {
    .seccion-cursos-disponibles .acordeon-menu {
        max-width: 100%;
        margin-top: 15px;
    }
    .seccion-cursos-detalle .ancla-volver-inicio a {
        margin-bottom: 20px;
    }
}

@media (max-width: 575px) {
    .seccion-noticias-capacitaciones .fichas-noticias-capacitacion {
        flex-direction: column;
    }
    .seccion-noticias-capacitaciones .fichas-noticias-capacitacion img {
        margin-right: 0;
        margin-bottom: 32px;
    }
    .seccion-noticias-capacitaciones .fichas-noticias-capacitacion {
        max-width: 300px;
        margin-left: auto;
        margin-right: auto;
    }
}

@media (max-width: 425px) {
    .seccion-banner-buscador .div-buscador {
        width: 100%;
    }
}

@media (max-width: 375px) {
    .seccion-banner-buscador .div-buscador input {
        width: 225px;
    }
    .ficha-cursos-destacados, .ficha-cursos-destacados .info-curso-destacado {
        height: auto;
    }
    .menu-capacitacion .dropdown-item {
        white-space: initial;
        font-size: 14px;
    }
    .seccion-cursos-disponibles .ficha-cursos-destacados{
        min-width: 290px;
        width: auto;
    }
}