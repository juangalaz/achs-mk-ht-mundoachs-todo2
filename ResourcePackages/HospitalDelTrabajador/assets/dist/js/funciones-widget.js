const c1 = document.getElementsByClassName('c-1');
if(c1){
[].forEach.call(document.querySelectorAll('.c-1'), function (el) {
  var slider = tns({
    container: el,
    items: 1,
    controls: true,
    nav: true,
    navPosition: 'bottom',
    preventScrollOnTouch: "force",
    mouseDrag: true
    });
  });
}

const cSlide = document.getElementsByClassName('c-home');
if(cSlide){
[].forEach.call(document.querySelectorAll('.c-home'), function (el) {
  var slider = tns({
    container: el,
    items: 1,
    controls: true,
    nav: true,
    navPosition: 'bottom',
    preventScrollOnTouch: "force"
    });
  });
}

const cInformate = document.getElementsByClassName('c-informate');
if(cInformate){
[].forEach.call(document.querySelectorAll('.c-informate'), function (el) {
  var slider = tns({
    container: el,
    items: 1,
    controls: true,
    nav: true,
    navPosition: 'bottom',
    preventScrollOnTouch: "force"
    });
  });
}

const cGal = document.getElementsByClassName('c-gal');
if(cGal){
[].forEach.call(document.querySelectorAll('.c-gal'), function (el) {
  var slider = tns({
    container: el,
    items: 1,
    controls: true,
    nav: true,
    navPosition: 'bottom',
    preventScrollOnTouch: "force"
    });
  });
}

/*--LAZY--*/
var lazyLoadInstance = new LazyLoad({
  elements_selector: ".lazy"
});
/*--WOW--*/
    wow = new WOW(
      {
        animateClass: 'animated',
        offset:       100,
        mobile: false,
        callback: function(box) {
        }
      }
    );
    wow.init();
/*--FIN WOW--*/
const acc = document.getElementsByClassName('accordion');
if(acc){
  for (let i = 0; i < acc.length; i++) {
      let item = acc[i];
      var accordion = new Accordion({
          element: item,
          oneOpen: true
      });
  }
}

$(function(){
  
  $('.menu-lateral a[href*=\\#]').click(function() {

  if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
      && location.hostname == this.hostname) {

          var $target = $(this.hash);

          $target = $target.length && $target || $('[name=' + this.hash.slice(1) +']');

          if ($target.length) {

              var targetOffset = $target.offset().top;

              $('html,body').animate({scrollTop: targetOffset}, 1000);

              return false;

         }

    }

});

});





const carrusel3 = document.getElementsByClassName('c-3');
if(carrusel3){
[].forEach.call(document.querySelectorAll('.c-3'), function (el) {
  var slider = tns({
    container: el,
    items: 3,
    controls: true,
    nav: true,
    gutter: 20,
    navPosition: 'bottom',
    preventScrollOnTouch: "force",
    responsive: {
      0 : {
        items: 1
      },
      480:{
        items: 2
      },
      640: {
        items: 3
      },
      767: {
        items: 3
      },
      900: {
        items: 3
      }
    }
    });
  });
}








$(document).ready(function() {
  $('.tabla-horarios').DataTable({
    "pageLength": 8,
    "language": {
      "search": "BUSCAR POR SEDE",
      "zeroRecords": "No hay datos disponibles",
      "paginate": {
        "first":      "Primera",
        "last":       "Última",
        "next":       "<i class='fas fa-chevron-right'></i>",
        "previous":   "<i class='fas fa-chevron-left'></i>"
      }
    },
    "order": [[ 0, 'asc' ], [ 1, 'asc' ]]
  });
  $('.table-licitacion').DataTable({
    "pageLength": 8,
    "language": {
      "search": "Buscar por licitación",
      "zeroRecords": "No hay datos disponibles",
      "paginate": {
        "first":      "Primera",
        "last":       "Última",
        "next":       "<i class='fas fa-chevron-right'></i>",
        "previous":   "<i class='fas fa-chevron-left'></i>"
      }
    },
    "order": [[ 0, 'asc' ], [ 1, 'asc' ]]
  });

  $('.datatable-achs').DataTable({
    "pageLength": 8,
    "language": {
      "search": "Buscar",
      "zeroRecords": "No hay datos disponibles",
      "paginate": {
        "first":      "Primera",
        "last":       "Última",
        "next":       "<i class='fas fa-chevron-right'></i>",
        "previous":   "<i class='fas fa-chevron-left'></i>"
      }
    },
    "order": [[ 0, 'asc' ], [ 1, 'asc' ]]
  });


 $('.cerrar').click(function(e) {  
      $(this).parent().addClass("d-none");
    });

});



if (document.querySelector(".twitter_link")) {
  $(".twitter_link").click(function (e) {
    e.preventDefault();
    var url = $(this).attr('href');
    var title = $(this).attr('title');
    window.open("http://twitter.com/share?url=" + encodeURIComponent(url) + "&text=" + encodeURIComponent(title) + "&count=none/", "tweet", "height=480,width=550,resizable=1")
  });
}
if (document.querySelector(".facebook_link")) {
  $(".facebook_link").click(function (e) {
    e.preventDefault();
    var url = $(this).attr('href');
    var title = $(this).attr('title');
    window.open("http://www.facebook.com/sharer.php?u=" + encodeURIComponent(url) + "&t=" + encodeURIComponent(title), "facebook", "height=500,width=550,resizable=1")
  });
}
if (document.querySelector(".linkedin_link")) {
  $(".linkedin_link").click(function (e) {
    e.preventDefault();
    var url = $(this).attr('href');
    var title = $(this).attr('title');
    window.open("https://www.linkedin.com/shareArticle?mini=true&url=" + encodeURIComponent(url) + "&title=" + encodeURIComponent(title), "linkedin", "eft=20,top=20,width=500,height=500,toolbar=1,resizable=0")
  });
}
if (document.querySelector(".whatsapp_link")) {
  $(".whatsapp_link").click(function (e) {
    e.preventDefault();
    var url = $(this).attr('href');
    var title = $(this).attr('title');
    window.open("whatsapp://send?text=" + encodeURIComponent(url) + " - " + encodeURIComponent(title), "whatsapp", "height=500,width=550,resizable=1")
  });
}

if (document.querySelector(".whatsapp_link")) {
  $(".whatsapp_link_web").click(function (e) {
    e.preventDefault();
    var url = $(this).attr('href');
    var title = $(this).attr('title');
    window.open("https://web.whatsapp.com/send?text=" + encodeURIComponent(url) + " - " + encodeURIComponent(title), "whatsapp", "height=1500,width=1550,resizable=1")
  });
}




