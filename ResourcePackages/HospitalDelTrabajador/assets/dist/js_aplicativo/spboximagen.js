﻿/*
 * jQuery TipTop v1.0
 * http://gilbitron.github.io/TipTop
 *
 * Copyright 2013, Dev7studios
 * Free to use and abuse under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 */

;(function($, window, document, undefined){

    var pluginName = 'tipTop',
        defaults = {
        	offsetVertical: 10, // Vertical offset
        	offsetHorizontal: 10  // Horizontal offset
        };

    function TipTop(element, options){
        this.el = element;
        this.$el = $(this.el);
        this.options = $.extend({}, defaults, options);

        this.init();
    }

    TipTop.prototype = {

        init: function(){
        	var $this = this;

			this.$el.mouseenter(function(){
				var title = $(this).attr('title'),
					tooltip = $('<div class="tiptop"></div>').html(title);
				tooltip.appendTo('body');
				$(this).data('title', title).removeAttr('title');
			}).mouseleave(function(){
				$('.tiptop').remove();
				$(this).attr('title', $(this).data('title'));
			}).mousemove(function(e) {
				var tooltip = $('.tiptop'),
					top = e.pageY + $this.options.offsetVertical,
					bottom = 'auto'
					left = e.pageX + $this.options.offsetHorizontal,
					right = 'auto';

				if(top + tooltip.outerHeight() >= $(window).scrollTop() + $(window).height()){
					bottom = $(window).height() - top + ($this.options.offsetVertical * 2);
					top = 'auto';
				}
				if(left + tooltip.outerWidth() >= $(window).width()){
					right = $(window).width() - left + ($this.options.offsetHorizontal * 2);
					left = 'auto';
				}

				$('.tiptop').css({ 'top': top, 'bottom': bottom, 'left': left, 'right': right });
			});

        }

    };

    $.fn[pluginName] = function(options){
        return this.each(function(){
            if(!$.data(this, pluginName)){
                $.data(this, pluginName, new TipTop(this, options));
            }
        });
    };

})(jQuery, window, document);
var countenvio=3;
$(document).ready(function () {

    $("input#RUNLOGIN").rut({minimumLength: 7,useThousandsSeparator : false}).on('rutInvalido', function(e) {
   	    $("#mesajerutnovalidopasouno").removeClass('d-none');
   	    $(this).val('');
   	     $("#RUNLOGIN").addClass("is-invalid");
           
     }).on('rutValido', function(){ 
         $("#mesajerutnovalidopasouno").addClass('d-none');
         $("#RUNLOGIN").removeClass("is-invalid");

	 }).click(function(){
	 
	      $("#mesajerutnovalidopasouno").addClass('d-none');
	      $("#RUNLOGIN").removeClass("is-invalid"); 

	 });
	 $("input#runrestablece").rut({minimumLength: 7,useThousandsSeparator : false}).on('rutInvalido', function(e) {
   	    $("#mensajerutnoencontradvalido").removeClass('d-none');
   	    $(this).val('');
   	     
         $("input#runrestablece").addClass("is-invalid");
     }).on('rutValido', function(){ 
          $("#mensajerutnoencontradvalido").addClass('d-none');
          $("input#runrestablece").removeClass("is-invalid");

	 });
	 
	  $("input#runobtener").rut({minimumLength: 7,useThousandsSeparator : false}).on('rutInvalido', function(e) {
   	    $("#mesajerutnovalidopasodos").removeClass('d-none');
   	    $(this).val('');
   	    $("input#runobtener").addClass("is-invalid");

     }).on('rutValido', function(){ 
          $("#mesajerutnovalidopasodos").addClass('d-none');
          $("input#runobtener").removeClass("is-invalid");

	 }).click(function(){
	      $("#mesajerutnovalidopasodos").addClass('d-none');
	       $("input#runobtener").removeClass("is-invalid");
           $(this).val('');
	 });


	 
	 
     $("#restablecerpasswordHT").click(function(e){
        
          $(".box-spinner").show();

        setTimeout(function(){
         $.when(spbox.recuperar()).done(function(){
			     $(".box-spinner").hide();
		});
		},3000);
        e.preventDefault();


     });
	 $("#obtener").click(function(e){
	    $("#obtenerform").show();
	    $("#loginimagen").hide();
        $("#restablecer").hide();
        $("#restablecerform").hide();
        $("#runobtener").val('');
        $("#rut-serie").val('');
        $("#phone1").val('');
        $("#mail").val('');
        e.preventDefault();
	 });
	 $("#enviovalidacion").click(function(e){
	 
        if(validationpasoobtener()){    
	          $(".box-spinner").show();
	        setTimeout(function(){
	         $.when(spbox.obtenerpreguntas()).done(function(){
				   
			});
			},4000);
		}
        e.preventDefault();



	 });
	 $("#restablecer").click(function(e){
	     $("#restablecerform").show();
	     $("#obtenerform").hide();
	     $("#loginimagen").hide();
         $("#validacionpreguntas").hide();
          e.preventDefault();
          
	 });
	 $("#aceptoterminos").click(function(){
	     if($(this).is(":checked")){
	           $("#mesajeraceptar").addClass("d-none");

	     }
	 });
	 $(".volverlogin").click(function(e){
	 
	     location.href='https://dev-hospitalweb.achs.cl/servicios-en-linea/resultados-imagenologia';
	     e.preventDefault();

	 });
	 $("#validacionusuario").click(function(e){
	   
   		if(validationpasouno()){      
            $(".box-spinner").show();
		    setTimeout(function(){
	         $.when(spbox.validar()).done(function(){
				  
			});
			},4000);
        }
			         
	       e.preventDefault();
	 });
	 $("#enviarvalidacion").click(function(e){
	  
	    if(validationpasoenviar()){
	        $(".box-spinner").show();
		    
		    setTimeout(function(){
	         $.when(spbox.enviarrespuestas()).done(function(){
				     $(".box-spinner").hide();
			});
			},4000);
		}	
         e.preventDefault();

	 });
	 $("#phone1").inputmask({"mask": "+56999999999"});
	 $(".allogin").click(function(e){
	       location.href='https://dev-hospitalweb.achs.cl/servicios-en-linea/resultados-imagenologia';
	     e.preventDefault();

	 });
	  $('#pass').bind("cut copy paste",function(e) {
          e.preventDefault();
          
      });
       $('#pass').bind("click",function(e) {
         $("#mensajeerrorlogin").html(data.R.mensaje).addClass("d-none");
          
      });


});
function show() { document.getElementById('cv-example').style.display = "block"; }
function hide() { document.getElementById('cv-example').style.display = "none"; }
var spbox={
     recuperar:function(){
           let valorretornado;
           let frmsel={  
           
                   "run": $("#runrestablece").val()
				    
				}
				 $.ajax({
                type: 'POST',
                url: "https://api.achs.lfi.cl/api/recupera",
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                async:false,
                data: JSON.stringify(frmsel),
                success: function (data) {
               
                   if(data.R.autorizado==="1"){
                    
                     $("#displayfelicitacion").show();
         			 $("#restablecerform").hide();
	                 $("#obtenerform").hide();
	                 $("#loginimagen").hide();
                     $("#validacionpreguntas").hide();
                    }else{
                          
                        $("#mensajerutnoencontradvalido").html(data.R.mensaje).addClass('text-danger').removeClass('d-none');
                    }
         

                },
                error: function (ex) {
                    tr = jQuery.parseJSON(response.responseText);

                }
            });
       return valorretornado;
     
     },validar:function(){
        let frmsel={"run": $("#RUNLOGIN").val(),"password":$("#pass").val()};
	    $.ajax({
                type: 'POST',
                url: "https://api.achs.lfi.cl/api/autenticacion",
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                async:false,
                data: JSON.stringify(frmsel),
                success: function (data) {
                   if(data.R.autorizado==="1"){
                           if(data.R.urlimag.length<=72){
		                           var url = data.R.urlimag.substring(1,data.R.urlimag.length-1);
		                           location.href = url ;
                           }else{
                           
                              
                              countenvio--;
                              if(countenvio>=0){
                                spbox.validar();
                              }else{
                                 $("#myModalMensaje").modal('show').find('.modal-body').append('<p>'+data.R.urlimag+'</p>');
                              }
                           }
                   }else{
                      $("#mensajeerrorlogin").html(data.R.mensaje).removeClass("d-none");
                         $(".box-spinner").hide();   

                   }
                 

                   
                },
                error: function (ex) {
                    tr = jQuery.parseJSON(data.responseText);

                }
            });
		

     
     },obtenerpreguntas:function(){
        let frmsel={"run":$("#runobtener").val(),
				   "serie":$("#rut-serie").val()}
		 
		 $.ajax({
                type: 'POST',
                url: "https://api.achs.lfi.cl/api/entregapregunta",
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                data: JSON.stringify(frmsel),
                success: function (data) {
                   if(data.estructura.autorizado==="1"){
                           data = jQuery.parseJSON(data.estructura.estructura);
				  
		                    let htmlcontenido="";
		                    for (f = 0; f < data.interactiveQuery.question.length; f++) {
		                        htmlcontenido+="<label for='question' class='question-form'>"+data.interactiveQuery.question[f].questionText+"</label>";
		                        htmlcontenido+="<select id='p"+f+"'  class='form-control'>";
		                        htmlcontenido +="<option value=0>Seleccione una alternativa</option>"
		                         for (d = 0; d < data.interactiveQuery.question[f].answerChoice.length; d++) {
		                              htmlcontenido+="<option value='" + data.interactiveQuery.question[f].answerChoice[d].answerId + "'>"+ data.interactiveQuery.question[f].answerChoice[d].value + "</option>";
		
		                         }
		                          htmlcontenido+="</select>";
		                    }
		                    $("#displaypreguntas").html(htmlcontenido);
		                    $("#obtenerform").hide();
				            $("#loginimagen").hide();
			                $("#restablecerform").hide();
		                    $("#validacionpreguntas").show();
		                    $("#tk").val(data.transactionKey);
                      $(".box-spinner").hide();
                     }else{
                       $(".box-spinner").hide();
                       //$("#myModalMensaje").modal('show').find('.modal-body').append('<p>'+data.estructura.mensaje+'</p>');

                       $("#mesajecaptura").html(data.estructura.mensaje);
                      


                     }                    
                },
                error: function (data) {
                   console.log(data);

                }
            });
            
	 },enviarrespuestas:function(){
	 
	       let frmrespuesta = {"p1":$("#p0").val(),"p2":$("#p1").val(),"p3":$("#p2").val(),"p4":$("#p3").val(),"tk":$("#tk").val()};
            console.log(frmrespuesta);
            $.ajax
            ({
                type: 'POST',
                url: "https://api.achs.lfi.cl/api/enviorespuesta",
                dataType:'json',
                data:JSON.stringify(frmrespuesta),
                contentType: "application/json; charset=utf-8",
                async:false,
                success:function(data) {
                   
                    if (data.estructura.autorizado==="1") {
                        spbox.crearregistro();
                       
                    } else {
                    
							$("#myModalMensaje").modal('show').find('.modal-body').append('<p>'+data.estructura.mensaje+'</p>');

                    }
                },
                error: function (ex) {
                    tr = jQuery.parseJSON(data.responseText);
                   
                }
            });
	 
	 },crearregistro:function(){
	      var frm = {
            "run": $("#runobtener").val().trim(),
            "serie": $("#rut-serie").val().trim(),
            "correo": $("#mail").val().trim(),
            "fono": $("#phone1").val().trim(),
            "transactionkey":$("#tk").val()};
         $.ajax
            ({
                type: 'POST',
                url: "https://api.achs.lfi.cl/api/crear",
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(frm),
                async:false,
              
                success: function (data) {
                   // console.log(data);
                    if(data.R.autorizado==="1"){
                        $("#displayfelicitacion").show();
                        $("#validacionpreguntas").hide();
	                    $("#tk").val(0); 
	                    $("#obtenerform").hide();
                       
                    } else {
                       
                       
                    }
                },
                error: function (ex) {
                    tr = jQuery.parseJSON(response.responseText);
                   
                }
            });
	 
	 }
}
function validationpasouno(){
   if ($("#RUNLOGIN").val() == "") {
      $("#RUNLOGIN").addClass("is-invalid");
      return false;
    }
    if($("#pass").val() == ""){
         $("#pass").addClass("is-invalid");
         return false;

    }
    if(!$("#aceptoterminos").is(':checked')){
       $("#mesajeraceptar").removeClass("d-none");
        return false;
      
    }
    
    if ($("#RUNLOGIN, #pass").val()!= "" && $("#aceptoterminos").is(':checked')) {
      $("#RUNLOGIN, #pass").removeClass("is-invalid");
      $("#mesajeraceptar").addClass("d-none");

      return true;
    }

}
function validationpasoobtener(){
  if ($("#runobtener").val() == "") {
      $("#runobtener").addClass("is-invalid");
      return false;
    }
    if($("#rut-serie").val() == ""){
         $("#rut-serie").addClass("is-invalid");
         return false;

    }
     
    if($("#phone1").val() == ""){
         $("#phone1").addClass("is-invalid");
         return false;

    }else if ($("#mail").val() == "") {
      $("#mail").addClass("is-invalid");
      return false;
    } else {
      var regMail = /^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$/;
      if (regMail.test($("#mail").val()) == false) {
        $("#mail").addClass("is-invalid");
        return false;
      } else {
        $("#mail").removeClass("is-invalid");
      }
    }
   
     if (($("#runobtener, #rut-serie").val()!= "") && $("#mail").val()!="" ||  $("#phone1").val() !="") {
      $("#runobtener,#rut-serie,#mail,#phone1").removeClass("is-invalid");
      return true;
    }

}
function validationpasoenviar(){
    if($("#p0").val() == 0) {
      $("#p0").addClass("is-invalid");
      return false;
    } else  if($("#p1").val() == 0) {
      $("#p1").addClass("is-invalid");
      return false;
    }else  if($("#p2").val() ==0) {
      $("#p2").addClass("is-invalid");
      return false;
    }else if($("#p3").val() ==0) {
      $("#p3").addClass("is-invalid");
      return false;
    }
    
     if ($("#p0, #p1,#p2,#p3").val()!=0) {
      $("#p0, #p1,#p2,#p3").removeClass("is-invalid");
      return true;
    }
}
$(document).on('click', '.toggle-password', function() {

    $(this).toggleClass("fa-eye fa-eye-slash");
    
    var input = $("#pass");
    input.attr('type') === 'password' ? input.attr('type','text') : input.attr('type','password')
});