//https://www.neoguias.com/funciones-flecha-es6/
//https://latteandcode.medium.com/vanilla-js-addeventlistener-queryselector-y-closest-ef95b3a0a2c1
//https://code.tutsplus.com/es/tutorials/how-to-use-map-filter-reduce-in-javascript--cms-26209



if(document.querySelector('#banco-de-img')) {
//INICIO BUSCADOR  
let url_banco = '/api/banco-de-imagenes-mk/bancodeimagens?$select=Title,UrlName&$expand=Imagen($select=Url)';
let array_banco = [];

const armar_html = (array) => {
  let = html = '';
  array.map(function (banco, index, array) {
    
    html =  html + `       <div class="col">
    <div class="img-detalle">
      <a data-fancybox="gallery" href="${banco.Imagen[0].Url}">
        <img src='${banco.Imagen[0].Url}' alt='${banco.Title}' title='${banco.Title}' />
      </a>
      <div class="botones-galeria" style="height: 30px;" >

        <div class="btn-gal-detalle btn-descarga-detalle" >
          <a href="${banco.UrlName}">Descargar<i class="far fa-arrow-alt-circle-down ml-2"></i></a>
        </div>
      </div>
    </div>
  </div>
  `;
 
  });
  return html;

},
cargar_banco_imagen = () => {
    
  fetch(url_banco).then((response) => {
    if (response.ok) {
      return response.json();
    } else {
      throw new Error('Something went wrong');
    }
  })
    .then((responseJson) => {
        array_banco = responseJson.value;

       //array_banco = array_banco.sort(compareValues('Nombre'));

       array_banco.map(function (banco, index, array) {
 
        banco.Buscar = slugify(banco.Title);
     
    });

    if(document.getElementById('txt_buscar').value!='') {
      buscar_banco();
    }

    })
    .catch((error) => {
      console.log(error)
    });

},
buscar_banco = () => {
  let filters = {};

  let text = slugify($('#txt_buscar').val());
  console.log(text);
  if(text!='') 
  {
    filters = { Buscar: Buscar =>Buscar.toLowerCase().includes(text.toLowerCase())  };
     
 }


 const filterResult = filterArray(array_banco, filters);
 const html = armar_html(filterResult);
 
 let htmlObject = document.getElementById('banco-de-img');
 htmlObject.innerHTML = html;

 
  
}, 
filterArray = (array, filters) => {
  const filterKeys = Object.keys(filters);
  return array.filter(item => {
    // validates all filter criteria
    return filterKeys.every(key => {
      // ignores non-function predicates
      if (typeof filters[key] !== 'function') return true;
      return filters[key](item[key]);
    });
  });
},
btn_buscar = document.querySelector('#btn_buscador'),
txt_buscar = document.querySelector('#txt_buscar');









  var url_string = window.location.href;
  var url = new URL(url_string);
  var b = url.searchParams.get("b");
  if (b != null) {
    document.getElementById('txt_buscar').value = b;

  }

  cargar_banco_imagen();
  
  btn_buscar.addEventListener('click', () => buscar_banco());
  txt_buscar.addEventListener("keyup", (event) => {
    // Number 13 is the "Enter" key on the keyboard
    if (event.keyCode === 13) {
      // Cancel the default action, if needed
      event.preventDefault();
      // Trigger the button element with a click
      buscar_banco();
    }
  });
  console.log("buscar...");
}
//FIN BUSCADOR


//INICIO BARRA BUSQUEDA
if(document.querySelector('#buscador_detalle')) {
const btn_buscar_detalle = document.querySelector('#btn_buscador_detalle'),
txt_buscar_detalle = document.querySelector('#txt_buscar_detalle'),
enviar_parametros_busqueda = () => {
    let b = txt_buscar_detalle.value;
    

    
    let url = `/banco-de-imagenes/buscador?b=${slugify(b)}`;
    window.location.href = url;
};




btn_buscar_detalle.addEventListener('click', () => enviar_parametros_busqueda());
txt_buscar_detalle.addEventListener("keyup", (event) => {
  // Number 13 is the "Enter" key on the keyboard
  if (event.keyCode === 13) {
    // Cancel the default action, if needed
    event.preventDefault();
    // Trigger the button element with a click
    enviar_parametros_busqueda();
  }
});




  console.log("detallle...");
}
//FIN BARRA BUSQUEDA
function slugify (str) {
  var map = {
      'a' : 'á|à|ã|â|À|Á|Ã|Â',
      'e' : 'é|è|ê|É|È|Ê',
      'i' : 'í|ì|î|Í|Ì|Î',
      'o' : 'ó|ò|ô|õ|Ó|Ò|Ô|Õ',
      'u' : 'ú|ù|û|ü|Ú|Ù|Û|Ü',
      'c' : 'ç|Ç'
  };
    str = str.toLowerCase();
  
  for (var pattern in map) {
      str = str.replace(new RegExp(map[pattern], 'g'), pattern);
  };

  return str;
  
};