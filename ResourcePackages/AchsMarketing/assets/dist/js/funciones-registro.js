 var $form = $('#iniciar-sesion');
	
	$.validator.addMethod("letters", function (value, element) {

        return this.optional(element) || value == value.match(/^[a-zA-Z\s\u00A1-\uFFFF]*$/);
        //return this.optional(element) || value == value.match(/^[a-zA-Z\s]*$/);
    });

  $form.validate({
 

       rules: {
            nombre: {
                required: true,
                minlength: 3,
                letters: true,
            },
            apellido: {
                required: true,
                minlength: 3,
                letters: true
            },
            email: {
                required: true,
                email: true
            },
            password : {
            	 required: true,
					minlength : 5
				},
				password2 : {
					 required: true,
					minlength : 5,
					equalTo : "#password"

		},
           
        },
        messages: {
            nombre: "Debes ingresar nombres válidos (sólo letras)",
            apellido: "Debes ingresar tu apellido paterno (sólo letras)",
            email: "Debes ingresar un email válido",
              password: "Mínimo 5 caracteres",
          password2: {
          	required: "Este campo es obligatorio",
    		min: "Mínimo 5 caracteres",
    		equalTo: "Las contraseñas deben coincidir"
 			 },
        }
    });

/*jQuery('.iniciar-sesion').validate({
			rules : {
				password : {
					minlength : 5
				},
				password2 : {
					minlength : 5,
					equalTo : "#password"

				}, messages: {
            password: "Debes ingresar nombres válidos (sólo letras)",
        }
			}
		});

*/


$('#btnRegistro').on('click',function(){
    let isFormValid = $form.valid();
  	if (isFormValid) {

         var frm = {
        nombre: $("#nombre").val(),
        apellido: $("#apellido").val(),
        email: $("#email").val(),
        password: $("#password").val(),
    };
         $.ajax({
            type: 'POST',
            url: "/registro/registro/insertar_usuario",
            contentType: "application/json; charset=utf-8",
             dataType: 'json',
        async: false,
            data: JSON.stringify(frm),
            success: function (resp) {
        console.log(resp)
        if (resp != 0){
        	
        	window.location.replace("http://marketingachs.lfi.cl/registro/exito-registro");
        	//alert('se ingresaron los datos')
        
        }else{
alert('Correo electrónico ya registrado')
//            window.location.replace("http://admisionuandes.lfi.cl/formularios/error");
        }
            },
            error: function (err) {
                let date = new Date();
                result = false;
            }
        });

     }else{

      alert('complete todos los campos')

      }

 })

