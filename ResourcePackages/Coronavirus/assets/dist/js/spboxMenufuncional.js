﻿var urlHome = window.location.href.toString().toLowerCase().indexOf('nuevoportal')>0?window.location.protocol+'//'+window.location.host+'/nuevoportal':window.location.protocol+'//'+window.location.host;



$.fn.extend({
  animateCss: function(animationName, callback) {
    var animationEnd = (function(el) {
      var animations = {
        animation: 'animationend',
        OAnimation: 'oAnimationEnd',
        MozAnimation: 'mozAnimationEnd',
        WebkitAnimation: 'webkitAnimationEnd',
      };

      for (var t in animations) {
        if (el.style[t] !== undefined) {
          return animations[t];
        }
      }
    })(document.createElement('div'));

    this.addClass('animated ' + animationName).one(animationEnd, function() {
      $(this).removeClass('animated ' + animationName);

      if (typeof callback === 'function') callback();
    });

    return this;
  },
});

 $(window).load(function(){
  
  if($("div.owl-carousel").hasClass("owl-carousel")){  
	  
	    $('.owl-carousel').owlCarousel({
			    loop:true,
			    margin:0,
			   items:1,
		});
   }
	
	  $(".lupabuscador_movil").click(function(){
	     placeholdersearch();
	  }); 
	     if(window.location.href.indexOf("FestivACHS") > -1) {
            $('meta[name=description]').attr('content', 'El domingo 5 de mayo Festivachs vivirá una nueva jornada en Movistar Arena. ¡La versión 2019 del festival confirmó la presencia de Jorge Alis, Denise Rosenthal, Noche de Brujas y Segurito con sus amigos!');
	  	 } 
});

$(document).ready(function (){
	valida.correo();
   if(window.location.href.indexOf("FestivACHS") > -1) {
			// Set the date we're counting down to
			var countDownDate = new Date("may 1, 2019,09:00:00").getTime();
		
			// Update the count down every 1 second
			var x = setInterval(function() {
		
			  // Get todays date and time
			  var now = new Date().getTime();
		
			  // Find the distance between now and the count down date
			  var distance = countDownDate - now;
		
			  // Time calculations for days, hours, minutes and seconds
			  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
			  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
			  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
		
		
			  // Output the result in an element with id="demo"
			  document.getElementById("contadordia").innerHTML = days + " ";
			  document.getElementById("contadorhora").innerHTML = hours + " ";
			  document.getElementById("contadorminuto").innerHTML = minutes + " ";
		
			  // If the count down is over, write some text 
			  if (distance < 0) {
				clearInterval(x);
				document.getElementById("contador").innerHTML = "EXPIRED";
			  }
			}, 1000);
			

	}








  const mq = window.matchMedia( "(max-width: 767px)" );
    
    if (mq.matches) {
		 var menu01=null;
		 menu01 = new MobileMenu;
	     menu01.init();
		  
		  
		  
	} else {
	// no mobil
	      	/*	var urlActual = window.location.href;
		        		var classActual = '';
		        		//console.log(url);
		        		if(urlActual.indexOf(url) > -1){
		        			//console.log('item actual es la URL de arriba');
		        			classActual= ' paginaactual';
		        		}
		        		
		        		if(url.toLowerCase().indexOf('postula.aspx') > -1 && (urlActual.indexOf('ficha.aspx') > -1 || 
		        										   urlActual.indexOf('postulacionoferta.aspx') > -1 ||
		        										   urlActual.indexOf('postula.aspx') > -1)){
		        			//console.log('item actual es POSTULA y la URL de arriba es cualquiera de las otras 3.');		        					        										   
		        			classActual= ' paginaactual';
		        		}*/


	}
$("#transparenciaACHS .botonera-trans .acordion").each(function() {
		  $(this).click(function() {
		     $(this).next('ul').slideToggle( "fast");
           });
});
     

});
window.addEventListener("resize", function(event) {
    
    const mq = window.matchMedia( "(max-width: 767px)" );
    if (mq.matches) {
		 var menu01=null;
		 menu01 = new MobileMenu;
	     menu01.init();
		  
		  
		  
	} else {
	// no mobil
	}

    
});

function placeholdersearch() {
    document.getElementById("gsc-i-id2").setAttribute("placeholder", "¿Qué estás buscando?");
   
}
function MobileMenu(){
	
	// set variables
	var $body = $('body');
	var $btnMenu = $('.btn-menu');
	// get the nav li elements from the 
	// desktop menu
	var navLiHTML = $('header nav  ul').html();
	// create the mobile menu from the desktop li elements...
	var mobileNavHTML = $('<nav class="mobile-nav"><ul>' + navLiHTML  + '</ul></nav>');
	
	// Add the mobile menu to the main element...
	$('.main').prepend(mobileNavHTML);
	
	// select the newly created mobile menu
	var $mobileNav = $('.mobile-nav');
	
	// select all the "a" links that have a 
	// sub menu
	var $dd = $('.mobile-nav .dd');
	
	// initialization method for the
	// MobileMenu class	
	this.init = function(){
		
		// measure height of menu in open state
		// then close the menu
		$body.addClass('show');
		var mobileNavOriginalHeight = $mobileNav.height();
		var mobileNavHeight = $mobileNav.height();
		$body.removeClass('show');
		
		// Open all the menus and the sub menus 
		// and measure the height of each
		// sub menu then close all the 
		// sub menus
		$body.addClass('show');
		$('.mobile-nav .dd').addClass('dd-show');
		// Loop through the sub menus add get the height
		// of the sub menus and set a data attribute to 
		// that height
		$('.mobile-nav .dd').each(function(){
			var theHeight = $(this).next().height();
			$(this).next().attr('data-height', theHeight);	
		}); // end each...
		// Close the menu and the sub menus
		$body.removeClass('show');
		$('.mobile-nav .dd').removeClass('dd-show');

		// Click event handler for the mobile menu
		$btnMenu.click(function(){
			// check if body element has the
			// class show
			if($body.hasClass('show')){
				// menu is open...
				// remove any heights set on the mobile nav
				$mobileNav.removeAttr('style');
				// remove the "show" class from the body
				// element
				$body.removeClass('show');
				// remove any heights set on the sub
				// menus
				$dd.next().removeAttr('style');
				// remove the "dd-show" class from the
				// links that have sub menu items 				
				$dd.removeClass('dd-show');	
			}else{
				// menu is closed...
				// set height of mobile menu to the open height
				$mobileNav.css('height', mobileNavOriginalHeight);
				// add the class "show" to the body element
				$body.addClass('show');	
			} // end if menu is open...
				
		}); // end mobile menu click event handler
				
		$dd.click(function(){
			// check if this sub menu link
			// is open
			if($(this).hasClass('dd-show')){
				// this sub menu is open...
				// get current height of mobile menu
				mobileNavHeight = $mobileNav.outerHeight();
				// set new height of mobile menu by taking the
				// existing height of the mobile menu and
				// subtracting the height of the sub menu that
				// was clicked on...
				$mobileNav.css('height', (mobileNavHeight - $(this).next().data('height')));
				// remove the height style that was applied to this
				// sub menu
				$(this).next().removeAttr('style');
				// remove the "dd-show" class from the sub menu link
				// that was clicked on
				$(this).removeClass('dd-show');	
			}else{
				// this sub menu is closed
				// remove any height styles applied
				// to any sub menus
				$dd.next().removeAttr('style');
				// remove the "dd-show" class from
				// any sub menu link elements
				$dd.removeClass('dd-show');
				// set the new height of the 
				// mobile menu by adding the
				// height of mobile navs orginal 
				// open state height to the height
				// of the sub menu item that was
				// clicked on
				$mobileNav.css('height', (mobileNavOriginalHeight + $(this).next().data('height')));
				// set the height of the sub menu that
				// was clicked on
				$(this).next().css('height', $(this).next().data('height'))
				// add the "dd-show" class to
				// sub menu link that was clicked on...
				$(this).addClass('dd-show');
			} // end if sub menu is open	
		}) // end sub menu click event handler

	} // end init()
	
} // end MobileMenu Constructor


var valida={
	correo:function(){
		$('input[id*=Email], input[id*=email], input[id*=correo], input[id*=Correo]').blur(function(){
			if($(this).val()=='sample@email.tst'){
				$(this).val('');
			}
			if($(this).val().indexOf('@email.')>0){
				$(this).val('');
			}
			if($(this).val().indexOf('@correo.')>0){
				$(this).val('');
			}
		});
	}
}

