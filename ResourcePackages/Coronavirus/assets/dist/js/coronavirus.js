//Modal, carrusel
$(document).ready(function () {

    $('.btn-verde.enviar').click(function () {

        $('.modal').show();
        $('.modal-bg').show();
    });

    $('.modal .close').click(function () {
        $('.modal').hide();
        $('.modal-bg').hide();
    })

    $(function () {
        $(".owl-carousel").owlCarousel({
            items: 1,
            itemsDesktop: [1199, 1],
            itemsDesktopSmall: [979, 1]
        });
    });

    $('.btn-verde.ver-mas').click(function () {

        $('.empresas').removeClass('d-none').addClass('d-block');
        $('.btn-verde.ver-mas').addClass('d-none');
    });

    $('.btn-verde.ver-mas-dos').click(function () {

        $('.trabajadores').removeClass('d-none').addClass('d-block');
        $('.btn-verde.ver-mas-dos').addClass('d-none');
    });

    // Script coronavirus
    //spbx_achsgestion.cargavideo();  
    $('.owl-carousel').owlCarousel({
        navText: ["<img src='https://www.achs.cl/portal/SiteAssets/images/flecha_prev.svg'>", "<img src='https://www.achs.cl/portal/SiteAssets/images/flecha_next.svg'>"],
        nav: true,
        responsiveClass: true,
        margin: 5,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            1000: {
                items: 3
            }
        }
    });

function playvideo(videoidaux) {

    var id = videoidaux;
    var iframaload = '<iframe id="ifrm-' + id + '" src="//www.youtube.com/embed/' + id + '?rel=0&amp;controls=0&amp;showinfo=0&amp;autoplay=1&amp;loop=0&amp;playlist=PLimBShHKRwbDX32y9diQgnOXIm7B7P4N6&amp;modestbranding=1&amp;enablejsapi=1&amp;origin=https%3A%2F%2Fwww.achs.cl&amp;widgetid=2" width="560" height="315" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';

    $('#videoPlayer-' + id).html(iframaload);
    muteallvideo(id)


}

function muteallvideo(id) {
    for (var i = 0; i < playerInfoList.length; i++) {
        if (playerInfoList[i].videoId != id) {
            //if( $("#ifrm-"+playerInfoList[i].videoId)[0].contentWindow!=undefined){
            //  $("#ifrm-"+playerInfoList[i].videoId)[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');   
            // }
            if ($("#ifrm-" + playerInfoList[i].videoId).length > 0) {
                $("#ifrm-" + playerInfoList[i].videoId).attr('src', $("#ifrm-" + playerInfoList[i].videoId).attr('src').replace('autoplay=1', "autoplay=0"));
            }
            //
        }
    }

}

$(".cerrar").on( "click", function() {
    $('.valoracion').hide(); //muestro mediante clase
});

})