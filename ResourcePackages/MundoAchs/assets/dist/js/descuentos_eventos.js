$(document).ready(function() {
    $('.ver-modal-descuento').click(function(e){
    e.preventDefault();
    $('.modal-descuento').addClass('visible');
    $('.w-obten-descuento').removeAttr('style');
  });
  $('.cerrar-descuento').click(function(e){
    e.preventDefault();
    $('.modal-descuento').removeClass('visible');
  });
});

document.getElementById("rut_1").maxLength = "10"
document.getElementById("rut_2").maxLength = "1"


function validarEmail(valor) {
    // creamos nuestra regla con expresiones regulares.
    var filter = /[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
    // utilizamos test para comprobar si el parametro valor cumple la regla
    if (filter.test(valor))
      return true;
    else
      return false;
  }

  function alphanumeric(numaric) {

    for (var j = 0; j < numaric.length; j++) {
      var alphaa = numaric.charAt(j);
      var hh = alphaa.charCodeAt(0);

      if ((hh > 47 && hh < 58) || (hh > 64 && hh < 91) || (hh > 96 && hh < 123)) {
        return true;
      } else {
        return false;
      }
    }
  }

  function clRutValido(value) {

    value = replaceAll(value, ".", "");

    if (value != null && value.indexOf("-") == -1) {

      value = value.substring(0, (value.length - 1)) + "-" + value.substring((value.length), value.length - 1);

    }

    if (value == '') {
      return false;
    }
    var rexp = new RegExp(/^([0-9])+\-([kK0-9])+$/);
    if (!value.match(rexp)) {
      return false;
    }
    var RUT = value.split("-");
    var elRut = RUT[0];
    var factor = 2;
    var suma = 0;
    var dv;
    for (i = (elRut.length - 1) ; i >= 0; i--) {
      factor = factor > 7 ? 2 : factor;
      suma += parseInt(elRut[i]) * parseInt(factor++);
    }
    var ret = true;
    dv = 11 - (suma % 11);
    if (dv == 11) {
      dv = 0;
    } else if (dv == 10) {
      dv = "k";
    }
    if (dv != RUT[1].toLowerCase()) {
      ret = false;
    }
    return ret;
  }


  function replaceAll(text, busca, reemplaza) {

    while (text.toString().indexOf(busca) != -1) {
      text = text.toString().replace(busca, reemplaza);
    }
    return text;
  }




  function numeric(numero) {
    if (!/^([0-9])*$/.test(numero)) {
      return false;
    } else {
      return true;
    }
  }


  function numericKeyPress() {
    if ((event.keyCode < 48) || (event.keyCode > 57))
      event.returnValue = false;
  }




  document.getElementById("rut_1").addEventListener("keyup", keypress_rut);

  function keypress_rut_completa() {

    var rut  = document.getElementById('rut_1').value;
    rut = rut.replace('-','');
    if(rut.length>1) {
      let l = rut.length;
      var last = rut[rut.length-1]
      var first = rut.substring(0, rut.length-1);


      var num = first.replace(/\./g,'');
      if(!isNaN(num)){
        num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1.');
        num = num.split('').reverse().join('').replace(/^[\.]/,'');
        console.log(num);
      }

      var res  = num+"-"+last;

    } else {
     var res = rut;
   }
   document.getElementById('rut_1').value = res;
    //console.log(res);

  }


  function keypress_rut(input)
  {
    var num = document.getElementById('rut_1').value.replace(/\./g,'');
    if(!isNaN(num))
    {
      num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1.');
      num = num.split('').reverse().join('').replace(/^[\.]/,'');
      document.getElementById('rut_1').value = num;
    }
  }


  document.getElementById("rut_1").onkeypress = function(e) {
    var chr = String.fromCharCode(e.which);
    if ("0123456789".indexOf(chr) < 0)
      return false;
  };

  document.getElementById("rut_2").onkeypress = function(e) {
    var chr = String.fromCharCode(e.which);
    if ("0123456789kK".indexOf(chr) < 0)
      return false;
  };





  $( document ).ready(function() {
    console.log( "ready!" );

    $("#btn-solicitar-descuento").on("click", function() {
      var rut_1  = document.getElementById('rut_1').value;
      var rut_2  = document.getElementById('rut_2').value;
      var rut = rut_1 + "-" + rut_2;

      if (!$("#form100")[0].checkValidity()) {
        $("#form100").find("#btn-submit").click();

      } else if (!clRutValido(rut)) 
      { 
        alert('Rut no válido');      
      }
      else {
        submit_data()
       
 
   }
 });

  });





//var input = document.getElementById("rut_2");
//input.addEventListener("blur", function( event ) {
  function submit_data(){
  //let rut_aux = '17189601-0';
  var rut_1  = document.getElementById('rut_1').value;
  var rut_2  = document.getElementById('rut_2').value;
  var rut = rut_1 + "-" + rut_2;
  //rut = "17150277-2";

  

  var rut_aux = rut.replace(".","").replace(".","").replace(".","");
  $.ajax({
   type: "POST",
   url: "https://api.achs.lfi.cl/api/validadorachs/validacion/" + rut_aux,

   contentType: "application/json; charset=utf-8",
   crossDomain: true,
   dataType: 'json',
   async: false,
   success: function (response) {
     var myJSON = JSON.parse(response);
     if(myJSON.length>0){
       let rut = myJSON[0].RUT_Pagador;
       rut = rut.replace('-','');
       if(rut.length>1) {       
        document.getElementById("btn-submit").disabled=true;
        document.getElementById("btn-solicitar-descuento").disabled=true;
        $("#btn-solicitar-descuento").html("Por favor espere...");
        $("#form100").submit();
      } else {
        alert("El RUT ingresado no se encuentra adherido a la ACHS")
      }
    } else {

      alert("El RUT ingresado no se encuentra adherido a la ACHS")

    }


  },
  failure: function (response) {

   console.log(response);
   alert("El RUT ingresado no se encuentra adherido a la ACHS")
 },
 error: function (response) {

   console.log(response);
   alert("El RUT ingresado no se encuentra adherido a la ACHS")

 }
}); 



}



  