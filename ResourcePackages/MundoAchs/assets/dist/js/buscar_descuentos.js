var url_descuentos = "/api/mundo-achs/descuentomundoachs?$select=Title,UrlName,Visitas,Nombre,Summary,FechaInicio,FechaTermino,Detalle,InstruccionesDescuento,MensajeEmail,Restriccion,LetraChica,TextoDescuento,ValorDescuento,TipoCanje,categoriasdescuentos,tagsdescuentosmundoachs,regiones&$expand=Imagen($select=Url),Empresa($select=Title)";
var url_categoria = '/api/mundo-achs/flat-taxa?$filter=TaxonomyId%20eq%204b6dd7c9-1d19-4612-a4a1-a7eaac74e676&$select=Title,Id,UrlName';
var url_regiones = '/ResourcePackages/MundoAchs/regiones.json';
//let url_like= '$filter=contains(Title,'gas') or contains(Summary,'gas') or contains(Detalle,'500')';
var html_sin_descuentos=`<div class="sin-eventos">
<div class="wrap-ma">
  <div class="w-sin-eventos">
    <figure>
      <img src="/ResourcePackages/MundoAchs/assets/dist/images/personaje.png" alt="">
    </figure>
    <div class="texto">
      <h3>¡Lo sentimos!</h3>
      <p>No hay resultados encontrados</p>
    </div>
  </div>
</div>
</div>`;
var array_descuentos = [];
var array_categoria = [];
var array_region=[];
var array_categoria_todas = [];
$body = $("body");
function cargar_descuentos() {

 $body.addClass("loading");

  arma_checkboxs_region(url_regiones, 'id_region', 'regiones');
  arma_checkboxs_categoria(url_categoria, 'id_categoria', 'categoria')



  fetch(url_descuentos).then((response) => {
    if (response.ok) {
      return response.json();
    } else {
      throw new Error('Something went wrong');
    }
  })
    .then((responseJson) => {
      let array_descuentos_aux = responseJson.value;
      let array_descuentos_top = responseJson.value;

      array_descuentos_aux = array_descuentos_aux.sort(compareValues('Title'));
      array_descuentos_top = array_descuentos_aux.sort(compareValues('Visitas','desc'));
        array_descuentos_aux.forEach(function (item, i) {
        item.Buscar = slugify(item.Title) + " " + slugify(item.Summary);
        array_descuentos.push(item);
         })


        let html_descuentos = '';
        let html_descuentos_top = '';
        array_descuentos.forEach(function (item, i) {

          let html_descuentos_aux = arma_html_descuentos(item);
          html_descuentos = html_descuentos + html_descuentos_aux;


          

        });


  array_descuentos_top.slice(0, 3).forEach(function (item, i) {

        let html_descuentos_aux_top = arma_html_descuentos_top(item);
          html_descuentos_top = html_descuentos_top + html_descuentos_aux_top;

    });


    if(array_descuentos_top.length==0)  {

      html_descuentos  = html_sin_descuentos;
    }


        let htmlObject = document.getElementById('id_descuentos');
        htmlObject.innerHTML = html_descuentos;
        

            //let htmlObject_top = document.getElementById('mas_buscado');
        //htmlObject_top.innerHTML = html_descuentos_top


        
        

         $body.removeClass("loading");
      
    })
    .catch((error) => {
      console.log(error)
    });
}



function arma_html_descuentos_top(item) {
  let html = '';
  let categoria = "";
  let categoria_aux = "";


item.categoriasdescuentos.forEach(function (item2, i) {
  
categoria_aux = getFilteredByKey(array_categoria_todas,'Id',item2)[0].Title;
categoria = categoria + categoria_aux  + ", ";
         })

categoria = categoria.slice(0, -2) ;

  html = `        <div class="item-mas-buscado">
          <figure>
           <img src="${item.Imagen[0].Url}" alt="${item.Summary}">
          </figure>
          <div class="texto">
            <h5>${categoria}</h5>
            <h4> ${item.Empresa[0].Title}:  ${item.Nombre}</h4>
            <div class="valoracion d-none">
              <img src="/ResourcePackages/MundoAchs/assets/dist/images/star1.svg" alt="">
              <img src="/ResourcePackages/MundoAchs/assets/dist/images/star1.svg" alt="">
              <img src="/ResourcePackages/MundoAchs/assets/dist/images/star1.svg" alt="">
              <img src="/ResourcePackages/MundoAchs/assets/dist/images/star1.svg" alt="">
              <img src="/ResourcePackages/MundoAchs/assets/dist/images/star2.svg" alt="">
              <span>(23)</span>
            </div>
          </div>
        </div>`;

  return html;

}



function arma_html_descuentos(item) {
  let html = '';
  let categoria = "";
  let categoria_aux = "";


item.categoriasdescuentos.forEach(function (item2, i) {
      
categoria_aux = getFilteredByKey(array_categoria_todas,'Id',item2)[0].Title;
categoria = categoria + categoria_aux  + ", ";
         })

categoria = categoria.slice(0, -2) ;

  html = `<div class="col-md-4"><div class="box-descuento">
  <a href="/descuento/${item.UrlName}">
            <figure>
              <img src="${item.Imagen[0].Url}" alt="${item.Summary}">
            </figure>
            </a>
            <div class="meta">
              <div class="valoracion d-none">
                <img src="/ResourcePackages/MundoAchs/assets/dist/images/star1.svg" alt="">
                <img src="/ResourcePackages/MundoAchs/assets/dist/images/star1.svg" alt="">
                <img src="/ResourcePackages/MundoAchs/assets/dist/images/star1.svg" alt="">
                <img src="/ResourcePackages/MundoAchs/assets/dist/images/star1.svg" alt="">
                <img src="/ResourcePackages/MundoAchs/assets/dist/images/star2.svg" alt="">
                <span>(23)</span>
              </div>
              <div class="descuento">
                <p class="d1">  ${item.TextoDescuento} </p>
                <p class="d2 d-none">Productos</p>
              </div>
            </div>
            <div class="texto">
              <h5>${categoria}</h5>
              <h3> ${item.Empresa[0].Title}:  ${item.Nombre}</h3>
              <p>${item.Summary}</p>
            </div>
            <a href="/descuento/${item.UrlName}" class="btn-detalle">Ver más <img src="/ResourcePackages/MundoAchs/assets/dist/images/flecha-der.svg" alt=""></a>
          </div>
        </div>`;

  return html;

}



function arma_checkboxs_categoria(url, id_html, name) {
  fetch(url).then((response) => {
    if (response.ok) {
      return response.json();
    } else {
      throw new Error('Something went wrong');
    }
  })
    .then((responseJson) => {
      data = responseJson.value;
      array_categoria_todas = data;
      console.log(data);
      //data = data.sort(compareValues('Title'));
      let html_check = '';
      let html_cat = '';
      let html_todos = ` <a href="#" data-id="idtodos"  id="idtodos_cat" data-nombre="todos" name="${name}" data-value="todos" ><span><img src="/ResourcePackages/MundoAchs/assets/dist/images/ic1b.svg" alt=""></span> Todos</a>`;
      data.forEach(function (item, i) {
        let id = item.Id;
        let title = item.Title;
        let urlname = item.UrlName;
        let html_check_aux = `<input type="checkbox" id="id${id}" data-nombre="${title}" name="${name}" value="${id}" >`;                
        html_check = html_check + html_check_aux;

        let html_cat_aux = ` <a href="#" data-nombre="${title}"  data-id="id${id}" data-name="${name}" data-value="${id}" class=""><span><img src="/ResourcePackages/MundoAchs/assets/dist/images/${urlname}.svg" alt="${title}"></span> ${title}</a>`;
        html_cat = html_cat + html_cat_aux;

      });
      let htmlObject = document.getElementById(id_html);
      //htmlObject.innerHTML =  html_check ;
      htmlObject.innerHTML = `<input type="checkbox" id="idtodos" data-nombre="todos" value="todos" >` +  html_check ;

      let htmlObjectCat = document.getElementById(id_html + "_cat");
       htmlObjectCat.innerHTML = html_todos +  html_cat;
      //var checkbox = document.querySelectorAll(`input[type=checkbox][name="${name}"]`);
      //checkbox.forEach(checkbox => checkbox.addEventListener('change', () => buscar_capacitaciones()));

      $('.g-categorias a' ).click(function( event ) {
        event.preventDefault();
        
         let dataNombre = $(this).attr('data-nombre');
         let dataValue = $(this).attr('data-value');
         let dataName = $(this).attr('data-name');
         let dataId = $(this).attr('data-id');

        

       

        if($('#'+ dataId).is(":checked"))
        {
              if(dataValue=='todos') 
            {
                checkAll('id_categoria',false);
                var all_cat = $('#id_categoria_cat a').removeClass('activo');
               
            } else {
            $('#'+ dataId).prop('checked', false);
            $(this).removeClass('activo');

            if($('#idtodos').is(":checked")) 
            {
                  $('#idtodos').prop('checked', false);
                 $('#idtodos_cat').removeClass('activo');
            }

          }

        } else {
          $('#'+ dataId).prop('checked', true);
          $(this).addClass('activo');

          if(dataValue=='todos') 
            {
                checkAll('id_categoria',true);
                var all_cat = $('#id_categoria_cat a').addClass('activo');
               
            }
        }

        buscar_descuentos();
      });



      


    })
    .catch((error) => {
      console.log(error)
    });


}






function arma_checkboxs_region(url, id_html, name) {
  fetch(url).then((response) => {
    if (response.ok) {
      return response.json();
    } else {
      throw new Error('Something went wrong');
    }
  })
    .then((responseJson) => {
      data = responseJson.value;
      //data = data.sort(compareValues('Title'));
      let html_check = '';
      data.forEach(function (item, i) {
        let id = item.Id;
        let title = item.Title;
        let html_check_aux = `<div class="item"><input class="checkbox_region" type="checkbox" id="id${id}" data-nombre="${title}"" name="${name}" value="${id}" ><label for="id${id}">${title}</label></div>`;                
        html_check = html_check + html_check_aux;

      });
      let htmlObject = document.getElementById(id_html);
      htmlObject.innerHTML = html_check;

      //var checkbox = document.querySelectorAll(`input[type=checkbox][name="${name}"]`);
      //checkbox.forEach(checkbox => checkbox.addEventListener('change', () => buscar_capacitaciones()));

      
      let lista_checkbox = document.querySelectorAll(".checkbox_region")

      lista_checkbox.forEach(element => {
        element.addEventListener("change", () => {
            buscar_descuentos();
           
        })
    });


      


    })
    .catch((error) => {
      console.log(error)
    });


}



function compareValues(key, order = 'asc') {
  return function innerSort(a, b) {
    if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
      // property doesn't exist on either object
      return 0;
    }

    const varA = (typeof a[key] === 'string')
      ? a[key].toUpperCase() : a[key];
    const varB = (typeof b[key] === 'string')
      ? b[key].toUpperCase() : b[key];

    let comparison = 0;
    if (varA > varB) {
      comparison = 1;
    } else if (varA < varB) {
      comparison = -1;
    }
    return (
      (order === 'desc') ? (comparison * -1) : comparison
    );
  };
}


function filterArray(array, filters) {
  const filterKeys = Object.keys(filters);
  return array.filter(item => {
    // validates all filter criteria
    return filterKeys.every(key => {
      // ignores non-function predicates
      if (typeof filters[key] !== 'function') return true;
      return filters[key](item[key]);
    });
  });
}


function getFilteredByKey(array, key, value) {
  return array.filter(function (e) {
    return e[key] == value;
  });
}



function getCheckedBoxes(chkboxName) {
  var checkboxes = document.getElementsByName(chkboxName);
  var checkboxesChecked = [];
  // loop over them all
  for (var i = 0; i < checkboxes.length; i++) {
    // And stick the checked ones onto an array...
    if (checkboxes[i].checked) {
      checkboxesChecked.push(checkboxes[i].value);
    }
  }
  // Return the array if it is non-empty, or null
  return checkboxesChecked.length > 0 ? checkboxesChecked : [];
}



function checkAll(divid,flag) {
  var checks = document.querySelectorAll('#' + divid + ' input[type="checkbox"]');
  for (var i = 0; i < checks.length; i++) {
    var check = checks[i];
    if (!check.disabled) {
      check.checked = flag;

    }
  }
}

$(document).ready(function(){
cargar_descuentos();

});



$( ".btn-filtrar" ).click(function() {
  buscar_descuentos();
});


$( ".nuevo-btn-filtrar" ).click(function() {
    buscar_descuentos();
  });




function buscar_descuentos() {



    let filters1 = {};
  array_categoria = getCheckedBoxes('categoria');
  if (array_categoria.length > 0) {
    filters1 = { categoriasdescuentos: categoriasdescuentos => categoriasdescuentos.find(x => array_categoria.includes(x)) };
  }


      let filters2 = {};
  array_region = getCheckedBoxes('regiones');
  if (array_region.length > 0) {
    filters1 = { regiones: regiones => regiones.find(x => array_region.includes(x)) };
  }



  let filters3 = {};

   let text = slugify($('#texto_descuento').val());
   if(text!='') 
   {
      filters3 = { Buscar: Buscar =>Buscar.toLowerCase().includes(text.toLowerCase())  };
      
  }


  




   const filters = { ...filters1 ,  ...filters2, ...filters3}
  
  const filterResult = filterArray(array_descuentos, filters);
  console.log(filterResult);


  let html_descuentos = '';
        filterResult.forEach(function (item, i) {

          let html_descuentos_aux = arma_html_descuentos(item);
          html_descuentos = html_descuentos + html_descuentos_aux;

        });
        

        if(filterResult.length==0)  {
          console.log('123');
          html_descuentos  = html_sin_descuentos;
        }

        let htmlObject = document.getElementById('id_descuentos');
        htmlObject.innerHTML = html_descuentos;


}


function slugify (str) {
    var map = {
        'a' : 'á|à|ã|â|À|Á|Ã|Â',
        'e' : 'é|è|ê|É|È|Ê',
        'i' : 'í|ì|î|Í|Ì|Î',
        'o' : 'ó|ò|ô|õ|Ó|Ò|Ô|Õ',
        'u' : 'ú|ù|û|ü|Ú|Ù|Û|Ü',
        'c' : 'ç|Ç'
    };
      str = str.toLowerCase();
    
    for (var pattern in map) {
        str = str.replace(new RegExp(map[pattern], 'g'), pattern);
    };

    return str;
    
};





    var input = document.getElementById("texto_descuento");
    input.addEventListener("keyup", function(event) {
      if (event.keyCode === 13) {
       event.preventDefault();
       buscar_descuentos();
      }
    });

