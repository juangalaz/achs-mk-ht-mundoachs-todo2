var url_eventos = '';
var url_categoria = '/api/mundo-achs/flat-taxa?$filter=TaxonomyId eq 28bd31a4-4374-4438-9013-881b4b8751a1&$select=Title,Id,UrlName';
var url_regiones = '/ResourcePackages/MundoAchs/regiones.json';
//let url_like= '$filter=contains(Title,'gas') or contains(Summary,'gas') or contains(Detalle,'500')';
var html_sin_eventos = `<div class="sin-eventos">
<div class="wrap-ma">
  <div class="w-sin-eventos">
    <figure>
      <img src="/ResourcePackages/MundoAchs/assets/dist/images/personaje.png" alt="">
    </figure>
    <div class="texto">
      <h3>¡Lo sentimos!</h3>
      <p>Por el momento no hay eventos agendados.<br>
        Visita la sección de eventos pasados para ver los registros.</p>
    </div>
  </div>
</div>
</div>`;
var array_categoria = [];
var array_region=[];
var array_categoria_todas = [];
$body = $("body");
function cargar_eventos() {

 $body.addClass("loading");

  arma_checkboxs_region(url_regiones, 'id_region', 'regiones');
  arma_checkboxs_categoria(url_categoria, 'id_categoria', 'categoria')


  //console.log(array_eventos);
      let array_eventos_aux = array_eventos;
      let array_eventos_top = array_eventos;

     // array_eventos_aux = array_eventos_aux.sort(compareValues('Title'));
      //array_eventos_top = array_eventos_aux.sort(compareValues('Visitas','desc'));
      

        let html_eventos = '';
        let html_eventos_top = '';
        array_eventos.forEach(function (item, i) {

          let html_eventos_aux = arma_html_eventos(item);
          html_eventos = html_eventos + html_eventos_aux;

        });


  array_eventos_top.slice(0, 3).forEach(function (item, i) {

        let html_eventos_aux_top = arma_html_eventos_top(item);
          html_eventos_top = html_eventos_top + html_eventos_aux_top;

    });

        let htmlObject = document.getElementById('id_eventos');
        htmlObject.innerHTML = html_eventos;
        

            let htmlObject_top = document.getElementById('mas_buscado');
        htmlObject_top.innerHTML = html_eventos_top


         $body.removeClass("loading");

         console.log(array_eventos);

         if(array_eventos.length==0)  {

            html_eventos  = html_sin_eventos;
          }
      

}



function arma_html_eventos_top(item) {
  let html = '';
  let categoria = "";
  let categoria_aux = "";





  html = `        <div class="item-mas-buscado">
          <figure>
           <img src="${item.Imagen}" alt="${item.Summary}">
          </figure>
          <div class="texto">
            
            <h4>  ${item.Title}</h4>
          
          </div>
        </div>`;

  return html;

}



function arma_html_eventos(item) {
  let html = '';
let cupos = item.Cupos;
let estado = "EVENTO " +  item.Estado;
if(cupos<=10 && cupos>0) {
    estado = "ÚLTIMOS CUPOS";
} else if(cupos==0) {
    estado = "SIN CUPOS DISPONIBLE";
}



html =  html + ` <div class="box-evento-ma tipo2">
                    <figure>
                        <img src='${item.Imagen}' alt='${item.Title}' title='${item.Title}' />
                    </figure>
                    <div class="texto-evento">
                        <h6> ${estado}</h6>
     <h2>${item.Title}</h2>

                        <div class="datos">
                            <p><i class="far fa-calendar-alt"></i> ${item.TextoFecha}</p>
                            <p><i class="far fa-clock"></i> ${item.TextoHora}</p>
                            <p><i class="fas fa-map-marker-alt"></i> ${item.Lugar}</p>
                        </div>

                        <p>${item.Summary}</p>

                        <a href="${item.UrlName}" class="ver-evento">Ver evento <img src="/ResourcePackages/MundoAchs/assets/dist/images/flecha-der.svg" alt=""></a>
                    </div>
                </div>`;
            

  return html;

}



function arma_checkboxs_categoria(url, id_html, name) {
  fetch(url).then((response) => {
    if (response.ok) {
      return response.json();
    } else {
      throw new Error('Something went wrong');
    }
  })
    .then((responseJson) => {
      data = responseJson.value;
      array_categoria_todas = data;
      //console.log(data);
      //data = data.sort(compareValues('Title'));
      let html_check = '';
      let html_cat = '';
      let html_todos = ` <a href="#" data-id="idtodos"  id="idtodos_cat" data-nombre="todos" name="${name}" data-value="todos" ><span><img src="/ResourcePackages/MundoAchs/assets/dist/images/ic1c.svg" alt=""></span> Todos</a>`;
      data.forEach(function (item, i) {
        let id = item.Id;
        let title = item.Title;
        let urlname = item.UrlName;
        let html_check_aux = `<input type="checkbox" id="id${id}" data-nombre="${title}" name="${name}" value="${id}" >`;                
        html_check = html_check + html_check_aux;

        let html_cat_aux = ` <a href="#" data-nombre="${title}"  data-id="id${id}" data-name="${name}" data-value="${id}" class=""><span><img  onerror="this.src='${"/ResourcePackages/MundoAchs/assets/dist/images/ic1c.svg"}'" src="/ResourcePackages/MundoAchs/assets/dist/images/${urlname}.svg" alt="${title}"></span> ${title}</a>`;
        html_cat = html_cat + html_cat_aux;

      });
      let htmlObject = document.getElementById(id_html);
      //htmlObject.innerHTML =  html_check ;
      htmlObject.innerHTML = `<input type="checkbox" id="idtodos" data-nombre="todos" value="todos" >` +  html_check ;

      let htmlObjectCat = document.getElementById(id_html + "_cat");
       htmlObjectCat.innerHTML = html_todos +  html_cat;
      //var checkbox = document.querySelectorAll(`input[type=checkbox][name="${name}"]`);
      //checkbox.forEach(checkbox => checkbox.addEventListener('change', () => buscar_capacitaciones()));

      $('.g-categorias a' ).click(function( event ) {
        event.preventDefault();
        
         let dataNombre = $(this).attr('data-nombre');
         let dataValue = $(this).attr('data-value');
         let dataName = $(this).attr('data-name');
         let dataId = $(this).attr('data-id');

        

       

        if($('#'+ dataId).is(":checked"))
        {
              if(dataValue=='todos') 
            {
                checkAll('id_categoria',false);
                var all_cat = $('#id_categoria_cat a').removeClass('activo');
               
            } else {
            $('#'+ dataId).prop('checked', false);
            $(this).removeClass('activo');

            if($('#idtodos').is(":checked")) 
            {
                  $('#idtodos').prop('checked', false);
                 $('#idtodos_cat').removeClass('activo');
            }

          }

        } else {
          $('#'+ dataId).prop('checked', true);
          $(this).addClass('activo');

          if(dataValue=='todos') 
            {
                checkAll('id_categoria',true);
                var all_cat = $('#id_categoria_cat a').addClass('activo');
               
            }
        }
        buscar_eventos();
      });


    })
    .catch((error) => {
      console.log(error)
    });


}


function arma_checkboxs_region(url, id_html, name) {
  fetch(url).then((response) => {
    if (response.ok) {
      return response.json();
    } else {
      throw new Error('Something went wrong');
    }
  })
    .then((responseJson) => {
      data = responseJson.value;
      //data = data.sort(compareValues('Title'));
      let html_check = '';
      data.forEach(function (item, i) {
        let id = item.Id;
        let title = item.Title;
        let html_check_aux = `<div class="item"><input  class="checkbox_region"  type="checkbox" id="id${id}" data-nombre="${title}"" name="${name}" value="${id}" ><label for="id${id}">${title}</label></div>`;                
        html_check = html_check + html_check_aux;

      });
      let htmlObject = document.getElementById(id_html);
      htmlObject.innerHTML = html_check;

      //var checkbox = document.querySelectorAll(`input[type=checkbox][name="${name}"]`);
      //checkbox.forEach(checkbox => checkbox.addEventListener('change', () => buscar_capacitaciones()));
      let lista_checkbox = document.querySelectorAll(".checkbox_region")

      lista_checkbox.forEach(element => {
        element.addEventListener("change", () => {
            buscar_eventos();
           
        })
    });


    })
    .catch((error) => {
      console.log(error)
    });


}



function compareValues(key, order = 'asc') {
  return function innerSort(a, b) {
    if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
      // property doesn't exist on either object
      return 0;
    }

    const varA = (typeof a[key] === 'string')
      ? a[key].toUpperCase() : a[key];
    const varB = (typeof b[key] === 'string')
      ? b[key].toUpperCase() : b[key];

    let comparison = 0;
    if (varA > varB) {
      comparison = 1;
    } else if (varA < varB) {
      comparison = -1;
    }
    return (
      (order === 'desc') ? (comparison * -1) : comparison
    );
  };
}


function filterArray(array, filters) {
  const filterKeys = Object.keys(filters);
  return array.filter(item => {
    // validates all filter criteria
    return filterKeys.every(key => {
      // ignores non-function predicates
      if (typeof filters[key] !== 'function') return true;
      return filters[key](item[key]);
    });
  });
}


function getFilteredByKey(array, key, value) {
  return array.filter(function (e) {
    return e[key] == value;
  });
}



function getCheckedBoxes(chkboxName) {
  var checkboxes = document.getElementsByName(chkboxName);
  var checkboxesChecked = [];
  // loop over them all
  for (var i = 0; i < checkboxes.length; i++) {
    // And stick the checked ones onto an array...
    if (checkboxes[i].checked) {
      checkboxesChecked.push(checkboxes[i].value);
    }
  }
  // Return the array if it is non-empty, or null
  return checkboxesChecked.length > 0 ? checkboxesChecked : [];
}



function checkAll(divid,flag) {
  var checks = document.querySelectorAll('#' + divid + ' input[type="checkbox"]');
  for (var i = 0; i < checks.length; i++) {
    var check = checks[i];
    if (!check.disabled) {
      check.checked = flag;

    }
  }
}

$(document).ready(function(){
	cargar_eventos();

});



$( ".btn-filtrar" ).click(function() {
  buscar_eventos();
});

$( ".nuevo-btn-filtrar" ).click(function() {
    buscar_eventos();
  });


function buscar_eventos() {



    let filters1 = {};
  array_categoria = getCheckedBoxes('categoria');
  if (array_categoria.length > 0) {
    filters1 = { categoriaseventos: categoriaseventos => categoriaseventos.find(x => array_categoria.includes(x)) };
  }


      let filters2 = {};
  array_region = getCheckedBoxes('regiones');
  if (array_region.length > 0) {
    filters1 = { regiones: regiones => regiones.find(x => array_region.includes(x)) };
  }



  let filters3 = {};

   let text = slugify($('#texto_descuento').val());
   if(text!='') 
   {
      filters3 = { Buscar: Buscar =>Buscar.toLowerCase().includes(text.toLowerCase())  };
      
  }


  




   const filters = { ...filters1 ,  ...filters2, ...filters3}
  
  const filterResult = filterArray(array_eventos, filters);
  //console.log(filterResult);


  let html_eventos = '';
        filterResult.forEach(function (item, i) {

          let html_eventos_aux = arma_html_eventos(item);
          html_eventos = html_eventos + html_eventos_aux;

        });

        if(filterResult.length==0)  {

            html_eventos  = html_sin_eventos;
          }
        let htmlObject = document.getElementById('id_eventos');
        htmlObject.innerHTML = html_eventos;


}


function slugify (str) {
    var map = {
        'a' : 'á|à|ã|â|À|Á|Ã|Â',
        'e' : 'é|è|ê|É|È|Ê',
        'i' : 'í|ì|î|Í|Ì|Î',
        'o' : 'ó|ò|ô|õ|Ó|Ò|Ô|Õ',
        'u' : 'ú|ù|û|ü|Ú|Ù|Û|Ü',
        'c' : 'ç|Ç'
    };
      str = str.toLowerCase();
    
    for (var pattern in map) {
        str = str.replace(new RegExp(map[pattern], 'g'), pattern);
    };

    return str;
    
};


var input = document.getElementById("texto_descuento");
input.addEventListener("keyup", function(event) {
  if (event.keyCode === 13) {
   event.preventDefault();
   buscar_eventos();
  }
});



//https://www.rapidtables.com/web/html/html-codes.html




