var url_descuentos = "/api/mundo-achs/descuentomundoachs?$select=Title,UrlName,Nombre,Summary,FechaInicio,FechaTermino,Detalle,InstruccionesDescuento,MensajeEmail,Restriccion,LetraChica,TextoDescuento,ValorDescuento,TipoCanje,categoriasdescuentos,tagsdescuentosmundoachs,regiones&$expand=Imagen($select=Url),Empresa($select=Title)";
var url_categoria = '/api/mundo-achs/flat-taxa?$filter=TaxonomyId%20eq%204b6dd7c9-1d19-4612-a4a1-a7eaac74e676&$select=Title,Id,UrlName';
var url_regiones = '/ResourcePackages/MundoAchs/regiones.json';
//let url_like= '$filter=contains(Title,'gas') or contains(Summary,'gas') or contains(Detalle,'500')';
var array_descuentos = [];
var array_categoria = [];


function cargar_descuentos() {


  
    arma_checkboxs_categoria(url_categoria, 'id_categoria', 'categoria')


  fetch(url_descuentos).then((response) => {
    if (response.ok) {
      return response.json();
    } else {
      throw new Error('Something went wrong');
    }
  })
    .then((responseJson) => {
      let array_descuentos_aux = responseJson.value;

      let a = [];
      array_descuentos_aux = array_descuentos_aux.sort(compareValues('Title'));
        array_descuentos_aux.forEach(function (item, i) {
        item.Buscar = item.Title + " " + item.Summary;
        array_descuentos.push(item);
         })


        let html_descuentos = '';
        array_descuentos.forEach(function (item, i) {

          let html_descuentos_aux = arma_html_descuentos(item);
          html_descuentos = html_descuentos + html_descuentos_aux;

        });
        let htmlObject = document.getElementById('id_descuentos');
        htmlObject.innerHTML = html_descuentos;
        

      
    })
    .catch((error) => {
      console.log(error)
    });
}


function cargar_descuentos_sf() {

arma_checkboxs_categoria(url_categoria, 'id_categoria', 'categoria')
     let array_descuentos_aux = array_descuentos_home;

      array_descuentos_aux = array_descuentos_aux.sort(compareValues('Title'));
        array_descuentos_aux.forEach(function (item, i) {
        item.Buscar = item.Title + " " + item.Summary;
        array_descuentos.push(item);
         })


        let html_descuentos = '';
        array_descuentos.forEach(function (item, i) {

          let html_descuentos_aux = arma_html_descuentos(item);
          html_descuentos = html_descuentos + html_descuentos_aux;

        });
        let htmlObject = document.getElementById('id_descuentos');
        htmlObject.innerHTML = html_descuentos;
}


function arma_html_descuentos(item) {
  let html = '';
  let categoria = "";
  
  html = ` <div class="col-lg-3 col-md-4 col-sm-6"><div class="box-descuento">
  <a href="/descuento/${item.UrlName}" >
            <figure>
              <img src="${item.Imagen}" alt="${item.Summary}">
            </figure>
            </a>
            <div class="meta">
              <div class="valoracion d-none">
                <img src="/ResourcePackages/MundoAchs/assets/dist/images/star1.svg" alt="">
                <img src="/ResourcePackages/MundoAchs/assets/dist/images/star1.svg" alt="">
                <img src="/ResourcePackages/MundoAchs/assets/dist/images/star1.svg" alt="">
                <img src="/ResourcePackages/MundoAchs/assets/dist/images/star1.svg" alt="">
                <img src="/ResourcePackages/MundoAchs/assets/dist/images/star2.svg" alt="">
                <span>(23)</span>
              </div>
              <div class="descuento">
                <p class="d1">  ${item.TextoDescuento} </p>
                <p class="d2 d-none">Productos</p>
              </div>
            </div>
            <div class="texto">
              <h5>${item.categoria}</h5>
              <h3> ${item.Empresa}:  ${item.Nombre}</h3>
              <p>${item.Summary}</p>
            </div>
            <a href="/descuento/${item.UrlName}" class="btn-detalle">Ver más <img src="/ResourcePackages/MundoAchs/assets/dist/images/flecha-der.svg" alt=""></a>
          </div>
        </div>`;

  return html;

}



         


function arma_checkboxs_categoria(url, id_html, name) {
  fetch(url).then((response) => {
    if (response.ok) {
      return response.json();
    } else {
      throw new Error('Something went wrong');
    }
  })
    .then((responseJson) => {
      data = responseJson.value;
      //data = data.sort(compareValues('Title'));
      let html_check = '';
      let html_cat = '';
      
      let html_todos = `<div class="item"><div class="item-tabs activo"><a href="#" data-id="idtodos"  id="idtodos_cat" data-nombre="todos" name="${name}" data-value="todos"  class=""><figure><img src="/ResourcePackages/MundoAchs/assets/dist/images/ic1.svg" alt="Todos" ></figure><p>Todos</p></a></div></div>`;
      data.forEach(function (item, i) {
        let id = item.Id;
        let title = item.Title;
        let urlname = item.UrlName;
        let html_check_aux = `<input type="checkbox" id="id${id}" data-nombre="${title}" name="${name}" value="${id}" >`;      
        
        html_check = html_check + html_check_aux;

        
        let html_cat_aux = `<div class="item"><div class="item-tabs"><a href="#" data-nombre="${title}"  data-id="id${id}" data-name="${name}" data-value="${id}" class=""><figure><img src="/ResourcePackages/MundoAchs/assets/dist/images/${urlname}.svg" alt="${title}" ></figure><p>${title}</p></a></div></div>`;            
        html_cat = html_cat + html_cat_aux;

      });
      let htmlObject = document.getElementById(id_html);
      //htmlObject.innerHTML =  html_check ;
      htmlObject.innerHTML = `<input type="checkbox" id="idtodos" data-nombre="todos" value="todos" >` +  html_check ;

      let htmlObjectCat = document.getElementById(id_html + "_cat");
       htmlObjectCat.innerHTML = html_todos +  html_cat;
      //var checkbox = document.querySelectorAll(`input[type=checkbox][name="${name}"]`);
      //checkbox.forEach(checkbox => checkbox.addEventListener('change', () => buscar_capacitaciones()));

      $('.item-tabs a' ).click(function( event ) {
        event.preventDefault();
        
         let dataNombre = $(this).attr('data-nombre');
         let dataValue = $(this).attr('data-value');  
         let dataName = $(this).attr('data-name');
         let dataId = $(this).attr('data-id');

         console.log(dataValue);

        checkAll('id_categoria',false);

        if($('#'+ dataId).is(":checked"))
        {


        } else {
        
            $('#'+ dataId).prop('checked', true);
          var all_cat = $('#id_categoria_cat a').parent().removeClass('activo');
          $(this).parent().addClass('activo');

          if(dataValue=='todos') 
            {
                checkAll('id_categoria',true);
               
               
            }
        }

        buscar_descuentos();
      });

      const carrusel7 = document.getElementsByClassName('c-7');
if(carrusel7){
[].forEach.call(document.querySelectorAll('.c-7'), function (el) {
  var slider = tns({
    container: el,
    items: 7,
    controls: true,
    nav: true,
    gutter: 0,
    navPosition: 'bottom',
    preventScrollOnTouch: "force",
    responsive: {
      0 : {
        items: 2
      },
      480:{
        items: 3
      },
      640: {
        items: 4
      },
      767: {
        items: 5
      },
      900: {
        items: 6
      },
      1200: {
        items: 7
      }
    }
    });
  });
}


    })
    .catch((error) => {
      console.log(error)
    });


}





function compareValues(key, order = 'asc') {
  return function innerSort(a, b) {
    if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
      // property doesn't exist on either object
      return 0;
    }

    const varA = (typeof a[key] === 'string')
      ? a[key].toUpperCase() : a[key];
    const varB = (typeof b[key] === 'string')
      ? b[key].toUpperCase() : b[key];

    let comparison = 0;
    if (varA > varB) {
      comparison = 1;
    } else if (varA < varB) {
      comparison = -1;
    }
    return (
      (order === 'desc') ? (comparison * -1) : comparison
    );
  };
}


function filterArray(array, filters) {
  const filterKeys = Object.keys(filters);
  return array.filter(item => {
    // validates all filter criteria
    return filterKeys.every(key => {
      // ignores non-function predicates
      if (typeof filters[key] !== 'function') return true;
      return filters[key](item[key]);
    });
  });
}


function getFilteredByKey(array, key, value) {
  return array.filter(function (e) {
    return e[key] == value;
  });
}



function getCheckedBoxes(chkboxName) {
  var checkboxes = document.getElementsByName(chkboxName);
  var checkboxesChecked = [];
  // loop over them all
  for (var i = 0; i < checkboxes.length; i++) {
    // And stick the checked ones onto an array...
    if (checkboxes[i].checked) {
      checkboxesChecked.push(checkboxes[i].value);
    }
  }
  // Return the array if it is non-empty, or null
  return checkboxesChecked.length > 0 ? checkboxesChecked : [];
}



function checkAll(divid,flag) {
  var checks = document.querySelectorAll('#' + divid + ' input[type="checkbox"]');
  for (var i = 0; i < checks.length; i++) {
    var check = checks[i];
    if (!check.disabled) {
      check.checked = flag;

    }
  }
}

$(document).ready(function(){


});



$( ".btn-filtrar" ).click(function() {
  buscar_descuentos();
});


function buscar_descuentos() {



    let filters1 = {};
  array_categoria = getCheckedBoxes('categoria');
  if (array_categoria.length > 0) {
    filters1 = { categoriasdescuentos: categoriasdescuentos => categoriasdescuentos.find(x => array_categoria.includes(x)) };
  }


 




  




   const filters = { ...filters1 }
  
  const filterResult = filterArray(array_descuentos, filters);
  console.log(filterResult);


  let html_descuentos = '';
        filterResult.forEach(function (item, i) {

          let html_descuentos_aux = arma_html_descuentos(item);
          html_descuentos = html_descuentos + html_descuentos_aux;

        });
        let htmlObject = document.getElementById('id_descuentos');
        htmlObject.innerHTML = html_descuentos;


}

cargar_descuentos_sf();
console.log(array_descuentos_home);